/**
 * Created by nikhil on 15-Jan-18.
 */
define(['jquery', 'app', 'bootstrap-datepicker','moment'], function($, App, dp, moment) {
    $.fn.dailymailer = function () {
        var init = function() {


            $("#download_as_csv").on("click",function(){
                str = "data:text/csv;charset=utf-8,";
                $(".date-box").each(function(dayi,day){
                    var counter = 0;
                    heading = $(day).find(".heading").html();
                    table = $(day).find(".data-table");
                    str += heading + "\n";
                    str += "Email Date :,\n";
                    str += "Changes:,\n";
                    str += "Total Events for the Day:,"+table.length+"\n";
                    for(x=0; x < table.length; x++) {
                        headings = $(table[x]).find(".column");
                        values = $(table[x]).find(".value");
                        var sfirst = 1;
                        for (i = 0; i < headings.length; i++) {
                            col = $(headings[i]).text();
                            arr = col.split("\n");
                            col = "";
                            for (c = 0; c < arr.length; c++) {
                                col += arr[c].trim();
                            }
                            if(sfirst){
                                sfirst = 0;
                                counterstr = (counter+1);
                            }
                            else
                                counterstr = "";

                            str += counterstr+","+col + "," + $(values[i]).text() + "\n";
                        }
                        str += ",\n";
                        counter++;
                    }
                });
                var encodedUri = encodeURI(str);
                var link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "Daily Mailer Form.csv");
                document.body.appendChild(link); // Required for FF
                link.click();
            });
        };
        init();
    };
    var $mkForm = $('body');

    if ($mkForm.length) {
        $mkForm.dailymailer();
    }
});
