define(['jquery'], function($) {
    $('.brand-report__button-download-csv').on('click', function () {
        var out = new Array;
        var str = "";
        var hr = $("#table thead tr th ");

        for (i = 0; i < hr.length; i++) {
            var x = $(hr[i]).find("a").text();
            if (x == "")
                x = $(hr[i]).text();
            str = str + x + ",";
        }

        out[0] = str;
        str = "";
        var trs = $("#table tbody tr");
        for (var i = 0; i < trs.length; i++) {
            str = "";
            var tds = $(trs[i]).find("td");
            for (var c = 0; c < tds.length; c++) {
                str = str + $(tds[c]).html() + ",";
            }
            out.push(str);
        }
        str = out.join("\n");
        str = "data:text/csv;charset=utf-8," + str;

        var uri = encodeURI(str);
        var link = document.createElement("a");
        link.setAttribute("href", uri);
        link.setAttribute("download", "brandreport.csv");
        link.setAttribute("style", "display:none");

        document.body.appendChild(link);

        link.click();
    });
});
