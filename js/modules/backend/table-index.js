define(['jquery', 'app'], function($, App) {
    $('.event__button-download-csv').on('click', function () {
        var date = $("#fromdate").val();
        var todate = $("#todate").val();
        var category = $("#category").val();
        var manager = $("#manager").val();
        var sales = $("#sales").val();
        var status = $("#status").val();
        document.location.href = App.createUrl("Backend/DownloadCSV") + "&from=" + date + "&to=" + todate + "&category=" + category + "&manager=" + manager + "&sales=" + sales + "&status=" + status;
    });

    $('.event__button-view-data').on('click', function () {
        var date = $("#fromdate").val();
        var todate = $("#todate").val();
        var category = $("#category").val();
        var manager = $("#manager").val();
        var sales = $("#sales").val();
        var status = $("#status").val();
        var tasks = "";
        $("input[type=checkbox]:checked").each(function (idx, o) {
            tasks += o.value + ",";
        });
        var url = App.createUrl("Backend/GetTableData") + '&fromdate=' + date + '&todate=' + todate + '&category=' + category + '&manager=' + manager + '&status=' + status + '&sales=' + sales + "&tasks=" + tasks;
        $.ajax(url, {
            success: function (res) {
                $("#targetcontent").html(res);
                $("#download").removeClass("hide");
            }
        });
    });
});
