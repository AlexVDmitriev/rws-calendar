String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

define(['jquery', 'app', 'vendor/moment'], function($, App, moment) {
    var events = null;
    $(".event__button-download-report").on("click",function(){
        //download as csv code
        var csv = "";

        $("#result > thead > tr > th").each(function (i, o) {
            csv += $(o).text() + ",";
        });

        csv += "\n";
        $("#result > tbody > tr").each(function(i,o){
            $(o).find("td").each(function(x,b){
                if($(b).attr("data-type")){
                    a = $(b).attr("data-type");
                    if(a === "brand"){
                        csv += "\""+$(b).text()+"\",";
                    }
                    else if(a === "target"){
                        csv += $(b).find("input").val() + ",";
                    }
                }
                else
                    csv += $(b).text().replaceAll(",","")+",";
            });
            csv += "\n";
        });

        var blob = new Blob([csv],{type : 'text/csv'});
        var link = document.createElement("a");
        link.setAttribute("href", window.URL.createObjectURL(blob));
        link.setAttribute("download", "prediction_data.csv");
        document.body.appendChild(link);
        link.click();
    });

    function calculaterevenue(events, fromdate, todate) {
        var totalrev = 0, totalrev2 = 0;
        var from = moment(fromdate);
        var to = moment(todate);

        for (var i = 0; i < events.length; i++) {
            var efrom = moment(events[i].data_start);
            var eto = moment(events[i].data_end).endOf('day');

            var a, d, dd;

            if ((moment(efrom).isSameOrAfter(from)) && moment(eto).isSameOrBefore(to)) // in the month
            {
                totalrev += parseFloat((events[i].data_revprediction == null) ? 0 : events[i].data_revprediction);
            }
            else if (moment(efrom).isBefore(from) && moment(eto).isBetween(from, to, null, '[]')) // before from ends in month
            {
                a = ((events[i].data_revprediction == null) ? 0 : events[i].data_revprediction); // *0.45
                dd = moment(eto).diff(efrom, 'days') + 1;
                d = moment(eto).diff(from, 'days') + 1;

                totalrev += parseFloat(a * (0.45 * (d / dd)));

            }
            else if (moment(efrom).isBetween(from, to, null, '[]') && (moment(eto).isAfter(to))) // starts in month ends in next month
            {
                a = ((events[i].data_revprediction == null) ? 0 : events[i].data_revprediction); //*0.55
                dd = moment(eto).diff(efrom, 'days') + 1;
                d = moment(to).diff(efrom, 'days') + 1;

                totalrev += parseFloat(a) * (0.55 + (0.45 * (d / dd)));
            }
            else if ((moment(efrom).isSameOrBefore(from) && moment(eto).isSameOrAfter(to))) //event starts outside month and ends outside month
            {
                a = ((events[i].data_revprediction == null) ? 0 : events[i].data_revprediction) * 0.45;
                dd = moment(eto).diff(efrom, 'days') + 1;
                d = moment(to).diff(from, 'days') + 1;

                totalrev += parseFloat(d * (a / dd));
            }
        }

        return totalrev;
    }


    var refreshReport = function () {
        var url = App.createUrl("Backend/GetTargetList");

        var month = $("#month").val();
        var year = $("#year").val();
        month = (month < 10) ? "0" + month : month;
        var currdate = year + "-" + month + "-01";
        cd = moment(currdate);
        var i = 0;
        var st = moment(cd).startOf('month').day(1).startOf('day');
        if (st.unix() > moment(cd).startOf('month').unix())
            st = moment(cd).startOf('month').day(-6).startOf('day');

        var ed = moment(st).add(6, 'week');
        url = url + "&start=" + st.unix() + "&end=" + ed.unix() + "&month=" + month + "&year=" + year;

        $("#result > tbody > tr").each(function (i, o) {
            $(o).remove();
        });

        $("#result > tbody").html("<tr><td colspan='4'><h1><div class='fa fa-spin fa-spinner'></div> LOADING...</h1></td></tr>");

        $.ajax({
            url: url,
            success: function (res) {
                var obj;
                res = "obj =" + res;
                eval(res);

                events = obj;
                var out = "";

                $.each(obj.targets, function (i, o) {
                    var rev = calculaterevenue(obj.events, moment(i).startOf('day'), moment(i).endOf('day'));
                    var target = o;
                    brands = obj.brands[i];
                    var perc = 0;
                    if (target != "" && target > 0) {
                        perc = ((rev - target) / target) * 100;
                    }
                    else
                        perc = 0;
                    out += "<tr><td>" + i + "</td>" +
                        "<td>" + numeral(rev).format("0,0") + "</td>" +
                        "<td data-type='target'><input type='number' step='any' name='Target[" + i + "]' value='" + target + "'></td>" +
                        "<td>" + numeral(perc).format("0,0") + "%</td>" +
                        "<td data-type='brand'>"+brands+"</td></tr>";
                });

                $("#result > tbody").html(out);
            }
        });
    };

    $('.event__button-save-targets').on('click', function () {
        var data = {};

        $("input[name*='Target']").each(function (i, o) {
            data[$(o).attr("name")] = $(o).val();
            console.log(data[$(o).attr("name")]);
        });
        $btn=$("button > div");
        $btn.attr("class", "fa fa-spin fa-spinner");
        $btn.attr("disabled", "disabled");

        $.ajax({
            url: App.createUrl("Backend/SaveTargets"),
            type: "POST",
            data: data,
            success: function (res) {
                if (res == "SUCCESS") {
                    $("button > div").attr("class", "fa fa-save");
                }

                $("button > div").removeAttr("disabled");
            },
            error: function () {
                $btn = $("button > div");
                $btn.removeAttr("disabled");
                $btn.attr("class", "fa fa-save");
            }
        });
    });

    $('.event__button-refresh-report').on('click', function () {
        refreshReport();
    });

    $('document').ready(function () {
        refreshReport();
    });
});
