define(['jquery', 'app', 'vendor/moment','bootstrap-multiselect'], function($, App, moment,bs3multiselect) {
    var event_data = [];
    var event_fetch_queue = [];
    var event_update_queue = [];
    var eq_isrunning = false;
    var showpopup = true;
    var currentview = "";
    var old_eq_l = 0;
    var dayview = 0;
    var event_fetcher_ajax = null;
    var fullscreen = false;
    var oldborderconfig;
    var currentindex = 0;
    var currmonth = true;
    var timer;
    var oldheight = 0;
    var filter_events_task = "";
    var filter_events_states = "";
    var filter_for_warehouse = "";
    var filter_for_brand = 0;
    var filter_for_stock = 0;
    var old_filter = "";
    var stock_type = '';
    var events_are_filtering = 0;
    $("#userfilter").multiselect();
    var updateincalendar = function (oevt) {
        var evt = $("#mycalender").fullCalendar("clientEvents", oevt.id);

        if (evt.length > 0) {
            $.each(oevt, function (i, o) {
                evt[0][i] = o;
            });

            $("#mycalender").fullCalendar("updateEvent", evt[0]);
        }
    };

    var checknewupdates = function () {
        $.ajax({
            url: App.createUrl("Backend/NewChanges") + "&dt=" + datetime_update,
            success: function (res) {
                var obj = {};
                res = "obj = " + res;
                eval(res);
                if (obj.data.length > 0) {
                    datetime_update = obj['lastdt'];
                    for (var i = 0; i < obj['data'].length; i++) {
                        var d = obj['data'][i];
                        if (d.operation === "UPDATE") {
                            update_event(d.id, [d.data]);
                        } else if (d.operation === "INSERT") {
                            var dat = d.data;
                            insertintoarray(dat);
                            $("#mycalender").fullCalendar("refetchEvents");
                        } else {
                            delete_thisevent(d.id);
                        }
                    }
                }
            },
            error: function(xhr, statusText, err) {
                if( xhr.status === 403) {
                    window.location.replace('/');
                }
            }
        });
    };

    // TODO need rebuild to long-polling connection
    setInterval(checknewupdates, 7000);

    var addto_queue = function (url, cb, data, type) {
        if (!url)
            return;

        if (!cb)
            cb = function (res) {
                console.log(res);
            };

        if (!data)
            data = null;

        if (!type)
            type = "GET";

        event_update_queue.push({
            url: url,
            callback: cb,
            data: data,
            type: type
        });
    };

    var execute_queue = function () {
        if (event_update_queue.length > 0 && eq_isrunning <= 2) {
            var evt = event_update_queue.pop();
            var type = evt.type;
            var url = evt.url;
            cb = evt.callback;
            var data = evt.data;
            eq_isrunning++;

            $.ajax({
                url: url,
                type: type,
                data: data,
                success: function (res) {
                    eq_isrunning--;
                    cb(res);
                },
                error: function () {
                    eq_isrunning--;
                    console.log("something went wrong");
                }
            });
        }
        if (old_eq_l != event_update_queue.length) {
            if (event_update_queue.length != 0) {
                $("#eq_status").html("<i class='fa fa-spin fa-spinner'></i>Syncing .. " + event_update_queue.length + " left");
            } else {
                $("#eq_status").html("");
            }

            old_eq_l = event_update_queue.length;
        }
    };

    // TODO need rebuild to long-polling connection
    setInterval(execute_queue, 1000);

    var dmy = function (dt) {
        return moment(dt).format("DD-MM-YYYY");
    };

    var ymd = function (dt) {
        return moment(dt).format("YYYY-MM-DD");
    };

    var canedit = function (dt) {
        return moment(dt).subtract(1, 'days').hours(18).minutes(0).seconds(0).isSameOrBefore(moment());
    };

    var caneditend = function (dt) {
        return moment(dt).isSameOrBefore(moment().subtract(14, 'days'));
    };

    var insertintoarray = function (evt) {
        for (var i = 0; i < event_data.length; i++) {
            if (
                (moment(evt.start).unix() <= event_data[i].start_dt
                && moment(evt.data_end).unix() >= event_data[i].start_dt)
                || (moment(evt.start).unix() >= event_data[i].start_dt
                    && moment(evt.data_end).unix() <= event_data[i].end_dt)
                || (moment(evt.start).unix() <= event_data[i].end_dt
                    && moment(evt.data_end).unix() >= event_data[i].end_dt)
            ) {
                event_data[i].events.push(evt);
            }
        }
    };

    var deleteeventer = function (e) {
        var rets = [];

        for (var i = 0; i < event_data.length; i++) {
            for (var c = 0; c < event_data[i].events.length; c++) {
                if (event_data[i].events[c].id == e) {
                    rets.push(event_data[i].events.splice(c, 1)[0]);
                }
            }
        }

        return rets;
    };

    var delete_thisevent = function (id) {
        var evts = event_data[getcurrentindex()].events;
        for (var i = 0; i < evts.length; i++) {
            if (parseInt(evts[i].id) == parseInt(id)) {
                xy = i;
                xx = event_data[getcurrentindex()].events.splice(xy, 1);
                $("#mycalender").fullCalendar("removeEvents", [parseInt(id)]);
                break;
            }
        }
    };

    var getcurrentindex = function () {
        var view = $("#mycalender").fullCalendar("getView");
        var start = view.start;
        var end = view.end;
        for (var i = 0; i < event_data.length; i++) {
            if (event_data[i].start_dt == start.unix() && event_data[i].end_dt == end.unix()) {
                return i;
            }
        }

        return 0;
    };

    var update_event = function (id, ob) {
        if (id != null) {
            if (typeof ob.length != "undefined") {
                for (var i = 0; i < ob.length; i++) {
                    deleteeventer(ob[i].id);
                    insertintoarray(ob[i]);
                    updateincalendar(ob[i]);
                }
            } else {
                deleteeventer(id);
                insertintoarray(ob);
                updateincalendar(ob);
            }
        }
    };

    var fetch_events = function (start, end, cb, skipreturn) {
        var found = 0;
        for (var i = 0; i < event_data.length; i++) {
            if (event_data[i].start_dt == start.unix() && event_data[i].end_dt == end.unix()) {
                if (typeof skipreturn == "undefined") {
                    for (var c = 0; c < event_data[i].events.length; c++) {
                        if (dayview)
                            event_data[i].events[c].end = event_data[i].events[c].start;
                        else
                            event_data[i].events[c].end = event_data[i].events[c].ori_end;
                    }
                }
                cb(event_data[i].events);
                return;
            }
        }

        var url = App.createUrl("Backend/GetNewEvents") + "&start=" + start.unix() + "&end=" + end.unix() + "&s=" + start.format() + "&e=" + end.format();

        event_fetcher_ajax = $.ajax({
            url: url,
            success: function (res) {
                var objects = JSON.parse(res);

                event_data.push({
                    start_dt: start.unix(),
                    end_dt: end.unix(),
                    events: objects,
                    query_time: moment()
                });

                cb(objects);
                event_fetcher_ajax = null;
            },
            error: function (xhr, statusText, err) {
                if( xhr.status === 403) {
                    window.location.replace('/');
                }
            }
        });
    };

    var filterevents = function () {
        var $fcEvent = $('.fc-event');

        var t = $("#tasks").val();
        var s = $("#states").val();
        filter_events_task = t;
        filter_events_states = s;
        events_are_filtering = 1;
        $("mycalender").fullCalendar("refetchEvents");
    };

    var filterforwarehouse = function () {
        var t = $("#warehouse_sel > option:selected").text();

        if (t === "All") {
            filter_for_warehouse = "";
        }
        else
            filter_for_warehouse = t;

        events_are_filtering = 1;
        $("#mycalender").fullCalendar("refetchEvents");
    };

    var updatecolors = function (obj) {
        var $fcEvent = $(".fc-event");
        var $myCalendar = $("#mycalender");
        var val = $("[name=as]:checked").val();

        if (events_are_filtering) {
            if (old_filter != val) {
                if (val !== "brandfilter")
                    filter_for_brand = "";
                if (val !== "task") {
                    filter_events_task = "";
                    filter_events_states = "";
                }
                if (val !== "warehouse")
                    filter_for_warehouse = "";

                $("mycalender").fullCalendar("refetchEvents");
                setTimeout(function () {
                    updatecolors(obj)
                }, 0);
                old_filter = val;
                events_are_filtering = 0;
                return;
            }
        }
        old_filter = val;
        if (obj) {
            if ($(".fc-header-title > h3").length == 0)
                $(".fc-header-title").append("<h3></h3>");

            $(".fc-header-title > h3").html("(" + $(obj).attr("data-label") + ")");
        }

        if (val === "warehouse")
            $("#warehouse").removeClass("hidden");
        else {
            $fcEvent.css("visibility", "");
            $("#warehouse").addClass("hidden");
        }

        val = $("[name=as]:checked").val();

        if ($(obj).attr("data-show3") == 1) {
            dayview = 1;
            $myCalendar.fullCalendar("refetchEvents");
        } else if (obj) {
            if (dayview === 1) {
                dayview = 0;
                $myCalendar.fullCalendar("refetchEvents");
            }
            $("#selected_from_date").hide();
            $("#selected_to_date").hide();
        }

        if ($(obj).attr("data-show4") || val === "brandfilter") {
            $("#filter_brand").show();
            val = "category";
            //setTimeout(filterforbrand,0);
        }
        else if (val != "brandfilter") {
            $("#filter_brand").hide();
        }
        if(val == "stocktype"){
            $("#stock_type").show();
        }
        else {
            $("#stock_type").hide();
        }
        if ($(obj).attr("data-show") == 1)
            $(".taskspart").removeClass("hidden");
        else {
            $fcEvent.css("visibility", "");
            $(".taskspart").addClass("hidden");
        }

        var a = $(".fc-event");
        var att = "";

        if (val === "category") {
            att = "data-category";
        } else if (val === "manager") {
            att = "data-manager";
        } else if (val === "status") {
            att = "data-status";
        } else if (val === "bracket") {
            att = "data-sales";
        } else if (val === "daywise") {
            att = "data-sales";
        } else if (val === "task") {
            att = "data-task";
        } else if (val === "warehouse") {
            att = "data-warehouse";
        } else if (val === "totalunitsavailable") {
            att = "data-totalunitsavailable";
        } else if(val == "repeat_styles"){
            att = "data-repeat-styles";
        } else if(val == "stocktype") {
            att = "data-stock-type";
        }


        if (att != "") {
            $(a).each(function (i, o) {
                if (att == "data-totalunitsavailable") {
                    if ($(o).attr(att + "-text") != "" && $(o).attr(att + "-text") > 0)
                        $(o).css("background-color", "#44aa44");
                    else
                        $(o).css("background-color", "#aa5555");
                } else if(att == "data-repeat-styles"){
                    if ($(o).attr(att) == "1")
                        $(o).css("background-color", "#44aa44");
                    else
                        $(o).css("background-color", "#aa5555");
                }
                else if(att == "data-stock-type") {
                    if($(o).attr('data-stock-type') == "SCS")
                        $(o).css("background-color","#8d8dcc");
                    else if($(o).attr('data-stock-type') == "Non-SCS")
                        $(o).css("background-color","#99ccc5");
                    else if($(o).attr('data-stock-type') == "RS")
                        $(o).css("background-color","#f5c1f5");
                    else if($(o).attr('data-stock-type') == "SCS & RS")
                        $(o).css("background-color","#f2e96a");
                }
                else
                    $(o).css("background-color", "#" + $(o).attr(att));

                var c = $(o).find(".fc-title");
                var text = $(o).attr("data-brand") + "-" + $(o).attr("data-eventid") + " ";
                if (text.indexOf("(") == -1)
                    if (att === "data-warehouse") {
                        text = text + " (" + $(o).attr(att + "-text") + " - " + $(o).attr("data-status-text") + ")";
                    }
                    else if (val === "daywise") {
                        text = $(o).attr("data-brand") + " (" + $(o).attr(att + "-text") + ")";
                    }
                    else
                        text = text + " (" + $(o).attr(att + "-text") + ")";
                else {
                    var arr = text.split("(");
                    if (att === "data-warehouse") {
                        text = arr[0] + " (" + $(o).attr(att + "-text") + " - " + $(o).attr("data-status-text") + ")";
                    }
                    else if (val === "daywise") {
                        text = $(o).attr("data-brand") + " (" + $(o).attr(att + "-text") + ")";
                    }
                    else
                        text = arr[0] + " (" + $(o).attr(att + "-text") + ")";
                }
                if(showvip)
                    text += "<div class='fa fa-copy copy-to-vip' title='Copy to VIP Side'></div>";
                $(c).html(text);
            });
        }
        $(".copy-to-vip").on("click",function(evt){
            ret = confirm("Are you sure you want to send the event `"+$(evt.currentTarget).parent().text()+"` moved to VIP side?");
            if(ret){
                $.ajax({url:App.createUrl("Event/copytovip")+"&id="+$(evt.currentTarget).parent().parent().parent().attr("data-id"),"success":function(res){
                    if(res == "OK")
                        alert("Done! Moved to VIP calendar.");
                    else {
                        alert("Opps :"+res);
                    }
                }});
            }
            evt.stopPropagation();
            event.preventDefault()
        });
    };

    var event_fetcher = function () {
        if (event_fetch_queue.length > 0 && event_fetcher_ajax == null) {
            fetch_events(event_fetch_queue[0].start, event_fetch_queue[0].end, function (obj) {
                event_fetch_queue.shift();
            }, 1);
        }
    };

    var changeuserfilter = function (sel) {
        val = [];
        v = $(sel).val()
        if(v != null)
            val = v.toString();
        console.log("val",val);

        $.ajax({
            url: App.createUrl('Event/GetBrandListForUser') + "&id=" + val,
            success: function (res) {
                $("#filter_brand").html(res);
            }
        });
        filter_for_user = v;
        events_are_filtering = 1;

        $("#mycalender").fullCalendar("refetchEvents");
    };

    var getmd_brand_list = function () {
        var v = $("#md_userlist").val();
        $.ajax({
            url: App.createUrl('Event/GetBrandListForUser') + "&id=" + v,
            success: function (res) {
                $("#md_brandlist").html(res);
            }
        });

        resetmd();
    };

    var calculateRevenue = function (events, fromDate, toDate) {
        var totalRevenue = 0;
        var from = moment(fromDate);
        var to = moment(toDate);

        for (var i = 0; i < events.length; i++) {
            var efrom = moment(events[i].data_start);
            var eto = moment(events[i].data_end).endOf('day');

            var a, d, dd;

            if ((moment(efrom).isSameOrAfter(from)) && moment(eto).isSameOrBefore(to)) // in the month
            {
                totalRevenue += parseFloat((events[i].data_revprediction == null) ? 0 : events[i].data_revprediction);
            }
            else if (moment(efrom).isBefore(from) && moment(eto).isBetween(from, to, null, '[]')) // before from ends in month
            {
                a = ((events[i].data_revprediction == null) ? 0 : events[i].data_revprediction); // *0.45
                dd = moment(eto).diff(efrom, 'days') + 1;
                d = moment(eto).diff(from, 'days') + 1;

                totalRevenue += parseFloat(a * (0.45 * (d / dd)));

            }
            else if (moment(efrom).isBetween(from, to, null, '[]') && (moment(eto).isAfter(to))) // starts in month ends in next month
            {
                a = ((events[i].data_revprediction == null) ? 0 : events[i].data_revprediction); //*0.55
                dd = moment(eto).diff(efrom, 'days') + 1;
                d = moment(to).diff(efrom, 'days') + 1;

                totalRevenue += parseFloat(a) * (0.55 + (0.45 * (d / dd)));
            }
            else if ((moment(efrom).isSameOrBefore(from) && moment(eto).isSameOrAfter(to))) //event starts outside month and ends outside month
            {
                a = ((events[i].data_revprediction == null) ? 0 : events[i].data_revprediction) * 0.45;
                dd = moment(eto).diff(efrom, 'days') + 1;
                d = moment(to).diff(from, 'days') + 1;

                totalRevenue += parseFloat(d * (a / dd));
            }
        }

        return totalRevenue;
    };

    var updatemainrevenue = function () {
        var $myCalendar = $("#mycalender");


        var view = $myCalendar.fullCalendar("getView");
        var events = $myCalendar.fullCalendar("clientEvents");
        var cdate = $myCalendar.fullCalendar("getDate");

        var from = moment(view.intervalStart);
        var to = moment(view.intervalEnd);

        var m = moment();
        var l = "";
        var inthismonth = (moment(m).isSame(to) || moment(m).isBefore(to)) && (moment(m).isSame(from) || moment(m).isAfter(from));

        tt = 0;
        var dat = moment(cdate).endOf('month');
        var dat1 = moment(cdate).startOf('month');
        var endm = dat.date();

        $("#revenue_list").html("");
        $("#revenue_list2").html("");

        // TODO need fix
        $("#revenue_list2").append("" +
            "<li class='revenue_download-csv'>" +
            "<a href='#'><div class='fa fa-download'></div> Download As CSV</a>" +
            "</li>"
        );

        for (var i = 1; i <= endm; i++) {
            var s = "";
            if (i === m.date() && inthismonth)
                s = "style='color:red'";
            var tarr = calculateRevenue(events, from, dat1);
            var t = tarr;
            var tt = tarr;
            var t2 = calculateRevenue(events, moment(dat1).startOf('day'), moment(dat1).add('0', 'day').endOf('day'));
            $("#revenue_list").append("<li data-amt='" + numeral(tt).format("0.") + "' data-date1='" + moment(dat1).format("YYYY-MM-DD") + "'><font " + s + "><b>(R" + numeral(tt).format("0,0") + " on " + dat1.format("Do") + ") </b></font></li>");
            $("#revenue_list2").append("<li data-amt='" + numeral(t2).format("0.") + "' data-date='" + moment(dat1).format("YYYY-MM-DD") + "'><font " + s + "><b>(R" + numeral(t2).format("0,0") + " on " + dat1.format("Do") + ") </b></font></li>");
            dat1.add(1, 'days');
        }

        if (inthismonth) {
            if (m.isBefore(to, 'day') && inthismonth)
                tt = calculateRevenue(events, from, m);
            else {
                tt = calculateRevenue(events, from, to);
                m = to;
            }
            l = "<font style='color:red;'><b>(R" + numeral(tt).format("0,0") + " on " + m.format("Do") + ") </b></font>";
        } else
            l = "";

        $("#revprediction").html(l + "R" + numeral(t).format("0,0"));
    };

    var closedialog = function (ob, type) {
        if (type === "delete") {
            for (var i = 0; i < ob.length; i++) {
                delete_thisevent(ob[i]);
            }
        } else {
            update_event(event_id, ob);
        }

        $("#eventmodal").modal("hide");
    };

    var hideonfullscreen = function () {
        $("#header").hide();
        $(".navbar").hide();
        $("#downloadascsv").hide();
        $("#accordion").hide();
        $("#calendarheader").hide();
        oldborderconfig = $(".fc-widget-content").css("border");
        $(".fc-widget-content").css("border", "1px solid #000");
        $("#mycalender").fullCalendar("option", "contentHeight", window.innerHeight);
    };

    var showonfullscreen = function () {
        $("#header").show();
        $(".navbar").show();
        $("#downloadascsv").show();
        $("#accordion").show();
        $("#calendarheader").show();
        $(".fc-widget-content").css("border-color", oldborderconfig);
        $("#mycalender").fullCalendar("option", "contentHeight", oldheight);
    };

    var switchfilter = function () {
        var $myCalendar = $("#mycalender");

        if (!currmonth) {
            $myCalendar.fullCalendar("next");
        } else {
            $myCalendar.fullCalendar("today");
            var rs = $("#accordion").find("input[type=radio]");
            $(rs[currentindex]).attr("checked", "checked");
            updatecolors(rs[currentindex]);
            currentindex++;

            if (currentindex === rs.length)
                currentindex = 0;
        }

        $myCalendar.fullCalendar("refetchEvents");
        currmonth = !currmonth;
    };

    var gofullscreen = function () {
        var $myCalender = $("#mycalender");

        hideonfullscreen();

        timer = window.setInterval(switchfilter, 30000);

        var d = document.documentElement;
        if (d.webkitRequestFullScreen)
            d.webkitRequestFullScreen();
        if (d.mozRequestFullScreen)
            d.mozRequestFullScreen();
        if (d.msRequestFullScreen)
            d.msRequestFullScreen();

        var off = $myCalender.offset();
        $myCalender.scrollTop(off.top);
        $myCalender.fullCalendar("option", "contentHeight", window.innerHeight);
    };

    var exitfullscreen = function () {
        window.clearTimeout(timer);
        showonfullscreen();

        if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();

        if (document.mozExitFullscreen)
            document.mozExitFullscreen();

        if (document.msExitFullscreen)
            document.msExitFullscreen();
    };

    var filterforday = function (tf) {
        var from = $("#selected_from_date").val();
        var to = $("#selected_to_date").val();
        var fr = moment(from);
        to = moment(to);

        var arr = $("#mycalender").fullCalendar("clientEvents");
        for (var i = 0; i < arr.length; i++) {
            if (tf) {
                arr[i].end = moment(arr[i].start).add(1, 'hour');
            } else {
                arr[i].end = arr[i].ori_end;
            }
        }
    };

    var filterforbrand = function () {
        filter_for_brand = $("#filter_brand").val();
        $("#mycalender").fullCalendar("refetchEvents");
    };

    var massdelete = function () {
        $("#md_btn").html("Mass Delete");
        $("#massdelete").modal("show");
    };

    var md_getstats = function () {
        var from = $("#md_from").val();
        var to = $("#md_to").val();
        var user = $("#md_userlist").val();
        var brand = $("#md_brandlist").val();

        if ($("#md_btn").html() == "Mass Delete") {
            $.ajax({
                url: App.createUrl('Event/getmassdstats') + "&start=" + from + "&to=" + to + "&brand=" + brand + "&user=" + user,
                success: function (res) {
                    $("#md_status").html(res);
                    $("#md_btn").html("Continue Do It!");
                }
            });
        } else {
            $.ajax({
                url: App.createUrl('Event/massdelete') + "&start=" + from + "&to=" + to + "&brand=" + brand + "&user=" + user,
                success: function (res) {
                    $("#md_status").html("");
                    $("#md_btn").html("Mass Delete");
                    $("#massdelete").modal("hide");
                }
            });
        }
    };

    var resetmd = function () {
        $("#md_btn").html("Mass Delete");
    };

    var adminHandlers = {
        select: function (start, end, jsevent, view) {
            end = end.subtract(1, 'days');

            showpopup = true;
            jsevent.preventDefault();
            jsevent.stopPropagation();

            $(jsevent.target).attr('data-html', 1);
            $(jsevent.target).attr('title', 'Create Event');
            $(jsevent.target).attr('data-trigger', 'focus');
            $(jsevent.target).attr('data-container', 'body');
            $(jsevent.target).attr("data-content", '<div class="text-center">' +
                '<b>From :</b> ' + dmy(start) + '<b> to </b>' + dmy(end) +
                '<button data-from="' + ymd(start) + '" data-to="' + ymd(end) + '" class="btn btn-primary event__button-create">' +
                'Create' +
                '</button>' +
                '</div>');
            $(jsevent.target).attr("data-placement", 'top');
            $(jsevent.target).popover('show');
        },

        eventDrop: function (evt, delta, revertfunc, jsevent, ui, view) {
            var id = evt.id;
            var linked = "";
            var ret = "";
            var today12 = moment().add(12, 'hours');
            var estart = evt.ori_end;
            if (canedit(evt.data_start)) {
                alert("This event has locked because it is about to start. The dates cannot be modified.");
                revertfunc();
                return;
            }

            var sendend = moment(evt.end).subtract(1, 'days').format("YYYY-MM-DD");
            if (evt.end == null)
                evt.end = evt.start;

            if (evt.linkedto != 0 && evt.linkedto != null) {
                if (confirm("WARNING This event is a repetition of another event. Changing the dates will cause it to become an independant event. Continue?")) {
                    linked = "&link=1";
                } else {
                    revertfunc();
                    return;
                }
            }

            if (evt.haslinks == 1) {
                if (confirm("WARNING This is a repeating event and has other events that are linked to it. Would you like to continue making changes to this event?")) {
                    if (confirm("Would you like to replicate the date changes on all the repeating events? or make this event independant form the rest?")) {
                        linked = "&link=1";
                    } else
                        linked = "&link=0";
                } else {
                    revertfunc();
                    return;
                }
            }

            var url = App.createUrl("Event/ChangeDateTime") + "&id=" + id + "&startdate=" + ymd(evt.start) + "&enddate=" + sendend + linked;

            addto_queue(
                url,
                function (res) {
                    try {
                        var r = "res = " + res + ";";
                        eval(r);
                        if (typeof res.success != "undefined") {
                            for (var i = 0; i < res.events.length; i++) {
                                deleteeventer(res.events[i].id);
                                insertintoarray(res.events[i]);
                            }
                        } else {
                            if (typeof res.error != "undefined")
                                alert(res.error);
                            else
                                alert("An Unknown Error Occurred");
                            revertfunc();
                        }
                    } catch (e) {
                        revertfunc();
                        alert("Unknown Error Occured : " + res + "\n" + e.toString());
                    }
                },
                {reason: ""},
                "POST"
            );
        },

        eventResize: function (evt, delta, revertfunc) {
            var id = evt.id;
            var linked = "";
            var today12 = moment().add(12, 'hours');
            var sendend = moment(evt.end).subtract(1, 'days').format("YYYY-MM-DD");

            if (caneditend(evt.data_end)) {
                alert("This event cannot be extended as its too old.");
                revertfunc();
                return;
            }

            if (evt.linkedto != 0 && evt.linkedto != null) {
                if (confirm("WARNING This event is a repetition of another event. Changing the dates will cause it to become an independant event. Continue?")) {
                    linked = "&link=1";
                } else {
                    revertfunc();
                    return;
                }
            }

            if (evt.haslinks == 1) {
                if (confirm("WARNING This is a repeating event and has other events that are linked to it. Would you like to continue making changes to this event?")) {
                    if (confirm("Would you like to replicate the date changes on all the repeating events? or make this event independant form the rest?")) {
                        linked = "&link=1";
                    } else
                        linked = "&link=0";
                } else {
                    revertfunc();
                    return;
                }
            }

            var url = App.createUrl("Event/ChangeDateTime") + "&id=" + id + "&startdate=" + ymd(evt.start) + "&enddate=" + sendend + linked;
            addto_queue(
                url,
                function (res) {
                    try {
                        var r = "var res = " + res;
                        eval(r);
                        if (typeof res.success != "undefined") {
                            console.log("success");

                            for (i = 0; i < res.events.length; i++) {
                                deleteeventer(res.events[i].id);
                                insertintoarray(res.events[i]);
                            }
                        } else {
                            if (typeof res.error != "undefined")
                                alert(res.error);
                            else
                                alert("An Unknown Error Occurred");
                            revertfunc();
                        }
                    } catch (e) {
                        revertfunc();
                        alert("Unknown Error Occured : " + res + "\n" + e.toString());
                    }
                },
                {reason: ""},
                "POST"
            );
        },

        eventClick: function (calevent, jsevent, view) {
            $.ajax(
                calevent.url,
                {
                    success: function (res) {
                        $('.event-modal').data('event-id', calevent.id);

                        console.log(calevent);

                        $(".event-modal .modal-body").html(res);
                        $("#eventmodal").modal("show");

                        $('.event-form').eventForm();
                        $('.event-comment').eventComment();

                        // disabled locked fields
                        if(canedit(calevent.data_start)){
                            $("#startdate").attr("disabled","disabled");
                            if(caneditend(calevent.data_end))
                                $("#enddate").attr("disabled","disabled");
                            else
                                $("#enddate").removeAttr("disabled");
                            $("#Event_revprediction").attr("disabled","disabled");
                        }
                        else{
                            $("#startdate").removeAttr("disabled");
                            $("#enddate").removeAttr("disabled");
                            $("#Event_revprediction").removeAttr("disabled");
                        }
                    }
                }
            );

            jsevent.preventDefault();
        }
    };

    $('body').on('click', '.popover .event__button-create', function () {
        var $button = $(this);
        var from = $button.data('from');
        var to = $button.data('to');

        var man = $("#userfilter").val();
        var cdate = $("#mycalender").fullCalendar("getDate");
        location.href = App.createUrl("event/create") + "&fromdate=" + from + "&todate=" + to + "&backToCalendar=1&manager=" + man + "&date=" + cdate.format("Y-MM-DD");
    });

    $("body").on("click", function () {
        if (!showpopup)
            $(".popover").remove();
        else
            showpopup = false;
    });

    $('#downloadascsv').on('click', function () {
        var view = $("#mycalender").fullCalendar("getView");
        var start = view.start;
        var end = view.end;
        document.location.href = App.createUrl("Backend/DownloadCSV") + "&from=" + start.format("YYYY-MM-DD") + "&to=" + end.format("YYYY-MM-DD");
    });

    $('.button-toggle-full-screen').on('click', function () {
        if (!fullscreen) {
            fullscreen = true;
            gofullscreen();
        } else {
            fullscreen = false;
            exitfullscreen();
        }
    });

    $('#userfilter').on('change', function () {
        changeuserfilter(this);
    });

    $('.calendar__radio-filter').on('click', function () {
        updatecolors(this);
    });

    $('#warehouse_sel').on('change', function () {
        filterforwarehouse();
    });

    $('#tasks').on('change', function () {
        filterevents();
    });

    $('#states').on('change', function () {
        filterevents();
    });

    $('#radioBrandFilter').on('click', function () {
        updatecolors(this);
    });

    $('#warehouse_sel').on('change', function () {
        filterforwarehouse();
    });

    
    var fullCalendarOptions = {
        height:'auto',
        viewRender: function (view, element) {
            if (currentview == "")
                currentview = view.name;

            if (currentview != view.name) {
                m = moment();
                currentview = view.name;
                $("#mycalender").fullCalendar('gotoDate', m);
            }
        },

        eventRender: function (event, element) {
            $(element[0]).attr("data-id", event.id);
            $(element[0]).attr("data-manager", event.data_manager);
            $(element[0]).attr("data-status", event.data_status);
            $(element[0]).attr("data-sales", event.data_sales);
            $(element[0]).attr('data-eventid', event.data_eventid);
            $(element[0]).attr('data-brand', event.data_brand);
            $(element[0]).attr('data-brandid', event.data_brandid);
            $(element[0]).attr('data-repeat-styles', event.data_repeat_styles);
            $(element[0]).attr("data-category", event.data_category);
            $(element[0]).attr("data-manager-text", event.data_manager_text);
            $(element[0]).attr("data-status-text", event.data_status_text);
            $(element[0]).attr("data-sales-text", event.data_sales_text);
            $(element[0]).attr("data-category-text", event.data_category_text);
            $(element[0]).attr("data-revprediction", event.data_revprediction);
            $(element[0]).attr("data-task-text", event.data_task_text);
            $(element[0]).attr("data-totalunitsavailable-text", event.data_totalunitsavailable_text);
            $(element[0]).attr("data-warehouse", event.data_warehouse);
            $(element[0]).attr("data-warehouse-text", event.data_warehouse_text);
            $(element[0]).attr("data-stock-type", event.data_stock_type);
            $(element[0]).attr("data-stock-type-text", event.data_stock_type);

            for (var i = 0; i < tasklist.length; i++) {
                $(element[0]).attr("data-" + tasklist[i], event['data_' + tasklist[i]]);
            }
        },

        eventAfterAllRender: function () {
            updatecolors();

            setTimeout(
                function () {
                    updatemainrevenue();
                },
                0
            );

            $(".fc-holiday").each(function (i, o) {
                $(o).removeClass("fc-holiday");
            });

            for (var i = 0; i < holidays.length; i++) {
                var dat = holidays[i].date;
                var $a = $(".fc-day[data-date='" + dat + "']");
                if ($a.length != 0) {
                    $a.attr("title", holidays[i].name);
                    if (!$a.hasClass("fc-sun") && !$a.hasClass("fc-sat")) {
                        $a.addClass("fc-holiday");
                    }
                }
            }
            $(".fc-day-grid-container").height($($(".fc-day-grid-container")[0]).height());
        },

        firstDay: 1,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'year,month,basicWeek,basicDay'
        },
        yearColumns: 3,
        nextDayThreshold: "00:00:00",

        events: function (start, end, timezone, callback) {
            fetch_events(
                start,
                end,
                function (evts) {
                    var newevts = [];
                    var i;

                    if (filter_for_user != null) {

                        for (i = 0; i < evts.length; i++) {
                            if (filter_for_user.indexOf(evts[i].manager) != -1 && filter_for_brand == "")
                                newevts.push(evts[i]);
                            else if (filter_for_user.indexOf(evts[i].manager) != -1 && filter_for_brand == evts[i].data_brandid) {
                                newevts.push(evts[i]);
                            }
                        }
                    }
                    else if (filter_for_brand != "") {
                        for (i = 0; i < evts.length; i++) {
                            if (evts[i].data_brandid == filter_for_brand) {
                                newevts.push(evts[i]);
                            }
                        }
                    }
                    else if (filter_events_task != "" && filter_events_states != "") {
                        for (i = 0; i < evts.length; i++) {
                            if (typeof evts[i]['data_' + filter_events_task + '_' + filter_events_states] != "undefined") {
                                newevts.push(evts[i]);
                            }
                        }
                    }
                    else if (filter_for_warehouse != "") {
                        for (i = 0; i < evts.length; i++) {
                            if (evts[i]['data_warehouse_text'] == filter_for_warehouse) {
                                newevts.push(evts[i]);
                            }
                        }
                    }
                    else if(filter_for_stock != ""){
                        for (i = 0; i < evts.length; i++) {
                            if ((filter_for_stock == 'SCS & RS' && (evts[i]['data_stock_type'] == 'SCS' || evts[i]['data_stock_type'] == 'RS')) || evts[i]['data_stock_type'] == filter_for_stock) {
                                newevts.push(evts[i]);
                            }
                        }
                    }
                    else
                        newevts = evts;


                    callback(newevts);
                }
            );

            var currdate = $("#mycalender").fullCalendar("getDate");

            var cd = moment(currdate).utcOffset(0).add(moment(currdate).utcOffset() * moment(currdate).utcOffset());
            console.log(moment(currdate).utcOffset());

            event_fetch_queue = [];

            for (var i = 0; i < 3; i++) {
                var st = moment(cd).subtract(i + 1, 'month').startOf('month').day(1).startOf('day');
                if (st.unix() > moment(cd).subtract(i + 1, 'month').startOf('month').unix())
                    st = moment(cd).subtract(i + 1, 'month').startOf('month').day(-6).startOf('day');

                var ed = moment(st).add(6, 'week');
                event_fetch_queue.push({'start': st, 'end': ed});
                st = moment(cd).add(i + 1, 'month').startOf('month').day(1).startOf('day');
                if (st.unix() > moment(cd).add(i + 1, 'month').startOf('month').unix())
                    st = moment(cd).add(i + 1, 'month').startOf('month').day(-6).startOf('day');

                ed = moment(st).add(6, 'week');
                event_fetch_queue.push({'start': st, 'end': ed});
            }
        }        
    };

    var initCalendar = function () {
        var $myCalendar = $("#mycalender");

        fullCalendarOptions.defaultDate = moment(currentDate);

        if (canUpdateEvent === 0) {
            fullCalendarOptions.editable = false;
            fullCalendarOptions.selectable = false;
            fullCalendarOptions.eventClick = adminHandlers.eventClick;

            $myCalendar.fullCalendar(fullCalendarOptions);
            return true;
        }

        fullCalendarOptions.editable = true;
        fullCalendarOptions.selectable = true;

        fullCalendarOptions.select = adminHandlers.select;
        fullCalendarOptions.eventDrop = adminHandlers.eventDrop;
        fullCalendarOptions.eventResize = adminHandlers.eventResize;
        fullCalendarOptions.eventClick = adminHandlers.eventClick;

        $myCalendar.fullCalendar(fullCalendarOptions);
    };

    var initFilter = function() {
        if($("#userfilter").length) {
            changeuserfilter($("#userfilter")[0]);
        }
    };

    $('#md_from').on('change', function () {
        resetmd();
    });

    $('#md_to').on('change', function () {
        resetmd();
    });

    $('#md_btn').on('click', function () {
        md_getstats();
    });

    $('#massdelete_btn').on('click', function () {
        massdelete();
    });

    $('#md_userlist').on('change', function () {
        getmd_brand_list();
    });

    $('#filter_brand').on('change', function () {
        filterforbrand();
    });

    $('#md_brandlist').on('change', function () {
        resetmd();
    });
    $("#stock_type").on('change',function(){
        filter_for_stock = $("#stock_type").val();
        $("#mycalender").fullCalendar("refetchEvents");
    });
    $('#revenue_list2').on('click', '.revenue_download-csv', function () {
        var out = "data:text/csv;charset=utf-8,Date, MTD AMT, Daily Prediction\n";
        var list = $("#revenue_list > li[data-date1]");
        $("#revenue_list2 > li[data-date]").each(function (i, o) {
            var date = $(o).attr("data-date");
            var val = $(o).attr("data-amt");
            var val2 = $(list[i]).attr("data-amt");
            out += date + "," + val2 + "," + val + "\n";
        });
        var encodedUri = encodeURI(out);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "daily_prediction.csv");
        document.body.appendChild(link); // Required for FF
        link.click();
    });

    initCalendar();
    initFilter();

    // TODO need rebuild to long polling
    setInterval(event_fetcher, 1000);

    changeuserfilter(document.querySelector("#userfilter"));
});
