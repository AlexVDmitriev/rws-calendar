/**
 * Created by nikhil
 */
define(['jquery', 'app', 'bootstrap-datepicker'], function($, App) {
    $.fn.vtrequest = function () {
        var init = function() {
            $("#exportbtn").on("click",function(){
                str = "data:text/csv;charset=utf-8,";
                str += $("#heading").html()+"\n";
                heading = "SHOOT PLAN \n";
                heading += "DAY, BRAND\n";
                for(i=0; i < heading; i++){
                    heading += day_dates[i]+", \n";
                }
                heading += "\n";
                heading += "PROBLEMS\n";
                heading += "SAMPLE DELIVERY,BRAND,ISSUE\n\n";
                heading += "SAMPLES,BRAND,ISSUE\n\n";
                heading += "STUDIO / SHOOT,BRAND,ISSUE\n\n";
                heading += "RETOUCHING,BRAND,ISSUE\n\n";
                headrow = "DAY,BRAND,CATEGORY,MANAGER,SAMPLE DATE,AMOUNT,SAMPLE TYPE,SHOOT,SUPPLIED IMAGES,REPEATS/DONE,DATE CHANGE,INFO DATE,COMMENTS\n";
                str += heading + headrow;
                body = "";
                $(".week_block").each(function(i,o){
                    str += "\n"+$(o).find("h4").text()+"\n";
                    str += headrow;
                    $(o).find(".data_table").each(function(i2,o2){
                        $(o2).find("tbody").find("tr").each(function(i3,o3){
                            $(o3).find("td").each(function(ii,oo){
                                str += $(oo).text()+",";
                            });
                            str += "\n";
                        });
                    });
                });
                str+= "\n CONCERNS\n";
                str += "BRAND, CONCERN\n";
                var encodedUri = encodeURI(str);
                var link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "VT request doc.csv");
                document.body.appendChild(link); // Required for FF
                link.click();
            });
        };
        init();
    };
    var $mkForm = $('body');

    if ($mkForm.length) {
        $mkForm.vtrequest();
    }
});
