/**
 * Created by nikhil
 */
var startdate = "";
var enddate = "";
var checked = 0;
var check_list = [];
define(['jquery', 'app', 'bootstrap-datepicker','moment'], function($, App, dp, moment) {
    $.fn.dailymailer = function () {
        var init = function() {
            dt = moment().format("YYYY-MM-DD");
            dp = $("#fromdate");
            tdt = $("#todate");
            tdt.datepicker({
                format:"yyyy-mm-dd",
                calendarWeeks:true
            });

            dp.datepicker({
                format:"yyyy-mm-dd",
                calendarWeeks:true
            });
            dp.on("show",function(e){
                $(".cw").each(function(i,o){
                    if(i > 0) {
                        chk = "";
                        chk = check_list[i - 1] ? "checked='checked'" : "";
                        $(o).html("<input "+chk+" type='checkbox' class='weekcheckbox'>");
                    }
                });
                check_list = [];
                checked = 0;
                $(".weekcheckbox").on("click",function(e){
                    e.stopPropagation();
                    e.bubbles = false;
                    $(".weekcheckbox").each(function(i,o){
                        check_list[i] = o.checked;
                    });

                    lst = $(".weekcheckbox:checked");
                    l2 = $(lst[0]).parent().parent().find("td[data-date]");
                    if(l2.length > 0)
                    {
                        dt = $(l2[0]).attr("data-date");
                        startdate = dt;
                    }
                    l2 = $(lst[lst.length-1]).parent().parent().find("td[data-date]");
                    if(l2.length > 0)
                    {
                        dt = $(l2[l2.length-1]).attr("data-date");
                        enddate = dt;
                    }
                    checked = 1;
                });
                $(".weekcheckbox:checked").click();
            });

            dp.on("hide",function(e){
                value = e.date;

                if(!checked && value) {
                    $('#fromdate').val(moment(value).format("YYYY-MM-DD"));
                    //$('#todate').val(moment(value, "YYYY-MM-DD").day(6).format("YYYY-MM-DD"));
                }
                else {
                    $('#fromdate').val(moment(parseInt(startdate)).format("YYYY-MM-DD"));
                    $('#todate').val(moment(parseInt(enddate)).format("YYYY-MM-DD"));
                }
            });
            dp.val(moment().day(0).format("YYYY-MM-DD"));
            tdt.val(moment().day(6).format("YYYY-MM-DD"));
        };
        init();
    };
    var $mkForm = $('body');

    if ($mkForm.length) {
        $mkForm.dailymailer();
    }
});
