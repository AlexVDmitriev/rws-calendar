define(['jquery', 'app'], function ($, App) {
    $('.event-log__date').on('change', function () {
        document.location.href = App.createUrl("backend/eventlog") + "&date=" + $("#date").val();
    });
});
