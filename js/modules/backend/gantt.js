define(['jquery', 'app'], function($, App) {

dt = moment().startOf("month");
var current_date = dt;
var timer = null;
var panel;


var back = function () {
    current_date.subtract('months',1);
    initchart();
};

var forward = function () {
    current_date.add('months',1);
    initchart();
};

var applyfilter = function () {
    var val = $('[name=filter]:checked').val();
    var att = "category";

    if(val === "category")
        att = "category";

    if(val === "sales")
        att = "sales";

    if(val === "manager")
        att = "manager";

    if(val === "status")
        att = "status";

    if(val === "warehouse")
        att = "warehouse";

    if(val === "unitsavailable")
        att = "unitsavailable";

    $.each($("#chart")[0].data, function (i, d) {
        if (att == "unitsavailable") {
            if (d.values[0].dataObj[att + "_text"] == 0
                || d.values[0].dataObj[att + "_text"] == ""
                || d.values[0].dataObj[att + "_text"] == null
            ) {
                $(d.bar[0]).css("background-color", "#ff9999");
            } else {
                $(d.bar[0]).css("background-color", "#99ff99");
            }
        } else {
            $(d.bar[0]).css("background-color", "#" + d.values[0].dataObj[att + "_color"]);
        }

        $(d.bar[0]).html(d.values[0].dataObj[att + "_text"]);
    });
};

var drawbackground = function (min,max) {
    console.log(min,max);

    var $dataPanel = $(".dataPanel");

    min = moment(min);

    $("#canvas").remove();

    var height = $dataPanel.css("height");
    var width = $dataPanel.css("width");

    $("body").append("<canvas id='canvas' style='position:absolute; display:none;'></canvas>");
    height = height.substring(0,height.length-2);
    width = width.substring(0,width.length-2);

    var can = $("#canvas")[0];
    can.height = height;
    can.width = width;

    c = $("#canvas")[0].getContext("2d");
    c.clearRect(0,0,width,height);
    c.stroke();
    c.fillStyle="#ffd263";

    for(var x=0; x < (width/24); x++) {
        if(min.day() == 0 || min.day() == 6)
            c.fillRect(x*24,0,24,height);
        c.moveTo((x*24)-0.5,0);
        c.lineTo((x*24)-0.5,height);
        min.add(1,"days");
    }

    for(var y=0; y < (height/24); y++) {
        c.moveTo(0,(y*24)-0.5);
        c.lineTo(width,(y*24)-0.5);
    }

    c.strokeStyle = "#999";
    c.lineWidth=1;
    c.stroke();

    $dataPanel.css("background","url('"+$("#canvas")[0].toDataURL("image/png")+"')");
};

var initchart = function (s) {
    if(typeof(s) == "undefined")
        s = App.createUrl("backend/getganttdata");

    dt = current_date.format("YYYY-MM-DD");
    $("#datetitle").html(current_date.format("MMMM YYYY"));
    s = s+"&fromdate="+dt;

    $("#chart").gantt({
        source: s,
        scale: "days",
        minscale: "weeks",
        maxscale: "months",
        itemsPerPage: 999,
        onItemClick: function (data) {
            console.log(data);

            $.ajax(
                data.url,
                {
                    success: function (res) {
                        $('.event-modal').data('event-id', data.id);

                        $(".modal-body").html(res);
                        $("#eventmodal").modal("show");

                        $('.event-form').eventForm();
                        $('.event-comment').eventComment();
                    }
                }
            );

            return false;
        },
        onRender: function () {
            $.ajax(
                App.createUrl("backend/getganttminmax") + "&fromdate=" + dt,
                {
                    type: "GET",
                    success: function (res) {
                        var obj = {};
                        var o = "obj=" + res;
                        eval(o);
                        drawbackground(obj.min, obj.max);
                    }
                }
            );

            applyfilter();
            makeheaderfloat();
        }
    });
};

var makeheaderfloat = function () {
    var totalheight = 0;

    var $chart = $('#chart');
    var $fnGantt = $("#fn-gantt");
    var $dataPanel = $(".dataPanel");
    var $leftPanel = $(".leftPanel");

    $(".dataPanel > .row").each(function(i,o){totalheight += $(o).height();});
    $(".dataPanel > .row").wrapAll("<div id='ganttheader'></div>");

    $("#ganttheader").clone().appendTo("body");
    $("body > #ganttheader").attr("id","ganttheader2");
    $("#ganttheader2").wrap("<div class='rightPanel' id='rightPanel'></div>");
    $("#rightPanel").wrap("<div class='fn-gantt' id='fn-gantt'></div>");

    $fnGantt.css("height",totalheight+"px");
    $fnGantt.hide();
    $("#fn-gantt > .rightPanel").css("overflow","hidden");
    $("#fn-gantt > .rightPanel").css("white-space","nowrap");
    $("#ganttheader2").css("width", $dataPanel.width());
    $fnGantt.addClass("navbar-fixed-top");

    $fnGantt.css("width",($chart.width() - $leftPanel.width())+"px");

    var off = $chart.offset();
    var off2 = $leftPanel.width();

    $fnGantt.css("left",off.left+off2);

    $dataPanel.on("scroll", function() {
        var l = $fnGantt.css("left");
        $fnGantt.css("left",l+$dataPanel.css("margin-left"));
    });

    panel = $chart.find(".dataPanel");

    $(document).on("scroll", function() {
        off = $("#ganttheader").offset();
        w = $("#ganttheader").width();
        h = $("#ganttheader").height();

        if(window.pageYOffset > off.top) {
            if(!$fnGantt.is(":visible")) {
                timer = setInterval(
                    function(){
                        a = $(panel).css("margin-left");
                        $("#ganttheader2").css("margin-left",a);
                    },
                    500
                );

                $fnGantt.show();
            }
        } else {
            if($fnGantt.is(":visible")) {
                clearInterval(timer);
                $fnGantt.hide();
            }
        }
    });
};

$('.chart__radio-filter').on('change', function () {
    applyfilter();
});

$('.chart__button-back').on('click', function () {
    back();
});

$('.chart__button-forward').on('click', function () {
    forward();
});

initchart();

});