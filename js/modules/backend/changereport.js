/**
 * Created by nikhil on 24-Aug-17.
 */
define(['jquery', 'app', 'vendor/moment','bootstrap-datepicker'], function($, App, moment) {
    $(".datepicker").datepicker({
        format: "yyyy-mm-dd"
    });
    $("#gen_btn").on("click",function(){
        _this = this;
        $(this).attr("disabled","disabled");
        manager = $("#manager").val();
        from_date = $("#fromdate").val();
        to_date = $("#todate").val();
        $.ajax({"url":App.createUrl("Backend/changereportresult"),
            "type":"POST",
            "data":{"manager":manager,"from_date":from_date,"to_date":to_date},
            "success":function(res){
                $(_this).removeAttr("disabled");
                $("#results").html(res);
                $("#downloadcsv").on("click",function(){
                    lines = [];
                    out = [];
                    $("#results > table > thead > tr > th").each(function (i, o) {
                        val = $(o).html();
                        out.push(val);
                    });
                    lines.push(out);
                    $("#results > table > tbody > tr").each(function (i, o) {
                        out = [];
                        $(o).find("td").each(function (ii, xx) {
                            out.push($(xx).html());
                        });
                        lines.push(out);
                    });
                    exportcsv.exportToCsv("changereport.csv", lines);
                });
            },"error":function(res){
                $(_this).removeAttr("disabled");
            }
        });
    });

});
function CSVEXPORT() {
    this.exportToCsv = function (filename, rows) {
        var processRow = function (row) {
            var finalVal = '';
            for (var j = 0; j < row.length; j++) {
                var innerValue = row[j] === null ? '' : row[j].toString();
                if (row[j] instanceof Date) {
                    innerValue = row[j].toLocaleString();
                }

                var result = innerValue.replace(/"/g, '""');
                if (result.search(/("|,|\n)/g) >= 0)
                    result = '"' + result + '"';
                if (j > 0)
                    finalVal += ',';
                finalVal += result;
            }
            return finalVal + '\n';
        };

        var csvFile = '';
        for (var i = 0; i < rows.length; i++) {
            csvFile += processRow(rows[i]);
        }

        var blob = new Blob([csvFile], {type: 'text/csv;charset=utf-8;'});
        if (navigator.msSaveBlob) { // IE 10+
            navigator.msSaveBlob(blob, filename);
        } else {
            var link = document.createElement("a");
            if (link.download !== undefined) { // feature detection
                // Browsers that support HTML5 download attribute
                var url = URL.createObjectURL(blob);
                link.setAttribute("href", url);
                link.setAttribute("download", filename);
                link.style.visibility = 'hidden';
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            }
        }
    }

}
exportcsv = new CSVEXPORT();