define(['jquery', 'app', 'vendor/moment'], function ($, App, moment) {
    $.fn.eventModal = function () {
        var self = this;

        var oldRevenue;
        var oldStart;
        var oldEnd;

        var closeDialog = function (event_id, ob, type) {
            var id = event_id;
            if (type === "delete") {
                for (var i = 0; i < ob.length; i++) {
                    // TODO need remove event
                    //delete_thisevent(ob[i]);
                }
            } else {
                // TODO need update event
                //update_event(id, ob);
            }

            $(self).modal("hide");
        };

        var addToQueue = function (url, successHandler, data, type) {
            $.ajax(
                url,
                {
                    data: data,
                    type: type,
                    success: successHandler
                }
            );
        };

        var eventSave = function (id, callback) {
            var rev = $("#Event_revprediction").val();
            var reason = "";
            var oldRevenue = $("#Event_revprediction").attr("data-revprediction");
            if(rev != oldRevenue && reason == "") {
                reason = prompt("You are changing the predicted revenue of this Event. Please provide a reason why you are changing it.","");
                if(reason == null)
                    return;

                while(reason.trim() == "") {
                    reason = prompt("You are changing the predicted revenue of this Event. Please provide a reason why you are changing it.","");
                    if(reason == null)
                        return;
                }
            }

            var url = App.createUrl("Event/Update") + "&id=" + id;
            var form = $("#event-form").serialize();
            form = form+"&reason="+encodeURI(reason);

            addToQueue(
                url,
                function (response) {
                    var obj = JSON.parse(response);

                    $('.form-group.has-errors').removeClass('.has-errors');
                    $('.help-block').html('');

                    if (obj.hasOwnProperty('errors')) {
                        for (var objectName in obj.errors) {
                            for (var propertyName in obj.errors[objectName]) {
                                var $formGroup = $('[name="' + objectName + '[' + propertyName + ']' + '"]').parent();
                                $formGroup.addClass('has-errors');

                                $('.help-block', $formGroup).html(obj.errors[objectName][propertyName]);
                            }
                        }
                    } else {
                        callback(id, obj, "update");
                    }
                },
                form,
                "POST"
            );
        };

        var eventDelete = function (id, callback) {
            var deleterepeat = 0;
            var checked = $("#Event_eventrepeat")[0].checked;

            if(checked) {
                var res = confirm("Please not you are deleting an event that is repeated. Do you want to continue?");
                if(res){
                    res = confirm("This event is a repeated event. When you delete you can make the repeating events independant. Or delete the repeating events too.\n Press Ok to Delete those repeating events too. Press Cancel to make the repeating events independant.");
                    if(res) {
                        deleterepeat = 1;
                    } else
                        deleterepeat = -1;
                } else {
                    return;
                }
            }

            var url = App.createUrl("event/delete") + "&ajax=1&id=" + id + "&deleterepeat=" + deleterepeat;
            var form = $("#event-form").serialize();
            form = form+"&reason=";
            addToQueue(
                url,
                function(res) {
                    res = "var obj="+res;
                    eval(res);
                    callback(id, obj, "delete");
                },
                form,
                "POST"
            );
        };

        var init = function () {
            oldStart = $("#Event_startdate").val();
            oldEnd = $("#Event_enddate").val();
            oldRevenue = $("#Event_revprediction").val();

            $('.event-modal__button-save').on('click', function () {
                var id = $('.event-modal').data('event-id');

                eventSave(id, closeDialog);
            });

            $('.event-modal__button-delete').on('click', function () {
                var id = $('.event-modal').data('event-id');

                eventDelete(id, closeDialog);
            });
        };

        init();
    };

    $('.event-modal').eventModal();
});
