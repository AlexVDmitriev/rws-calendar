/**
 * Created by nikhil on 05-Jan-18.
 */
define(['jquery', 'app', 'bootstrap-datepicker'], function($, App) {
    $.fn.mkrequest = function () {
        var init = function() {
            $("#saveall").on("click",function(res){
                $(".row-save-btn").click();
            });
            $(".row-save-btn").on("click",function(){
                id = $(this).attr("data-id");
                _this = this;
                final_data = {};
                $("[data-rowid="+id+"]").find("input").each(function(i,o){
                    if($(o).attr("type") == "checkbox" && o.checked){
                        final_data[o.name] = 1;
                    }
                    else if(o.type == "text" && o.value != ""){
                        final_data[o.name] = o.value;
                    }
                });
                final_data['id']=  id;
                console.log(final_data);
                $.ajax({url:App.createUrl('Event/mkreportdata'),"type":"POST","data":final_data,success:function(res){
                    if(res != "OK"){
                        alert("Opps something went wrong : "+res);
                    }
                    $(_this).html("<span class='fa fa-check'></span>");
                }});
            });
            $(".row-lock-btn").on("click",function(){
                id = $(this).attr("data-id");
                _this = this;
                if($(this).find(".fa-unlock").length > 0){
                    $.ajax({"url":App.createUrl("Event/mkreportlockrow"),"type":"POST","data":{id:id,"lock":1},success:function(res){
                        if(res == "OK") {
                            $("[data-rowid="+id+"]").addClass("lockedrow");
                            $("[data-rowid="+id+"]").find("input").each(function(i,o){
                                $(o).attr("disabled","disabled");
                            });
                            $(_this).html("<span class='fa fa-lock'></span>");
                        }
                    }});
                }
                else if($(this).find(".fa-lock").length > 0){
                    $.ajax({"url":App.createUrl("Event/mkreportlockrow"),"type":"POST","data":{id:id,"lock":0},success:function(res){
                        if(res == "OK") {
                            $("[data-rowid="+id+"]").removeClass("lockedrow");
                            $("[data-rowid="+id+"]").find("input").each(function(i,o){
                                $(o).removeAttr("disabled");
                            });
                            $("[data-rowid="+id+"]").find(".row-save-btn").removeAttr("disabled");
                            $(_this).html("<span class='fa fa-unlock'></span>");
                        }
                    }});
                }
            });
            $("#exportbtn").on("click",function(){
                str = "data:text/csv;charset=utf-8,";
                str += $("#heading").html()+"\n";
                headingline="";
                dt = $("#data_table");
                dt.find("thead").find("tr").find("th").each(function(i,o){
                    h = $(o).text();
                    if(h != "Action") {
                        if (h != "")
                            headingline += "\"" + h + "\",";
                        else
                            headingline += ",";
                    }
                });
                headingline += "\n";
                str += headingline;
                bodylines = "";
                dt.find("tbody").find("tr").each(function(i,row){
                    bodylines = "";
                    $(row).find("td").each(function(ii,td){
                        if($(td).find("input").length ==0 && $(td).find("button").length ==0) {
                            h = $(td).text();
                            if (h != "")
                                bodylines += '"' + h + "\",";
                            else
                                bodylines += ",";
                        }
                        else {
                            if($(td).find("button").length ==0) {
                                if ($(td).find("input").attr("type") == "text") {
                                    v = $(td).find("input").val();
                                    if (v != "")
                                        bodylines += '"' + v + '",';
                                    else
                                        bodylines += ',';
                                }
                                else if ($(td).find("input").attr("type") == "checkbox") {
                                    v = $(td).find("input")[0].checked;
                                    if (v == 1)
                                        bodylines += 'Yes,';
                                    else {
                                        bodylines += 'No,';
                                    }
                                }
                            }
                        }
                    });
                    bodylines += "\n";
                    str += bodylines;
                });
                console.log(str);
                var encodedUri = encodeURI(str);
                var link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "MK request doc.csv");
                document.body.appendChild(link); // Required for FF
                link.click();
            });
            $(".container2").on("scroll",function(e){
                topscroll = e.currentTarget.scrollTop;
                fh = $("#floating_head");
                if(topscroll > 0){
                    fh.html("<table><thead>"+$("#data_table").find("thead").html()+"</thead></table>");

                    fh.width($("#data_table").width());
                    cols = fh.find("table > thead > tr > th");
                    $("#data_table").find("thead > tr > th").each(function(i,o){
                        $(cols[i]).css("width",($(o).width()+16)+"px");
                        $(cols[i]).css("padding","8px");
                    });
                    fh.css("top",topscroll+"px");
                    fh.removeClass("hidden");
                }
                else {
                    fh.html("");
                    fh.addClass("hidden");
                }
            });
        };
        init();
    };
    var $mkForm = $('body');

    if ($mkForm.length) {
        $mkForm.mkrequest();
    }
});