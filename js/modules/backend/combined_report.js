/**
 * Created by nikhil
 */
/**
 * Created by nikhil
 */
define(['jquery', 'app', 'bootstrap-datepicker'], function($, App) {
    $.fn.vtrequest = function () {
        var init = function() {
            $("#exportbtn").on("click",function(){
                str = "data:text/csv;charset=utf-8,";
                str += $("#heading").html()+"\n";
                headrow = "";
                $("#data_table > thead > tr >th").each(function(i,o){
                    text = $(o).text();
                    headrow += text+",";
                });
                str += headrow + "\n";

                body = "";
                $("#data_table > tbody > tr").each(function(i,o){
                    $(o).find("td").each(function(ii,oo){
                        text = $(oo).text();
                        body += "\""+text+"\",";
                    });
                    body += "\n";
                });
                str += body;
                var encodedUri = encodeURI(str);
                var link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "VT request doc.csv");
                document.body.appendChild(link); // Required for FF
                link.click();
            });
        };
        init();
    };
    var $mkForm = $('body');

    if ($mkForm.length) {
        $mkForm.vtrequest();
    }
});
