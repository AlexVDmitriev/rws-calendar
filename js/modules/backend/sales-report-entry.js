define(['jquery', 'app', 'bootstrap-datepicker'], function($, App) {
    $('.event__manufacturer').on('change', function () {
        $.ajax({
            url: App.createUrl("Event/GetSuppDetails"),
            type: "GET",
            data: {mid: $("#brand").val(), cid: 'any'},
            success: function (res) {
                var obj = {};
                res = "obj=" + res;
                eval(res);
                if (typeof obj != "undefined") {
                    $("#supplier > option").each(function (i, o) {
                        $(o).remove();
                    });

                    $("#supplier").append("<option value=''>Any</option>");
                    for (var i = 0; i < obj.suppliers.length; i++) {
                        $("#supplier").append("<option value='" + obj.suppliers[i].id + "'>" + obj.suppliers[i].name + "</option>");
                    }
                }
            }
        });
    });

    $(".datepicker").datepicker({
        format: "yyyy-mm-dd"
    });
});
