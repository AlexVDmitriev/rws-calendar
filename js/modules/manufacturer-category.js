define(['jquery', 'app'], function($, App) {
    var refershList = function () {
        var v = $("#Manufacturer_id").val();

        $.ajax({
            url: App.createUrl("ManufacturerCategory/GetLists") + "&id=" + v,
            success: function (res) {
                if (res === "") {
                    return;
                }

                var obj = {};
                eval("obj=" + res);

                var $supplierList = $("#supplierlist");
                var $supplierListOption = $("#supplierlist > option");
                $supplierListOption.removeAttr("selected");

                $supplierList.multiselect("refresh");

                $supplierListOption.each(function (idx, val) {
                    for (var i = 0; i < obj.supplier.length; i++) {
                        $supplierList.multiselect("select", obj.supplier[i]);
                    }
                });
                $("#managerlist").val(obj.manager);
                $("#stock_type").val(obj.stocktype);
                $("#work_required").val(obj.work_required);
                var $categoryList = $("#categorylist");
                var $categoryListOption = $("#categorylist > option");
                $categoryListOption.removeAttr("selected");

                $categoryList.multiselect("refresh");

                $categoryListOption.each(function (idx, val) {
                    for (var i = 0; i < obj.category.length; i++) {
                        $categoryList.multiselect("select", obj.category[i]);
                    }
                });
            }
        });
    };

    $('document').ready(function () {
        $("#categorylist").multiselect({includeSelectAllOption: true, enableFiltering: true});
        $("#supplierlist").multiselect({includeSelectAllOption: true, enableFiltering: true});
    }).ready(function () {
        refershList();
    });

    $('.manufacturer__list').on('change', function () {
        refershList();
    });
});
