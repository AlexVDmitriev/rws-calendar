/**
 * Created by nikhil on 24-Apr-17.
 */

define(['jquery', 'app'], function ($, App) {
    function IssuesReports() {
        var self = this;

        this.openIssueDialog = function () {
            $.ajax({
                url: App.createUrl("IssueReports/index"),
                success: function (res) {
                    $("#issue_modal_body").html(res);
                    $("#issue_modal").modal("show");
                }
            });
        };

        this.saveIssue = function () {
            var title = $("#issue_title").val();
            var whathappened = $("#what_happened").val();
            var steps = $("#issue_steps").val();
            var expected_results = $("#issue_expected_result").val();

            if (title === "") {
                alert("Please enter a title to describe the issue");
                return;
            }
            if (whathappened === "") {
                alert("Please what happened when this error occured.");
                return;
            }
            var data = {
                "title": title,
                "steps": steps,
                "expected_results": expected_results,
                "what_happened": whathappened
            };
            var files = $("#screenshots")[0].files;
            var data1 = new FormData();

            $.each(files, function (key, value) {
                data1.append(key, value);
            });

            $.each(data, function (key, value) {
                data1.append(key, value);
            });

            $.ajax({
                url: App.createUrl("IssueReports/saveIssue"),
                data: data1,
                type: "POST",
                processData: false, // Don't process the files
                contentType: false,
                success: function (res) {
                    if (res === "OK") {
                        alert("The issue was submitted to the developers and will be solved soon. Thank you.");
                        $("#issue_modal").modal("hide");
                    }
                    else {
                        alert(res);
                    }
                }
            });
        };

        $("#submit_report").on('click', self.saveIssue);
        $("#issue_report_icon").on('click', self.openIssueDialog);
    }

    return new IssuesReports();
});
