define(['jquery', 'app', 'bootstrap-datepicker'], function($, App) {

    $.fn.eventForm = function () {
        var isfirsttrigger = false;

        var isNewModel = false;

        var adjustSelect = function () {
            var $repeatType = $(".event__repeat__type");

            if ($repeatType.val() == 1) {
                $('.event__repeat-items').html('Months');
            }

            if ($repeatType.val() == 2) {
                $('.event__repeat-items').html('Years');
            }

            if ($repeatType.val() == 3) {
                $('.event__repeat-items').html('Weeks');
            }
        };

        var getManuDetails = function () {
            $.ajax({
                url: App.createUrl("Event/GetManuDetails"),
                type: "GET",
                data: {mid: $("#manu").val()},
                success: function (res) {
                    var obj = {};
                    res = "obj =" + res;
                    eval(res);
                    if (typeof obj != "undefined") {
                        if (!isfirsttrigger) {
                            $("#Event_manager").val(obj.mkt_manager);
                            $("#EventMarketingDetails_manager").val(obj.mkt_manager);
                            $("#Event_stocktype").val(obj.stocktype);
                            $("#EventMarketingDetails_work_required").val(obj.work_required);
                            syncWithMatketing();
                        }
                        else {
                            isfirsttrigger = false;
                        }

                        $("#cid > option").each(function (i, o) {
                            $(o).remove();
                        });

                        for (var i = 0; i < obj.category.length; i++) {
                            $("#cid").append("<option value='" + obj.category[i].id + "'>" + obj.category[i].name + "</option>");
                        }

                        if (!isNewModel) {
                            var $category = $("#cid");
                            $category.val($category.data('value'));
                        }

                        getSuppDetails();
                    }
                }
            });
        };

        var getSuppDetails = function () {
            $.ajax({
                url: App.createUrl("Event/GetSuppDetails"),
                type: "GET",
                data: {
                    mid: $("#manu").val(),
                    cid: $("#cid").val()
                },
                success: function (res) {
                    var obj;
                    res = "obj =" + res;
                    eval(res);
                    if (typeof obj != "undefined") {
                        $("#Event_supplierid > option").each(function (i, o) {
                            $(o).remove();
                        });

                        console.log(obj);
                        for (var i = 0; i < obj.suppliers.length; i++) {
                            $("#Event_supplierid").append("<option value='" + obj.suppliers[i].id + "'>" + obj.suppliers[i].name + "</option>");
                        }

                        if (!isNewModel) {
                            var $supplierId = $("#Event_supplierid");
                            $supplierId.val($supplierId.data('value'));
                        }
                    }
                }
            });
        };
        var syncWithMatketing = function(){
            $("#EventMarketingDetails_brand").val($("#manu").val());
            $("#EventMarketingDetails_start_datetime").val($("#startdate").val());
            $("#EventMarketingDetails_start_date").val($("#startdate").val());
            $("#EventMarketingDetails_end_datetime").val($("#enddate").val());
            $("#EventMarketingDetails_manager").val($("#Event_manager").val());

        };
        var updateSellThrough = function () {
            var units = $("[name *= totunits]").val();
            var soldUnits = $("[name *= actualunitsales]").val();
            try {
                units = parseInt(units);
                soldUnits = parseInt(soldUnits);
                var sellThrough = (soldUnits / units) * 100;
                $("[name *= actualsellthrough]").val(Math.round(sellThrough));
            } catch (e) {
            }
        };

        var init = function() {
            $(".datepicker").datepicker({
                "format":"yyyy-mm-dd",
                zIndexOffset:9999
            });
            $('.event__repeat__date.datepicker').datepicker({
                format: "yyyy-mm-dd",
                zIndexOffset: 9999
            });

            $('.checkboxer').bootstrapSwitch();

            $('.event__repeat__switch').on('switchChange.bootstrapSwitch', function () {
                $("#repeatpanel").toggleClass("hidden");
            });

            $('.event__repeat__type').on('change', function () {
                adjustSelect();
            });

            // $('.event__repeat__date').on('change', function () {
            //     $("#fromdate").val($("#enddate").val());
            //     $("#todate").val($("#enddate").val());
            // });

            $('.event__manufacturer').on('change', function () {
                getManuDetails();
            });

            $('.event__category').on('change', function () {
                getSuppDetails();
            });

            $('.event__rev-prediction, .event__actual-revenue').on('change', function () {
                var rev = $("[name *= revprediction]").val();
                var actrev = $("[name *= actualrevenue]").val();
                if (rev > 0) {
                    var pct = ((actrev - rev) / rev) * 100;
                    $("#Event_predvsact").val(Math.round(pct));
                } else {
                    $("#Event_predvsact").val(0);
                }

                for (var i = 0; i < revlist.length; i++) {
                    if (parseFloat(revlist[i].from) <= parseFloat(rev) && parseFloat(revlist[i].to) >= parseFloat(rev)) {
                        $("[name *= salesid]").val(revlist[i].salesid);
                        break;
                    }
                }
            });
            $("#startdate").on("change",function(){
                syncWithMatketing();
            });
            $("#manu").on("change",function(){
                syncWithMatketing();
            });
            $("#enddate").on("change",function(){
                syncWithMatketing();
            });
            $("#Event_manager").on("change",function(){
                syncWithMatketing();
            });
            $("#EventMarketingDetails_work_required").on("change",function(){
                v = $("#EventMarketingDetails_work_required").val();
                if(v == "1" || v == "3"){
                    $("#EventMarketingDetails_sample_type").val('');
                }
            });
            $("#EventMarketingDetails_manager").on("change",function(){
                $("#Event_manager").val($("#EventMarketingDetails_manager").val());
            });

            $('.event__tot_units, .event__actual-unit-sales').on('keyup', function () {
                updateSellThrough();
            });

            /*$('.btn-group > button').on('click', function () {
                var sn = $(this).attr('data-sn');
                var state = $(this).attr('data-state');
                $('.btn-group[data-sn=' + sn + '] button').removeClass('active');
                $(this).addClass('active');
                $('[name=event-task-' + sn + ']').val(state);
            });*/

            $("#vt_save").on("click",function(e){
                e.preventDefault();
                id = $(".event-comment__button-add").attr("data-id");
                dat = $("#vkaccordian").find("[name]").serialize();
                dat += "&eventid="+id;
                $.ajax({"url":App.createUrl("Event/savevtdata"),data:dat,"type":"POST","success":function(res){

                }})
            });
            $("#mk_save").on("click",function(e){
                e.preventDefault();
                id = $(".event-comment__button-add").attr("data-id");
                dat = $("#mkaccordian").find("[name]").serialize();
                dat += "&eventid="+id;
                $.ajax({"url":App.createUrl("Event/savemkdata"),data:dat,"type":"POST","success":function(res){

                }})
            });
            isNewModel = $('#event-form').data("is-new");
            if(!isNewModel)
                isfirsttrigger = true;
            else
                isfirsttrigger = false;

            adjustSelect();
            getManuDetails();

            if (!isNewModel) {
                updateSellThrough();
            }
        };

        init();
    };

    var $eventForm = $('.event-form');

    if ($eventForm.length) {
        $eventForm.eventForm();
    }
});
