define(['jquery', 'app'], function ($, App) {
    $.fn.eventComment = function() {

        var self = {

            init: function () {
                $('.event-comment__body').on('click', '.event-comment__button-delete', function () {
                    console.log('delete comment');

                    var $button = $(this);
                    self.delete($button.data('id'));
                });

                $('.event-comment__button-add').on('click', function () {
                    console.log('add comment');

                    var $button = $(this);
                    self.add($button.data('id'));
                });
            },

            delete: function (id) {
                if (id == "")
                    return;
                var url = App.createUrl("event/deletecomment") + "&ajax=1&id=" + id;

                $.ajax(
                    url,
                    {
                        async: 1,
                        success: function (res) {
                            if (res == "SUCCESS") {
                                $("#comment" + id).remove();
                            } else {
                                alert("Error Occured : " + res);
                            }
                        }
                    }
                );
            },

            add: function (event_id) {
                var url = App.createUrl("event/addcomment");
                $.ajax(
                    url,
                    {
                        "type":"POST",
                        data: {
                            "content":$(".event-comment__message").val(),
                            "id":event_id
                        },
                        async: 1,
                        success: function (res) {
                            try {
                                var r = {};
                                res = "r = " + res;
                                eval(res);
                                var out = "<div class='panel panel-default' id='comment" + r.id + "'>";
                                out += "<div class='panel-body'>";
                                out += "<div class='col-xs-10'><b>" + r.username + "</b></div>";
                                if (App.user.isAdmin)
                                    out += "<div class='col-xs-2'>" +
                                        "<button data-id='" + r.id + "' type='button' class='btn btn-danger event-comment__button-delete'>" +
                                        "<div class='fa fa-times'></div>" +
                                        "</button>" +
                                        "</div>" +
                                        "<br />" +
                                        "<hr />";
                                out += $(".event-comment__message").val() + "<hr>";
                                out += "<div class='col-xs-offset-8'>" +
                                    "<i>" +
                                    "<small>" + r.datetime + "</small>" +
                                    "</i>" +
                                    "</div>" +
                                    "</div></div>";

                                $(".event-comment__body").append(out);
                                $(".event-comment__message").val("");
                            } catch (e) {
                                alert("Unknown error : " + e.toString() + "\n " + res);
                            }
                        }
                    }
                );
            }
        };

        self.init();
    };

    $('.event-comment').eventComment();
});
