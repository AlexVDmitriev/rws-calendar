define(['jquery'], function($) {
    $('.event__admin__filter-button').on('click', function () {
        var man = $("#manager").val();
        var month = $("#month").val();
        var year = $("#year").val();

        var loc = document.location.href;
        var arr = loc.split("?");
        var cleanurl = arr[0];
        var a = arr[1];
        var b = a.split("&");
        for (var i = 0; i < b.length; i++) {
            var cc = b[i].split("=");
            if (cc[0] == "manager") {
                b[i] = "manager=" + man;
                man = "";
            }
            if (cc[0] == "month") {
                b[i] = "month=" + month;
                month = "";
            }
            if (cc[0] == "year") {
                b[i] = "year=" + year;
                year = "";
            }
        }

        var u = "";
        for (i = 0; i < b.length; i++) {
            if (b[i] != "")
                u += b[i] + "&";
        }
        if (man != "") {
            u += "&manager=" + man;
        }
        if (month != "") {
            u += "&month=" + month;
        }
        if (year != "") {
            u += "&year=" + year;
        }

        loc = cleanurl + "?" + u;
        document.location.href = loc;
    });
});
