define(['jquery', 'app'], function($, App) {
    $('.competition-entries__button-save-result').on('click', function () {
        var compitionEntry = $(this).data('competition-id');

        var units = $("#soldamount" + compitionEntry).val();
        if (units == "") {
            alert("Please enter the estimated units sale");
            return;
        }

        var amount = $("#revenueamount" + compitionEntry).val();
        if (amount == "") {
            alert("Please enter the predicted revenue amount");
            return;
        }

        $.ajax({
            type: "POST",
            url: App.createUrl("CompetitionEntries/SaveEntry"),
            data: {
                revenueamount: amount,
                soldamount: units,
                compid: compitionEntry
            },
            success: function (res) {
                if (res == "OK")
                    alert("Entry Saved Successfully!");
                else
                    alert(res);
            }
        });
    });

    $('.competition-entries__button-apply-filter').on('click', function () {
        var fdate = $("#fromdate").val();
        var tdate = $("#todate").val();
        var buyer = $("#buyer").val();
        var eventid = $("#eventid").val();
        document.location.href = App.createUrl("CompetitionEntries/Index") + "&fdate=" + fdate + "&tdate=" + tdate + "&buyer=" + buyer + "&eventid=" + eventid;
    });
});
