define(['jquery', 'app'], function($, App) {
    $('.update-before__button-change-buyer').on('click', function () {
        var buyer = $("#buyer").val();
        var month = $("#month").val();
        var year = $("#year").val();

        document.location.href = App.createUrl("CompetitionEntries/UpdateBefore") + '&buyer=' + buyer + "&month=" + month + "&year=" + year;
    });
});

