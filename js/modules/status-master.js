define(['jquery', 'app'], function($, App) {
    var selected = null;
    var showconfirm = true;

    $("#status-master-grid a.delete").on('click', function () {
        if (showconfirm && !confirm('Are you sure you want to delete this item?')) {
            return false;
        }

        if (!showconfirm) {
            showconfirm = true;
        }

        selected = this;

        var $statusMasterGrid = $('#status-master-grid');

        $statusMasterGrid.yiiGridView('update', {
            type: 'POST',
            url: $(this).attr('href'),
            success: function (data) {
                if (data == "") {
                    $statusMasterGrid.yiiGridView('update');
                    return;
                }

                var res = {};
                var r = "res = " + data;
                try {
                    eval(r);
                    if (res.status == "changeid") {
                        var h = "";
                        for (var i = 0; i < res.list.length; i++)
                            h += "<option value='" + res.list[i].id + "'>" + res.list[i].descpn + "</option>";
                        console.log(h);
                        $("#changeidlist").html(h);
                        $("#oristatus").val(res.id);
                        $("#changeid").modal("show");
                    }
                } catch (e) {
                    alert("Unknown Error Occured : " + e.toString() + "\n Response :" + data)
                }
            }
        });
        return false;
    });

    $('.status-master__modal__delete-button').on('click', function () {
        $.ajax(
            App.createUrl("StatusMaster/UpdateEvents") + "&oristatus=" + $("#oristatus").val() + "&newstatus=" + $("#changeidlist").val(),
            {
                async: true,
                success: function (res) {
                    if (res !== "SUCCESS") {
                        alert("Error While Updating status : " + res);
                    }

                    $("#changeid").modal("hide");
                    showconfirm = false;
                    $(selected).click();
                }
            }
        );
    });
});
