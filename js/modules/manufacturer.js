define(['jquery'], function($) {
    var updateDoingBusiness = function () {
        var $terminationDate = $("#Manufacturer_terminationdate");
        if ($("#doingbusiness").is(":checked")) {
            $terminationDate.val("").hide();
        } else {
            if ($terminationDate.val() == "") {
                $terminationDate.val(currentDate).show();
            }

        }
    };

    $('document').ready(function () {
        updateDoingBusiness();
    });

    $('#doingbusiness').on('click', function () {
        updateDoingBusiness();
    });
});
