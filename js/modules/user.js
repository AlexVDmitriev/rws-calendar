define(['jquery', 'app','bootstrap-multiselect'], function ($, App,multiselect) {
    $('.user__button-reset-password').on('click', function () {
        var id = $(this).data('id');

        $.ajax({
            url: App.createUrl('user/resetpassword') + "&id=" + id,
            success: function (res) {
                alert(res);
            }
        });
    });
    $(".multiselect-control").multiselect();
});
