$(function () {
    $('document').on('ready', function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').tooltip();
    });
});
