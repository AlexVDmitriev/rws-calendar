define(['jquery', 'vendor/colorpicker'], function ($, ColorPicker) {
    $.fn.ColorPicker = ColorPicker;

    $('document').ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').tooltip();

        $('.mycolorpicker').ColorPicker({
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).val(hex);
                $(el).ColorPickerHide();
            },
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            }
        }).bind('keyup', function () {
            $(this).ColorPickerSetColor(this.value);
        });
    });
});
