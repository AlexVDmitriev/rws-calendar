requirejs.config({
    urlArgs: 'bust=' + (new Date()).getTime(),
    baseUrl: '/js/',
    paths: {
        'app': 'app',

        'jquery': 'vendors/jquery-1.10.2',
        'moment': 'cal3/lib/moment.min',

        'bootstrap': 'vendors/bootstrap-3.1.1.min',
        'bootstrap-datepicker': 'vendors/bootstrap-datepicker-1.7.1.min',
        'bootstrap-multiselect': 'vendors/bootstrap-multiselect',
        'bootstrap-switch': 'vendors/bootstrap-switch-3.0.0.min',
        'bootstrap-modal': 'vendors/bootstrap-modal-3.1.1',

        'vendor/colorpicker': 'vendors/colorpicker',
        'vendor/numeral': 'vendors/numeral-1.4.5.min',
        'vendor/moment': 'vendors/moment-2.17.0.min',
        'vendor/fullcalendar': 'fullcalendar2',

        'layouts/backend': 'layouts/backend'
    },
    waitSeconds: 0,
    shim: {
        'app': {
            deps: ['jquery'],
            exports: 'App'
        },
        'bootstrap': {
            deps: ['jquery']
        },
        'bootstrap-datepicker' : {
            deps: ['bootstrap']
        },
        'bootstrap-multiselect': {
            deps: ['bootstrap']
        },
        'bootstrap-switch': {
            deps: ['bootstrap']
        },
        'bootstrap-modal': {
            deps: ['bootstrap']
        },
        'vendor/colorpicker': {
            deps: ['jquery'],
            exports: "jQuery.fn.ColorPicker",
            init: function ($) {
                return $.fn.ColorPicker;
            }
        },
        'layouts/backend': {
            deps: [
                'jquery',
                'bootstrap',
                'vendor/colorpicker',
                'modules/issue-report'
            ]
        },
        'modules/backend/brand-report': {
            deps: ['jquery']
        },
        'modules/backend/event-log': {
            deps: ['jquery', 'app']
        },
        'modules/backend/gantt': {
            deps: [
                'jquery',
                'app',
                'bootstrap-modal',
                'vendors/jquery.fn.gantt',
                'vendors/moment-2.7.0.min',
                'modules/backend/modal',
                'modules/event/_form',
                'modules/event/comment'
            ]
        },
        'modules/backend/modal': {
            deps: [
                'jquery',
                'app',
                'vendor/moment'
            ]
        },
        'modules/backend/new-index': {
            deps: [
                'jquery',
                'app',
                'bootstrap-modal',
                'vendor/fullcalendar',
                'vendor/moment',
                'vendor/numeral',
                'modules/backend/modal',
                'modules/event/_form',
                'modules/event/comment'
            ]
        },
        'modules/backend/revenue-prediction': {
            deps: [
                'jquery',
                'app',
                'vendor/moment',
                'vendor/numeral'
            ]
        },
        'modules/backend/sales-report-entry': {
            deps: ['jquery', 'app']
        },
        'modules/backend/table-index': {
            deps: ['jquery', 'app']
        },
        'modules/competition-entries/index': {
            deps: ['jquery', 'app']
        },
        'modules/competition-entries/update-after': {
            deps: ['jquery', 'app']
        },
        'modules/competition-entries/update-before': {
            deps: ['jquery', 'app']
        },
        'modules/event/admin' : {
            deps: ['jquery']
        },
        'modules/event/comment' : {
            deps: ['jquery', 'app']
        },
        'modules/event/_form' : {
            deps: [
                'jquery',
                'app',
                'bootstrap-switch',
                'bootstrap-datepicker',
                'modules/event/comment'
            ]
        },
        'modules/issue-report': {
            deps: ['jquery', 'app']
        },
        'modules/user': {
            deps: ['jquery', 'app']
        },
        'modules/status-master' : {
            deps: ['jquery']
        },
        'modules/manufacturer': {
            deps: ['jquery']
        },
        'modules/manufacturer-category' : {
            deps: [
                'jquery',
                'app',
                'bootstrap-multiselect'
            ]
        },
        'cal3/lib/moment.min': {
            exports: 'moment'
        },
        'vendor/fullcalendar': {
            deps: [
                'jquery',
                'moment'
            ]
        }
    }
});
