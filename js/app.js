define(["jquery"], function ($) {
    var App = {
        createUrl: function (route) {
            return '/index.php?r=' + route;
        }
    };

    window.App = App;

    return App;
});
