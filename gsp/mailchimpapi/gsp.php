<?php
include_once "mailchimp.php";

/*Break the username up into seperate variables for First Name and Last Name*/

$temp = explode(" ", $_POST['FNAME']);
$FNAME = $temp[0];
$LNAME = $temp[1];
for($a=2;$a<count($temp);$a++){
	$LNAME .= " " . $temp[$a];
}
	

$MailChimp = new MailChimp('157f59f4cbce16fd8c5ed3ad743d56da-us5');
$result = $MailChimp->call('lists/subscribe', array(
                'id'                => '5aa68b246c',
                'email'             => array('email'=>$_POST['EMAIL']),
                'merge_vars'        => array('FNAME'=>$FNAME, 'LNAME'=>$LNAME, 'MMERGE1'=>$_POST['MMERGE1']),
                'double_optin'      => false,
                'update_existing'   => true,
                'replace_interests' => false,
                'send_welcome'      => false,
            ));
?>

<html>
	<head>
		<title>Thank You!</title>
		<!-- Google Code for Sign-up-GSP Conversion Page -->
		
			<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1002865705/?label=p_TqCOuNhF0QqYia3gM&amp;guid=ON&amp;script=0"/>
			</div>
		</noscript>
	</head>
	<body>
	<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 1002865705;
			var google_conversion_language = "en";
			var google_conversion_format = "3";
			var google_conversion_color = "ffffff";
			var google_conversion_label = "p_TqCOuNhF0QqYia3gM";
			var google_remarketing_only = false;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style='width:558px;display:block;margin:0 auto;'>
			<a href='http://www.runwaysale.co.za'>
				<img src='../images/logo.png'>
				<img src='../images/banner01.png'>
			</a>
				<div style='width:50%;display:block;margin:0 auto;font-family:Open Sans, sans-serif;'>
					<p style='font-size:16px;margin:100px 0 20px;text-align:center;'>Thank you for subscribing!</p>
					<a style='text-decoration:none' href='http://www.runwaysale.co.za'><p style='letter-spacing:1px;width:100%;background-color:#F04F33;color:#FFF;text-align:center;font-size:18px;padding-top:5px;padding-bottom:5px;margin-bottom:150px'>START SHOPPING NOW</p></a>
				</div>
			</form>
			<img src='../images/footer.png'>
		</div>
	</body>
</html>
