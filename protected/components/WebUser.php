<?php

/**
 * Class WebUser
 */
class WebUser extends CWebUser
{
    /** @var User */
    private $model = null;

    public function getModel()
    {
        if (!isset($this->id)) {
            $this->model = new User;
        }

        if ($this->model === null) {
            $this->model = User::model()->findByPk($this->id);
        }

        return $this->model;
    }

	public function isAdmin()
	{
	    /** @var User $user */
		$user = User::model()->findByPk($this->id);
		
		if($user=== NULL) {
			echo "failed to find user ".$this->id;
			return false;
		}

        if ($user->has_admin_permission) {
            return true;
        }

        return false;
	}

	protected function beforeLogin($id,$states,$fromcookie)
	{
        if ($fromcookie) {

            // check permissions for VIP mode
            $attributes = array(
                "userid"     => $id,
                "logintoken" => $states['randomStr']
            );

            if (Yii::app()->params['isVip']) {
                $attributes['has_vip_permission'] = true;
            }

            /** @var User $user */
            $user = User::model()->findByAttributes($attributes);

			if ($user === false) {
                die("Returning false for $id states");
            } else {
                return true;
            }
		}

		return true;
	}
	
    public function __get($name) {
        try {
            return parent::__get($name);
        } catch (CException $e) {
            $m = $this->getModel();
            if($m->__isset($name))
                return $m->{$name};
            else throw $e;
        }
    }
 
    public function __set($name, $value) {
        try {
            return parent::__set($name, $value);
        } catch (CException $e) {
            $m = $this->getModel();
            $m->{$name} = $value;
        }

        return null;
    }
 
    public function __call($name, $parameters) {
        try {
            return parent::__call($name, $parameters);  
        } catch (CException $e) {
            $m = $this->getModel();
            return call_user_func_array(array($m,$name), $parameters);
        }
    }
	
}
