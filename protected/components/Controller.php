<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view.
     */
    public $layout = '//layouts/backend';

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    protected $js = array(
        "vendors/requirejs-2.3.3.min.js",
        "main.js",
    );

    public $requireJsModules = array(
        'app',
        'layouts/backend'
    );

    protected $css = array(
        "bootstrap.min.css",
        "cupertino/jquery-ui.custom.min.css",
        "colorpicker.css",
        "layouts/backend.css",
    );

    /**
     * @param string $cssFile
     */
    public function registerCssFile($cssFile)
    {
        /** @var CWebApplication $app */
        $app = Yii::app();

        $app->clientScript->registerCssFile("/css/{$cssFile}");
    }

    /**
     * @param string $jsModule
     */
    public function registerJsModule($jsModule)
    {
        $this->requireJsModules[] = $jsModule;
    }

    public function init()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();

        header('X-User: ' . ($app->user->isGuest ? 'guest' : $app->user->getId()));

        foreach ($this->css as $cssFile) {
            $app->clientScript->registerCssFile("/css/{$cssFile}");
        }

        foreach ($this->js as $jsFile) {
            $app->clientScript->registerScriptFile("/js/{$jsFile}");
        }

        parent::init();
    }

    public function defineRequireJs()
    {
        $jsonModules = json_encode($this->requireJsModules);

        $isAdmin = (int) Yii::app()->user->isAdmin();

        $script = <<<JS
        require(
            {$jsonModules},
            function (App) {
                console.log('main');
                
                App.user = {
                    isAdmin: {$isAdmin}
                };
            }
        );
JS;
        /** @var CWebApplication $app */
        $app = Yii::app();

        $app->clientScript->registerScript('main', $script, CClientScript::POS_END);
    }

    public function allowOnlyIsVip() {
        if(!Yii::app()->params['isVip']) {
            return true;
        }

        return Yii::app()->user->has_vip_permission;
    }
}
