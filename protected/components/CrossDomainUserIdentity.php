<?php

/**
 * Class CrossDomainUserIdentity
 */
class CrossDomainUserIdentity extends CUserIdentity
{
    /**
     * Authenticates a user.
     * @return boolean whether authentication succeeds.
     */
    private $userid;

    public function loadById($id)
    {
        /** @var User $user */
        $user = User::model()->findByPk(
            $id,
            'has_vip_permission = :application_isVip_param',
            [':application_isVip_param' => (int)Yii::app()->params['isVip']]
        );

        if ($user === null) {
            return;
        }

        $this->userid = $user->userid;
    }

    public function authenticate()
    {
        /** @var User $user */
        $user = User::model()->findByAttributes(array("userid" => $this->username, 'active' => 1));

        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } elseif ($user->logintoken !== $this->password
            || (Yii::app()->params['isVip'] && !$user->has_vip_permission)
        ) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->errorCode = self::ERROR_NONE;

            $this->setState("randomStr", $this->password);
            $this->setState("username", $user->username);
            $this->setState("fullname", $user->fullname);

            $this->userid = $user->userid;
        }

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->userid;
    }
}
