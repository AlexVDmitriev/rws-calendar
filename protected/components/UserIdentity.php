<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	private $userid;

	public function loadById($id)
	{
        /** @var User $user */
        $user = User::model()->findByPk($id, 'has_vip_permission = :application_isVip_param',[':application_isVip_param' => (int)Yii::app()->params['isVip']]);

        if ($user === null) {
            return;
        }
		
		$this->userid = $user->userid;
	}

	public function authenticate()
	{
	    /** @var User $user */
		$user = User::model()->findByAttributes(array("username"=>$this->username,'active'=>1));

        if ($user === null)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        elseif ($user->password !== crypt($this->password, '12345')
                || (Yii::app()->params['isVip'] && !$user->has_vip_permission)
        )
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->errorCode = self::ERROR_NONE;
            //$this->setState("userid",$user->userid);
            $str = $this->generateRandomString();
            $this->setState("randomStr", $str);
            $this->setState("username", $user->username);
            $this->setState("fullname", $user->fullname);
            $user->logintoken = $str;
            $user->save();
            $this->userid = $user->userid;
        }

		return !$this->errorCode;
	}

	private function generateRandomString($length = 10) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';

		for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

		return $randomString;
	}

	public function getId()
	{
		return $this->userid;
	}
}
