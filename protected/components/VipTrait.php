<?php

/**
 * Trait VipEventTrait
 */
trait VipTrait
{
    public function find($condition = '', $params = array())
    {
        if (is_string($condition)) {
            $condition .= empty($condition) ? ' isVip = :application_is_vip_param' : ' AND isVip = :application_is_vip_param';
            $params[':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        } elseif (is_array($condition)) {
            $condition['condition'] = empty($condition['condition'])
                ? ' isVip = :application_is_vip_param'
                : $condition['condition'] . ' AND isVip = :application_is_vip_param';
            $condition['params'] = empty($condition['params']) ? [] : $condition['params'];
            $condition['params'][':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        } elseif ($condition instanceof CDbCriteria) {
            $condition->addCondition('isVip = :application_is_vip_param');
            $condition->params[':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        }

        return parent::find($condition, $params);
    }

    public function findAll($condition = '', $params = array())
    {
        if (is_string($condition)) {
            $condition .= empty($condition) ? ' isVip = :application_is_vip_param' : ' AND isVip = :application_is_vip_param';
            $params[':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        } elseif (is_array($condition)) {
            $condition['condition'] = empty($condition['condition'])
                ? ' isVip = :application_is_vip_param'
                : $condition['condition'] . ' AND isVip = :application_is_vip_param';
            $condition['params'] = empty($condition['params']) ? [] : $condition['params'];
            $condition['params'][':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        } elseif ($condition instanceof CDbCriteria) {
            $condition->addCondition('isVip = :application_is_vip_param');
            $condition->params[':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        }

        return parent::findAll($condition, $params);
    }

    public function findByPk($pk, $condition = '', $params = array())
    {
        if (is_string($condition)) {
            $condition .= empty($condition) ? ' isVip = :application_is_vip_param' : ' AND isVip = :application_is_vip_param';
            $params[':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        } elseif (is_array($condition)) {
            $condition['condition'] = empty($condition['condition'])
                ? ' isVip = :application_is_vip_param'
                : $condition['condition'] . ' AND isVip = :application_is_vip_param';
            $condition['params'] = empty($condition['params']) ? [] : $condition['params'];
            $condition['params'][':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        } elseif ($condition instanceof CDbCriteria) {
            $condition->addCondition('isVip = :application_is_vip_param');
            $condition->params[':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        }

        return parent::findByPk($pk, $condition, $params);
    }

    public function findAllByPk($pk, $condition = '', $params = array())
    {
        if (is_string($condition)) {
            $condition .= empty($condition) ? ' isVip = :application_is_vip_param' : ' AND isVip = :application_is_vip_param';
            $params[':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        } elseif (is_array($condition)) {
            $condition['condition'] = empty($condition['condition'])
                ? ' isVip = :application_is_vip_param'
                : $condition['condition'] . ' AND isVip = :application_is_vip_param';
            $condition['params'] = empty($condition['params']) ? [] : $condition['params'];
            $condition['params'][':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        } elseif ($condition instanceof CDbCriteria) {
            $condition->addCondition('isVip = :application_is_vip_param');
            $condition->params[':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        }

        return parent::findAllByPk($pk, $condition, $params);
    }

    public function findByAttributes($attributes, $condition = '', $params = array())
    {
        $attributes['isVip'] = (int)Yii::app()->params['isVip'];

        return parent::findByAttributes($attributes, $condition, $params);
    }

    public function findAllByAttributes($attributes, $condition = '', $params = array())
    {
        $attributes['isVip'] = (int)Yii::app()->params['isVip'];

        return parent::findAllByAttributes($attributes, $condition, $params);
    }

    public function count($condition = '', $params = array())
    {
        if (is_string($condition)) {
            $condition .= empty($condition) ? ' isVip = :application_is_vip_param' : ' AND isVip = :application_is_vip_param';
            $params[':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        } elseif (is_array($condition)) {
            $condition['condition'] = empty($condition['condition'])
                ? ' isVip = :application_is_vip_param'
                : $condition['condition'] . ' AND isVip = :application_is_vip_param';
            $condition['params'] = empty($condition['params']) ? [] : $condition['params'];
            $condition['params'][':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        } elseif ($condition instanceof CDbCriteria) {
            $condition->addCondition('isVip = :application_is_vip_param');
            $condition->params[':application_is_vip_param'] = (int)Yii::app()->params['isVip'];
        }

        return parent::count($condition, $params);
    }

    public function countByAttributes($attributes, $condition = '', $params = array())
    {
        $attributes['isVip'] = (int)Yii::app()->params['isVip'];

        return parent::countByAttributes($attributes, $condition, $params);
    }
}
