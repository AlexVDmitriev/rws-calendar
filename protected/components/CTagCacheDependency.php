<?php

/**
 * CTagCacheDependency class.
 *
 * CTagCacheDependency represents a dependency based on a autoincrement(timestamp) of tags
 *
 * @author Roman <astronin@gmail.com>
 * @since 1.0
 *
 * @changed by Ruslan Korobkin
 * @link https://habrahabr.ru/post/140414/
 */
class CTagCacheDependency extends CCacheDependency
{
    /**
     * autoincrement(timestamp) param is used to check if the dependency has been changed.
     * @var integer
     */
    public $tag;
    /**
     * Cache component, by default used - cache
     * @var CCache
     */
    public $cache;
    /**
     * Name of cache component, by default used - cache
     * @var string
     */
    public $cacheName;

    /**
     * Constructor.
     *
     * @param string $tag value of the tag for module
     * @param string $cacheName name of cache component
     */
    public function __construct($tag = null, $cacheName = 'cache')
    {
        $this->tag = $tag . "_" . (int)Yii::app()->params['isVip'];
        $this->cacheName = $cacheName;
    }

    /**
     * Generates the data needed to determine if dependency has been changed.
     * This method returns the integer(timestamp).
     * @return mixed the data needed to determine if dependency has been changed.
     */
    protected function generateDependentData()
    {
        if ($this->tag !== null) {
            $this->cache = Yii::app()->getComponent($this->cacheName);
            $t           = $this->cache->get($this->tag);
            if ($t === false) {
                $t = microtime(true);
                $this->cache->set($this->tag, $t);
            }

            return $t;
        }

        return null;
    }

    /**
     * @param string $tag
     */
    public static function reset($tag)
    {
        Yii::app()->getCache()->set(
            $tag . "_" . (int)Yii::app()->params['isVip'],
            microtime(true),
            0
        );
    }
}
