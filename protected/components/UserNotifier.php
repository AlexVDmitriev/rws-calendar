<?php

/**
 * Class UserNotifier
 */
class UserNotifier extends CComponent
{
    /** @var YiiMailer */
    protected $mailer = null;

    public function init()
    {
        $this->mailer = new YiiMailer();
    }

    /**
     * @param Event $model
     *
     * @return bool
     */
    public function notifyAboutCreate($model)
    {
        $subject = "New Event Created ID:" . $model->eventid . " Brand : " . $model->manufacturerlist->name . " starting on " . $model->startdate;

        $body =<<<HTML
<center>
    <h2>New Event Created</h2>
</center>
<table>
    <tbody>
        <tr><td>Event Id</td><td>{$model->eventid}</td></tr>
        <tr><td>Brand</td><td>{$model->manufacturerlist->name}</td></tr>
        <tr><td>Start Date</td><td>{$model->startdate}</td></tr>
        <tr><td>End Date</td><td>{$model->enddate}</td></tr>
        <tr><td>Category</td><td>{$model->categorylist->name}</td></tr>
        <tr><td>Manager</td><td>{$model->managerlist->fullname}</td></tr>
        <tr><td>Supplier</td><td>{$model->supplist->name}</td></tr>
        <tr><td>Sales</td><td>{$model->saleslist->name}</td></tr>
        <tr><td>Warehouse</td><td>{$model->warehouselist->name}</td></tr>
        <tr><td>Revenue Prediction</td><td>{$model->revprediction}</td></tr>
    </tbody>
</table>
HTML;

        return $this->notify($subject, $body);
    }

    /**
     * @param Event $model
     * @param string $previousStartDate
     * @param string $previousEndDate
     *
     * @return bool
     */
    public function notifyAboutMove($model, $previousStartDate, $previousEndDate)
    {
        $emails = $this->getEmails();

        if (empty($emails)) {
            return false;
        }

        $subject = "Event Dates Changed Created ID:" . $model->eventid . " Brand : " . $model->manufacturerlist->name . " starting on " . $model->startdate;
        $body = <<<HTML
<b>Change to event with event id : {$model->eventid} date has been changed.</b>
<br />
<br />
<table width='50%'>
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Original Values</th>
            <th>New Values</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Start Date</b></td>
            <td>{$previousStartDate}</td>
            <td>{$model->startdate}</td>
        </tr>
        <tr>
            <td><b>End Date</b></td>
            <td>{$previousEndDate}</td>
            <td>{$model->enddate}</td>
        </tr>
    </tbody>
</table>
HTML;

        return $this->notify($subject, $body);
    }

    /**
     * @param string $subject
     * @param string $body
     *
     * @return bool
     */
    protected function notify($subject, $body)
    {
        $emails = $this->getEmails();

        if (empty($emails)) {
            return false;
        }

        $this->mailer->clear();

        $this->mailer->setFrom("issue_report@calendar.runwaysale.co.za", "RSC Issue Reporter");
        $this->mailer->addReplyTo("issue_report@calendar.runwaysale.co.za", "RSC Issue Reporter");

        foreach ($emails as $email) {
            $this->mailer->AddAddress($email);
        }

        $this->mailer->setSubject($subject);
        $this->mailer->setBody($body);

        if ( ! $this->mailer->send()) {
            Yii::log(
                "Mailer Error: " . $this->mailer->ErrorInfo . PHP_EOL .
                "Body: " . $body . PHP_EOL .
                "To: " . var_export($emails, true) . PHP_EOL,
                'warning',
                'event.update.has_errors'
            );

            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    protected function getEmails()
    {
        $emails = Yii::app()->db->createCommand("select emailaddress from `user` where receive_notification=1")
            ->queryColumn();

        return empty($emails) ? [] : $emails;
    }
}
