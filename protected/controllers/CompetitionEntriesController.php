<?php

/**
 * Class CompetitionEntriesController
 */
class CompetitionEntriesController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            // TODO need checking. This actions haven't error, but they are possible by guest user
            array(
                'actions' => array(
                    'mwtest',
                    'monthlywinner',
                    'processWinners',
                ),
                'users' => array('*'),
            ),
            array(
                'allow',
                'actions' => array(
                    'index',
                    'past',
                    'updateBefore',
                    'updateAfter',
                    'saveentry',
                ),
                'users' => array('@'),
                'expression' => array('Controller', 'allowOnlyIsVip')
            ),
            array('allow', 'actions' => array('sendemail'), 'users' => array("*")),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	public function actionIndex()
	{
        $buyer = empty($_GET['buyer']) ? "" : $_GET['buyer'];
        $fromDate = empty($_GET['fdate']) ? "" : $_GET['fdate'];
        $toDate = empty($_GET['tdate']) ? "" : $_GET['tdate'];
        $eventId = empty($_GET['eventid']) ? "" : $_GET['eventid'];

        $command = Yii::app()->db->createCommand()
            ->select('c.*')
            ->from(
                array(
                    'competition c',
                    'event e'
                )
            )
            ->where(
                "`c`.totunits != ''"
            )
            ->andWhere(
                '`c`.winner IS NULL OR `c`.winner = 0'
            )
            ->andWhere(
                '`e`.enddate >= date(now())'
            )
            ->andWhere(
                '`e`.id = c.eventid'
            )
            ->andWhere(
                '(TO_DAYS(`e`.enddate) - TO_DAYS(date(now()))) < 31'
            )
            ->order(
                '(TO_DAYS(`e`.enddate) - TO_DAYS(date(now()))) ASC'
            );

        $queryParams = array();
        if (!empty($eventId)) {
            $command = $command->andWhere('`e`.eventid = :eventId ', array(':eventId' => $eventId));
            $queryParams[':eventId'] = $eventId;
        }

        if (!empty($fromDate)) {
            $command = $command->andWhere('enddate >= :fromDate', array(':fromDate' => $fromDate));
            $queryParams[':fromDate'] = $fromDate;
        }

        if (!empty($toDate)) {
            $command = $command->andWhere('enddate <= :toDate', array(':toDate' => $toDate));
            $queryParams[':toDate'] = $toDate;
        }

        if (!empty($buyer)) {
            $command = $command->andWhere('manager = :manager', array(':manager' => $buyer));
            $queryParams[':manager'] = $buyer;
        }

        $command = $command->andWhere('isVip = :application_isVip_param');
        $queryParams[':application_isVip_param'] = (int)Yii::app()->params['isVip'];

        /** @var Competition[] $competitions */
        $competitions = Competition::model()->findAllBySql($command->text, $queryParams);

		$this->render(
		    'index',
            array(
                'buyer' => $buyer,
                'fdate' => $fromDate,
                'tdate' => $toDate,
                'competitions' => $competitions,
            )
        );
	}

	public function actionpast()
	{
	    $query = <<<SQL
SELECT `c`.*
FROM `competition` `c`, `event` `e`
WHERE `c`.`totunits` != ''
      AND `c`.`winner` != 0
      AND `c`.`winner` IS NOT NULL
      AND `e`.`id` = `c`.`eventid`
      AND `e`.`enddate` <= date(now())
      AND `e`.`isVip` = :application_isVip_param
ORDER BY `e`.`enddate` DESC 
SQL;

        /** @var Competition[] $comps */
        $comps = Competition::model()->findAllBySql(
            $query,
            [':application_isVip_param' => (int)Yii::app()->params['isVip']]
        );

        /** @var CompetitionMonthWinner[] $competitionMonthWinners */
        $competitionMonthWinners = CompetitionMonthWinner::model()->findAll(array('order'=>'date desc'));

		$this->render(
		    'past',
                array(
                    'comps' => $comps,
                    'competitionMonthWinners' => $competitionMonthWinners,
                )
        );
	}

    public function actionUpdateBefore()
    {
		$out = "";
		if(isset($_POST)){
			$arr = $_POST;
			foreach($arr as $name=>$val){
				if(strstr($name,"_")){
					$ar = explode("_",$name);
					$field = "";
					$ufield = "";
					$id = $ar[1];
					/** @var Event $event */
					$event = Event::model()->findByPk($id);
					if($event != NULL){
						$s = $ar[0];
						switch($s){
							case "totunits":
								$field = "totunits";
								$ufield = "Total Units";
								break;
							case "avgpricepoint":
								$field = "avgpricepoint";
								$ufield = "Average price point";
								break;
							case "link":
								$field = "link";
								$ufield = "Link";
								break;
							case "avgdiscount":
								$field = "avgdiscount";
								$ufield = "Average Discount";
								break;
							case "totstyles":
								$field = "totstyles";
								$ufield = "Total Styles";
								break;
						}
						if($field != "" && $val != NULL){
							$event->competition->$field = $val;
							$event->competition->save();
							$out .= "<b>Event : ".$event->eventid." $ufield updated to ".$val."</b><br>\n";
						}
					}
				}
			}
		}
		echo $out;

        $manager = empty($_GET['buyer']) ? "" : $_GET['buyer'];
        $month = empty($_GET['month']) ? "" : $_GET['month'];
        $year = empty($_GET['year']) ? "" : $_GET['year'];

        $criteria = new CDbCriteria();
        $criteria->addCondition('enddate >= NOW()');
        $criteria->order = 'enddate ASC';

        if (!empty($manager)) {
            $criteria->addCondition('manager = :manager');
            $criteria->params[':manager'] = $manager;
        }

        if (!empty($month)) {
            $criteria->addCondition('month(enddate) = :month');
            $criteria->params[':month'] = $month;
        }

        if (!empty($year)) {
            $criteria->addCondition('year(enddate)= :year');
            $criteria->params[':year'] = $year;
        }

        /** @var Event[] $events */
        $events = Event::model()->findAll($criteria);

        $this->render(
            "updatebefore",
            array(
                'events' => $events,
                'manager' => $manager,
                'month' => $month,
                'year' => $year,
            )
        );
	}

    public function actionUpdateAfter()
    {
		$out = "";
		if(isset($_POST)){
			$arr = $_POST;
			foreach($arr as $name=>$val){
				if(strstr($name,"_")){
					$ar = explode("_",$name);
					$field = "";
					$ufield = "";
					$id = $ar[1];
					/** @var Event $event */
					$event = Event::model()->findByPk($id);
					if($event != NULL){
						$s = $ar[0];
						switch($s){
							case "actualunitsales":
								$field = "actualunitsales";
								$ufield = "Actual Units Sales";
								break;
							case "actualrevenue":
								$field = "actualrevenue";
								$ufield = "Actual Revenue";
								break;
						}
						if($field != "" && $val != NULL){
							$event->$field = $val;
							if($field == "actualunitsales" && $val > 0){
								$event->actualsellthrough = round(($event->actualunitsales / $event->competition->totunits) *100);
							}

							if($field == "actualrevenue" && $val > 0){
								$event->predvsact = round((($event->actualrevenue-$event->revprediction)/$event->revprediction) * 100);
							}
							
							$event->save();
							$out .= "<b>Event : ".$event->eventid." $ufield updated to ".$val."</b><br>\n";
						}
					}
				}
			}
		}
        echo $out;

        $manager = empty($_GET['buyer']) ? "" : $_GET['buyer'];
        $month = empty($_GET['month']) ? "" : $_GET['month'];
        $year = empty($_GET['year']) ? "" : $_GET['year'];

        $criteria = new CDbCriteria();
        $criteria->addCondition('enddate >= NOW()');
        $criteria->order = 'enddate ASC';

        if (!empty($manager)) {
            $criteria->addCondition('manager = :manager');
            $criteria->params[':manager'] = $manager;
        }

        if (!empty($month)) {
            $criteria->addCondition('month(enddate) = :month');
            $criteria->params[':month'] = $month;
        }

        if (!empty($year)) {
            $criteria->addCondition('year(enddate)= :year');
            $criteria->params[':year'] = $year;
        }

        /** @var Event[] $events */
        $events = Event::model()->findAll($criteria);

        $this->render(
            "updateafter",
            array(
                'events' => $events,
                'manager' => $manager,
                'month' => $month,
                'year' => $year,
            )
        );
	}

	public function actionmwtest(){
		$date = new DateTime(date("2015-12-01"));
		$date->modify("-1 day");
		$cdate = new DateTime("2015-12-01");
		$cdate->modify("-1 day");
		echo $date->format("Y-m-d");
		echo " ".$cdate->format("Y-m-d");

		if($date->format("d") == $cdate->format("d")) {
			$sql =<<<SQL
SELECT c.winner, count(c.eventid) 
FROM 
  competition c,
  event e 
WHERE 
  c.eventid = e.id
  AND year(e.enddate)= :year
  AND month(e.enddate)= :month
  AND c.winner!=0
  AND e.isVip = :application_isVip_param
GROUP BY c.winner
ORDER BY count(c.eventid) DESC;
SQL;
            $winner = Yii::app()->db->createCommand($sql)->queryScalar(
                array(
                    ':month' => $date->format("m"),
                    ':year' => $date->format("Y"),
                    ':application_isVip_param' => (int)Yii::app()->params['isVip'],
                )
            );

			echo $winner;
		}		
	}

	public function actionmonthlywinner()
	{
		$date = new DateTime(date("Y-m-01"));
		$date->modify("-1 day");

		$cdate = new DateTime();
		$cdate->modify("-1 day");

		echo $date->format("Y-m-d");
		echo " ".$cdate->format("Y-m-d");

		if($date->format("d") == $cdate->format("d")) {
		    $sql = <<<SQL
SELECT 
  c.winner,
  count(c.eventid)
FROM
  competition c,
  event e
WHERE
  c.eventid = e.id
  AND year(e.enddate)=:year
  AND month(e.enddate) = :month
  AND c.winner!=0
  AND isVip = :application_isVip_param
GROUP BY c.winner
ORDER BY count(c.eventid) DESC
SQL;
            $winner = Yii::app()->db->createCommand($sql)->queryScalar(
                array(
                    ':month' => $date->format("m"),
                    ':year' => $date->format("Y"),
                    ':application_isVip_param' => (int) Yii::app()->params['isVip'],
                )
            );

			if($winner !== false) {
				$cmw = new CompetitionMonthWinner();
				$cmw->date = $date->format("Y-m-d");
				$cmw->winner= $winner;
				$cmw->isVip = (int) Yii::app()->params['isVip'];

                if ( ! $cmw->save()) {
                    print_r($cmw->getErrors());
                }

				echo $winner;
			}
		}
	}
	
	public function actionProcessWinners()
	{
		$date = new DateTime();
		$date->modify("-1 day");

        $sql = <<<SQL
SELECT
  c.*
FROM
  competition c,
  event e
WHERE
  e.id = c.eventid
  AND e.enddate<= :enddate
  AND (
    c.winner=0
    OR c.winner IS NULL
  ) 
  AND e.actualunitsales != 0
  AND e.actualrevenue != 0
  AND isVip = :application_isVip_param
SQL;

		/** @var Competition[] $comps */
        $comps = Competition::model()
            ->findAllBySql(
                $sql,
                array(
                    ':enddate' => $date->format("Y-m-d"),
                    ':application_isVip_param' => (int)Yii::app()->params['isVip'],
                )
            );

		for($i=0; $i < count($comps); $i++)
		{
			echo $comps[$i]->id." ";

			/** @var CompetitionEntries[] $entries */
			$entries = CompetitionEntries::model()->findAllByAttributes(array('competitionid' =>$comps[$i]->id));	
			$aunit=$comps[$i]->event->actualunitsales;
			$arevenue = $comps[$i]->event->actualrevenue;
			$mindiff = 0;
			$mindiffuser = 0;
			for($x=0; $x < count($entries); $x++)
			{
				$diff = $aunit - $entries[$x]->soldamount;
				echo $aunit." ".$diff." ";
				$diff = $diff * $diff;
				$diff2 = $arevenue - $entries[$x]->revenueamount;
				echo $arevenue. " " .$diff2. " ";
				$diff2 = $diff2 * $diff2;
				$diff = $diff + $diff2;
				if($mindiff > $diff || $mindiff == 0)
				{
					$mindiff = $diff;
					$mindiffuser = $entries[$x]->user;
				}
				echo $entries[$x]->user."<br>";
			}
			$comps[$i]->winner = $mindiffuser;
			echo $mindiffuser;
			$comps[$i]->save();
		}
	}
	
	public function actionsaveentry()
	{
		if(!isset($_POST['revenueamount']) || !is_numeric($_POST['revenueamount']))
		{
			echo "Please enter a valid number for the predicted revenue amount";
			return;
		}
		if(!isset($_POST['soldamount']) || !is_numeric($_POST['soldamount']))
		{
			echo "Please enter a valid number for the predicted unit sold amount";
			return;
		}
		if(!isset($_POST['compid']) || $_POST['compid'] == "")
		{
			echo "Competition not found!";
			return;
		}
		$comp = Competition::model()->findByAttributes(array('id'=>$_POST['compid']));
        if ($comp == null) {
            echo "Competition not found!";
            return;
        }

        /** @var CWebApplication $app */
        $app = Yii::app();

		/** @var CompetitionEntries[] $compentry */
        $compentry = CompetitionEntries::model()
            ->findByAttributes(array('user' => $app->user->id, 'competitionid' => $_POST['compid']));

        if ($compentry == null) {
            $compentry = new CompetitionEntries();
            $compentry->user = $app->user->id;
            $compentry->competitionid = $_POST['compid'];
        }

		$compentry->datetime = date("Y-m-d H:i:s");
		$compentry->revenueamount = $_POST['revenueamount'];
		$compentry->soldamount = $_POST['soldamount'];
		$compentry->save();

		echo "OK";
	}
}
