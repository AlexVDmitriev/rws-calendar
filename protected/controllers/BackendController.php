<?php

/**
 * Class BackendController
 */
class BackendController extends Controller
{
    const CACHE_DURATION_GET_EVENTS = 3600;
    const CACHE_DURATION_GET_NEW_EVENTS = 3600;
    const CACHE_DURATION_GET_FILTERED_EVENTS = 3600;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array(
                    'index',
                    'gettargetlist',
                    'revenueprediction',
                    'newchanges',
                    'calender',
                    'brandreport',
                    'getevents',
                    'getnewevents',
                    'eventlog',
                    'gantt',
                    'getganttdata',
                    'filterforday',
                    'getmasterdata',
                    'indexgantt',
                    'downloadcsv',
                    'salesreport',
                    'savetargets',
                    'SalesReportGen',
                    'SalesReportCSV',
                    'gettable',
                    'changeuserfilter',
                    'gettabledata',
                    'testemail',
                    'changereport',
                    'getganttminmax',
                    'changereportresult',
                    'mkrequestform',
                    'mkrequest',
                    'vtrequestform',
                    'vtrequest',
                    'dailymailerform',
                    'dailymailer',
                    'combinedreportform',
                    'combinedreport'
                ),
				'users'=>array('@'),
                'expression' => array('Controller', 'allowOnlyIsVip')
			),
			array('allow','actions'=>array('sendemail'),'users'=>array("*")),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	public function actionCombinedreportform(){
	    $this->render("combined_report_form");
    }
    public function actionCombinedreport(){
        $fromdate = isset($_POST['fromdate'])?$_POST['fromdate']:date("Y-m-d");
        $todate = isset($_POST['todate'])?$_POST['todate']:date("Y-m-d");
        $this->render("combined_report",array('fromdate'=>$fromdate,'todate'=>$todate));
    }
    public function actiondailymailer(){
	    $fromdate = isset($_POST['fromdate'])?$_POST['fromdate']:date("Y-m-d");
        $todate = isset($_POST['todate'])?$_POST['todate']:date("Y-m-d");
        $this->render("daily_mailer",array('fromdate'=>$fromdate,'todate'=>$todate));
    }
	public function actionDailyMailerForm(){
	    $this->render("daily_mailer_form");
    }

    public function actionmkrequestform(){
	    $this->render("mkrequestform");
    }

    public function actionvtrequestform(){
        $this->render("vtrequestform");
    }

    public function actionvtrequest(){
        if(!isset($_POST['fromdate']) || $_POST['fromdate'] == ""){
            $fdt = date('Y-m-d');
        }
        else {
            $fdt = $_POST['fromdate'];
        }
        if(!isset($_POST['todate']) || $_POST['todate'] == ""){
            $tdt = date('Y-m-d');
        }
        else {
            $tdt = $_POST['todate'];
        }

        $this->render("vtrequest",array('fromdate'=>$fdt,'todate'=>$tdt));
    }
    public function actionmkrequest(){
        if($_POST['fromdate'] == ""){
            $fdt = date('Y-m-d');
        }
        else {
            $fdt = $_POST['fromdate'];
        }
        if($_POST['todate'] == ""){
            $tdt = date('Y-m-d');
        }
        else {
            $tdt = $_POST['todate'];
        }

        $this->render("mkrequest",array('fromdate'=>$fdt,'todate'=>$tdt));
    }

    public function actionChangeReportResult(){
	    $man = $_POST['manager'];
	    $from = $_POST['from_date'];
	    $to = $_POST['to_date'];

	    $logs = null;
	    if($man == "")
	        $logs = ChangeLog::model()->findAllBySql("SELECT * from change_log where date(`datetime`)>=:fromdt and changetarget='event' and (fieldname='start date' or fieldname='end date' or fieldname='revenue prediction') and date(`datetime`)<=:todt order by user, eventid,datetime;",array(':fromdt'=>$from,":todt"=>$to));
	    else
            $logs = ChangeLog::model()->findAllBySql("SELECT * from change_log where `user`=:manager and changetarget='event' and (fieldname='start date' or fieldname='end date' or fieldname='revenue prediction') and `datetime`>=:fromdt and `datetime`<=:todt order by `user`, eventid,datetime;",array(':fromdt'=>$from,":todt"=>$to,':manager'=>$man));

        $this->renderPartial("changereport_data",array('logs'=>$logs));
    }

	public function actionChangeReport(){
	    $this->render("changereport_form");
    }

    public function actiontestmailer(){
	    $mailer = new PHPMailer();
    }

    public function actionGetTable()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();

        $category = $app->session['category'];
        $manager = $app->session['manager'];
        $sales = $app->session['sales'];
        $status = $app->session['status'];

        $this->render(
            "tableindex",
            array(
                'category' => $category,
                'manager' => $manager,
                'sales' => $sales,
                'status' => $status,
            )
        );
    }

	public function actionsaveTargets(){
		foreach($_POST['Target'] as $idx=>$val){
			if($val != "" && $val != 0){
				$tar = DailyTarget::model()->findByAttributes(array('date'=>$idx));
				if($tar == NULL){
					$tar= new DailyTarget();
					$tar->date = $idx;
				}
				//echo $idx." ".$val."\n";
				$tar->amount = $val;
				if(!$tar->save())
					print_r($tar->getErrors());
			}
		}
		echo "SUCCESS";
	}

    public function actionRevenuePrediction()
    {
        $month = empty($_GET['month']) ? date("m") : $_GET['month'];
        $year = empty($_GET['year']) ? date('Y') : $_GET['year'];

        $this->render(
            "revenueprediction",
            array(
                'month' => $month,
                'year' => $year,
            )
        );
    }
	
	public function actionnewchanges(){
		$dt = $_GET['dt'];
		/** @var Changes[] $qry */
		$qry = "SELECT * from changes where `datetime`> :datetime order by `datetime` asc;";
		$changes = Changes::model()->findAllBySql(
		    $qry,
            array(
                ':datetime' => $dt,
            )
        );
		$out = array('lastdt'=>'','data'=>[]);
        $lastdt = null;
		for($i=0; $i < count($changes); $i++){
			if($changes[$i]->operation == "UPDATE" || $changes[$i]->operation == "INSERT"){
			    /** @var Event $event */
				$event = Event::model()->findByPk($changes[$i]->eventid);
				$data = -1;
				if($event != null)
				    $data = $event->getEventData(0,1);
			}
			else
				$data = null;
			if($data != -1)
				$out['data'][] = array('operation'=>$changes[$i]->operation,'id'=>$changes[$i]->eventid,'data'=>$data);
			$lastdt = $changes[$i]->datetime;
		}
		$out['lastdt'] = $lastdt;
		echo json_encode($out);
	}

	public function actionGettabledata()
	{
		$from = isset($_GET['fromdate'])?$_GET['fromdate']:"";
		$to = isset($_GET['todate'])?$_GET['todate']:"";
		$manager = isset($_GET['manager'])?$_GET['manager']:"";
		$status = isset($_GET['status'])?$_GET['status']:"";
		$category = isset($_GET['category'])?$_GET['category']:"";
		$sales = isset($_GET['sales'])?$_GET['sales']:"";
		$tasks = isset($_GET['tasks'])?$_GET['tasks']:"";
		
		$events = $this->getFilteredEvents($from,$to,$manager,$category,$sales,$status,$tasks);
		$data = new CArrayDataProvider($events,array('id'=>'id'));
		$this->renderPartial("tableonly",array("data"=>$data));
	}

	/**
	 * @param $from
	 * @param $to
	 * @param string $manager
	 * @param string $category
	 * @param string $sales
	 * @param string $status
	 * @param string $tasks
	 * @param string $warehouse
	 *
	 * @return Event[]
	 */
    private function getFilteredEvents(
        $from,
        $to,
        $manager = "",
        $category = "",
        $sales = "",
        $status = "",
        $tasks = "",
        $warehouse = ''
    )
	{
        if ($from == "") {
            $from = date("Y-m-01");
        }

		if($to == "") {
			$dt = new DateTime($from);
			$dt->modify("+1 month");
			$dt->modify("-1 day");
			$to = $dt->format("Y-m-d");
		}

        $params = [];

        if ($manager != "") {
            $params[':manager'] = $manager;
            $manager            = " AND `manager` = :manager";
        } elseif (isset($_SESSION['manager']) && $_SESSION['manager'] != "") {
            $params[':manager'] = $_SESSION['manager'];
            $manager            = " AND `manager` = :manager";
        } else {
            $manager = "";
        }

        if ($category != "") {
            $params[':category'] = $category;
            $category            = " AND `category` = :category";
        } elseif (isset($_SESSION['category']) && $_SESSION['category'] != "") {
            $params[':category'] = $_SESSION['category'];
            $category            = " AND `category` = :category";
        } else {
            $category = "";
        }

        if ($sales != "") {
            $params[':sales_id'] = $sales;
            $sales               = " AND `salesid` = :sales_id";
        } elseif (isset($_SESSION['sales']) && $_SESSION['sales'] != "") {
            $params[':sales_id'] = $_SESSION['sales'];
            $sales               = " AND `salesid` = :sales_id";
        } else {
            $sales = "";
        }

        if ($status != "") {
            $params[':status'] = $status;
            $status            = " AND `status` = :status";
        } elseif (isset($_SESSION['status']) && $_SESSION['status'] != "") {
            $params[':status'] = $_SESSION['status'];
            $status            = " AND `status` = :status";
        } else {
            $status = "";
        }

        if ($warehouse != "") {
            $params[':warehouse'] = $warehouse;
            $warehouse            = " AND `warehouse` = :warehouse";
        } elseif (isset($_SESSION['warehouse']) && $_SESSION['warehouse'] != "") {
            $params[':warehouse'] = $_SESSION['warehouse'];
            $warehouse            = " AND `warehouse` = :warehouse";
        } else {
            $warehouse = "";
        }

        if ($tasks != "") {
            $tasks = explode(",", $tasks);

            $tasks[] = "_"; // make $tasks as nonempty array

            foreach ($tasks as $key => $task) {
                $tasks[$key] = '"' . $task . '"';
            }

            $tasksStr = implode(",", $tasks);

            $tasks = " AND `id` IN (
			        SELECT `eventid` FROM `event_tasks` WHERE `task` IN ({$tasksStr}) AND `state` > 0
			    )
            ";
        }

        $applicationIsVip = 'AND isVip = :application_isVip_param';
        $params[':application_isVip_param'] = (int)Yii::app()->params['isVip'];

		$sql =<<<SQL
SELECT *
FROM `event`
WHERE (
  (`startdate` >= :fromdate AND `enddate` <= :todate)
  OR (`startdate` <= :fromdate AND `enddate` >= :todate)
  OR (`startdate` <= :todate AND `startdate` >= :fromdate)
  OR (`enddate` <= :todate AND `enddate` >= :fromdate)
)
{$manager}
{$category}
{$sales}
{$status}
{$tasks}
{$warehouse}
{$applicationIsVip}
ORDER BY `startdate`
SQL;

        $params[':fromdate'] = $from;
        $params[':todate']   = $to;

        /** @var Event[] $events */
        $events = Event::model()
                       ->cache(static::CACHE_DURATION_GET_FILTERED_EVENTS, new CTagCacheDependency('BackendController::getFilteredEvents'))
                       ->findAllBySql($sql, $params);

		return $events;
	}
	
	public function actionchangeuserfilter()
	{
		$userid = $_GET['userid'];
		$_SESSION['manager'] = mysql_escape_string($userid);
		echo "OK";
	}

	public function actionBrandReport()
	{
        $command = Yii::app()->db->createCommand()
                                 ->select([
                                     'eventid',
                                     'manufacturer',
                                     'category',
                                     'manager',
                                     'startdate',
                                     'enddate',
                                     'supplierid'
                                 ])
                                 ->from('event')
                                 ->where('isVip = :application_isVip_param')
                                 ->group(['eventid'])
                                 ->order(['enddate ASC']);

        $res = $command->queryAll(true, [':application_isVip_param' => (int)Yii::app()->params['isVip']]);
		$out = null;
		$c = 0;
		foreach($res as $row) {
		    /** @var Manufacturer $manu */
			$manu = Manufacturer::model()->findByPk($row['manufacturer']);

            $out[$c]['manufacturer'] = (empty($manu)) ? "" : $manu->name;

            if ($manu == null || $manu->terminationdate == "" || $manu->terminationdate == "0000-00-00") {
                $out[$c]['terminated'] = 1;
            } else {
                $out[$c]['terminated'] = 0;
            }

            /** @var User $user */
			$user = User::model()->findByPk($row['manager']);
			$out[$c]['manager'] = $user->fullname;

			/** @var Category $cat */
			$cat = Category::model()->findByPk($row['category']);

			/** @var Supplier $supp */
			$supp = Supplier::model()->findByPk($row['supplierid']);

			if ($supp != null)
                $out[$c]['suppler'] = $supp->name;

            if ($cat != null)
                $out[$c]['category'] = $cat->name;

            $co = Yii::app()->db->createCommand()
                                ->select('to_days(now())-to_days(enddate) as endd')
                                ->from('event')
                                ->andWhere('manufacturer = :manufacturer')
                                ->andWhere('isVip = :application_isVip_param')
                                ->andWhere('enddate < NOW()')
                                ->order(['enddate DESC'])
                                ->limit(1);

            $re = $co->queryAll(
                true,
                array(
                    ':manufacturer' => $row['manufacturer'],
                    ':application_isVip_param' => (int)Yii::app()->params['isVip']
                )
            );

            if (count($re) == 0) {
                $out[$c]['lastevent'] = "none";
            } else {
                $out[$c]['lastevent'] = $re[0]['endd'];
            }

            $co = Yii::app()->db->createCommand()
                                ->select('to_days(startdate)-to_days(now()) as endd')
                                ->from('event')
                                ->andWhere('manufacturer = :manufacturer')
                                ->andWhere('isVip = :application_isVip_param')
                                ->andWhere('startdate > NOW()')
                                ->order(['enddate DESC'])
                                ->limit(1);

			$re = $co->queryAll(
			    true,
                array(
                    ':manufacturer' => $row['manufacturer'],
                    ':application_isVip_param' => (int)Yii::app()->params['isVip']
                )
            );

            if (count($re) == 0) {
                $out[$c]['nextevent'] = "none";
            } else {
                $out[$c]['nextevent'] = $re[0]['endd'];
            }

            $co = Yii::app()->db->createCommand()
                                ->select('count(*) as endd')
                                ->from('event')
                                ->andWhere('startdate <= NOW()')
                                ->andWhere('manufacturer = :manufacturer');

            $re = $co->queryAll(
                true,
                array(
                    ':manufacturer' => $row['manufacturer'],
                )
            );

            if (count($re) == 0) {
                $out[$c]['totalrun'] = "none";
            } else {
                $out[$c]['totalrun'] = $re[0]['endd'];
            }

			$c++;
		}

		$filtersForm=new FiltersForm;
		if (isset($_GET['FiltersForm']))
			$filtersForm->filters=$_GET['FiltersForm'];
		 
		// Get rawData and create dataProvider
		$rawData=$out;
		
		$filteredData=$filtersForm->filter($rawData);

        $data = new CArrayDataProvider(
            $filteredData,
            array(
                'id' => 'eventid',
                'keyField' => false,
                'pagination' => false,
                'sort' => array(
                    'defaultOrder' => 'lastevent desc',
                    'attributes' => array('manufacturer', 'manager', 'category', 'totalrun', 'nextevent', 'lastevent')
                )
            )
        );

        $this->render("brandreport", array('data' => $data, 'filtersForm' => $filtersForm));
	}

	public function actiongetganttminmax()
	{
		$ret = $this->actiongetganttdata(true);
		$min = null;
		$max = null;
		for($i=0; $i < count($ret); $i++)
		{
			$r = $ret[$i];
			$f = $ret[$i]['values'][0]['from'];
			$t = $ret[$i]['values'][0]['to'];
			$from = strtotime($f);
			$to = strtotime($t);
			if($min == null)
				$min = $from;
			else if(($min - $from) >0)
				$min = $from;
			//echo "min : ".$min->format("Y-m-d")." ".$from->format("Y-m-d")."<br>";
			if($max == null)
				$max = $to;
			else if(($max - $to) < 0)
				$max = $to;
			//echo "max : ".$max->format("Y-m-d")." ".$to->format("Y-m-d")."<br>";
		}
		
		$min = $min - (24*60*60*3);
		$max = $max + (24*60*60*3);
		//echo $min->format("Y-m-d"). " ".$max->format("Y-m-d");
		echo json_encode(array("min"=>date("Y-m-d",$min),"max"=>date("Y-m-d",$max)));
	}

	public function actiongetganttdata($ret = false)
	{
		if(isset($_GET['fromdate']))
			$_SESSION['date'] = $_GET['fromdate'];
		
		$fromdate = (isset($_SESSION['date'])?date("Y-m-01",strtotime($_SESSION['date'])):date('Y-m-d'));
		$to = new DateTime(date("Y-m-01",strtotime($fromdate)));
		$to->modify("+1 month");
		$to->modify("-1 day");
		$todate = $to->format("Y-m-d");
		$events = $this->getFilteredEvents($fromdate,$todate);

		if(count($events) == 0) {
			return null;
		}

		$out = array();

		foreach($events as $event) {
			$x = $event->manufacturerlist->name;

			$outEvent = array();
			$outEvent['name'] = $x;
			$outEvent['desc'] = $event->eventid;
			$outEvent['values'][0]['from'] = $event->startdate;

			$obj = array();
			$obj['id'] = $event->id;

			/** @var CWebApplication $app */
			$app = Yii::app();

			/** @var User $webUser */
			$webUser = $app->user;

            $obj['url'] = ($webUser->has_admin_permission || $webUser->has_can_add_permission)
                ? Yii::app()->createUrl("Event/Update", array('id' => $event->id, 'gantt' => 1))
                : Yii::app()->createUrl("Event/view", array('id' => $event->id, 'dialog' => 1));

			if (!empty($event->categorylist)) {
				$obj['category_color'] = $event->categorylist->color;
				$obj['category_text']  = $event->categorylist->name;
			}

			if(!empty($event->managerlist)) {
				$obj['manager_color'] = $event->managerlist->color;
				$obj['manager_text'] = $event->managerlist->fullname;
			}

			if(!empty($event->statuslist)) {
				$obj['status_color'] = $event->statuslist->color;
				$obj['status_text'] = $event->statuslist->status;
			}

			if(!empty($event->warehouselist)) {
				$obj['warehouse_color'] = $event->warehouselist->color;
				$obj['warehouse_text'] = $event->warehouselist->name;
			}

			if(!empty($event->saleslist)) {
				$obj['sales_color'] = $event->saleslist->color;
				$obj['sales_text'] = $event->saleslist->name;
			}

			if(!empty($event->competition)) {
				$obj['unitsavailable_text'] = $event->competition->totunits;
			}

			$outEvent['values'][0]['dataObj'] = $obj;
			$outEvent['values'][0]['to'] = $event->enddate;

			$out[] = $outEvent;
		}

		if(!$ret) {
			echo json_encode($out);
		}

		return $out;
	}

	public function actionindexgantt()
	{
		if(isset($_GET['date']) && $_GET['date'] != "")
		{
			$_SESSION['date'] = $_GET['date'];
		}
		else 
		{
			$_SESSION['date'] = date("d-m-Y");
		}
		if(isset($_GET['manager']) && $_GET['manager'] != "")
		{
			$_SESSION['manager'] = $_GET['manager'];
		}
		else 
		{
			$_SESSION['manager'] = "";
		} 
		if(isset($_GET['category']) && $_GET['category'] != "")
		{
			$_SESSION['category'] = $_GET['category'];
		}
		else 
		{
			$_SESSION['category'] = '';
		}
		if(isset($_GET['sales']) && $_GET['sales'] != "")
		{
			$_SESSION['sales'] = $_GET['sales'];
		}
		else 
		{
			$_SESSION['sales'] = '';
		}
		if(isset($_GET['status']) && $_GET['status'] != "")
		{
			$_SESSION['status'] = $_GET['status'];
		}
		else 
		{
			$_SESSION['status'] = '';
		}
		
		$this->actionGantt();
	}
	public function actionfilterforday(){
		$v = $_GET['v'];
		$_SESSION['dayview']=$v;
	}

    public function actionGantt()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();

        $date = $app->session['date'];

        $this->render("gantt", array('date' => $date));
    }

	public function actionDownloadCsv()
	{
		if(isset($_GET['from']) && $_GET['from'] != "")
			$from = date("Y-m-d",strtotime($_GET['from']));
		else
			$from = "";
		if(isset($_GET['to']) && $_GET['to'] != "")
			$to = date("Y-m-d",strtotime($_GET['to']));
		else
			$to = "";
		
		$manager = isset($_GET['manager'])?$_GET['manager']:"";
		$status = isset($_GET['status'])?$_GET['status']:"";
		$category = isset($_GET['category'])?$_GET['category']:"";
		$sales = isset($_GET['sales'])?$_GET['sales']:"";
		$warehouse = isset($_GET['warehouse'])?$_GET['warehouse']:"";
		if($from == "") {
			$from = date("Y-m-d");
			$to = new DateTime($from);
			$to->modify("+1 month");
			$to->modify("-1 day");
			$to = $to->format("Y-m-d");
		}
		
		$events = $this->getFilteredEvents($from,$to,$manager,$category,$sales,$status,"",$warehouse);

		if(count($events) == 0) {
			return;
		}
		
		$out = NULL;
		foreach($events as $event) {
			if($out == NULL) {
				$out = $event->getAttributeLabel("id").",";
				$out .= $event->getAttributeLabel("manufacturer").",";
				$out .= $event->getAttributeLabel("eventid").",";
				$out .= $event->getAttributeLabel("categorylist.name").",";
				$out .= $event->getAttributeLabel("managerlist.fullname").",";
				$out .= $event->getAttributeLabel("saleslist.name").",";
				$out .= $event->getAttributeLabel("statuslist.status").",";
				$out .= $event->getAttributeLabel("startdate").",";
				$out .= $event->getAttributeLabel("enddate").",";
				$out .= $event->getAttributeLabel("revprediction").",";
				$out .= $event->getAttributeLabel("orderprediction").",";
				$out .= $event->getAttributeLabel("unitprediction")."\n";
			}
			
			$out .= $event->id.",";
			$out .= $event->manufacturerlist->name.",";
			$out .= $event->eventid.",";
			$out .= $event->categorylist->name.",";
			$out .= $event->managerlist->fullname.",";
			$out .= $event->saleslist->name.",";
			$out .= $event->statuslist->status.",";
			$out .= $event->startdate.",";
			$out .= $event->enddate.",";
			$out .= $event->revprediction.",";
			$out .= $event->orderprediction.",";
			$out .= $event->unitprediction;
			$out .="\n";
		}
		
		header("Cache-Control: private");
		header("Content-Type: text/plain");
		header("Content-Length: ".strlen($out));
		header("Content-Disposition: attachment; filename=download.csv");
		
		echo $out;
	}
	
	public function actionGetMasterData(){
		$for = $_GET['for'];
		$out = [];
		if($for == "category" || $for == ""){
			$cat = Category::model()->findAll();
			$cats = [];
			for($i=0; $i < count($cat); $i++){
				$obj = [];
				foreach($cat[$i]->attributes as $idx=>$val){
					$obj[$idx] = $val;
				}
				$cats[] = $obj;
			}
			$out["category"] = $cats;
		}
		if($for == "user" || $for == ""){
			$user = User::model()->findAll();
			$users = [];
			for($i=0; $i < count($user); $i++){
				$obj = [];
				foreach($user[$i]->attributes as $idx=>$val){
					$obj[$idx] = $val;
				}
				$users[] = $obj;
			}
			$out["user"] = $user;
		}
		if($for == "tasks" || $for == ""){
			$task = Tasks::model()->findAll();
			$tasks = [];
			for($i=0; $i < count($task); $i++){
				$obj = [];
				foreach($task[$i]->attributes as $idx=>$val){
					$obj[$idx] = $val;
				}
				$tasks[] = $obj;
			}
			$out["tasks"] = $tasks;
		}
		if($for == "supplier" || $for == ""){
			$supp = Supplier::model()->findAll();
			$supps = [];
			for($i=0; $i < count($supp); $i++){
				$obj = [];
				foreach($supp[$i]->attributes as $idx=>$val){
					$obj[$idx] = $val;
				}
				$supps[] = $obj;
			}
			$out["supplier"] = $supps;
		}
		if($for == "warehouse" || $for == ""){
			$ware = Warehouse::model()->findAll();
			$wares = [];
			for($i=0; $i < count($ware); $i++){
				$obj = [];
				foreach($ware[$i]->attributes as $idx=>$val){
					$obj[$idx] = $val;
				}
				$wares[] = $obj;
			}
			$out["warehouse"] = $wares;
		}
		if($for == "status_master" || $for == ""){
			$status = StatusMaster::model()->findAll();
			$statuss = [];
			for($i=0; $i < count($status); $i++){
				$obj = [];
				foreach($status[$i]->attributes as $idx=>$val){
					$obj[$idx] = $val;
				}
				$statuss[] = $obj;
			}
			$out["status_master"] = $statuss;
		}
		if($for == "sales_bracket" || $for == ""){
			$sale = SalesBracket::model()->findAll();
			$sales = [];
			for($i=0; $i < count($sale); $i++){
				$obj = [];
				foreach($sale[$i]->attributes as $idx=>$val){
					$obj[$idx] = $val;
				}
				$sales[] = $obj;
			}
			$out["sales_bracket"] = $sales;
		}
		echo json_encode($out);
	}
	
	public function actiongettargetlist(){
		$start = $_GET['start'];
		$end = $_GET['end'];
		$month = $_GET['month'];
		$year = $_GET['year'];
		$fromdate = gmdate("Y-m-d",$start);
		$todate = gmdate('Y-m-d',$end);
		
		$events = $this->getFilteredEvents($fromdate,$todate);
		$out = array('events'=>array(),'targets'=>array());
		if(count($events) > 0)
		{
			for($i=0; $i < count($events); $i++)
			{
				$out['events'][$i] = $events[$i]->getEventData(0,1);
			}
		}
		$startd = $year."-".$month."-01";
		$dt = new DateTime($startd);
		$dt->modify("+1 month");
		$dt->modify("-1 day");
		$enddate = $dt->format("d");
		$dt = new DateTime($startd);
		while($dt->format("m") == $month){

            /** @var Event[] $events */
		    $events = Event::model()->findAllByAttributes(array('startdate'=>$dt->format("Y-m-d")));
            $brands = [];
            foreach ($events as $event) {
                $name = $event->manufacturerlist->name;
                if (array_search($name, $brands) === false) {
                    $brands[] = $name;
                }
            }
            $out['brands'][$dt->format('Y-m-d')] = implode(" , ",$brands);

            /** @var DailyTarget $dtarget */
			$dtarget = DailyTarget::model()->findByAttributes(array('date'=>$dt->format("Y-m-d")));
            if ($dtarget != null) {
                $out['targets'][$dt->format("Y-m-d")] = $dtarget->amount;
            } else {
                $out['targets'][$dt->format("Y-m-d")] = "";
            }
			$dt->modify("+1 day");
		}
		echo json_encode($out);
	}

    /**
     * Get new events list
     */
    public function actionGetNewEvents()
    {
        $start = empty($_GET['start']) ? '' : $_GET['start'];
        $end   = empty($_GET['end']) ? '' : $_GET['end'];

        /** @var CWebApplication $app */
        $app = Yii::app();

        $cache = Yii::app()->getCache();
        $cacheKey = __METHOD__ . "_" . implode("_", [
                $start,
                $end,
                $app->user->getId(),
                (int)Yii::app()->params['isVip']
            ]);

        $output = $cache->get($cacheKey);

        if(!empty($output)) {
            echo $output;

            return;
        }

        $events = $this->getFilteredEvents(
            gmdate("Y-m-d", $start),
            gmdate("Y-m-d", $end)
        );

        $out = [];
        foreach ($events as $event) {
            $out[] = $event->getEventData(0, 1);
        }

        $output = json_encode($out);

        $cache->set(
            $cacheKey,
            $output,
            static::CACHE_DURATION_GET_NEW_EVENTS,
            new CTagCacheDependency("BackendController::actionGetNewEvents")
        );

        echo $output;
    }

    /**
     * Get events list
     */
	public function actionGetEvents()
	{
        $start = empty($_GET['start']) ? '' : $_GET['start'];
        $end   = empty($_GET['end']) ? '' : $_GET['end'];

        $sessionDayView = empty($_SESSION['dayview']) ? 0 : $_SESSION['dayview'];

        /** @var CWebApplication $app */
        $app = Yii::app();

        $cache = Yii::app()->getCache();
        $cacheKey = __METHOD__ . "_" . implode("_", [
                $start,
                $end,
                $sessionDayView,
                $app->user->getId(),
                (int)Yii::app()->params['isVip']
            ]);

        $output = $cache->get($cacheKey);

        if(!empty($output)) {
            echo $output;

            return;
        }

	    $events = $this->getFilteredEvents(
            gmdate("Y-m-d", $start),
            gmdate("Y-m-d", $end)
        );

        $out = [];
        foreach ($events as $event) {
            $out[] = $event->getEventData($sessionDayView);
        }

        $output = json_encode($out);

        $cache->set(
            $cacheKey,
            $output,
            static::CACHE_DURATION_GET_EVENTS,
            new CTagCacheDependency("BackendController::actionGetEvents")
        );

        echo $output;
	}

	public function actiontestemail()
	{
	    /** @var Event $e */
		$e = Event::model()->find();
		$e->getEventData();
	}

	public function actionsendemail()
	{
		$emails = Email::model()->findAllByAttributes(array('purpose'=>''));
		$mail = new YiiMailer();
		$mail->setLayout("");
		//$mail->setView("dailymail");
		$date = new DateTime();
		$date->modify("-1 day");
		$mail->setFrom(Yii::app()->params['adminEmail']);
		$mail->setSubject("Changes happen on ".$date->format("d-m-Y"));
		$mail->setBody($this->actionEventLog(1));
		$adds = array();

        for ($i = 0; $i < count($emails); $i++) {
            $adds[$emails[$i]->email] = $emails[$i]->name;
        }
		$mail->isSMTP();
		$mail->setTo($adds);

        if (!$mail->send()) {
            echo "ERROR " . $mail->getError();
        }
	}

    /**
     * Lists change logs for events
     * @param int $emailmode
     * @return mixed
     */
    public function actionEventLog($emailmode = 0)
    {
        if (isset($_GET['date'])) {
            $date = $_GET['date'];
        } elseif ($emailmode) {
            $date = new DateTime("-1 day");
            $date = $date->format("Y-m-d");
        } else {
            $date = date("Y-m-d");
        }

        /**
         * @var ChangeLog[] $logs
         */
        $logs = ChangeLog::model()->findAllBySql(
            "SELECT * from change_log where date(datetime)= :date order by datetime;",
            array(
                ':date' => $date,
            )
        );

        if ($emailmode) {
            return $this->renderPartial(
                "emaileventlog",
                array(
                    "logs" => $logs,
                    "date" => date("d-m-Y", strtotime($date))
                ),
                true
            );
        }

        $this->render("eventlog", array("logs" => $logs, "date" => $date));

        return null;
    }

    /**
     * Show main page with calendar (js fullCalendar)
     */
    public function actionIndex()
    {
        $_SESSION['category'] = empty($_GET['category']) ? '' : $_GET['category'];
        $_SESSION['sales']    = empty($_GET['sales']) ? '' : $_GET['sales'];
        $_SESSION['status']   = empty($_GET['status']) ? '' : $_GET['status'];

        $_SESSION['dayview'] = 0;

        $date      = isset($_GET['date']) ? $_GET['date'] : null;
        $manager   = isset($_GET['manager']) ? $_GET['manager'] : null;
        $warehouse = isset($_SESSION['warehouse']) ? $_SESSION['warehouse'] : null;

        $this->render(
            "newindex",
            array(
                'date'          => $date,
                'selected_user' => $manager,
                'warehouse'     => $warehouse,
                'manager'       => $manager,
            )
        );
    }

	public function actionSalesReport(){
		$this->render("salesreportentry");
	}

    /**
     * Generate html sales report
     */
    public function actionSalesReportGen()
    {
        $fromDate = empty($_POST['fromdate']) ? "" : $_POST['fromdate'];
        $toDate   = empty($_POST['todate']) ? "" : $_POST['todate'];
        $manager  = empty($_POST['manager']) ? "" : $_POST['manager'];
        $brand    = empty($_POST['brand']) ? "" : $_POST['brand'];
        $supplier = empty($_POST['supplier']) ? "" : $_POST['supplier'];

        $startDateTime = new DateTime($fromDate);
        $endDateTime   = new DateTime($toDate);

        $criteria = new CDbCriteria();
        $criteria->addCondition('startdate >= :startdate');
        $criteria->params[':startdate'] = $startDateTime->format("Y-m-d");

        $criteria->addCondition('startdate <= :enddate');
        $criteria->params[':enddate'] = $endDateTime->format("Y-m-d");

        if ( ! empty($brand)) {
            $criteria->addCondition('manufacturer = :manufacturer');
            $criteria->params[':manufacturer'] = $brand;
        }

        if ( ! empty($supplier)) {
            $criteria->addCondition("supplierid = :supplier");
            $criteria->params[':supplier'] = $supplier;
        }

        if ( ! empty($manager)) {
            $criteria->addCondition("manager = :manager");
            $criteria->params[':manager'] = $manager;
        }

        /** @var Event[] $events */
        $events = Event::model()->findAll($criteria);

        $this->render(
            "salesreportgen",
            array(
                'events'   => $events,
                'fromdate' => $fromDate,
                'todate'   => $toDate,
                'manager'  => $manager,
                'brand'    => $brand,
                'supplier' => $supplier,
            )
        );
    }

    /**
     * Generate CSV report
     */
    public function actionSalesReportCSV()
    {
        $fromDate = empty($_GET['fromdate']) ? "" : $_GET['fromdate'];
        $toDate   = empty($_GET['todate']) ? "" : $_GET['todate'];
        $manager  = empty($_GET['manager']) ? "" : $_GET['manager'];
        $supplier = empty($_GET['supplier']) ? "" : $_GET['supplier'];
        $brand    = empty($_GET['brand']) ? "" : $_GET['brand'];

        $out = "Manager,Event Code,Brand,Category,Supplier,Start Date,End Date,Week,Year,Warehouse,Stock Type,% Discount from RRP,Average Price Point,Total Styles,Total Units Available,Units Sold,Sell-Through %,Number Of Customers, Total cost inc. Vat., Total Sales,Buyers Sale Prediction, Predicted Vs Actual Sales(%)\n";

        $startDateTime = new DateTime($fromDate);
        $endDateTime   = new DateTime($toDate);

        $criteria = new CDbCriteria();
        $criteria->addCondition("startdate >= :startdate");
        $criteria->params[':startdate'] = $startDateTime->format("Y-m-d");

        $criteria->addCondition("enddate <= :enddate");
        $criteria->params[':enddate'] = $endDateTime->format("Y-m-d");

        if ( ! empty($supplier)) {
            $criteria->addCondition("supplierid = :supplier");
            $criteria->params[':supplier'] = $supplier;
        }

        if ( ! empty($brand)) {
            $criteria->addCondition("manufacturer = :manufacturer");
            $criteria->params[':manufacturer'] = $brand;
        }
        if ($manager != 'all' && $manager != '') {
            $criteria->addCondition("manager = :manager");
            $criteria->params[':manager'] = $manager;
        }

        /** @var Event[] $events */
        $events = Event::model()->findAll($criteria);

        foreach ($events as $event) {
            $st = new DateTime($event->startdate);
            $ed = new DateTime($event->enddate);

            /** @var Supplier $supplierModel */
            $supplierModel = Supplier::model()->findByPk($event->supplierid);

            /** @var Competition $competitionModel */
            $competitionModel = Competition::model()->findByAttributes(array('eventid' => $event->id));

            $out .= implode(
                        ",",
                        array(
                            $event->managerlist->fullname,
                            $event->eventid,
                            $event->manufacturerlist->name,
                            $event->categorylist->name,
                            $supplierModel->name,
                            $st->format("d-m-Y"),
                            $ed->format("d-m-Y"),
                            $st->format("W"),
                            $st->format("Y"),
                            $event->warehouselist->name,
                            $event->stocktype,
                            $competitionModel->avgdiscount,
                            $competitionModel->avgpricepoint,
                            $competitionModel->totalstyles,
                            $competitionModel->totunits,
                            $event->actualunitsales,
                            $event->actualsellthrough,
                            $event->noofcustomers,
                            $event->costincvat,
                            $event->actualrevenue,
                            $event->revprediction,
                            $event->predvsact,
                            $event->predvsact
                        )
                    ) . PHP_EOL;
        }

        header('Content-Type: text/csv');
        header("Content-disposition: attachment; filename=\"sales_report.csv\"");

        echo $out;
    }

    /**
     * @param ChangeLog $log
     * @return string
     */
    protected function getLogDescription($log)
    {
        $name = $log->userlist->firstname." ".$log->userlist->lastname;

        if ($log->changetype == "update") {
            $action = "changed";
        } elseif ($log->changetype == "create") {
            $action = "added";
        } else {
            $action = "deleted";
        }

        $what = "";
        $id = "";
        $extra = "";

        switch($log->changetarget) {
            case "event":
                $what = "an event";
                /** @var Event $event */
                $event = Event::model()->findByPk($log->changeid);
                if ($action != "deleted") {
                    if ($action == "added") {
                        $event = Event::model()->findByAttributes(array('eventid' => $log->newvalue));
                    }

                    if ($event != null) {
                        $id = "<a href='" . Yii::app()->createAbsoluteUrl(
                                "Event/View",
                                array("id" => $event->id)
                            ) . "'>" . $event->eventid . "</a>";
                    }
                } else {
                    $id = $log->oldvalue;
                }

                if ($action == "changed") {
                    $extra = "changing <u>" . $log->fieldname . "</u> from <i>" . $log->oldvalue . "</i> to <i>" . $log->newvalue . "</i>";
                }
                break;

            case "comment":
                $what = "a comment";
                /** @var Event $event */
                $event = Event::model()->findByPk($log->changeid);
                if($event != NULL) {
                    $id = "for <a href='".Yii::app()->createAbsoluteUrl("event/view",array("id"=>$log->changeid))."'>".$log->eventid."</a>";
                } else {
                    $id = "for ".$log->eventid;
                }
                break;

            case "task":
                $what = "task";
                $id = "for <a href='".Yii::app()->createAbsoluteUrl("event/view",array("id"=>$log->changeid))."'>".$log->eventid."</a>";
                if($action == "changed") {
                    $extra = "changing <u>".$log->fieldname."</u> from <i>".$log->oldvalue."</i> to <i>".$log->newvalue."</i>";
                }
                break;
        }

        $out = "<b>$name</b> $action $what $id $extra";

        return $out;
    }
}
