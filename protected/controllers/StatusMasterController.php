<?php

/**
 * Class StatusMasterController
 */
class StatusMasterController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array(
                    'admin',
                    'delete',
                    'create',
                    'index',
                    'update',
                    'updateevents',
                    'view',
                ),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new StatusMaster;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['StatusMaster']))
		{
			$model->attributes=$_POST['StatusMaster'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['StatusMaster']))
		{
			$model->attributes=$_POST['StatusMaster'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionupdateevents()
	{
		if(!isset($_GET['oristatus']))
		{
			echo "No Original Status provided!";
			return;
		}
		if(!isset($_GET['newstatus']))
		{
			echo "No New Status provided!";
			return;
		}
		$os = $_GET['oristatus'];
		$ns = $_GET['newstatus'];
		$m = Event::model()->findAllByAttributes(array("status"=>$os));
		for($i=0; $i < count($m); $i++)
		{
			$m[$i]->status = $ns;
			$m[$i]->save();
		}
		echo "SUCCESS";
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	
	public function actionDelete($id)
	{
		$m = Event::model()->findAllByAttributes(array('status'=>$id));
		if($m != NULL)
		{
			$status = StatusMaster::model()->findAll();
			$ids = NULL;
			for($i=0; $i < count($status); $i++)
			{
				if($status[$i]->id != $id)
				{
					$obj['id'] = $status[$i]->id;
					$obj['descpn']= $status[$i]->status;
					$ids[] = $obj;
				}
			}
			echo json_encode(array("status"=>"changeid","id"=>$id,"list"=>$ids));
			return;
		}
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new StatusMaster('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['StatusMaster']))
			$model->attributes=$_GET['StatusMaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return StatusMaster the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
	    /** @var StatusMaster $model */
		$model=StatusMaster::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param StatusMaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='status-master-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
