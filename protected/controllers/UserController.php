<?php

/**
 * Class UserController
 */
class UserController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array(
                    'admin',
                    'changepassword',
                    'create',
                    'delete',
                    'index',
                    'resetpassword',
                    'update',
                    'updatepassword',
                    'view',
                ),
                'users' => array('@'),
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionUpdatePassword()
	{
		$old = $_POST['oldpassword'];
		$new = $_POST['newpassword'];
		$renew = $_POST['renewpassword'];

        /** @var CWebApplication $app */
        $app = Yii::app();

        if ($new == "") {

            $app->user->setFlash("errormsg", "The new Password cannot be blank!");
            $this->actionChangePassword();
            return;
        }

        if ($new != $renew) {
            $app->user->setFlash("errormsg", "The new typed Passwords do not match!");
            $this->actionChangePassword();
            return;
        }

        /** @var User $user */
		$user = User::model()->findByPk($app->user->getId());

        if ($user->checkandchangepassword($old, $new)) {
            $app->user->setFlash("successmsg", "Your password has been changed successfully!");
            $this->redirect("Site/index");
        } else {
            $app->user->setFlash(
                "errormsg",
                "Failed to change password please check that you have typed you old password correctly!"
            );
            $this->actionChangePassword();
        }
	}
	
	public function actionResetPassword()
	{
		if(!isset($_GET['id']))
		{
			echo "ERROR no id was present!";
			return;
		}
		$id = $_GET['id'];

		/** @var CWebApplication $app */
		$app = Yii::app();

		/** @var WebUser $webUser */
        $webUser = $app->user;

		if($webUser->isAdmin()) {
		    /** @var User $user */
			$user = User::model()->findByPk($id);
			if($user === NULL)
			{
				echo "ERROR : user not found $id";
				return;
			}
			$newpass = $user->resetPassword();
			echo "The password has been reset to : ".$newpass;
		} else {
            echo "ERROR you are not authroized to reset password!";
        }
	}
	
	public function actionChangePassword()
	{
		$this->render("changepassword");
	}

	public function actionCreate()
	{
        $model = new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User'])) {

			$user = User::model()->findAllByAttributes(array('username'=>$_POST['User']['username']));
            if (count($user) > 0) {
                $model->addError("username", "Username already exists!");
            } else {
                $model->attributes = $_POST['User'];
                $pass = $_POST['User']['password'];
                $r = $model->changepassword($pass);
                $qry = "DELETE from buyer_notifications where userid=:userid";
                Yii::app()->db->createCommand($qry)->query(array('userid'=>$model->userid));
                if(isset($_POST['notification_mk']) && count($_POST['notification_mk'])){
                    for($i=0; $i < count($_POST['notification_mk']); $i++){
                        $userid = $_POST['notification_mk'][$i];
                        $buyer = new BuyerNotifications();
                        $buyer->buyerid = $userid;
                        $buyer->userid = $model->userid;
                        $buyer->notification_type = 'MK';
                        $buyer->save();
                    }
                }
                if(isset($_POST['notification_vt']) && count($_POST['notification_vt'])){
                    for($i=0; $i < count($_POST['notification_vt']); $i++){
                        $userid = $_POST['notification_vt'][$i];
                        $buyer = new BuyerNotifications();
                        $buyer->buyerid = $userid;
                        $buyer->userid = $model->userid;
                        $buyer->notification_type = 'VT';
                        $buyer->save();
                    }
                }
                if ($r) {
                    $this->redirect(array('view', 'id' => $model->userid));
                } else {
                    $model->password = $pass;
                }
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['User']))
		{
			if($model->username != $_POST['User']['username'])
			{
				$u = User::model()->findAllByAttributes(array("username"=>$_POST['User']['username']));
				if($u != NULL)
					$model->addError("username","This username already exists!");
			}
			
			$model->attributes=$_POST['User'];			
			if(!$model->hasErrors())
			{
				if($model->save()) {
                    $qry = "DELETE from buyer_notifications where userid=:userid";
                    Yii::app()->db->createCommand($qry)->query(array('userid'=>$model->userid));
                    if(isset($_POST['notification_mk']) && count($_POST['notification_mk'])){


                        for($i=0; $i < count($_POST['notification_mk']); $i++){
                            $userid = $_POST['notification_mk'][$i];
                            $buyer = new BuyerNotifications();
                            $buyer->buyerid = $userid;
                            $buyer->userid = $model->userid;
                            $buyer->notification_type = 'MK';
                            $buyer->save();
                        }
                    }
                    if(isset($_POST['notification_vt']) && count($_POST['notification_vt'])){
                        for($i=0; $i < count($_POST['notification_vt']); $i++){
                            $userid = $_POST['notification_vt'][$i];
                            $buyer = new BuyerNotifications();
                            $buyer->buyerid = $userid;
                            $buyer->userid = $model->userid;
                            $buyer->notification_type = 'VT';
                            $buyer->save();
                        }
                    }
                    $this->redirect(array('view', 'id' => $model->userid));
                }
				else
					$model->password =$_POST['User']['password'];
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
	    /** @var User $model */
		$model=User::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
