<?php

/**
 * Class ManufacturerController
 */
class ManufacturerController extends Controller
{
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array(
                    'admin',
                    'create',
                    'delete',
                    'index',
                    'update',
                    'view'
                ),
                'users' => array('@'),
                'expression' => array('Controller', 'allowOnlyIsVip')
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Manufacturer;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manufacturer']))
		{
			$model->attributes=$_POST['Manufacturer'];
			$mh = new ManufacturerHandler();
			$mh->handlerid = $model->handlerid;
			$mh->wefdate = date('Y-m-d');
			
			if($model->save())
			{
				$mh->manufacturerid = $model->id;
				$mh->save();
				$model->handler = $mh->handler;
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manufacturer']))
		{
			$oldid =$model->handlerid;
			$model->attributes=$_POST['Manufacturer'];
			$mh = NULL;
			if($model->handlerid != $oldid)
			{
				$mh = new ManufacturerHandler();
				$mh->handlerid = $model->handlerid;
				$mh->wefdate = date('Y-m-d');
				$mh->manufacturerid = $model->id;
			}
			if($model->save())
			{
				if($mh != NULL)
				{
					$mh->save();
					$model->handler = $mh->handler;
				}
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     * @throws CHttpException
     */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$model=new Manufacturer('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Manufacturer']))
			$model->attributes=$_GET['Manufacturer'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return Manufacturer the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
        /** @var Manufacturer $model */
		$model=Manufacturer::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param Manufacturer $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='manufacturer-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}