<?php
/**
 *  Class IssueReportsController
 */

class IssueReportsController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/backend';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions' => array(
                    'index',
                    'SaveIssue',
                ),
                'users' => array('@'),
                'expression' => array('Controller', 'allowOnlyIsVip')
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Generate the default issue report form
     */
    public function actionIndex()
    {
        $this->renderPartial("issue_report_form");
    }

    /**
     * Save the issue that was submitted from the form.
     */
    public function actionSaveIssue()
    {

        $title = empty($_POST['title']) ? "no title" : $_POST['title'];
        /** @var CWebApplication $app */
        $app = Yii::app();
        $userid = $app->user->getId();
        $steps = empty($_POST['steps']) ? "no steps details provided" : $_POST['steps'];
        $expected_results = empty($_POST['expected_results']) ? "no expected results provided" : $_POST['expected_results'];
        $what_happened = empty($_POST['what_happened']) ? "no description of what went wrong provided" : $_POST['what_happened'];

        $issue = new IssueReports();
        $issue->reported_by = $userid;
        $issue->title = $title;
        $issue->steps_to_reproduce = $steps;
        $issue->what_was_expected = $expected_results;
        $issue->what_happened = $what_happened;
        $issue->datetime = date('Y-m-d H:i:s');
        $issue->save();
        /** @var User $user */
        $user = User::model()->findByPk($userid);
        $files = [];
        for ($i = 0; $i < count($_FILES); $i++) {
            $mime = mime_content_type($_FILES[$i]['tmp_name']);

            if (!strstr($mime, "image/")) {
                echo "Opps your attached file " . $_FILES[$i]['name'] . " is not an image. Only image can be uploaded.";
                return;
            }
            $path = Yii::app()->basePath . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "issue_files" . DIRECTORY_SEPARATOR . $issue->id . "_" . $_FILES[$i]['name'] . ".jpg";
            $url_path = "/issue_files/".rawurlencode($issue->id . "_" . $_FILES[$i]['name'] . ".jpg");
            move_uploaded_file($_FILES[$i]['tmp_name'], $path);
            $files[] = Yii::App()->getBaseUrl(true).$url_path;
        }
        
        $mailer = new YiiMailer();
        $mailer->setFrom("issue_report@calendar.runwaysale.co.za", "RSC Issue Reporter");
        $mailer->addReplyTo("issue_report@calendar.runwaysale.co.za", "RSC Issue Reporter");
        $emails = Yii::app()->params['devEmails'];

        foreach ($emails as $record) {
            $mailer->AddAddress($record);
        }

        $mailer->setSubject($issue->title);
        $body = $user->fullname . " raised an issue " . $issue->title . "\n<br>";
        $plainbody = $user->fullname . " raised an issue " . $issue->title . "\n";
        $body .= "<br><b>Steps to reproduce</b><br>\n";
        $plainbody .= "Steps to reproduce.\n";
        $body .= "<p>" . $issue->steps_to_reproduce . "</p><br>\n";
        $plainbody .= "" . $issue->steps_to_reproduce . "\n";
        $body .= "<br><b>What was Expected?</b><br>\n";
        $plainbody .= "What was Expected?\n";
        $body .= "<p>" . $issue->what_was_expected . "</p>\n";
        $plainbody .= "" . $issue->what_was_expected . "\n";
        $body .= "<br><b>What was output by the program?</b><br>\n";
        $plainbody .= "What was output by the program?\n";
        $body .= "<p>" . $issue->what_happened . "</p><br>\n";
        $plainbody .= "" . $issue->what_happened . "\n";
        $body .= "<b>Posted At :</b>" . $issue->datetime . "<br>\n";
        $plainbody .= "Posted At :" . $issue->datetime . "\n";
        $plainbody .= "Attachments:\n";
        $body .= "<b>Attachments : <br>\n";

        for ($i = 0; $i < count($files); $i++) {
            $plainbody .= $files[$i]."\n";
            $body .= $files[$i]."<br>\n";
        }
        $mailer->setBody($body);
        $mailer->AltBody = $plainbody;

        if (!$mailer->send())
            echo "Mailer Error : " . $mailer->ErrorInfo;
        else
            echo "OK";
    }
}