<?php

/**
 * Class ManufacturerCategoryController
 */
class ManufacturerCategoryController extends Controller
{
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array(
                    'admin',
                    'create',
                    'delete',
                    'getlists',
                    'index',
                    'update',
                    'view',
                    'create'
                ),
                'users' => array('@'),
                'expression' => array('Controller', 'allowOnlyIsVip')
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     * @throws CHttpException
     */
	public function actionView($id)
	{
		$model = Manufacturer::model()->findByPk($id);
		if($model == NULL)
			throw new CHttpException(404,'The requested page does not exist.');
		
		$this->render('view',array(
			'model'=>$model,
		));
	}

    public function actiongetlists()
    {
        $id = $_GET['id'];
        if ($id == "") {
            return;
        }

        /** @var Manufacturer $man */
        $man = Manufacturer::model()->findByPk($id);
        if ($man == null) {
            return;
        }

        $out = array();
        $out['supplier'] = array();
        $out['category'] = array();
        for ($i = 0; $i < count($man->manufacturerSupplier); $i++) {
            $out['supplier'][] = $man->manufacturerSupplier[$i]->supplierid;
        }

        for ($i = 0; $i < count($man->manufacturerCategory); $i++) {
            $out['category'][] = $man->manufacturerCategory[$i]->category;
        }
        if($man->marketingDetails != null) {
            $out['manager'] = $man->marketingDetails->managerid;
            $out['stocktype'] = $man->marketingDetails->stocktype;
            $out['work_required'] = $man->marketingDetails->work_required;
        }
        else {
            $out['stocktype'] = null;
            $out['work_required'] = null;
            $out['manager'] = null;
        }
        echo json_encode($out);
    }

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Manufacturer;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manufacturer']))
		{
		    /** @var Manufacturer $man */
			$man = Manufacturer::model()->findByPk($_POST['Manufacturer']['id']);
			if($man != NULL){
				for($i=0; $i < count($man->manufacturerSupplier); $i++){
					$man->manufacturerSupplier[$i]->delete();
				}
				for($i=0; $i < count($man->manufacturerCategory); $i++){
					$man->manufacturerCategory[$i]->delete();
				}
				for($i=0; $i < count($_POST['category']); $i++){
					$mc = new ManufacturerCategory();
					$mc->category = $_POST['category'][$i];
					$mc->manufacturer = $man->id;
					$mc->save();
				}
				for($i=0; $i < count($_POST['supplier']); $i++){
					$ms = new ManufacturerSupplier();
					$ms->supplierid = $_POST['supplier'][$i];
					$ms->entryid = $man->id;
					$ms->save();
				}
                if($man->marketingDetails != null) {
				    $man->marketingDetails->managerid = $_POST['manager'];
                    $man->marketingDetails->stocktype = $_POST['stocktype'];
                    $man->marketingDetails->work_required = $_POST['work_required'];
                    if($man->marketingDetails->managerid != $_POST['manager']){
                        $qry = "UPDATE event set manager=:manager where startdate>=now() and manufacturer=:brand;";
                        Yii::app()->db->createCommand($qry)->query(array('manager'=>$_POST['manager'],'brand'=>$man->id));
                    }
                    $man->marketingDetails->save();
                }
                else {
				    $md = new ManufacturerMarketingDetails();
				    $md->brandid = $man->id;
				    $md->managerid = $_POST['manager'];
				    $md->stocktype = $_POST['stocktype'];
				    $md->work_required = $_POST['work_required'];
				    $md->save();
                    $qry = "UPDATE event set manager=:manager where startdate>=now() and manufacturer=:brand;";
                    Yii::app()->db->createCommand($qry)->query(array('manager'=>$_POST['manager'],'brand'=>$man->id));
                }
			}
			
				
			//print_r($_POST);
			//return;
			$this->redirect(array('view','id'=>$man->id));
		}

		$this->render('create',array(
		'model'=>$model,
		));
	}

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     * @throws CHttpException
     */
	public function actionUpdate($id)
	{
		$model=Manufacturer::model()->findByPk($id);
		if($model == NULL)
		{
			throw new CHttpException(404,'The requested page does not exist.');
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manufacturer']))
		{
		    /** @var Manufacturer $man */
			$man = Manufacturer::model()->findByPk($_POST['Manufacturer']['id']);
			if($man != NULL){
				for($i=0; $i < count($man->manufacturerSupplier); $i++){
					$man->manufacturerSupplier[$i]->delete();
				}
				for($i=0; $i < count($man->manufacturerCategory); $i++){
					$man->manufacturerCategory[$i]->delete();
				}
				for($i=0; $i < count($_POST['category']); $i++){
					$mc = new ManufacturerCategory();
					$mc->category = $_POST['category'][$i];
					$mc->manufacturer = $man->id;
					$mc->save();
				}
				for($i=0; $i < count($_POST['supplier']); $i++){
					$ms = new ManufacturerSupplier();
					$ms->supplierid = $_POST['supplier'][$i];
					$ms->entryid = $man->id;
					$ms->save();
				}
                if($man->marketingDetails != null){
				    $man->marketingDetails->stocktype = $_POST['stocktype'];
				    $man->marketingDetails->work_required = $_POST['work_required'];
				    if($man->marketingDetails->managerid != $_POST['manager']){
                        $qry = "UPDATE event set manager=:manager where startdate>=now() and manufacturer=:brand;";
                        Yii::app()->db->createCommand($qry)->query(array('manager'=>$_POST['manager'],'brand'=>$man->id));
                    }
				    $man->marketingDetails->managerid = $_POST['manager'];
				    $man->marketingDetails->save();
                }
                else {
                    $md = new ManufacturerMarketingDetails();
                    $md->managerid = $_POST['manager'];
                    $md->brandid = $man->id;
                    $md->work_required = $_POST['work_required'];
                    $md->stocktype = $_POST['stocktype'];
                    $md->save();
                    $qry = "UPDATE event set manager=:manager where startdate>=now() and manufacturer=:brand;";
                    Yii::app()->db->createCommand($qry)->query(array('manager'=>$_POST['manager'],'brand'=>$man->id));
                }
			}

			$this->redirect(array('view','id'=>$man->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     * @throws CHttpException
     */
	public function actionDelete($id)
	{
        // we only allow deletion via POST request
        if (!Yii::app()->request->isPostRequest) {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }

        /** @var Manufacturer $m */
        $m = Manufacturer::model()->findByPk($id);
        if ($m == null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

        // TODO must delete via 1 query
        // TODO may be error, relation manufacturerSupplier is absent and property is manufacturerSuppliers
        for ($i = 0; $i < count($m->manufacturerSupplier); $i++) {
            $m->manufacturerSupplier[$i]->delete();
        }

        // TODO must delete via 1 query
        for ($i = 0; $i < count($m->manufacturerCategory); $i++) {
            $m->manufacturerCategory[$i]->delete();
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
	}

	/**
	* Lists all models.
	*/
	public function actionIndex()
	{
		$this->actionAdmin();
	}

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Manufacturer('search');
        $model->unsetAttributes();

        if (isset($_GET['Manufacturer'])) {
            $model->attributes = $_GET['Manufacturer'];
        }

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

	/**
	* Returns the data model based on the primary key given in the GET variable.
	* If the data model is not found, an HTTP exception will be raised.
	* @param integer $id the ID of the model to be loaded
	* @return ManufacturerCategory the loaded model
	* @throws CHttpException
	*/
	public function loadModel($id)
	{
	    /** @var ManufacturerCategory $model */
		$model=ManufacturerCategory::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
		$model = $model->manufacturer;

		return $model;
	}

	/**
	* Performs the AJAX validation.
	* @param ManufacturerCategory $model the model to be validated
	*/
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='manufacturer-category-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}