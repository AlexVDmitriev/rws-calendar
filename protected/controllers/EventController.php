<?php

/**
 * Class EventController
 */
class EventController extends Controller
{
    const CACHE_DURATION_GET_BRAND_LIST_FOR_USER = 3600;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array(
                    'senddailymail'
                ),
                'users' => array('*'),
            ),
            array(
                'allow',
                'actions' => array(
                    'index',
                    'admin',
                    'delete',
                    'create',
                    'update',
                    'changeDateTime',
                    'addcomment',
                    'deletecomment',
                    'testrepeats',
                    'getmanudetails',
                    'getsuppdetails',
                    'view',
                    'getbrandlistforuser',
                    'getmassdstats',
                    'massdelete',
                    'copytovip',
                    'mkreportdata',
                    'mkreportlockrow',
                    'savevtdata',
                    'savemkdata'
                ),
                'users' => array('@'),
                'expression' => array('Controller', 'allowOnlyIsVip')
            ),
            array(
                'deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionSavevtdata(){
        $eventid = $_POST['eventid'];
        /* @var $evt Event */
        $evt = Event::model()->findByPk($eventid);
        if($evt == null){
            echo "Please save this event first then update the VT data.";
            return;
        }
        if($evt->marketingDetails != null){
            $evt->marketingDetails->attributes = $_POST['EventMarketingDetails'];
            $evt->marketingDetails->shouldSendEmail = 'vt';
            $evt->manager = $evt->marketingDetails->manager;
            $evt->save();
            $evt->marketingDetails->save();
        }
        else {
            $mkdt = new EventMarketingDetails();
            $mkdt->eventid = $eventid;
            $mkdt->attributes = $_POST['EventMarketingDetails'];
            $mkdt->shouldSendEmail = "vt";
            $evt->manager = $mkdt->manager;
            $evt->save();
            $mkdt->save();
        }
        echo "OK";
    }

    public function actionSavemkdata(){
        $eventid = $_POST['eventid'];
        /* @var $evt Event */
        $evt = Event::model()->findByPk($eventid);
        if($evt == null){
            echo "Please save this event first then update the VT data.";
            return;
        }
        if($evt->marketingDetails != null){
            $evt->marketingDetails->attributes = $_POST['EventMarketingDetails'];
            $evt->manager = $evt->marketingDetails->manager;
            $evt->save();
            $evt->marketingDetails->shouldSendEmail = 'mk';
            $evt->marketingDetails->save();
        }
        else {
            $mkdt = new EventMarketingDetails();
            $mkdt->eventid = $eventid;
            $mkdt->attributes = $_POST['EventMarketingDetails'];
            $evt->manager = $mkdt->manager;
            $evt->save();

            $mkdt->shouldSendEmail = "mk";
            $mkdt->save();

        }
        echo "OK";
    }

    public function actionMkreportdata(){
        $id = isset($_POST['id'])?$_POST['id']:"";
        if($id == ""){
            echo "NO event id sent!";
            return;
        }
        $data = $_POST;
        $mkrecord = null;
        $mkrecord = MkreportData::model()->findByAttributes(array('eventid'=>$id));
        if($mkrecord == NULL){
            $mkrecord = new MkreportData();
            $mkrecord->eventid = $id;
        }
        $mkrecord->modified_datetime = date('Y-m-d H:i:s');
        $mkrecord->islocked = 0;
        $mkrecord->data = json_encode($data);
        $mkrecord->save();
        echo "OK";
    }

    public function actionMkreportlockrow(){
        $id = Yii::app()->request->getPost("id","");
        if($id == ""){
            echo "NO event id sent!";
            return;
        }
        $lock = Yii::app()->request->getPost("lock","0");
        $mkrecord = null;
        $mkrecord = MkreportData::model()->findByAttributes(array('eventid'=>$id));
        if($mkrecord == NULL){
            $mkrecord = new MkreportData();
            $mkrecord->eventid = $id;
        }
        $mkrecord->modified_datetime = date('Y-m-d H:i:s');
        $mkrecord->islocked = $lock;
        $mkrecord->save();
        echo "OK";
    }
    public function actionCopyToVip(){
        $id =$_GET['id'];
        $user = User::model()->findByPk(Yii::app()->user->getId());
        if($user == NULL || !$user->has_vip_permission){
            echo "Sorry you are not allowed to move this event to VIP section, you need that permission from the admin.";
            return;
        }
        else {
            $evt = Event::model()->findByPk($id);
            $newevt = new Event();
            $newevt->attributes = $evt->attributes;
            $newevt->id = null;
            $newevt->isVip = 1;
            $newevt->repeat_styles = $evt->repeat_styles;
            $newevt->save();
            $comp = Competition::model()->findByAttributes(array('eventid'=>$evt->id));
            $newcomp = new Competition();
            $newcomp->attributes = $comp->attributes;
            $newcomp->id = null;
            $newcomp->eventid = $newevt->id;
            $newcomp->save();
            $tasks = EventTasks::model()->findAllByAttributes(array('eventid'=>$evt->id));
            for($i=0; $i < count($tasks); $i++){
                $ntask = new EventTasks();
                $ntask->attributes = $tasks[$i]->attributes;
                $ntask->eventid = $newevt->id;
                $ntask->save();
            }
            echo "OK";
        }
    }
    /**
     * Get brand list for user
     */
    public function actionGetBrandListForUser()
    {
        $id = empty($_GET['id']) ? "" : $_GET['id'];

        $cache    = Yii::app()->getCache();
        $cacheKey = __METHOD__ . "_" . $id . "_" . (int)Yii::app()->params['isVip'];

        $output = $cache->get($cacheKey);

        if ( ! empty($output)) {
            echo $output;

            return;
        }

        /** @var Manufacturer[] $manufacturers */
        if ( ! empty($id)) {
            $sql           = <<<SQL
SELECT *
FROM `manufacturer`
WHERE `id` IN (
  SELECT `manufacturerid`
  FROM `manufacturer_handler`
  WHERE `handlerid` in (:id)
)
ORDER BY `name` 
SQL;
            $manufacturers = Manufacturer::model()->findAllBySql($sql, [':id' => $id]);
        } else {
            $sql           = <<<SQL
SELECT *
FROM `manufacturer`
ORDER BY `name`
SQL;
            $manufacturers = Manufacturer::model()->findAllBySql($sql);
        }

        $output = "<option value=''>All</option>" . PHP_EOL;

        foreach ($manufacturers as $manufacturer) {
            $output .= "<option value='" . $manufacturer->id . "'>" . $manufacturer->name . "</option>" . PHP_EOL;
        }

        $cache->set(
            $cacheKey,
            $output,
            static::CACHE_DURATION_GET_BRAND_LIST_FOR_USER,
            new CTagCacheDependency("Manufacturer")
        );

        echo $output;
    }

    public function actiongetmassdstats()
    {
        $from = empty($_GET['start']) ? '' : $_GET['start'];
        $to   = empty($_GET['to']) ? '' : $_GET['to'];

        $condition = new CDbCriteria();
        $condition->addBetweenCondition('startdate', $from, $to);

        if ( ! empty($_GET['brand'])) {
            $condition->addColumnCondition(['manufacturer' => $_GET['brand']]);
        } else if ( ! empty($_GET['user'])) {
            $condition->addColumnCondition(['manager' => $_GET['user']]);
        }

        /** @var Event[] $events */
        $events = Event::model()->findAll($condition);

        $skipped  = 0;
        $starting = 0;

        foreach ($events as $event) {
            $start = new DateTime($event->startdate);
            $now   = new DateTime();
            $diff  = $now->diff($start);

            if ( ! $diff->invert) {
                $starting++;
            } else {
                $skipped++;
            }
        }

        $html = <<<HTML
    <b class='text-center'>There are {$starting} events but {$skipped} events skipped because they already started.</b>
HTML;

        echo $html;
    }

    public function actionmassdelete()
    {
        $from = empty($_GET['start']) ? '' : $_GET['start'];
        $to   = empty($_GET['to']) ? '' : $_GET['to'];

        $condition = new CDbCriteria();
        $condition->addBetweenCondition('startdate', $from, $to);

        if ( ! empty($_GET['brand'])) {
            $condition->addColumnCondition(['manufacturer' => $_GET['brand']]);
        } else if ( ! empty($_GET['user'])) {
            $condition->addColumnCondition(['manager' => $_GET['user']]);
        }

        /** @var Event[] $events */
        $events = Event::model()->findAll($condition);

        foreach ($events as $event) {
            $start = new DateTime($event->startdate);
            $now   = new DateTime();
            $diff  = $now->diff($start);

            if ( ! $diff->invert) {
                $event->delete();
            }
        }

        echo "OK";
    }

	public function actiongetmanudetails()
	{
		if(!isset($_GET['mid']))
			return;
		$mid = $_GET['mid'];

		/** @var Manufacturer $m */
		$m = Manufacturer::model()->findByPk($mid);
        if ($m != null) {
            $manager = $m->handlerid;
        } else {
            $manager = "";
        }
		
		$categories = array();
		$mcs = ManufacturerCategory::model()->findAllByAttributes(array("manufacturer"=>$mid));
		for($i=0; $i < count($mcs); $i++)
		{
			$categories[$i]['id'] = $mcs[$i]->rcategory->id;
			$categories[$i]['name'] = $mcs[$i]->rcategory->name;
		}
		$ms = ManufacturerSupplier::model()->findAllByAttributes(array("entryid"=>$mid));
		
		$suppliers = array();
		
		for($i=0; $i < count($ms); $i++)
		{
			$suppliers[$i]['id'] = $ms[$i]->supplierid;
			$suppliers[$i]['name'] = $ms[$i]->supplier->name;
		}
		
		$out = array();
		if($m->marketingDetails != null) {
            $out['stocktype'] = $m->marketingDetails->stocktype;
            $out['work_required'] = $m->marketingDetails->work_required;
            $out['mkt_manager'] = $m->marketingDetails->managerid;
        }
        else {
            $out['stocktype'] = "Non-SCS";
            $out['work_required'] = null;
            $out['mkt_manager']= null;
        }
        $out['manager'] = $manager;
		$out['suppliers'] = $suppliers;
		$out['category'] = $categories;
		echo json_encode($out);
	}

	public function actiongetsuppdetails()
	{
		if(!isset($_GET['mid']) || !isset($_GET['cid']))
			return;

		$mid = $_GET['mid'];

		/** @var Manufacturer $mc */
		$mc = Manufacturer::model()->findByPk($mid);
		$suppliers = array();
		
		for($i=0; $i < count($mc->manufacturerSupplier); $i++) {
			$suppliers[$i]['id'] = $mc->manufacturerSupplier[$i]->supplierid;
			$suppliers[$i]['name'] = $mc->manufacturerSupplier[$i]->supplier->name;
		}
		
		$out = array();
		$out['suppliers'] = $suppliers;

		echo json_encode($out);
	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        if (isset($_GET['dialog']) && $_GET['dialog'] != "") {
            $this->renderPartial('view', array('model' => $this->loadModel($id)));
        } else {
            $this->render(
                'view',
                array(
                    'model' => $this->loadModel($id),
                )
            );
        }
    }

	/**
	 * Move event from one dates to another
	 */
	public function actionChangeDateTime()
	{
	    if(empty($_GET['id'])) {
	        return;
        }

        $id = $_GET['id'];

		$startDate = $_GET['startdate'];
		$endDate = $_GET['enddate'];
		$eventsData = [];
		$reason  =$_POST['reason'];

		/** @var Event $model */
		$model = Event::model()->findByPK($id);
        if ($model === null) {
            echo json_encode(array("error" => "The event you are trying to modify does not exist!"));

            return;
        }

		$oldStart         = $model->startdate;
		$oldEnd           = $model->enddate;
		$model->startdate = $startDate;
		$model->enddate   = $endDate;

		if($model->linkedto != 0 && isset($_GET['link']) && $_GET['link'] == "1") {
			$model->linkedto = 0;
		}

        /** @var Event[] $linkedEvents */
        $linkedEvents = Event::model()->findAllByAttributes(array('linkedto'=>$model->id));

		if(!empty($linkedEvents) && isset($_GET['link'])) {

			if($_GET['link']) { // move all events
				$os = new DateTime($oldStart);
				$cs = new DateTime($model->startdate);

				$oe = new DateTime($oldEnd);
				$ce = new DateTime($model->enddate);

                $sd = $this->date_diff($os, $cs);
                $ed = $this->date_diff($oe, $ce);

                foreach ($linkedEvents as $linkedEvent) {
                    $cs = new DateTime($linkedEvent->startdate);
                    $ce = new DateTime($linkedEvent->enddate);
                    $cs = $cs->modify((($sd < 0) ? "" : "+") . $sd . " DAY");
                    $ce = $ce->modify((($ed < 0) ? "" : "+") . $ed . " DAY");
                    $linkedEvent->startdate = $cs->format("Y-m-d");
                    $linkedEvent->enddate = $ce->format("Y-m-d");
                    $linkedEvent->save();
                    $eventsData[] = $linkedEvent->getEventData(0, 1);
                }
			} else { // delink all events
                foreach ($linkedEvents as $linkedEvent) {
                    $linkedEvent->linkedto = 0;
                    $linkedEvent->eventrepeat = 0;
                    $linkedEvent->save();
                }
			}
		}

		$model->reason = $reason;

        if ( ! $model->save()) {
            echo json_encode(array("error" => "The event cannot be saved error saving!"));

            return;
        }

        /** @var UserNotifier $notifier */
        $notifier = Yii::app()->getComponent('notifier');
        $notifier->notifyAboutMove($model, $oldStart, $oldEnd);

        $eventsData[] = $model->getEventData(0, 1);

        echo json_encode(array('success' => 'OK', 'id' => $id, "events" => $eventsData));
	}

    /**
     * Create new events
     */
	public function actionCreate()
	{
        $model = new Event;
        $repeating = new Repeating();
        $competition = new Competition();

        $transactionHadRollback = false;

        if (isset($_POST['Event'])) {
            $transaction       = Yii::app()->db->beginTransaction();
            $model->attributes = $_POST['Event'];
            $model->isVip = (int)Yii::app()->params['isVip'];

            /** @var SalesBracket[] $sales */
            $sales = SalesBracket::model()->findAll();

            foreach ($sales as $sale) {
                if ($sale->from <= $model->revprediction && $sale->to >= $model->revprediction) {
                    $model->salesid = $sale->id;
                    break;
                }
            }

            if ( ! $model->save()) {
                $transaction->rollback();
                $transactionHadRollback = true;

                /** @var CWebApplication $app */
                $app = Yii::app();

                Yii::log(
                    "UserId: " . $app->user->getId() . PHP_EOL .
                    "Results: " . var_export($model->getErrors(), true) . PHP_EOL .
                    "Post: " . var_export($_POST, true),
                    'warning',
                    'event.create.has_errors'
                );
            }
            $emd = new EventMarketingDetails();
            $emd->attributes = $_POST['EventMarketingDetails'];
            $emd->eventid = $model->id;
            $emd->shouldSendEmail = null;
            $emd->save();

            if ( ! $transactionHadRollback && isset($_POST['Competition'])) {
                $competition->attributes = $_POST['Competition'];
                $competition->eventid    = $model->id;
                if ( ! $competition->save()) {
                    $transaction->rollback();
                    $transactionHadRollback = true;

                    /** @var CWebApplication $app */
                    $app = Yii::app();

                    Yii::log(
                        "UserId: " . $app->user->getId() . PHP_EOL .
                        "Results: " . var_export($competition->getErrors(), true) . PHP_EOL .
                        "Post: " . var_export($_POST, true),
                        'warning',
                        'event.create.has_errors'
                    );
                }
            }

            /*if(! $transactionHadRollback) {
                /** @var Tasks[] $tasks
                $tasks = Tasks::model()->findAll();
                foreach ($tasks as $task) {
                    $shortName          = $task->shortname;
                    $eventTask          = new EventTasks();
                    $eventTask->state   = $_POST['event-task-' . $shortName];
                    $eventTask->eventid = $model->id;
                    $eventTask->task    = $shortName;
                    if ( ! $eventTask->save()) {
                        $transaction->rollback();
                        $transactionHadRollback = true;

                        /** @var CWebApplication $app
                        $app = Yii::app();

                        Yii::log(
                            "UserId: " . $app->user->getId() . PHP_EOL .
                            "Results: " . var_export($eventTask->getErrors(), true) . PHP_EOL .
                            "Post: " . var_export($_POST, true),
                            'warning',
                            'event.create.has_errors'
                        );

                        break;
                    }
                }
            }*/

            if ( ! $transactionHadRollback && $model->eventrepeat && ! empty($_POST['Repeating'])) {
                $repeating->attributes = $_POST['Repeating'];
                $repeating->eventid    = $model->id;

                if ( ! $repeating->save()) {
                    $transaction->rollback();
                    $transactionHadRollback = true;

                    /** @var CWebApplication $app */
                    $app = Yii::app();

                    Yii::log(
                        "UserId: " . $app->user->getId() . PHP_EOL .
                        "Results: " . var_export($repeating->getErrors(), true) . PHP_EOL .
                        "Post: " . var_export($_POST, true),
                        'warning',
                        'event.create.has_errors'
                    );

                    $repeatDates = [];
                } else {
                    $repeatDates = $repeating->getRepeatDates();
                }
            } else {
                $repeatDates = [];
            }

            $eventIds = $model->extractLastNumbers(count($repeatDates));

            // create duplicated events, if current event hasn't childs events and it isn't link

            foreach ($repeatDates as $index => $repeatDate) {

                // create duplicate of base event

                $duplicateOfEvent             = new Event();
                $duplicateOfEvent->attributes = $model->attributes;

                unset($duplicateOfEvent->id);

                $duplicateOfEvent->eventid   = $eventIds[$index];
                $duplicateOfEvent->startdate = $repeatDate['from'];
                $duplicateOfEvent->enddate   = $repeatDate['to'];

                $duplicateOfEvent->linkedto = $model->id;

                if ( ! $duplicateOfEvent->save()) {
                    $transaction->rollback();
                    $transactionHadRollback = true;

                    /** @var CWebApplication $app */
                    $app = Yii::app();

                    Yii::log(
                        "UserId: " . $app->user->getId() . PHP_EOL .
                        "Results: " . var_export($duplicateOfEvent->getErrors(), true) . PHP_EOL .
                        "Post: " . var_export($_POST, true),
                        'warning',
                        'event.create.has_errors'
                    );

                    break;
                }

                /** @var EventTasks[] $tasks */
                /*$tasks = EventTasks::model()->findAllByAttributes(array('eventid' => $model->id));
                foreach ($tasks as $task) {
                    $eventTask             = new EventTasks();
                    $eventTask->attributes = $task->attributes;
                    $eventTask->eventid    = $duplicateOfEvent->id;
                    if ( ! $eventTask->save()) {
                        $transaction->rollback();
                        $transactionHadRollback = true;

                        /** @var CWebApplication $app
                        $app = Yii::app();

                        Yii::log(
                            "UserId: " . $app->user->getId() . PHP_EOL .
                            "Results: " . var_export($eventTask->getErrors(), true) . PHP_EOL .
                            "Post: " . var_export($_POST, true),
                            'warning',
                            'event.create.has_errors'
                        );

                        break 2;
                    }
                }*/

                if (isset($_POST['Competition'])) {
                    $competitionDuplicateOfEvent             = new Competition;
                    $competitionDuplicateOfEvent->attributes = $_POST['Competition'];
                    $competitionDuplicateOfEvent->eventid    = $duplicateOfEvent->id;
                    if ( ! $competitionDuplicateOfEvent->save()) {
                        $transaction->rollback();
                        $transactionHadRollback = true;

                        /** @var CWebApplication $app */
                        $app = Yii::app();

                        Yii::log(
                            "UserId: " . $app->user->getId() . PHP_EOL .
                            "Results: " . var_export($competitionDuplicateOfEvent->getErrors(), true) . PHP_EOL .
                            "Post: " . var_export($_POST, true),
                            'warning',
                            'event.create.has_errors'
                        );

                        break;
                    }
                }
            }

            if ( ! $transactionHadRollback) {
                $transaction->commit();

                /** @var UserNotifier $notifier */
                $notifier = Yii::app()->getComponent('notifier');
                $notifier->notifyAboutCreate($model);

                if (empty($_POST['backToCalendar'])) {
                    $this->redirect(array('view', 'id' => $model->id));

                    return;
                }

                /** @var CWebApplication $app */
                $app = Yii::app();
                $app->user->setFlash("success", "Event Added successfully!");

                $this->redirect(
                    Yii::app()->createUrl(
                        "Backend/index",
                        array('date' => $_GET['date'], 'manager' => $_GET['manager'])
                    )
                );

                return;
            }
        }

        $model->isNewRecord = true;

        $model->startdate = empty($_GET['fromdate']) ? "" : $_GET['fromdate'];
        $model->enddate   = empty($_GET['todate']) ? "" : $_GET['todate'];
        $model->stocktype = empty($model->stocktype) ? "Non-SCS" : $model->stocktype;

        $this->render(
            'create',
            array(
                'model'          => $model,
                'competition'    => $competition,
                'repeating'      => $repeating,
                'backToCalendar' => Yii::app()->request->getParam('backToCalendar', 0)
            )
        );
	}

	public function actionsendDailyMail()
	{
		$cdate = new DateTime(date("Y-m-d"));
		$edate = new DateTime(date("Y-m-d"));
		$edate->modify("+2 MONTH");
		$user = User::model()->findAll();
		for($i=0; $i < count($user); $i++)
		{
			if($user[$i]->emailaddress != "")
			{
                $criteria = new CDbCriteria();
                $criteria->addCondition("startdate >= :startdate");
                $criteria->params[':startdate'] = $cdate->format("Y-m-d");
                $criteria->addCondition("startdate <= :enddate");
                $criteria->params[':enddate'] = $edate->format("Y-m-d");
                $criteria->addCondition('manager = :manager');
                $criteria->params[':manager'] = $user[$i]->userid;

				$events = Event::model()->findAll($criteria);
				if($events != NULL && count($events) > 0)
				{
					$out = "<html><head></head><body><h3 class='text-center'>Hello ".$user[$i]->firstname." ".$user[$i]->lastname."</h3><br><h4>Status report for :".$cdate->format("d-m-Y")."</h4><p>";
					$out .= "<table><tr><thead><th>Manufacturer</th><th>Event Id</th><th>Start Date</th><th>Duration</th><th>Status</th></tr></thead>";
					$out .= "<tbody>";
					for($c =0; $c < count($events); $c++)
					{
						$sdate = new DateTime($events[$c]->startdate);
						$endate = new DateTime($events[$c]->enddate);
						$diff = round(($endate->format('U') - $sdate->format('U')) / (60*60*24));
						$duration = $diff;
						$out .= "<tr><td>".$events[$c]->manufacturer."</td>
						<td>".$events[$c]->eventid."</td>
						<td>".$sdate->format("d-m-Y")."</td>
						<td>".$duration." days long</td>
						<td>".$events[$c]->statuslist->status."</td>
						</tr>\n";
					}
					$out .= "</tbody>";
					$out .= "</table></body></html>";
					$mail = new YiiMailer();
					$mail->setLayout("");
					//$mail->setView("dailymail");
					$mail->setFrom(Yii::app()->params['adminEmail']);
					$mail->setSubject("Status Report on ".date("d-m-Y"));
					$mail->setBody($out);
					$mail->isSMTP();
					
					$mail->setTo($user[$i]->emailaddress);
					if(!$mail->send())
					{
						echo "ERROR";
						echo $mail->getError();
					}
				}
			}
		}
	}

    /**
     * Updates event model and related models.
     *
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if ( ! isset($_POST['Event'])) {
            /** @var EventComments[] $comments */
            $comments = EventComments::model()->findAllByAttributes(array('eventid' => $model->id));

            $competition = Competition::model()->findByAttributes(array('eventid' => $model->id));
            $competition = (empty($competition)) ? new Competition() : $competition;

            $repeats = Repeating::model()->findByAttributes(array("eventid" => $model->id));
            if ( ! empty($repeats)) {
                $model->eventrepeat = 1;
            } else {
                $repeats = new Repeating();
            }

            $this->renderPartial(
                "update",
                array(
                    "model"       => $model,
                    'comments'    => $comments,
                    'competition' => $competition,
                    'repeats'     => $repeats,
                )
            );

            return;
        }

        $transaction = Yii::app()->db->beginTransaction();

        $oldStartDate     = $model->startdate;
        $oldEndDate       = $model->enddate;
        $oldRevPrediction = $model->revprediction;

        $model->attributes = $_POST['Event'];

        if ($oldRevPrediction !== $model->revprediction && empty($_POST['reason'])) {
            $model->addError("revprediction", "Cannot change rev prediction without a reason being provided!");
        }

        if ($oldStartDate !== $model->startdate && empty($_POST['reason'])) {
            $model->addError("startdate", "Cannot change startdate with out providing a reason!");
        }

        if ($oldEndDate !== $model->enddate && empty($_POST['reason'])) {
            $model->addError("enddate", "Cannot change enddate with out providing a reason!");
        }

        if ( ! empty($_POST['reason'])) {
            $model->reason = $_POST['reason'];
        }

        /** @var SalesBracket[] $sales */
        $sales = SalesBracket::model()->findAll();
        foreach ($sales as $saleBracket) {
            if ($saleBracket->from <= $model->revprediction && $saleBracket->to >= $model->revprediction) {
                $model->salesid = $saleBracket->id;
                break;
            }
        }

        if ( ! $model->save()) {
            $transaction->rollback();

            $results = [
                'errors' => [
                    'Event' => $model->getErrors()
                ]
            ];

            echo json_encode($results);

            /** @var CWebApplication $app */
            $app = Yii::app();

            Yii::log(
                "UserId: " . $app->user->getId() . PHP_EOL .
                "Results: " . var_export($results, true) . PHP_EOL .
                "Post: " . var_export($_POST, true),
                'warning',
                'event.update.has_errors'
            );

            return;
        }
        if($model->marketingDetails != null) {
            $model->marketingDetails->attributes = $_POST['EventMarketingDetails'];
            $model->marketingDetails->shouldSendEmail = null;
            $model->marketingDetails->save();
        }
        else {
            $emd = new EventMarketingDetails();
            $emd->eventid = $model->id;
            $emd->attributes = $_POST['EventMarketingDetails'];
            $emd->shouldSendEmail = null;
            $emd->save();
        }

        if ($oldStartDate != $model->startdate || $oldEndDate != $model->enddate) {
            /** @var UserNotifier $notifier */
            $notifier = Yii::app()->getComponent('notifier');
            $notifier->notifyAboutMove($model, $oldStartDate, $oldEndDate);
        }

        if (isset($_POST['Competition'])) {
            $competition = Competition::model()->findByAttributes(array('eventid' => $model->id));

            if (empty($competition)) {
                $competition          = new Competition();
                $competition->eventid = $model->id;
            }

            $competition->attributes = $_POST['Competition'];

            if ( ! $competition->save()) {
                $transaction->rollback();

                $results = [
                    'errors' => [
                        'Competition' => $competition->getErrors()
                    ]
                ];

                echo json_encode($results);

                /** @var CWebApplication $app */
                $app = Yii::app();

                Yii::log(
                    "UserId: " . $app->user->getId() . PHP_EOL .
                    "Results: " . var_export($results, true) . PHP_EOL .
                    "Post: " . var_export($_POST, true),
                    'warning',
                    'event.update.has_errors'
                );

                return;
            }
        }

        /** @var Tasks[] $tasks */
        /*$tasks = Tasks::model()->findAll();

        foreach ($tasks as $task) {
            $shortName = $task->shortname;

            /** @var EventTasks $eventTask
            $eventTask = EventTasks::model()->findByAttributes(array('eventid' => $model->id, 'task' => $shortName));

            if (empty($eventTask)) {
                $eventTask          = new EventTasks();
                $eventTask->eventid = $model->id;
                $eventTask->task    = $shortName;
            }

            $eventTask->state = empty($_POST['event-task-' . $shortName]) ? 0 : $_POST['event-task-' . $shortName];
            if ( ! $eventTask->save()) {
                $transaction->rollback();

                // this case must be inpossible

                $results = [
                    'errors' => [
                        'EventTasks' => $eventTask->getErrors()
                    ]
                ];

                echo json_encode($results);

                /** @var CWebApplication $app
                $app = Yii::app();

                Yii::log(
                    "UserId: " . $app->user->getId() . PHP_EOL .
                    "Results: " . var_export($results, true) . PHP_EOL .
                    "Post: " . var_export($_POST, true),
                    'warning',
                    'event.update.has_errors'
                );

                return;
            }
        }
        */
        $eventsData = [
            $model->getEventData(0, 1)
        ];

        // create duplicated events, if current event hasn't childs events and it isn't link

        /** @var Event[] $childEvents */
        $childEvents = Event::model()->findAllByAttributes(array('linkedto' => $model->id));

        $hasChildEvents = empty($childEvents) ? false : true;

        if ( ! $hasChildEvents
             && empty($model->linkedto)
             && ! empty($model->eventrepeat)
             && ! empty($_POST['Repeating'])
        ) {

            /** @var Repeating $repeating */
            $repeating = Repeating::model()->findByAttributes(array('eventid' => $model->id));
            $repeating = empty($repeating) ? new Repeating() : $repeating;

            $repeating->attributes = $_POST['Repeating'];
            $repeating->eventid = $model->id;

            if ( ! $repeating->save()) {
                $transaction->rollback();

                $results = [
                    'errors' => [
                        'Repeating' => $repeating->getErrors()
                    ]
                ];

                echo json_encode($results);

                /** @var CWebApplication $app */
                $app = Yii::app();

                Yii::log(
                    "UserId: " . $app->user->getId() . PHP_EOL .
                    "Results: " . var_export($results, true) . PHP_EOL .
                    "Post: " . var_export($_POST, true),
                    'warning',
                    'event.update.has_errors'
                );

                return;
            }

            $repeatDates = $repeating->getRepeatDates();

            $eventIds = $model->extractLastNumbers(count($repeatDates));

            foreach($repeatDates as $index => $repeatDate) {
                // create duplicate of base event

                $duplicateOfEvent = new Event();
                $duplicateOfEvent->attributes = $model->attributes;

                $duplicateOfEvent->eventid = $eventIds[$index];

                unset($duplicateOfEvent->id);

                $duplicateOfEvent->startdate = $repeatDate['from'];
                $duplicateOfEvent->enddate   = $repeatDate['to'];

                $duplicateOfEvent->linkedto = $model->id;


                if ( ! $duplicateOfEvent->save()) {
                    $transaction->rollback();

                    $results = [
                        'errors' => [
                            'Event' => $duplicateOfEvent->getErrors()
                        ],
                        'isDuplicated' => true,
                    ];

                    echo json_encode($results);

                    /** @var CWebApplication $app */
                    $app = Yii::app();

                    Yii::log(
                        "UserId: " . $app->user->getId() . PHP_EOL .
                        "Results: " . var_export($results, true) . PHP_EOL .
                        "Post: " . var_export($_POST, true),
                        'warning',
                        'event.update.has_errors'
                    );

                    return;
                }


                if (isset($_POST['Competition'])) {
                    $competition             = new Competition;
                    $competition->attributes = $_POST['Competition'];
                    $competition->eventid    = $duplicateOfEvent->id;

                    if ( ! $competition->save()) {

                        $transaction->rollback();

                        $results = [
                            'errors' => [
                                'Competition' => $competition->getErrors()
                            ],
                            'isDuplicated' => true,
                        ];

                        echo json_encode($results);

                        /** @var CWebApplication $app */
                        $app = Yii::app();

                        Yii::log(
                            "UserId: " . $app->user->getId() . PHP_EOL .
                            "Results: " . var_export($results, true) . PHP_EOL .
                            "Post: " . var_export($_POST, true),
                            'warning',
                            'event.update.has_errors'
                        );

                        return;
                    }
                }

                /** @var EventTasks[] $tasks */
                $tasks = EventTasks::model()->findAllByAttributes(array('eventid' => $model->id));

                foreach ($tasks as $task) {
                    $duplicateEventTask             = new EventTasks();
                    $duplicateEventTask->attributes = $task->attributes;
                    $duplicateEventTask->eventid    = $duplicateOfEvent->id;

                    if ( ! $duplicateEventTask->save()) {
                        $transaction->rollback();

                        $results = [
                            'errors' => [
                                'EventTasks' => $duplicateEventTask->getErrors()
                            ],
                            'isDuplicated' => true,
                        ];

                        echo json_encode($results);

                        /** @var CWebApplication $app */
                        $app = Yii::app();

                        Yii::log(
                            "UserId: " . $app->user->getId() . PHP_EOL .
                            "Results: " . var_export($results, true) . PHP_EOL .
                            "Post: " . var_export($_POST, true),
                            'warning',
                            'event.update.has_errors'
                        );

                        return;
                    }
                }

                $eventsData[] = $duplicateOfEvent->getEventData(0, 1);
            }
        }

        $transaction->commit();

        echo json_encode($eventsData);
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$dr = $_GET['deleterepeat'];
		$model = $this->loadModel($id);
		$ids = [];
		$ids[] = $model->id;
		if($dr == 1){
			$er = Event::model()->findAllByAttributes(array('linkedto'=>$model->id));
			for($i=0; $i < count($er); $i++){
				$ids[] = $er[$i]->id;
				$er[$i]->delete();
			}
		}
		else if($dr == -1) {
			$er = Event::model()->findAllByAttributes(array('linkedto'=>$model->id));
			for($i=0; $i < count($er); $i++){
				$er[$i]->linkedto = 0;
				$er[$i]->save();
			}
		}
		
		$model->delete();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		else
			echo json_encode($ids);
	}

	public function actionDeleteComment()
	{
	    /** @var CWebApplication $app */
	    $app = Yii::app();
	    /** @var WebUser $webUser */
	    $webUser = $app->user;

        if (!$webUser->isAdmin()) {
            echo "ERROR";
            return;
        }

		$id = $_GET['id'];
		$m = EventComments::model()->findByPk($id);
        if ($m) {
            $m->delete();
        }

		echo "SUCCESS";
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	public function actionAddComment()
	{
		$content = $_POST['content'];
		if($content == "")
			return;
		$eventid = $_POST['id'];
		$m = new EventComments();
		$m->content = $content;
		$m->eventid = $eventid;
		$m->datetime= date("Y-m-d H:i:s");
		$dt = date("d-m-Y H:i:s",strtotime($m->datetime));

		/** @var CWebApplication $app */
		$app = Yii::app();

		$m->owner = $app->user->getId();

		if($m->save())
			echo json_encode(array("status"=>"success",
			"username"=>$m->user->firstname." ".$m->user->lastname,
			"datetime"=>$dt,
			'id'=>$m->commentid));
		else
			echo json_encode(array("status"=>"error",'reason'=>$m->getErrors()));
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        if (isset($_POST)) {
            foreach ($_POST as $idx => $val) {
                if (strstr($idx, "_") && $val != "") {
                    $arr = explode("_", $idx);
                    $name = $arr[0];
                    $id = $arr[1];

                    /** @var Event $event */
                    $event = Event::model()->findByPk($id);
                    if ($event != null) {
                        if ($name == "totunits" || $name == "totalstyles" || $name == "avgdiscount" || $name == "avgpricepoint") {
                            $event->competition->$name = $val;
                            $event->competition->save();
                        } else {
                            $event->{$name} = $val;
                            if (!$event->save())
                                print_r($event->getErrors());
                        }
                    }
                }
            }
        }

		$model=new Event('search');
		$model->unsetAttributes();  // clear any default values

        if (isset($_GET['Event'])) {
            $model->attributes = $_GET['Event'];
        }

        $manager = empty($_GET['manager']) ? null : $_GET['manager'];
        $month = empty($_GET['month']) ? null : $_GET['month'];
        $year = empty($_GET['year']) ? null : $_GET['year'];

        $this->render(
            'admin',
            array(
                'model' => $model,
                'manager' => $manager,
                'month' => $month,
                'year' => $year,
            )
        );
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Event the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
	    /** @var Event $model */
		$model=Event::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }

		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Event $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='event-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    /**
     * @param DateTime $date1
     * @param DateTime $date2
     * @return float
     */
	private function date_diff($date1,$date2)
	{
		//date2 - date1;
		return round(($date2->format("U") - $date1->format("U")) / (60*60*24));
	}

    /**
     * @param string $str
     * @return array
     */
    private function extractlastnumbers($str)
    {
        $rev = strrev($str);
        $num = "";
        $chars = "";
        $fail = 0;
        for ($i = 0; $i < strlen($rev); $i++) {
            $ch = $rev[$i];
            if (is_numeric($ch) && !$fail) {
                $num .= $ch;
            } else {
                $fail = 1;
                $chars .= $ch;
            }
        }

        return array(strrev($num), strrev($chars));
    }
}
