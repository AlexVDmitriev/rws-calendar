<?php

/**
 * Class SiteController
 */
class SiteController extends Controller
{
    public $layout='//layouts/column1';

    public function init()
    {
        $this->js = array(
            "layouts/main.js",
            "vendors/bootstrap-3.1.1.min.js",
        );

        $this->css = array(
            "bootstrap.min.css",
            "bootstrap-theme.min.css",
            "layouts/main.css",
        );
       
        parent::init();
    }

    /**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		/** @var CWebApplication $app */
		$app = Yii::app();
		if(!$app->user->isGuest)
		{
			$this->redirect(Yii::app()->createUrl('Backend/index'));
		}
		else
			$this->redirect(Yii::app()->createUrl('Site/login'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				/** @var CWebApplication $app */
				$app = Yii::app();
				$app->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;
		/** @var CWebApplication $app */
        $app = Yii::app();
		if(!$app->user->isGuest)
		{
			$this->redirect(Yii::app()->createUrl("Backend/index"));
			return;
		}	
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->createUrl("Backend/index"));
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

    /**
     * Cross domain authorization
     * @throws CHttpException
     */
    public function actionCrossAuth()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();

        if (!$app->user->isGuest) {
            $this->redirect($app->createUrl("Backend/Index"));
            return;
        }

        if (!empty($_GET['authKey']) && !empty($_GET['userId'])) {
            $model = new CrossDomainAuthForm();
            $model->userId = $_GET['userId'];
            $model->authKey = $_GET['authKey'];

            if ($model->validate() && $model->login()) {
                $this->redirect($app->createUrl("Backend/Index"));
                return;
            }
        }

        throw new CHttpException(403, "Incorrect data for cross domain authorization");
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        /** @var CWebApplication $app */
        $app = Yii::app();
        $app->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}