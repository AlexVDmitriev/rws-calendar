<?php

/**
 * This is the model class for table "manufacturer_category".
 *
 * The followings are the available columns in table 'manufacturer_category':
 * @property integer $manufacturer
 * @property integer $category
 * @property integer $entryid
 *
 * @property Category $rcategory
 * @property Manufacturer $rmanufacturer
 */
class ManufacturerCategory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manufacturer_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('manufacturer, category', 'required'),
			array('manufacturer, category', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('manufacturer, category, entryid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rcategory'=>array(self::BELONGS_TO,'Category','category'),
			'rmanufacturer'=>array(self::BELONGS_TO,'Manufacturer','manufacturer')
		);
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'manufacturer' => 'Brand',
			'category' => 'Category',
			'entryid' => 'Entryid',
			'rcategory.name'=>'Category',
			'rmanufacturer.name'=>'Manufacturer'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CSqlDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$sql = "SELECT mc.category as categoryid, mc.manufacturer as brandid, m.name as manufacturer, c.name as category, mc.entryid as entry from manufacturer_category mc, manufacturer m, category c where m.id=mc.manufacturer and c.id=mc.category";
		$arr = Yii::app()->db->createCommand($sql);
		$c = count($arr->queryAll());


        $ret = new CSqlDataProvider(
            $arr,
            array(
                'sort' => array('defaultOrder' => 'm.name'),
                'totalItemCount' => $c,
                'keyField' => 'entry',
                'pagination' => array('pageSize' => 99999),
            )
        );

		return $ret;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ManufacturerCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var ManufacturerCategory $model */
	    $model = parent::model($className);

		return $model;
	}

    /**
     * @param integer $manufacturerId
     * @return array
     */
    public static function getCategoryIdsByManufacturer($manufacturerId)
    {
        $query = "SELECT `category` FROM `manufacturer_category` WHERE `manufacturer` = :id";
        $command = Yii::app()->db->createCommand(
            $query
        );

        return $command->queryColumn(array(':id' => $manufacturerId));
    }
}
