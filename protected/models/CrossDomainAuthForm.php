<?php

/**
 * CrossDomainAuthForm class.
 */
class CrossDomainAuthForm extends CFormModel
{
    public $userId;
    public $authKey;

    private $_identity;

    public function rules()
    {
        return array(
            array('userId, authKey', 'required'),
        );
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if ($this->_identity === null) {
            $this->_identity = new CrossDomainUserIdentity($this->userId, $this->authKey);
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            /** @var CWebApplication $app */
            $app = Yii::app();
            $app->user->login($this->_identity, 0);

            return true;
        }

        return false;
    }
}
