<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property integer $id
 * @property string $manufacturer
 * @property string $eventid
 * @property string $startdate
 * @property string $enddate
 * @property integer $category
 * @property integer $manager
 * @property integer $salesid
 * @property integer $status
 * @property double $revprediction
 * @property double $orderprediction
 * @property double $unitprediction
 * @property integer $linkedto
 * @property integer $supplierid
 * @property boolean $deliverycomplete
 * @property integer $eventrepeat
 * @property double $actualunitsales
 * @property double $actualrevenue
 * @property integer $actualsellthrough
 * @property string $stocktype
 * @property double $predvsact
 * @property double $costincvat
 * @property integer $noofcustomers
 * @property boolean $isVip
 *
 * The followings are the available model relations:
 * @property Category $category0
 * @property Competition $competition
 * @property SalesBracket $sales
 * @property User $manager0
 * @property StatusMaster $status0
 * @property Category $categorylist
 * @property User $managerlist
 * @property StatusMaster $statuslist
 * @property SalesBracket $saleslist
 * @property Manufacturer $manufacturerlist
 * @property Repeating $repeat
 * @property Warehouse $warehouselist
 * @property EventComments[] $comments
 * @property EventTasks[] $tasks
 * @property Supplier $supplist
 * @property EventMarketingDetails $marketingDetails
 * @property MkreportData $report_data
 * @property bool $repeat_styles
 */
class Event extends CActiveRecord
{
    use VipTrait;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'event';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category,manufacturer, manager,salesid, status, supplierid, repeat_styles', 'numerical', 'integerOnly'=>true),
			array('revprediction,actualrevenue,actualsellthrough, actualunitsales, orderprediction, unitprediction,deliverycomplete', 'numerical'),
			array(' eventid', 'length', 'max'=>45),
			array('manufacturer,stocktype,warehouse,eventid,startdate,enddate,revprediction','required'),
			array('startdate,stocktype,noofcustomers, predvsact, costincvat, enddate', 'safe'),

            [
                implode(",",
                    [
                        'linkedto',
                        'eventrepeat',
                        'warehouse',
                        'isVip',
                    ]),
                'numerical',
                'integerOnly' => true
            ],

            // date interval validations rules
            array('startdate', 'date', 'allowEmpty' => false, 'format' => 'yyyy-MM-dd'),
            array('enddate', 'date', 'allowEmpty' => false, 'format' => 'yyyy-MM-dd'),
            array('enddate', 'validatorCompareDateTime', 'compareAttribute' => 'startdate', 'condition' => '>='),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, manufacturer, brand, eventid, startdate, enddate, category, manager, salesid, status, revprediction, orderprediction, unitprediction, isVip', 'safe', 'on'=>'search'),
		);
	}

    /**
     * @param string $attribute
     * @param array $params
     */
    public function validatorCompareDateTime($attribute, $params)
    {
        $compareAttribute = $params['compareAttribute'];
        $condition        = $params['condition'];
        if ($this->hasErrors($attribute) || $this->hasErrors($compareAttribute) || empty($this->$compareAttribute)) {
            return;
        }
        $validateValue = new DateTime($this->$attribute, new DateTimeZone(Yii::app()->getTimeZone()));
        $compareValue  = new DateTime($this->$compareAttribute, new DateTimeZone(Yii::app()->getTimeZone()));
        switch ($condition) {
            case '>=':
                if (($validateValue >= $compareValue) === false) {
                    $this->addError($attribute,
                        sprintf('The value in the "%s" field must be greater than or equal to the value in the "%s" field',
                            $this->getAttributeLabel($attribute), $this->getAttributeLabel($compareAttribute)));
                }
                break;

            case '>':
                if (($validateValue > $compareValue) === false) {
                    $this->addError($attribute,
                        sprintf('The value in the "%s" field must be greater than the value in the "%s" field',
                            $this->getAttributeLabel($attribute), $this->getAttributeLabel($compareAttribute)));
                }
                break;
        }
    }

	public $brand;

	/** @var string */
	public $reason;

    /**
     * @param int $dayview
     * @param int $new
     * @return array
     */
	public function getEventData($dayview = 0,$new =1){
	    /** @var Manufacturer $manu */
		$manu = Manufacturer::model()->findByPk($this->manufacturer);
		$evt = [];
		if($manu != NULL)
			$x = $manu->name."-".$this->eventid;
		else
			$x = "-".$this->eventid;
		
		$evt['title'] = $x;
		$evt['start'] = $this->startdate;
		$evt['data_start'] = $this->startdate;
		$evt['data_end'] = $this->enddate;

		/** @var CWebApplication $app */
		$app = Yii::app();

		/** @var User $webUser */
        $webUser = $app->user;

        $evt['url'] = ($webUser->has_admin_permission || $webUser->has_can_add_permission)
            ? Yii::app()->createUrl("Event/Update", array('id' => $this->id))
            : Yii::app()->createUrl("Event/view", array('id' => $this->id, 'dialog' => 1));

		if($dayview ==1)
			$evt['end'] = $this->startdate;
		else {
			if($new == 1){
				$dt = new DateTime($this->enddate);
				$dt = $dt->modify("+1 day");
				$evt['end'] = $dt->format("Y-m-d");
			}
			else{
				$evt['end'] = $this->enddate;
			}
			$evt['ori_end'] = $evt['end'];			
		}
		$evt['id'] = $this->id;
		$evt['allDay'] = true;
		$evt['haslinks'] = ($this->linkedto ==0 && count(Event::model()->findAllByAttributes(array('linkedto'=>$this->id)))>0)?"1":"0";
		$evt['linkedto'] = $this->linkedto;
		$evt['data_eventid'] = $this->eventid;

        if ( ! empty($manu)) {
            $evt['data_brand']   = $manu->name;
            $evt['data_brandid'] = $manu->id;
        } else {
            $evt['data_brand']   = "";
            $evt['data_brandid'] = null;
        }

		$evt['manager'] = $this->manager;

		$evt['data_manager'] = empty($this->managerlist) ? null : $this->managerlist->color;
		$evt['data_status'] = empty($this->statuslist) ? null : $this->statuslist->color;
		$evt['data_category'] = empty($this->categorylist) ? null : $this->categorylist->color;
		$evt['data_sales'] = empty($this->saleslist) ? null : $this->saleslist->color;
		$evt['data_manager_text'] = empty($this->managerlist) ? null : $this->managerlist->fullname;
		$evt['data_status_text'] = empty($this->statuslist) ? null : $this->statuslist->status;
		$evt['data_category_text'] = empty($this->categorylist) ? null : $this->categorylist->name;
        $evt['data_repeat_styles'] = empty($this->repeat_styles) ? "0" : $this->repeat_styles;
        $evt['data_stock_type'] = empty($this->stocktype)?null:$this->stocktype;
        $evt['data_stock_type_text'] = empty($this->stocktype)?null:$this->stocktype;
		$evt['data_sales_text'] = "R".number_format($this->revprediction,0,".",",");

		$evt['data_totalunitsavailable_text'] = empty($this->competition) ? null
            : ($this->competition->totunits==""?"0":$this->competition->totunits);

        $evt['data_warehouse_text'] = empty($this->warehouselist) ? null : $this->warehouselist->name;
        $evt['data_warehouse']      = empty($this->warehouselist) ? null : $this->warehouselist->color;

		$evt['data_revprediction'] = $this->revprediction;

		/** @var Tasks[] $tasks */
		$tasks = Tasks::model()->findAll();
		$taskout = "";

		for($c=0; $c < count($tasks); $c++) {
		    /** @var EventTasks $et */
			$et = EventTasks::model()->findByAttributes(array('eventid'=>$this->id,'task'=>$tasks[$c]->shortname));
			if($et != NULL) {
				if($et->state != 0) {
					$evt['data_'.$et->task] = $et->state;
					$tasklabel = "";
					if($et->state == 0)
						$tasklabel = "label-default";
					if($et->state == 1)
						$tasklabel = "label-danger";
					if($et->state == 2)
						$tasklabel = "label-warning";
					if($et->state == 3)
						$tasklabel = "label-success";
						
					$taskout .= "<label class='label $tasklabel'>".$et->task."</label> ";
				}
			}
		}

		$evt['data_task_text'] = $taskout;
		$evt['data_task'] = "#f7cff8";
		$event = $this;
		
		$obj = [];
		foreach($event->attributes as $idx=>$val){
			//echo $idx." ".$val;
			$obj[$idx]=$val;
		}

        $obj['repeat'] = (empty($event->repeat)) ?
            [
                'eventid'    => null,
                'type'       => null,
                'every'      => null,
                'everytype'  => null,
                'fromdate'   => null,
                'todate'     => null,
                'occurcount' => null,
                'occurtype'  => null
            ]
            : [
                'eventid'    => $event->repeat->eventid,
                'type'       => $event->repeat->type,
                'every'      => $event->repeat->every,
                'fromdate'   => $event->repeat->fromdate,
                'todate'     => $event->repeat->todate,
                'occurcount' => $event->repeat->occurcount,
            ];

        $obj['competition'] = (empty($event->competition)) ?
            [
                'id'            => null,
                'eventid'       => null,
                'totunits'      => null,
                'avgpricepoint' => null,
                'winner'        => null,
                'link'          => null,
                'avgdiscount'   => null,
                'totalstyles'   => null,
            ]
            : [
                'id'            => $event->competition->id,
                'eventid'       => $event->competition->eventid,
                'totunits'      => $event->competition->totunits,
                'avgpricepoint' => $event->competition->avgpricepoint,
                'winner'        => $event->competition->winner,
                'link'          => $event->competition->link,
                'avgdiscount'   => $event->competition->avgdiscount,
                'totalstyles'   => $event->competition->totalstyles,
            ];

		$obj['comments'] = [];
		for($i=0; $i < count($event->comments); $i++){
            $obj['comments'][] = array(
                'commentid' => $event->comments[$i]->commentid,
                'content' => $event->comments[$i]->content,
                'eventid' => $event->comments[$i]->eventid,
                'datetime' => $event->comments[$i]->datetime,
                'owner' => $event->comments[$i]->owner
            );
		}

		$obj['tasks'] = [];
		for($i=0; $i < count($event->tasks); $i++){
			$obj['tasks'][] = array('eventid'=>$event->tasks[$i]->eventid,
			'task'=>$event->tasks[$i]->task,
			'state'=>$event->tasks[$i]->state);
		}
		$evt['event_data'] = $obj;
		return $evt;
	}

    /**
     * @return array relational rules.
     */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categorylist' => array(self::BELONGS_TO, 'Category', 'category'),
			'saleslist' => array(self::BELONGS_TO, 'SalesBracket', 'salesid'),
			'managerlist' => array(self::BELONGS_TO, 'User', 'manager'),
			'supplist'=>array(self::BELONGS_TO,'Supplier','supplierid'),
			'manufacturerlist' => array(self::BELONGS_TO, 'Manufacturer', 'manufacturer'),
			'statuslist' => array(self::BELONGS_TO, 'StatusMaster', 'status'),
			'repeat'=> array(self::BELONGS_TO,'Repeating','id'),
			'competition'=>array(self::HAS_ONE,"Competition",'eventid'),
			'warehouselist'=>array(self::BELONGS_TO,"Warehouse","warehouse"),
			'comments'=>array(self::HAS_MANY,'EventComments','eventid'),
			'tasks'=>array(self::HAS_MANY,'EventTasks','eventid'),
            'manuMarketingDetails'=>array(self::HAS_ONE,"ManufacturerMarketingDetails",array('brandid'=>'manufacturer')),
            'marketingDetails'=>array(self::HAS_ONE,"EventMarketingDetails",'eventid'),
            'report_data'=>array(self::HAS_ONE,"MkreportData",array('eventid'=>'id'))
		);
	}

	public function getLabels($min)
	{
	    /** @var EventTasks[] $et */
		$et = EventTasks::model()->findAllBySql(
		    "SELECT * from event_tasks where eventid = :id and state >= :state",
            array(
                ':id' => $this->id,
                ':state' => $min,
            )
        );

		$out = "";
		$cols= array('default','danger','warning','primary','success');
		for($i=0; $i < count($et); $i++)
		{
			$out .= "<div class='label label-".$cols[$et[$i]->state]."'>".$et[$i]->task."</div>\n";
		}
		return $out;
	}

	protected function actionAfterFind(){
		$this->brand = $this->manufacturerlist->name;
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'actualunitsales'=> 'Actual Unit Sales',
			'actualrevenue'=>'Actual Revenue',
			'manufacturer' => 'Brand',
			'brand' => 'Brand',
			'manufacturerlist.name'=>'Brand',
			'eventid' => 'Eventid',
			'startdate' => 'Start date',
			'enddate' => 'End date',
			'supplist.name'=>'Supplier',		
			'category' => 'Category',
			'categorylist.name'=>'Category',
			'managerlist.fullname'=>'Manager',
			'statuslist.status'=>'Status',
			'saleslist.name'=>'Sales Bracket',
			'manager' => 'Manager',
			'salesid' => 'Sales Category',
			'status' => 'Status',
			'eventrepeat'=>'Repeat Event?',
			'revprediction' => 'Rev. Prediction',
			'orderprediction' => 'Order Prediction',
			'unitprediction' => 'Unit Prediction',
			'deliverycomplete'=>'Delivery Completed?',
			'actualsellthrough'=>'Actual Sell Through (%)',
			'stocktype'=>'Stock Type',
			'costincvat'=>'Total Cost Inc. Vat',
			'noofcustomers'=>'No. Of Customers',
			'predvsact'=>'Predicted vs Actual Rev. (%)',
            'isVip' => 'VIP',
            'receive_notification'=>'Receives Notification on Event change?',
            'repeat_styles'=>'Repeat styles'
		);
	}

	private function addtolog($user,$changetype,$changetarget,$fieldname,$oldvalue,$newvalue,$changeid,$eventid,$reason='')
	{
			$c = new ChangeLog();
			$c->user = $user;
			$c->changetype = $changetype;
			$c->changetarget = $changetarget;
			$c->fieldname = $fieldname;
			$c->datetime = date("Y-m-d H:i:s");
			$c->oldvalue = $oldvalue;
			$c->newvalue = $newvalue;
			$c->changeid = $changeid;
			$c->eventid = $eventid;
			$c->reason = $reason;
			$c->save();		
	}

	protected function beforeDelete()
	{
	    /** @var CWebApplication $app */
	    $app = Yii::app();
		$user = $app->user->getId();
		$changetarget = "event";
		$field = "eventid";
		$changetype="delete";
		$oldvalue = $this->eventid;
		$newvalue = $this->eventid;
		$changeid = $this->id;
		$eventid = $this->eventid;
		$changes = new Changes();
		$changes->eventid = $this->id;
		$changes->operation="DELETE";
		$changes->datetime = date('Y-m-d H:i:s');
		$changes->save();
		
		$this->addtolog($user,$changetype,$changetarget,$field,$oldvalue,$newvalue,$changeid,$eventid,$this->reason);

		$comments = EventComments::model()->findAllByAttributes(array('eventid'=>$this->id));
		for($i=0; $i < count($comments); $i++)
		{
			$comments[$i]->delete();
		}

		$tasks = EventTasks::model()->findAllByAttributes(array('eventid'=>$this->id));
		for($i=0; $i < count($tasks); $i++)
		{
			$tasks[$i]->delete();
		}

		return true;
	}

	protected function afterSave(){
		if(!$this->this_updated){
			$changes = new Changes();
			$changes->eventid = $this->id;
			$changes->operation="INSERT";
			$changes->datetime = date('Y-m-d H:i:s');
			$changes->save();
		}

        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');
        CTagCacheDependency::reset('BackendController::getFilteredEvents');
	}

    protected function afterDelete()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');
        CTagCacheDependency::reset('BackendController::getFilteredEvents');

        parent::afterDelete();
    }

    public $this_updated = false;
	
	protected function beforeSave()
	{
		if(!$this->isNewRecord) {
		    /** @var Event $m */
			$m = Event::model()->findByPk($this->id);
			if($this->startdate != $m->startdate || $this->enddate != $m->enddate) {
			    /** @var EventTasks $et */
				$et = EventTasks::model()->findByAttributes(array("eventid"=>$this->id,"task"=>"Date"));
				if($et != NULL)
				{
					if($et->state != 0)
						$et->state = 1;
					$et->save();
				}
			}
			if($this->marketingDetails != NULL) {
                $this->marketingDetails->start_datetime = $this->startdate;
                $this->marketingDetails->end_datetime = $this->enddate;
                $this->marketingDetails->start_date = $this->startdate;
                $this->marketingDetails->save();
            }
			$changes = new Changes();
			$changes->eventid = $this->id;
			$changes->operation="UPDATE";
			$changes->datetime = date('Y-m-d H:i:s');
			$changes->save();
			$this->this_updated= true;
		}

		/** @var CWebApplication $app */
		$app          = Yii::app();
		$user         = $app->user->getId();
        $changeTarget = "event";
        $changeType   = ($this->isNewRecord) ? "create" : "update";
		
		if($this->isNewRecord) {
			$oldfield = $this->eventid;
			$newfield = $this->eventid;
			$c = new ChangeLog();
			$c->user = $user;
			$c->changetype = $changeType;
			$c->changetarget = $changeTarget;
			$c->fieldname = "eventid";
			$c->datetime = date("Y-m-d H:i:s");
			$c->oldvalue = $oldfield;
			$c->newvalue = $newfield;
			$c->changeid =$this->id;
			$c->eventid = $oldfield;
			if(!$c->save()) {
				print_r($c->getErrors());
				die();
			}

			return true;
		}

		$id = $this->id;
		$eventId = $this->eventid;

        // TODO if $m is undefined, after will be error "try to get access to non object"
        if (empty($m)) {
            return false;
        }

		if($this->startdate != $m->startdate) {
			$oldvalue = date("d-m-Y",strtotime($m->startdate));
			$newvalue = date("d-m-Y",strtotime($this->startdate));
			$field = "start date";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId,$this->reason);
		}

		if($this->enddate != $m->enddate) {
			$oldvalue = date("d-m-Y",strtotime($m->enddate));
			$newvalue = date("d-m-Y",strtotime($this->enddate));
			$field = "end date";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId,$this->reason);
		}

		if($this->manufacturer != $m->manufacturer) {
			$oldvalue = $m->manufacturer;
			$newvalue = $this->manufacturer;
			$field = "manufacturer";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId);
		}

		if($this->eventid != $m->eventid) {
			$oldvalue = $m->eventid;
			$newvalue = $this->eventid;
			$field = "event ID";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId);
		}

		if($this->category != $m->category) {
			$oldvalue = $m->categorylist->name;
			$newvalue = $this->categorylist->name;
			$field = "category";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId);
		}

		if($this->manager != $m->manager) {
			$oldvalue = $m->managerlist->firstname." ".$m->managerlist->lastname;
			$newvalue = $this->managerlist->firstname." ".$this->managerlist->lastname;
			$field = "manager";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId);
		}

		if($this->salesid != $m->salesid) {
			$oldvalue = $m->saleslist->name;
			$newvalue = $this->saleslist->name;
			$field = "sales";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId);
		}

		if($this->status != $m->status) {
			$oldvalue = $m->statuslist->status;
			$newvalue = $this->statuslist->status;
			$field = "status";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId);
		}

		if($this->revprediction != $m->revprediction) {
			$oldvalue = $m->revprediction;
			$newvalue = $this->revprediction;
			$field = "revenue prediction";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId,$this->reason);
		}

		if($this->orderprediction != $m->orderprediction) {
			$oldvalue = $m->orderprediction;
			$newvalue = $this->orderprediction;
			$field = "order prediction";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId);
		}

		if($this->unitprediction != $m->unitprediction) {
			$oldvalue = $m->unitprediction;
			$newvalue = $this->unitprediction;
			$field = "unit prediction";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId);
		}

		if($this->deliverycomplete != $m->deliverycomplete) {
            $oldvalue = $m->deliverycomplete ? "Yes" : "No";
            // TODO Is it error? Why other field name
            $newvalue = $this->unitprediction ? "Yes" : "No";

			$field = "delivery complete";
			$this->addtolog($user,$changeType,$changeTarget,$field,$oldvalue,$newvalue,$id,$eventId);
		}

        $fieldsForLog = [
            'actualunitsales',
            'costincvat',
            'actualrevenue',
            'actualsellthrough',
            'noofcustomers',
            'predvsact',
        ];

        foreach ($fieldsForLog as $field) {
            if ($this->$field === $m->$field) {
                continue;
            }

            $this->addtolog(
                $user,
                $changeType,
                $changeTarget,
                $this->attributeLabels()[$field],
                $m->$field,
                $this->$field,
                $id,
                $eventId
            );
        }

        return true;
	}

    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array("manufacturerlist");
		$criteria->compare('manufacturerlist.name',$this->brand,true);
		if(isset($_GET['manager'])){
			$this->manager = $_GET['manager'];
		}
		$criteria->compare('id',$this->id);
		$criteria->compare('manufacturer',$this->manufacturer,true);
		$criteria->compare('eventid',$this->eventid,true);
		if(isset($_GET['month']) && $_GET['month'] != ""){
			$criteria->addCondition("month(startdate)=".$_GET['month']);
		}
		if(isset($_GET['year']) && $_GET['year'] != ""){
			$criteria->addCondition("year(startdate)=".$_GET['year']);
		}
		
		//$criteria->compare('startdate',$this->startdate,true);
		$criteria->compare('enddate',$this->enddate,true);
		$criteria->compare('category',$this->category);
		$criteria->compare('manager',$this->manager);
		$criteria->compare('salesid',$this->salesid);
		$criteria->compare('status',$this->status);
		$criteria->compare('revprediction',$this->revprediction);
		$criteria->compare('orderprediction',$this->orderprediction);
		$criteria->compare('unitprediction',$this->unitprediction);
		//$criteria->order = "startdate desc";
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			
			'sort'=>array(
				'defaultOrder'=>'startdate desc',
				'attributes'=>array(
				'brand'=>array(
					'asc'=>'manufacturerlist.name',
					'desc'=>'manufacturerlist.name DESC'
					),
					'*'
				)
			)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Event the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var Event $model */
	    $model = parent::model($className);

		return $model;
	}

    /**
     * @return bool
     */
    public function isEnabledRepeats()
    {
        if (empty($this->repeat) && $this->linkedto != 0) {
            return false;
        }

        if (!empty($this->repeat)) {
            return false;
        }

        return true;
    }

    /**
     * @param integer $count
     *
     * @return array
     */
    public function extractLastNumbers($count)
    {
        $rev   = strrev($this->eventid);
        $num   = "";
        $chars = "";
        $fail  = 0;
        for ($i = 0; $i < strlen($rev); $i++) {
            $ch = $rev[$i];
            if (is_numeric($ch) && ! $fail) {
                $num .= $ch;
            } else {
                $fail  = 1;
                $chars .= $ch;
            }
        }

        $num = strrev($num);
        $len = strlen($num);

        if ($len == 0) {
            $len = 2;
        }

        $num          = intval($num);
        $eventIdStart = strrev($chars);

        $eventIds = [];

        for ($i = 0; $i < $count; $i++) {
            $num++;
            $eventIds[] = $eventIdStart . sprintf("%0" . $len . "d", $num);
        }

        return $eventIds;
    }
}
