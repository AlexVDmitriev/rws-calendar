<?php

/**
 * This is the model class for table "change_log".
 *
 * The followings are the available columns in table 'change_log':
 * @property integer $id
 * @property integer $user
 * @property string $changetype
 * @property string $changetarget
 * @property string $fieldname
 * @property string $datetime
 * @property string $oldvalue
 * @property string $newvalue
 * @property integer $changeid
 * @property string $eventid
 * @property string $reason
 *
 * @property User $userlist
 */
class ChangeLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'change_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user', 'numerical', 'integerOnly'=>true),
			array('changetype, changetarget, fieldname', 'length', 'max'=>45),
			array('oldvalue, newvalue', 'length', 'max'=>100),
			array('datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user, changetype, changetarget, fieldname, datetime, oldvalue, newvalue', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'userlist'=>array(self::BELONGS_TO,'User','user')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user' => 'User',
			'changetype' => 'Changetype',
			'changetarget' => 'Changetarget',
			'fieldname' => 'Fieldname',
			'datetime' => 'Datetime',
			'oldvalue' => 'Oldvalue',
			'newvalue' => 'Newvalue',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user',$this->user);
		$criteria->compare('changetype',$this->changetype,true);
		$criteria->compare('changetarget',$this->changetarget,true);
		$criteria->compare('fieldname',$this->fieldname,true);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('oldvalue',$this->oldvalue,true);
		$criteria->compare('newvalue',$this->newvalue,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * @return Event
     */
    public function getEvent()
    {
        $criteria = new CDbCriteria();

        if ($this->changetype == 'create') {
            $criteria->addCondition("eventid = :event_id");
            $criteria->params[':event_id'] = $this->newvalue;
        } else {
            $criteria->addCondition('id = :id');
            $criteria->params[':id'] = $this->changeid;
        }

        /** @var Event $event */
        $event = Event::model()->find($criteria);

        return $event;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChangeLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var ChangeLog $model */
	    $model = parent::model($className);

		return $model;
	}
}
