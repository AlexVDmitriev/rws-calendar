<?php

/**
 * This is the model class for table "competition_month_winner".
 *
 * The followings are the available columns in table 'competition_month_winner':
 * @property string $date
 * @property integer $winner
 *
 * The followings are the available model relations:
 * @property User $winner0
 */
class CompetitionMonthWinner extends CActiveRecord
{
    use VipTrait;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'competition_month_winner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date', 'required'),
			array('winner', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('date, winner', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'winner0' => array(self::BELONGS_TO, 'User', 'winner'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'date' => 'Date',
			'winner' => 'Winner',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('date',$this->date,true);
		$criteria->compare('winner',$this->winner);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompetitionMonthWinner the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var CompetitionMonthWinner $model */
	    $model = parent::model($className);

		return $model;
	}

    /**
     * @param string $format
     * @return string
     */
    public function getDateTimeFormat($format)
    {
        $dateTime = new DateTime($this->date);

        return $dateTime->format($format);
    }
}
