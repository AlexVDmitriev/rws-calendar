<?php

/**
 * This is the model class for table "manufacturer".
 *
 * The followings are the available columns in table 'manufacturer':
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $contactperson
 * @property string $contactnumber
 * @property string $terminationdate
 *
 * The followings are the available model relations:
 * @property ManufacturerHandler $manufacturerHandlers
 * @property ManufacturerSupplier[] $manufacturerSuppliers
 * @property ManufacturerCategory[] manufacturerCategory
 * @property ManufacturerMarketingDetails marketingDetails
 */
class Manufacturer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manufacturer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, address', 'length', 'max'=>145),
			array('contactperson, contactnumber,terminationdate', 'length', 'max'=>45),
			array('handlerid','numerical','integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, address, contactperson, contactnumber', 'safe', 'on'=>'search'),
		);
	}
	public $handler;
	public $handlerid;
	/** @var  ManufacturerSupplier[] */
	public $manufacturerSupplier;

	protected function afterFind()
	{
	    /** @var ManufacturerHandler $manu */
        $manu = ManufacturerHandler::model()
            ->findBySQL(
                "SELECT * from manufacturer_handler where manufacturerid=:manu order by wefdate desc",
                array(":manu" => $this->id)
            );

        if ($manu != null) {
            $this->handler = $manu->handler;
            $this->handlerid = $manu->handlerid;
        }

        $this->manufacturerSupplier = ManufacturerSupplier::model()
            ->findAllBySql(
            "SELECT * from manufacturer_supplier where entryid = :entryid ;",
            array(
                ':entryid' => $this->id,
            )
        );

		return true;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manufacturerHandlers' => array(self::HAS_ONE, 'ManufacturerHandler', 'manufacturerid'),
			'manufacturerCategory' => array(self::HAS_MANY, 'ManufacturerCategory', 'manufacturer'),
            'marketingDetails'=>array(self::HAS_ONE,"ManufacturerMarketingDetails","brandid")
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'address' => 'Address',
			'contactperson' => 'Contactperson',
			'contactnumber' => 'Contactnumber',
			'handler.fullname'=>'RS Buyer'
		);
	}

	public function getsupplierlist()
	{
		$out = "";
		for($i=0; $i < count($this->manufacturerSupplier); $i++) {
			$out .= $this->manufacturerSupplier[$i]->supplier->name.",";
		}

		return $out;
	}

    /**
     * @return string
     */
    public function getCategoryList()
    {
        $out = "";

        /** @var ManufacturerCategory[] $manufacturerCategories */
        $manufacturerCategories = $this->manufacturerCategory;

        if (empty($manufacturerCategories)) {
            return '';
        }

        foreach ($manufacturerCategories as $manufacturerCategory) {
            if (empty($manufacturerCategory->rcategory)) {
                continue;
            }

            $out .= $manufacturerCategory->rcategory->name . ",";
        }

        return $out;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('contactperson',$this->contactperson,true);
		$criteria->compare('contactnumber',$this->contactnumber,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
			'sort'=>array('defaultOrder'=>'name asc'),
		));
	}

    public function afterSave()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');
        CTagCacheDependency::reset('Manufacturer');

        parent::afterSave();
    }

    public function afterDelete()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');
        CTagCacheDependency::reset('Manufacturer');

        parent::afterDelete();
    }

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Manufacturer the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var Manufacturer $model */
	    $model = parent::model($className);

		return $model;
	}
}
