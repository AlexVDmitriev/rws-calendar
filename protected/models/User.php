<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $userid
 * @property string $firstname
 * @property string $lastname
 * @property string $createdate
 * @property string $password
 * @property string $username
 * @property string $color
 * @property string $logintoken
 * @property string $emailaddress
 * @property string $fullname
 * @property boolean $has_admin_permission
 * @property boolean $has_can_add_permission
 * @property boolean $has_vip_permission
 * @property boolean $receive_notification
 * @property BuyerNotifications[] $subscribers
 * * @property BuyerNotifications[] $subscribed
 * @property-readonly string $fullName
 *
 * The followings are the available model relations:
 * @property Event[] $events
 */
class User extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public $fullname;

	public function tableName()
	{
		return 'user';
	}
    public $vt_notification;
	public $mk_notification;
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstname, lastname, username', 'required'),
            array(
                implode(',',
                    [
                        'active',
                        'evt_manager',
                        'has_vip_permission',
                        'has_can_add_permission',
                        'has_admin_permission',
                        'receive_notification',
                        'vt_notification',
                        'mk_notification'
                    ]),
                'numerical',
                'integerOnly' => true
            ),
			array('firstname,  lastname, password, username, color, logintoken,emailaddress', 'length', 'max'=>45),
			array('createdate', 'safe'),

            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                implode(',', [
                    'userid',
                    'firstname',
                    'active',
                    'lastname',
                    'createdate',
                    'username',
                    'color',
                    'logintoken',
                    'has_vip_permission',
                    'has_admin_permission',
                    'has_can_add_permission',
                    'receive_notification',
                ]),
                'safe',
                'on' => 'search'
            ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'events' => array(self::HAS_MANY, 'Event', 'manager'),
            'subscribers'=>array(self::HAS_MANY,"BuyerNotifications","buyerid"),
            'subscribed'=>array(self::HAS_MANY,"BuyerNotifications","userid"),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'Userid',
			'firstname' => 'First name',
			'lastname' => 'Last name',
			'createdate' => 'Create date',
			'password' => 'Password',
			'username' => 'User name',
			'active'=>'Active?',
			'color' => 'Color',
			'logintoken' => 'Logintoken',
            'vt_notification'=>'VT Notification',
            'mk_notification'=>'MK Notification',
            'receive_notification' =>'Event Notification',
            'has_vip_permission'     => 'Has access to VIP section',
            'has_can_add_permission' => 'Has access to add new and update another events',
            'has_admin_permission'   => 'Has access to admin actions',
		);
	}
	public function beforeSave()
	{
	    if($this->vt_notification){
            $eml = Email::model()->findByAttributes(array('email'=>$this->emailaddress,'purpose'=>'vt'));
            if($eml == null){
                $eml = new Email();
                $eml->name = $this->fullname;
                $eml->email = $this->emailaddress;
                $eml->purpose = 'vt';
                $eml->save();
            }
            else {
                $eml->email = $this->emailaddress;
                $eml->name = $this->fullname;
                $eml->save();
            }
        }
        else {
            $eml = Email::model()->findByAttributes(array('email'=>$this->emailaddress,'purpose'=>'vt'));
            if($eml != null)
                $eml->delete();
        }
        if($this->mk_notification){
            $eml = Email::model()->findByAttributes(array('email'=>$this->emailaddress,'purpose'=>'mk'));
            if($eml == null){
                $eml = new Email();
                $eml->name = $this->fullname;
                $eml->email = $this->emailaddress;
                $eml->purpose = 'mk';
                $eml->save();
            }
            else {
                $eml->email = $this->emailaddress;
                $eml->name = $this->fullname;
                $eml->save();
            }
        }
        else {
            $eml = Email::model()->findByAttributes(array('email'=>$this->emailaddress,'purpose'=>'mk'));
            if($eml != null)
                $eml->delete();
        }

		if($this->isNewRecord)
		{
			$this->createdate = date("Y-m-d H:i:s");
		}
		return true;
	}
	protected function afterFind()
	{
		$this->fullname = $this->firstname." ".$this->lastname;
		$this->vt_notification = Email::model()->findByAttributes(array('email'=>$this->emailaddress,'purpose'=>'vt'))==null?0:1;
        $this->mk_notification = Email::model()->findByAttributes(array('email'=>$this->emailaddress,'purpose'=>'mk'))==null?0:1;
	}	
	
	private function generateRandomString($length = 10) 
	{
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) 
		{
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
	
	public function resetPassword()
	{
		$new = $this->generateRandomString(5);
		$this->password = crypt($new,'12345');
		$this->save();
		return $new;
	}
	public function checkandchangepassword($old,$new)
	{
		if($this->password == crypt($old,'12345'))
		{
			$this->password = crypt($new,'12345');
			$r = $this->save();
			
			var_dump($r);
			return $r;
		}
		else
		{
			return false;
		}
	}

	public function changepassword($password)
	{
		$this->password = crypt($password,'12345');
		return $this->save();
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('createdate',$this->createdate,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('color',$this->color,true);
        $criteria->compare('has_vip_permission', $this->has_vip_permission, true);
        $criteria->compare('has_can_add_permission', $this->has_can_add_permission, true);
        $criteria->compare('has_admin_permission', $this->has_admin_permission, true);
        $criteria->compare('receive_notification', $this->receive_notification, true);

		return new CActiveDataProvider($this, array(
			'pagination'=>false,
			'criteria'=>$criteria,
		));
	}

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

    public function afterSave()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');

        parent::afterSave(); // TODO: Change the autogenerated stub
    }

    public function afterDelete()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');

        parent::afterDelete(); // TODO: Change the autogenerated stub
    }

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var User $model */
	    $model = parent::model($className);

		return $model;
	}
}
