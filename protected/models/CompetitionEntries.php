<?php

/**
 * This is the model class for table "competition_entries".
 *
 * The followings are the available columns in table 'competition_entries':
 * @property integer $entryid
 * @property integer $competitionid
 * @property integer $user
 * @property double $soldamount
 * @property double $revenueamount
 * @property string $datetime
 *
 * The followings are the available model relations:
 * @property Competition $competition
 * @property User $user0
 * @property User $ruser
 */
class CompetitionEntries extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'competition_entries';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('competitionid, user', 'numerical', 'integerOnly'=>true),
			array('soldamount, revenueamount', 'numerical'),
			array('datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('entryid, competitionid, user, soldamount, revenueamount, datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'competition' => array(self::BELONGS_TO, 'Competition', 'competitionid'),
			'ruser' => array(self::BELONGS_TO, 'User', 'user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'entryid' => 'Entryid',
			'competitionid' => 'Competitionid',
			'user' => 'User',
			'soldamount' => 'Soldamount',
			'revenueamount' => 'Revenueamount',
			'datetime' => 'Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('entryid',$this->entryid);
		$criteria->compare('competitionid',$this->competitionid);
		$criteria->compare('user',$this->user);
		$criteria->compare('soldamount',$this->soldamount);
		$criteria->compare('revenueamount',$this->revenueamount);
		$criteria->compare('datetime',$this->datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CompetitionEntries the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var CompetitionEntries $model */
	    $model = parent::model($className);

		return $model;
	}
}
