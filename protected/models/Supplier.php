<?php

/**
 * This is the model class for table "supplier".
 *
 * The followings are the available columns in table 'supplier':
 * @property integer $supplierid
 * @property string $name
 * @property string $address
 * @property string $contactno
 *
 * The followings are the available model relations:
 * @property ManufacturerSupplier[] $manufacturerSuppliers
 */
class Supplier extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplier';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'length', 'max'=>145),
			array('address','length','max'=>255),
			array('contactno', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('supplierid, name, address, contactno', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manufacturerSuppliers' => array(self::HAS_MANY, 'ManufacturerSupplier', 'supplier'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'supplierid' => 'Supplierid',
			'name' => 'Name',
			'address' => 'Address',
			'contactno' => 'Contactno',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('supplierid',$this->supplierid);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('contactno',$this->contactno,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
			'sort'=>array('defaultOrder'=>'name asc'),
		));
	}

    public function afterSave()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');

        parent::afterSave();
    }

    public function afterDelete()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');

        parent::afterDelete();
    }

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Supplier the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var Supplier $model */
	    $model = parent::model($className);

		return $model;
	}
}
