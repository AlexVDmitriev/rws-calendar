<?php

/**
 * This is the model class for table "repeating".
 *
 * The followings are the available columns in table 'repeating':
 * @property integer $eventid
 * @property integer $type
 * @property integer $every
 * @property integer $everytype // isn't used more
 * @property string $fromdate
 * @property string $todate
 * @property integer $occurcount
 * @property string $occurtype // isn't used more
 */
class Repeating extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'repeating';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('eventid', 'required'),
			array('eventid, type, every, occurcount, everytype', 'numerical', 'integerOnly'=>true),
			array('fromdate, todate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('eventid, type, every, everytype, fromdate, todate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'eventid' => 'Eventid',
			'type' => 'Type',
			'every' => 'Every',
			'everytype' => 'Everytype',
			'fromdate' => 'Fromdate',
			'todate' => 'Todate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('eventid',$this->eventid);
		$criteria->compare('type',$this->type);
		$criteria->compare('every',$this->every);
		$criteria->compare('fromdate',$this->fromdate,true);
		$criteria->compare('todate',$this->todate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function afterSave()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');

        parent::afterSave();
    }

    public function afterDelete()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');

        parent::afterDelete();
    }

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Repeating the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var Repeating $model */
	    $model = parent::model($className);

		return $model;
	}

    /**
     * @return array
     */
    public function getRepeatDates()
    {
        $repeatDates = array();

        /** @var Event $model */
        $model = Event::model()->findByPk($this->eventid);

        if (empty($model) || empty($this->every) || empty($this->type) || empty($this->occurcount)) {
            return [];
        }

        $startDate  =$model->startdate;
        $endDate = $model->enddate;

        $frequency = empty($this->every) ? 1 : $this->every;

        $startDateTime = new DateTime($startDate);
        $endDateTime = new DateTime($endDate);

        switch($this->type) {
            case 1:
                $increments = "MONTH";
                break;

            case 2:
                $increments = "YEAR";
                break;

            case 3:
                $increments = "WEEK";
                break;

            default:
                return [];
        }

        $newStartDateTime = new DateTime($startDateTime->format("Y-m-d"));

        $duration = round(($endDateTime->format("U") - $startDateTime->format("U")) / (60 * 60 * 24));

        for($i=0; $i < $this->occurcount-1; $i++) {
            $newStartDateTime = $newStartDateTime->modify("+".$frequency." $increments");

            $newEndDateTime = new DateTime($newStartDateTime->format("Y-m-d"));
            $newEndDateTime = $newEndDateTime->modify("+$duration DAY");

            $repeatDates[] = [
                'from' => $newStartDateTime->format("Y-m-d"),
                'to'   => $newEndDateTime->format("Y-m-d"),
            ];
        }

        return $repeatDates;
    }
}
