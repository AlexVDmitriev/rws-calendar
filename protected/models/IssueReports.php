<?php

/**
 * This is the model class for table "issue_reports".
 *
 * The followings are the available columns in table 'issue_reports':
 * @property integer $id
 * @property integer $reported_by
 * @property string $title
 * @property string $steps_to_reproduce
 * @property string $what_was_expected
 * @property string $what_happened
 * @property string $datetime
 */
class IssueReports extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'issue_reports';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('reported_by', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>100),
			array('steps_to_reproduce, what_was_expected, what_happened, datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, reported_by, title, steps_to_reproduce, what_was_expected, what_happened, datetime', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'reported_by' => 'Reported By',
			'title' => 'Title',
			'steps_to_reproduce' => 'Steps To Reproduce',
			'what_was_expected' => 'What Was Expected',
			'what_happened' => 'What Happened',
			'datetime' => 'Datetime',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('reported_by',$this->reported_by);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('steps_to_reproduce',$this->steps_to_reproduce,true);
		$criteria->compare('what_was_expected',$this->what_was_expected,true);
		$criteria->compare('what_happened',$this->what_happened,true);
		$criteria->compare('datetime',$this->datetime,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IssueReports the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
