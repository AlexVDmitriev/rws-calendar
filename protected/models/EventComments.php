<?php

/**
 * This is the model class for table "event_comments".
 *
 * The followings are the available columns in table 'event_comments':
 * @property integer $commentid
 * @property string $content
 * @property integer $eventid
 * @property string $datetime
 * @property integer $owner
 *
 * @property User $user
 */
class EventComments extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'event_comments';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('eventid, owner', 'numerical', 'integerOnly'=>true),
			array('content', 'length', 'max'=>1000),
			array('datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('commentid, content, eventid, datetime, owner', 'safe', 'on'=>'search'),
		);
	}

    protected function beforeSave()
    {
        /** @var Event $event */
		$event = Event::model()->findByPk($this->eventid);
		$c = new ChangeLog();
		/** @var CWebApplication $app */
		$app = Yii::app();
		$c->user = $app->user->getId();
		$c->changetype = "create";
		$c->changetarget = "comment";
		$c->fieldname = "commentid";
		$c->datetime = date("Y-m-d H:i:s");
		$c->oldvalue = $this->commentid;
		$c->newvalue = $this->commentid;
		$c->changeid = $this->eventid;
		$c->eventid = $event->eventid;
		$c->save();		
		return true;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user'=>array(self::BELONGS_TO,'User','owner')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'commentid' => 'Commentid',
			'content' => 'Content',
			'eventid' => 'Eventid',
			'datetime' => 'Datetime',
			'owner' => 'Owner',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('commentid',$this->commentid);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('eventid',$this->eventid);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('owner',$this->owner);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function afterSave()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');

        parent::afterSave();
    }

    public function afterDelete()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');

        parent::afterDelete();
    }

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EventComments the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var EventComments $model */
	    $model = parent::model($className);

		return $model;
	}
}
