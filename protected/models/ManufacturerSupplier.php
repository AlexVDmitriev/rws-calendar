<?php

/**
 * This is the model class for table "manufacturer_supplier".
 *
 * The followings are the available columns in table 'manufacturer_supplier':
 * @property integer $entryid
 * @property integer $supplierid
 */
class ManufacturerSupplier extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manufacturer_supplier';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('entryid, supplierid', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('entryid, supplierid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'supplier'=>array(self::BELONGS_TO,'Supplier','supplierid'),
			'manufacturer'=>array(self::BELONGS_TO,'Manufacturer','entryid')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'entryid' => 'Entryid',
			'supplierid' => 'Supplierid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('entryid',$this->entryid);
		$criteria->compare('supplierid',$this->supplierid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ManufacturerSupplier the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var ManufacturerSupplier $model */
	    $model = parent::model($className);

		return $model;
	}

    /**
     * @param integer $manufacturerId
     * @return array
     */
    public static function getSupplierIdsByManufacturer($manufacturerId)
    {
        $query = "SELECT `supplierid` FROM `manufacturer_supplier` WHERE `entryid` = :id";
        $command = Yii::app()->db->createCommand(
            $query
        );

        return $command->queryColumn(array(':id' => $manufacturerId));
    }
}
