<?php

/**
 * This is the model class for table "competition".
 *
 * The followings are the available columns in table 'competition':
 * @property integer $id
 * @property integer $eventid
 * @property integer $totunits
 * @property double $avgpricepoint
 * @property integer $winner
 * @property string $link
 * @property double $avgdiscount
 * @property string $totalstyles
 *
 * The followings are the available model relations:
 * @property Event $event
 * @property CompetitionEntries[] $competitionEntries
 */
class Competition extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'competition';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('eventid, totunits, winner', 'numerical', 'integerOnly'=>true),
			array('avgpricepoint,totalstyles, avgdiscount', 'numerical'),
			array('link', 'length', 'max'=>245),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, eventid, totunits,totalstyles, avgpricepoint, winner, link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'event' => array(self::BELONGS_TO, 'Event', 'eventid'),
			'competitionEntries' => array(self::HAS_MANY, 'CompetitionEntries', 'competitionid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'eventid' => 'Event',
			'totunits' => 'Total Numer of Units available',
			'avgpricepoint' => 'Average price Point',
			'winner' => 'Winner',
			'link' => 'Link For more Details',
			'totalstyles' => 'Total Number Of Styles',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('eventid',$this->eventid);
		$criteria->compare('totunits',$this->totunits);
		$criteria->compare('avgpricepoint',$this->avgpricepoint);
		$criteria->compare('totalstyles', $this->totalstyles);
		$criteria->compare('winner',$this->winner);
		$criteria->compare('link',$this->link,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function afterSave()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');

        parent::afterSave();
    }

    public function afterDelete()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');

        parent::afterDelete();
    }

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Competition the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var Competition $model */
	    $model = parent::model($className);

		return $model;
	}

    /**
     * @return string
     */
    public function getClosedHtmlCode()
    {
        $date = new DateTime($this->event->enddate);
        $today = new DateTime(date("Y-m-d"));
        $days = round(($date->format("U") - $today->format("U")) / (60 * 60 * 24));

        return (($days < 0) ? "<div class='label label-danger'>Closed</div> " : $days);
    }

    /**
     * @param integer $userId
     * @return CompetitionEntries
     */
    public function getCompetitionEntryByUser($userId)
    {
        /** @var CompetitionEntries $competitionEntry */
        $competitionEntry = CompetitionEntries::model()->findByAttributes(
            array(
                'user' => $userId,
                'competitionid' => $this->id
            )
        );

        if ($competitionEntry == null) {
            $competitionEntry = new CompetitionEntries();

            /** @var Event $event */
            $event = Event::model()->findByAttributes(
                array(
                    'manager' => $userId,
                    'id' => $this->eventid
                )
            );

            if (!empty($event)) {
                $competitionEntry->revenueamount = $event->revprediction;
                $competitionEntry->soldamount = $event->orderprediction;
            }
        }

        return $competitionEntry;
    }

    /**
     * @param integer $userId
     * @return CompetitionEntries[]
     */
    public function getOtherCompetitionEntriesWithoutUser($userId)
    {
        /** @var CompetitionEntries[] $competitionEntries */
        $competitionEntries = CompetitionEntries::model()->findAllBySql(
            "SELECT * from competition_entries where user != :user and competitionid = :competitionid",
            array(
                ':user' => $userId,
                ':competitionid' => $this->id,
            )
        );

        return $competitionEntries;
    }
}
