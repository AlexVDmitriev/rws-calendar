<?php

/**
 * This is the model class for table "manufacturer_marketing_details".
 *
 * The followings are the available columns in table 'manufacturer_marketing_details':
 * @property integer $brandid
 * @property integer $managerid
 * @property string $stocktype
 * @property string $work_required
 * @property Manufacturer $brand
 * @property User $user
 */
class ManufacturerMarketingDetails extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manufacturer_marketing_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('brandid', 'required'),
			array('brandid, managerid', 'numerical', 'integerOnly'=>true),
			array('stocktype, work_required', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('brandid, managerid, stocktype, work_required', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'manager'=>array(self::BELONGS_TO,"User","managerid"),
		    'brand'=>array(self::BELONGS_TO,"Manufacturer","id")
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'eventid' => 'Eventid',
			'managerid' => 'Managerid',
			'stocktype' => 'Stocktype',
			'work_required' => 'Work Required',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('eventid',$this->eventid);
		$criteria->compare('managerid',$this->managerid);
		$criteria->compare('stocktype',$this->stocktype,true);
		$criteria->compare('work_required',$this->work_required,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ManufacturerMarketingDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
