<?php

/**
 * This is the model class for table "event_tasks".
 *
 * The followings are the available columns in table 'event_tasks':
 * @property integer $eventid
 * @property string $task
 * @property integer $state
 */
class EventTasks extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'event_tasks';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('eventid', 'required'),
			array('eventid', 'numerical', 'integerOnly'=>true),
			array('task', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('eventid, task', 'safe', 'on'=>'search'),
		);
	}

    /**
     * TODO change to Formatter component
     * @param integer $state
     * @return string
     */
	private function statetotext($state)
	{
		if($state == 0) {
			return "Not Required";
		} elseif($state == 1) {
			return "Pending";
		} elseif($state == 2) {
			return "Doing";
		} elseif($state == 3) {
			return "Done";
		}

		return '';
	}
	
	protected function beforeSave()
	{
        if ($this->isNewRecord) {
            return true;
        }

		/** @var Event $event */
		$event = Event::model()->findByPk($this->eventid);

		/** @var EventTasks $m */
		$m = EventTasks::model()->findByAttributes(array('eventid'=>$this->eventid, 'task'=>$this->task));

		if($this->state != $m->state) {
			$c = new ChangeLog();

			/** @var CWebApplication $app */
			$app = Yii::app();
			$c->user = $app->user->getId();
			$c->changetype = "update";
			$c->changetarget = "task";
			$c->fieldname = $this->task;
			$c->datetime = date("Y-m-d H:i:s");
			$c->oldvalue = $this->statetotext($m->state);
			$c->newvalue = $this->statetotext($this->state);
			$c->changeid = $this->eventid;
			$c->eventid = $event->eventid;
			
			$c->save();
		}

		return true;		
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'event'=>array(self::BELONGS_TO,'Event','eventid'),
			'tasko'=>array(self::BELONGS_TO,'Tasks','shortname'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'eventid' => 'Eventid',
			'task' => 'Task',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('eventid',$this->eventid);
		$criteria->compare('task',$this->task,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function afterSave()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');
        CTagCacheDependency::reset('BackendController::getFilteredEvents');

        parent::afterSave();
    }

    public function afterDelete()
    {
        // reset tags
        CTagCacheDependency::reset('BackendController::actionGetNewEvents');
        CTagCacheDependency::reset('BackendController::actionGetEvents');
        CTagCacheDependency::reset('BackendController::getFilteredEvents');

        parent::afterDelete();
    }

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EventTasks the static model class
	 */
	public static function model($className=__CLASS__)
	{
	    /** @var EventTasks $model */
	    $model = parent::model($className);

		return $model;
	}
}
