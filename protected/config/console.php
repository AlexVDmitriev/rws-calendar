<?php

$local = !file_exists(__DIR__ . DIRECTORY_SEPARATOR . "local.php")
    ? []
    : require __DIR__ . DIRECTORY_SEPARATOR . "local.php";

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',

	// preloading 'log' component
	'preload'=>array('log'),

	// application components
	'components'=>array(

        'cache' => array(
            'class'        => 'CMemCache',
            'useMemcached' => true,
            'keyPrefix' => Yii::app()->params['cacheKeyPrefix'],
            'servers'      => array(
                array(
                    'host'   => 'localhost',
                    'port'   => 11211,
                    'weight' => 100,
                ),
            ),
        ),

        'db' => array_merge(
            array(
                'connectionString' => 'mysql:host=localhost;dbname=calendar',
                'emulatePrepare' => true,
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8',
            ),
            empty($local['db']) ? [] : $local['db']
        ),

        'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array_merge(
        array(
            // this is used in contact page
            'adminEmail' => 'calendar@runwaysale.co.za',

            // cache default settings
            'cacheKeyPrefix' => 'calendar_',

            // deployment default settings
            'deploymentPath' => '/var/www/html',
            'deploymentUser' => 'www-data',

            'isVip' => false,
        ),
        empty($local['params']) ? [] : $local['params']
    ),
);
