<?php

$local = !file_exists(__DIR__ . DIRECTORY_SEPARATOR . "local.php")
    ? []
    : require __DIR__ . DIRECTORY_SEPARATOR . "local.php";

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name'=>'RunwaySale',
	'aliases'=>array('bootstrap'=>'ext.bootstrap3'),
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'bootstrap.widgets.*',
		'bootstrap.behaviors.*',
		'bootstrap.helpers.*',
		'ext.yiimailer.YiiMailer',
		'ext.cal.CalModule.php',
	),

	'modules'=>array(
        'cal'=>array('debug'=>true),

		// uncomment the following to enable the Gii tool
        /*'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'abc123',
			'generatorPaths'=>array('bootstrap.gii'),
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('*'),
		),*/
	),

	// application components
	'components'=>array(
		'bootstrap'=>array('class'=>'bootstrap.components.BsApi'),

        'user'=>array(
			// enable cookie-based authentication
			'class'=>'WebUser',
			'allowAutoLogin'=>true,
		),

        'cache' => array(
	        'class'        => 'CMemCache',
            'useMemcached' => true,
            'keyPrefix'    => Yii::app()->params['cacheKeyPrefix'],
            'servers'      => array(
                array(
                    'host'   => 'localhost',
                    'port'   => 11211,
                    'weight' => 100,
                ),
            ),
        ),

        'db' => array_merge(
            array(
                'connectionString' => 'mysql:host=localhost;dbname=calendar',
                'emulatePrepare' => true,
                'username' => 'root',
                'password' => '',
                'charset' => 'utf8',
            ),
            empty($local['db']) ? [] : $local['db']
        ),

        'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

        'notifier' => array(
            'class' => 'UserNotifier',
        ),
	),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array_merge(
        array(
            // this is used in contact page
            'adminEmail' => 'calendar@runwaysale.co.za',

            // cache default settings
            'cacheKeyPrefix' => 'calendar_',

            // mails of dev team
            'devEmails' => array('niksad8@gmail.com', 'cto@psystems.biz', 'randolf.s@runwaysale.co.za'),

            'isVip' => false,

            'vipDomain' => 'vip.calendar.runwaysale.co.za',
            'commonDomain' => 'calendar.runwaysale.co.za',
        ),
        empty($local['params']) ? [] : $local['params']
    ),
);
