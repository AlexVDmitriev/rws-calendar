<?php

/**
 * Class DeployCommand
 */
class DeployCommand extends CConsoleCommand
{
    /**
     * Update dependencies
     * @return bool
     */
    public function actionDependency()
    {
// TODO enable when we will have used composer
//        $command = "composer update --no-dev";
//        echo $command . PHP_EOL;
//
//        system($command, $exitCode);
//
//        return $exitCode === 0;

        return true;
    }

    /**
     * Copy files
     * @return bool
     */
    public function actionCopy()
    {
        $commandMask = "rsync %s %s";

        if (!empty(Yii::app()->params['deploymentUser'])) {
            $commandMask = sprintf("sudo -u %s %s", Yii::app()->params['deploymentUser'], $commandMask);
        }

        $commandArguments = array(
            "-av",
            "--owner",
            "--group",
            "--ignore-times",
            "--checksum",
            "--links",
            "--perms",
            "--recursive",
            "--delete",
            "--size-only",
            "--force",
            "--numeric-ids",
            "--stats",
            "--exclude .git",
            "--exclude protected/runtime",
            "--exclude assets",
            "--exclude issue_files"
        );

        $command = sprintf(
            $commandMask . " " . implode(" ", $commandArguments),
            __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "..",
            Yii::app()->params['deploymentPath']
        );

        echo $command . PHP_EOL;

        system($command, $exitCode);

        return $exitCode === 0;
    }

    /**
     * Apply migrations
     * @return bool
     */
    public function actionMigrate()
    {
        $commandMask = "php %s/protected/yiic migrate --interactive=0";

        if (!empty(Yii::app()->params['deploymentUser'])) {
            $commandMask = sprintf("sudo -u %s %s", Yii::app()->params['deploymentUser'], $commandMask);
        }

        $command = sprintf($commandMask, Yii::app()->params['deploymentPath']);

        echo $command . PHP_EOL;

        system($command, $exitCode);

        return $exitCode === 0;
    }

    /**
     * Remove temp assets files
     * @return bool
     */
    public function actionReset()
    {
        $commandMask = "rm -rf %s/assets/*";

        if (!empty(Yii::app()->params['deploymentUser'])) {
            $commandMask = sprintf("sudo -u %s %s", Yii::app()->params['deploymentUser'], $commandMask);
        }

        $command = sprintf($commandMask, Yii::app()->params['deploymentPath']);

        echo $command . PHP_EOL;

        system($command, $exitCode);

        return $exitCode === 0;
    }

    /**
     * Full deployment process to deploymentPath
     */
    public function actionIndex()
    {
        $this->actionDependency() or die('[!] Error: can\'t update dependencies.');
        $this->actionCopy() or die('[!] Error: can\'t copy files.');
        $this->actionMigrate() or die('[!] Error: can\'t apply migrations.');
        $this->actionReset() or die('[!] Error: can\'t remove temp assets files.');
    }
}
