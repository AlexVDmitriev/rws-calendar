<?php

class m180115_135814_modify_event_marketing_details extends CDbMigration
{
	public function up()
	{
        $this->addColumn("event_marketing_details","repeats_done","tinyint");
	}

	public function down()
	{
	    $this->dropColumn("event_marketing_details","repeats_done");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}