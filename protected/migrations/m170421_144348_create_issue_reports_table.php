<?php

class m170421_144348_create_issue_reports_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('issue_reports',array(
			'id'=>'pk',
			'reported_by'=>'integer',
			'title'=>'string',
			'steps_to_reproduce'=>'text',
			'what_was_expected'=>'text',
			'what_happened'=>'text',
			'datetime'=>'datetime'
		));
	}

	public function down()
	{
		$this->dropTable("issue_reports");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
