<?php

/**
 * Class m170617_000653_update_users_table_notifications
 */
class m170617_000653_update_users_table_notifications extends CDbMigration
{
	public function up()
	{
        $this->addColumn('user', 'receive_notification', 'TINYINT(1) NOT NULL DEFAULT 0');
	}

	public function down()
	{
        $this->dropColumn('user', 'receive_notification');
	}
}
