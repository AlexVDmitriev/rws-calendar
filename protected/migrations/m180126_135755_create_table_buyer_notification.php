<?php

class m180126_135755_create_table_buyer_notification extends CDbMigration
{
	public function up()
	{
	    $this->createTable('buyer_notifications',array(
	        'id'=>'pk',
            'buyerid'=>'int',
            'userid'=>'int',
            'notification_type'=>'VARCHAR(30)'
        ));
	}

	public function down()
	{
	    $this->dropTable('buyer_notifications');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}