<?php

class m180116_202542_modify_event_marketing_details extends CDbMigration
{
	public function up()
	{
	    $this->alterColumn("event_marketing_details","start_datetime",'DATE');
        $this->alterColumn("event_marketing_details","end_datetime",'DATE');
	}

	public function down()
	{
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}