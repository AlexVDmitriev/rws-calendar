<?php

/**
 * Class m170530_235558_drop_column__usertype__from__user
 */
class m170530_235558_drop_column__usertype__from__user extends CDbMigration
{
    public function up()
    {
        $this->dropColumn('user', 'usertype');
    }

    public function down()
    {
        $this->addColumn('user', 'usertype', "int(11) DEFAULT NULL COMMENT '0=readonly\n1=createevents\n2=admin'");

        $this->update('user', ['usertype' => 1], '`has_can_add_permission` = 1');

        $this->update('user', ['usertype' => 2], '`has_admin_permission` = 1');
    }
}
