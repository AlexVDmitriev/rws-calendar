<?php

class m180111_110951_edit_manufacturer_handler extends CDbMigration
{
	public function up()
	{
        $this->addColumn("manufacturer_handler","stock_type","VARCHAR(50)");
        $this->addColumn("manufacturer_handler","work_required","VARCHAR(50)");
	}

	public function down()
	{
	    $this->dropColumn("manufacturer_handler","stock_type");
        $this->dropColumn("manufacturer_handler","work_required");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}