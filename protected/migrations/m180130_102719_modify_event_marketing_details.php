<?php

class m180130_102719_modify_event_marketing_details extends CDbMigration
{
	public function up()
	{
	    $this->addColumn("event_marketing_details","final_destination",'VARCHAR(100)');
	}

	public function down()
	{
	    $this->dropColumn("event_marketing_details","final_destination");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}