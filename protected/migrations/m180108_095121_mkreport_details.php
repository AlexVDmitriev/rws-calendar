<?php

class m180108_095121_mkreport_details extends CDbMigration
{
	public function up()
	{
	    $this->createTable('mkreport_data',array(
	        'id'=>'pk',
            'eventid'=>'int',
            'data'=>'TEXT',
            'modified_datetime'=>'DATETIME',
            'islocked'=>'TINYINT'
        ));
	}

	public function down()
	{
	    $this->dropTable('mkreport_data');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}