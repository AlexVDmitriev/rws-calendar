<?php

class m170824_034945_alter_event_table_repeat_styles extends CDbMigration
{
    public function up()
    {
        $this->addColumn('event', 'repeat_styles', 'TINYINT(1) NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('event', 'repeat_styles');
    }
}