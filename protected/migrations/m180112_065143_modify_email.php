<?php

class m180112_065143_modify_email extends CDbMigration
{
	public function up()
	{
	    $this->addColumn("email","purpose","VARCHAR(50)");
	}

	public function down()
	{
	    $this->dropColumn("email","purpose");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}