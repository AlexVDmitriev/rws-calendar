<?php

class m170601_092600_add_column__isVip__to__event extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('event', 'isVip', "TINYINT(1) NOT NULL DEFAULT 0");
    }

    public function safeDown()
    {
        $this->dropColumn('event', 'isVip');
    }
}
