<?php

/**
 * Class m170530_215619_add_columns__has_vip_permission__to__user
 */
class m170530_215619_add_columns__has_vip_permission__to__user extends CDbMigration
{
    public function up()
    {
        $this->addColumn('user', 'has_vip_permission', 'TINYINT(1) NOT NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('user', 'has_vip_permission');
    }
}
