<?php

class m180108_095107_event_details extends CDbMigration
{
	public function up()
	{
	    $this->createTable("event_marketing_details",array(
	        'id'=>'pk',
            'eventid'=>'int',
            'event_name'=>'VARCHAR(60)',
            'gender_product'=>'VARCHAR(60)',
            'start_datetime'=>'DATETIME',
            'end_datetime'=>'DATETIME',
            'manager'=>'int',
            'lifestyle_images'=>'VARCHAR(255)',
            'event_link'=>'VARCHAR(255)',
            'product_images'=>'VARCHAR(255)',
            'max_discount'=>'DOUBLE',
            'lowest_price'=>'DOUBLE',
            'delivery'=>'VARCHAR(100)',
            'promotions'=>'VARCHAR(100)',
            'extra_comments'=>'TEXT',
            'brand'=>'int',
            'start_date'=>'date',
            'work_required'=>'VARCHAR(50)',
            'samples_arrived_date'=>'DATE',
            'sample_type'=>'VARCHAR(70)',
            'gender'=>'VARCHAR(40)',
            'sample_quantity'=>'int',
            'extra_comments2'=>'TEXT'
        ));
	}

	public function down()
	{
		$this->dropTable("event_marketing_details");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}