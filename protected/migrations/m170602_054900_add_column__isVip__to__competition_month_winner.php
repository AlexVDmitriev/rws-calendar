<?php

class m170602_054900_add_column__isVip__to__competition_month_winner extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('competition_month_winner', 'isVip', "TINYINT(1) NOT NULL DEFAULT 0");
    }

    public function safeDown()
    {
        $this->dropColumn('competition_month_winner', 'isVip');
    }
}