<?php

class m180115_103242_create_manufacturer_marketing_details extends CDbMigration
{
	public function up()
	{
	    $this->createTable("manufacturer_marketing_details",array(
	        'brandid'=>'pk',
            'managerid'=>'integer',
            'stocktype'=>'VARCHAR(50)',
            'work_required'=>'VARCHAR(50)'
        ));
	}

	public function down()
	{
	    $this->dropTable("manufacturer_marketing_details");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}