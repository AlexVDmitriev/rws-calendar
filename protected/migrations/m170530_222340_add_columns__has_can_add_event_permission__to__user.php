<?php

/**
 * Class m170530_222340_add_columns__has_can_add_event_permission__to__user
 */
class m170530_222340_add_columns__has_can_add_event_permission__to__user extends CDbMigration
{
    public function up()
    {
        $this->addColumn('user', 'has_can_add_permission', 'TINYINT(1) NOT NULL DEFAULT 0');

        $this->update('user', ['has_can_add_permission' => 1], '`usertype` = 1');
    }

    public function down()
    {
        $this->dropColumn('user', 'has_can_add_permission');
    }
}
