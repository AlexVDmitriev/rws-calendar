<?php

/**
 * Class m170530_222320_add_columns__has_admin_permission__to__user
 */
class m170530_222320_add_columns__has_admin_permission__to__user extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('user', 'has_admin_permission', 'TINYINT(1) NOT NULL DEFAULT 0');

        $this->update('user', ['has_admin_permission' => 1], '`usertype` = 2');
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'has_admin_permission');
    }
}
