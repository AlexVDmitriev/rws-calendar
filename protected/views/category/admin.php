<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Manage',
);
?>
<h1>Manage Categories</h1>
<button class='btn btn-primary pull-right' type='button' onclick='document.location.href="<?php echo Yii::app()->createUrl("category/create"); ?>"'><span class='fa fa-plus'></span>Add New Category</button>

<?php $this->widget('bootstrap.widgets.BsGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>$model->search(),
	'enableSorting'=>true,
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		array('name'=>'color','value'=>'\'<div style="width:30px; height:30px; background-color:#\'.$data->color.\'"></div>\'','type'=>'raw'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
