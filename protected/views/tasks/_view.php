<?php
/* @var $this TasksController */
/* @var $data Tasks */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('shortname')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->shortname), array('view', 'id'=>$data->shortname)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taskname')); ?>:</b>
	<?php echo CHtml::encode($data->taskname); ?>
	<br />


</div>