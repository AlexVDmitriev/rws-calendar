<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	'Manage',
);
?>

<h1>Manage Tasks</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tasks-grid',
	'columns'=>array(
		'taskname',
		'shortname',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
