<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	$model->shortname,
);
?>
<h1>View Tasks #<?php echo $model->shortname; ?></h1>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'taskname',
		'shortname',
	),
)); ?>
