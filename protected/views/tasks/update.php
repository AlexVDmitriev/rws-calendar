<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	$model->shortname=>array('view','id'=>$model->shortname),
	'Update',
);

?>
<h1>Update Tasks <?php echo $model->shortname; ?></h1>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>