<?php
/* @var $this TasksController */
/* @var $model Tasks */

$this->breadcrumbs=array(
	'Tasks'=>array('index'),
	'Create',
);

?>

<h1>Create Tasks</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>