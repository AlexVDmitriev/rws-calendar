<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->userid,
);
?>

<h1>View User #<?php echo $model->userid; ?></h1>

<?php
$backgroundColor = "#" . $model->color;
$this->widget(
    'bootstrap.widgets.BsDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'userid',
            'firstname',
            'lastname',
            'username',
            'emailaddress',
            'active:boolean',
            'evt_manager:boolean',
            array(
                'name' => 'color',
                'value' => '<div style="width:30px; height:30px; background-color:' . $backgroundColor . '"></div>',
                'type' => 'raw'
            ),
            array(
                'name' => 'createdate',
                'value' => date("d-m-Y", strtotime($model->createdate))
            ),
            'has_vip_permission:boolean',
            'has_admin_permission:boolean',
            'has_can_add_permission:boolean',
            'receive_notification:boolean',
            'vt_notification:boolean',
            'mk_notification:boolean',
        ),
    )
);
?>
