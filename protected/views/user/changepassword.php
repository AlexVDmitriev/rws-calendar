<h1>Change your Password</h1>
<hr><p>
<form method='post' action='<?php echo Yii::app()->createUrl("User/updatepassword") ?>'>
<div class='container'>
	<div class='row col-xs-4 col-xs-offset-4'>
		<div class='panel panel-default'>
			<div class='panel-heading'>
				<h2>Reset Password</h2>
			</div>
			<div class='panel-body'>
				<div class='form-group'>
					<label for="oldpassword" >Please enter your current Password : </label>
					<input id="oldpassword" type='password' class='form-control' name='oldpassword'>
				
				</div>
				<div class='form-group'>
					<label for="newpassword">Enter New Password : </label>
					<input id="newpassword" type='password' class='form-control' name='newpassword'>
				</div>			
				<div class='form-group'>
					<label for="renewpassword">Re-Enter New Password : </label>
					<input id="renewpassword" type='password' class='form-control' name='renewpassword'>
				</div>
				<div class='row text-center'>
                    <button class='btn btn-warning' type='submit'>Change Password</button>
				</div>
			</div>
	</div>
</div>
