<?php
/**
 * @var UserController $this
 * @var User $model
 * @var CActiveForm $form
 */
$this->registerJsModule( "modules/user" );
$this->registerCssFile("bootstrap-multiselect.css");
?>
<div class="form">
<?php $form=$this->beginWidget('BsActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'firstname'); ?>
		<?php echo $form->textField($model,'firstname',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'firstname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastname'); ?>
		<?php echo $form->textField($model,'lastname',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'emailaddress'); ?>
		<?php echo $form->textField($model,'emailaddress',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'emailaddress'); ?>
	</div>	
	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>
	<?php if(!$model->isNewRecord) { ?>
    <?php $this->registerJsModule("modules/user"); ?>
	<div class="row">
        <?= CHTML::button(
            "Reset Password",
            array(
                'class' => 'btn btn-danger user__button-reset-password',
                'data-id' => $model->userid,
                'type' => 'button',
            )
        ) ?>
	</div>
	<?php } else { ?>
	<div class='row'>
		<?php echo $form->labelEx($model,"Password"); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	<?php } ?>
	<div class="row">
		<?php echo $form->labelEx($model,'color'); ?>
		<?php echo $form->textField($model,'color',array('size'=>6,'maxlength'=>6,'class'=>'mycolorpicker')); ?>
		<?php echo $form->error($model,'color'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'evt_manager'); ?>
		<?php echo $form->checkBox($model,'evt_manager'); ?>
		<?php echo $form->error($model,'evt_manager'); ?>
	</div>		
	<div class="row">
		<?php echo $form->labelEx($model,'active'); ?>
		<?php echo $form->checkBox($model,'active'); ?>
		<?php echo $form->error($model,'active'); ?>
	</div>
    <div class="row">
        <?php echo $form->labelEx($model, 'has_vip_permission'); ?>
        <?php echo $form->checkBox($model, 'has_vip_permission'); ?>
        <?php echo $form->error($model, 'has_vip_permission'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'has_can_add_permission'); ?>
        <?php echo $form->checkBox($model, 'has_can_add_permission'); ?>
        <?php echo $form->error($model, 'has_can_add_permission'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'has_admin_permission'); ?>
        <?php echo $form->checkBox($model, 'has_admin_permission'); ?>
        <?php echo $form->error($model, 'has_admin_permission'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'receive_notification'); ?>
        <?php echo $form->checkBox($model, 'receive_notification'); ?>
        <?php echo $form->error($model, 'receive_notification'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'vt_notification'); ?>
        <?php echo $form->checkBox($model, 'vt_notification'); ?>
        <?php echo $form->error($model, 'vt_notification'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'mk_notification'); ?>
        <?php echo $form->checkBox($model, 'mk_notification'); ?>
        <?php echo $form->error($model, 'mk_notification'); ?>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h4>Users subscribed by this user as buyer</h4>
        </div>
        <div class="col-xs-6">
            <b>MK notifications : </b><br>
            <select name="notification_mk[]" multiple class="hidden form-control multiselect-control"><?php
                $users = User::model()->findAll( array( 'condition' => 'active=1' ));
                for($i=0; $i < count($users); $i++){
                    $s = "";
                    for($c=0; $c < count($model->subscribed); $c++){
                        if($model->subscribed[$c]->notification_type=='MK' && $model->subscribed[$c]->buyerid == $users[$i]->userid){
                            $s = "selected";
                        }
                    }
                    echo "<option value='".$users[$i]->userid."' $s>".$users[$i]->fullname."</option>\n";
                }
                ?>
            </select>
        </div>
        <div class="col-xs-6">
            <b>VT notifications : </b><br>
            <select name="notification_vt[]" multiple class="hidden form-control multiselect-control"><?php
                for($i=0; $i < count($users); $i++){
                    $s = "";
                    for($c=0; $c < count($model->subscribed); $c++){
                        if($model->subscribed[$c]->notification_type=='VT' && $model->subscribed[$c]->buyerid == $users[$i]->userid){
                            $s = "selected";
                        }
                    }
                    echo "<option value='".$users[$i]->userid."' $s>".$users[$i]->fullname."</option>\n";
                }
                ?>
            </select>
        </div>
    </div>
    <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->