<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('userid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->userid), array('view', 'id'=>$data->userid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firstname')); ?>:</b>
	<?php echo CHtml::encode($data->firstname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastname')); ?>:</b>
	<?php echo CHtml::encode($data->lastname); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('color')); ?>:</b>
	<?php echo CHtml::encode($data->color); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('has_vip_permission')); ?></b>
    <?php echo CHtml::encode($data->has_vip_permission); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('has_can_add_permission')); ?></b>
    <?php echo CHtml::encode($data->has_can_add_permission); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('has_admin_permission')); ?></b>
    <?php echo CHtml::encode($data->has_admin_permission); ?>
    <br/>
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('createdate')); ?>:</b>
	<?php echo CHtml::encode($data->createdate); ?>
	<br />

</div>