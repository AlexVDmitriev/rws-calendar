<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);
?>
<h1>Manage Users</h1>
<button class='btn btn-primary pull-right' type='button' onclick='document.location.href="<?php echo Yii::app()->createUrl("User/create"); ?>"'><span class='fa fa-plus'>&nbsp;</span>Add New user</button>

<?php $this->widget('bootstrap.widgets.BsGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'enableSorting'=>true,
	'filter'=>$model,
	'enablePagination'=>false,
	'columns'=>array(
		'userid',
		'firstname',
		'lastname',
		'username',
		'emailaddress',
		'active:boolean',
		array('name'=>'color','value'=>'\'<div style="width:30px; height:30px; background-color:#\'.$data->color.\'"></div>\'','type'=>'raw'),
		array('name'=>'createdate','value'=>'date("d-m-Y",strtotime($data->createdate))','type'=>'raw'),
        array(
            'type'  => 'raw',
            'header' => 'Permissions',
            'value' =>
                implode(" . '<br />' . ", array(
                    '($data->has_vip_permission ? BsHtml::label("vip", "permission-vip") : "")',
                    '($data->has_can_add_permission ? BsHtml::label("can add", "permission-can_add") : "")',
                    '($data->has_admin_permission ? BsHtml::label("admin","permission-admin") : "")',
                    '($data->receive_notification ? BsHtml::label("event-notification","receive-notification") : "")',
                    '($data->mk_notification ? BsHtml::label("mk-notification","mk-notification") : "")',
                    '($data->vt_notification ? BsHtml::label("vt-notification","vt-notification") : "")'
                ))
        ),
		array('class'=>'CButtonColumn')
	),
	'type'=>BsHtml::GRID_TYPE_STRIPED
)); ?>
