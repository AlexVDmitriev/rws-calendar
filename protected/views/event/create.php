<?php

/**
 * @var EventController $this
 * @var Event $model
 * @var Competition $competition
 * @var Repeating $repeating
 * @var integer $backToCalendar
 */

$this->breadcrumbs=array(
	'Events'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Event', 'url'=>array('index')),
	array('label'=>'Manage Event', 'url'=>array('admin')),
);
?>

<h1>Create Event</h1>

<?php
$this->renderPartial(
    '_form',
    array(
        'model' => $model,
        'backToCalendar' => $backToCalendar,
        'comments' => array(),
        'c' => $competition,
        'repeats' => $repeating,
    )
);
?>