<?php
/* @var $this EventController */
/* @var $model Event */

$this->breadcrumbs=array(
	'Events'=>array('index'),
	$model->id,
);
?>

<h1>View Event</h1>

<?php $this->widget('bootstrap.widgets.BsDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'manufacturer',
		'eventid',
		array('name'=>'startdate','value'=>date("d-m-Y",strtotime($model->startdate))),
		array('name'=>'enddate','value'=>date("d-m-Y",strtotime($model->enddate))),
		'categorylist.name',
		'managerlist.fullname',
		'saleslist.name',
		'statuslist.status',
		array('name'=>'revprediction','value'=>number_format($model->revprediction,2,".",","),'type'=>'raw'),
		array('name'=>'orderprediction','value'=>number_format($model->orderprediction,2,".",","),'type'=>'raw'),
		array('name'=>'unitprediction','value'=>number_format($model->unitprediction,0,".",","),'type'=>'raw'),
	),
)); 
?>
