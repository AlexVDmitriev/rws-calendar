<?php

/**
 * @var EventController $this
 * @var Event $model
 * @var CActiveForm $form
 * @var EventComments[] $comments
 * @var integer $backToCalendar
 * @var Competition $c
 * @var Repeating $repeats
 */

/** @var CWebApplication $app */
$app = Yii::app();

/** @var WebUser $currentUser */
$currentUser = $app->user;

$this->registerCssFile("bootstrap-datepicker.min.css");
$this->registerCssFile("bootstrap-switch.min.css");

$this->registerJsModule( "modules/event/_form" );

?>
<script language="JavaScript">
    var revlist = <?= json_encode(SalesBracket::getRevList()) ?>;
</script>
<div class="form event-form">

    <?php
    $form = $this->beginWidget(
        'BsActiveForm',
        array(
            'id'                   => 'event-form',
            'htmlOptions'         => array(
                'data-is-new' => ($model->isNewRecord) ? 1 : 0,
            ),
            'enableAjaxValidation' => false,
        ));
    ?>
    <input type="hidden" name="backToCalendar" value="<?= empty($backToCalendar) ? 0 : 1 ?>">
    <input type='hidden' name='islinked' value=''>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>
	<div class='panel panel-default'>
		<div class='panel-heading'>
			<h3>General Information</h3>
		</div>
		<div class='panel-body'>
			<div class='row'>
				<div class='col-xs-12'>
					<div class='col-xs-4 form-group'>
						<?php echo $form->labelEx($model,"eventrepeat"); ?>
                        <?php echo $form->checkBox(
                            $model,
                            "eventrepeat",
                            array(
                                'data-on-text' => 'Yes',
                                'data-off-text' => 'No',
                                'class' => 'event__repeat__switch checkboxer',
                                'disabled' => $model->isEnabledRepeats() ? "" : "disabled",
                            )
                        ); ?>
						<?php echo $form->error($model,"eventrepeat"); ?>
                        <div class="help-block with-errors error"></div>
                    </div>
                    <br />
                    <div class='col-xs-11 col-ms-offset-2 col-ms-8 hidden' id='repeatpanel'>
						<div class='panel panel-default'>
							<div class='panel-heading text-center'>
								<b>Repeat</b>
							</div>
							<div class='panel-body'>
								<div class='row'>
                                    <div class="col-xs-6">
                                        <label for="Repeating_type">Select Repeat Type</label>
                                    </div>
                                    <div class="col-xs-6">
                                        <?php echo $form->dropDownList(
                                            $repeats,
                                            'type',
                                            [
                                                1 => 'Months',
                                                'Years',
                                                'Weekly'
                                            ],
                                            array(
                                                'class' => 'form-control event__repeat__type',
                                                'id'    => 'Repeating_type',
                                            )
                                        ); ?>
                                        <?php echo $form->error($repeats, 'type'); ?>
                                        <div class="help-block with-errors error"></div>
                                    </div>
								</div>
								<div class='row'>
                                    <div class="col-xs-6">
                                        <label for="frequency">Every (Frequency) :</label>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php echo $form->numberField(
                                            $repeats,
                                            'every',
                                            [
                                                'id'    => 'frequency',
                                                'class' => 'form-control'
                                            ]
                                        ); ?>
                                        <?php echo $form->error($repeats, 'every'); ?>
                                        <div class="help-block with-errors error"></div>
                                    </div>
                                    <div class="col-xs-3">
                                        <i class="event__repeat-items"></i>
                                    </div>
								</div>
								<div class='row'>
									<div class='col-xs-6'>
                                        <label for="occur_count">Repeat For(Occurrences):</label>
 									</div>
									<div class='col-xs-3'>
                                        <?php echo $form->numberField(
                                            $repeats,
                                            'occurcount',
                                            [
                                                'class' => 'form-control',
                                                'id'    => 'occur_count',
                                                'step'  => '1',
                                            ]
                                        ); ?>
                                        <?php echo $form->error($repeats, 'occurcount'); ?>
                                        <div class="help-block with-errors error"></div>
									</div>
									<div class='col-xs-3'>
                                        <i>Times</i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class='col-xs-6'>
					<?php echo $form->labelEx($model,'startdate', ['for' => 'startdate']); ?>
                    <?php echo $form->textField(
                        $model,
                        'startdate',
                        array(
                            'class' => 'event__repeat__date datepicker',
                            'id' => 'startdate',
                        )
                    ); ?>
					<?php echo $form->error($model,'startdate'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
				<div class='col-xs-6'>
					<?php echo $form->labelEx($model,'enddate', ['for' => 'enddate']); ?>
                    <?php echo $form->textField(
                        $model,
                        'enddate',
                        array(
                            'class' => 'event__repeat__date datepicker',
                            'id' => 'enddate',
                        )
                    ); ?>
					<?php echo $form->error($model,'enddate'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
			</div>
			<div class="row">
				<div class='col-xs-6'>
					<?php echo $form->labelEx($model,'manufacturer'); ?>
                    <?php echo $form->dropDownList(
                        $model,
                        'manufacturer',
                        CHtml::listData(
                            Manufacturer::model()->findAll(array('order' => 'name asc')),
                            'id',
                            'name'
                        ),
                        array(
                            'class' => 'event__manufacturer',
                            'id' => 'manu'
                        )
                    ); ?>
					<?php echo $form->error($model,'manufacturer'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
				<div class='col-xs-6'>
					<?php echo $form->labelEx($model,'category'); ?>
                    <?php echo $form->dropDownList(
                        $model,
                        'category',
                        CHtml::listData(
                            Category::model()->findAll(array('order' => 'name asc')),
                            'id',
                            'name'
                        ),
                        array(
                            'class' => 'event__category',
                            'id' => 'cid',
                            'data-value' => $model->category,
                        )
                    ); ?>
					<?php echo $form->error($model,'category'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
			</div>	
			<div class="row">
				<div class='col-xs-6'>
					<?php echo $form->labelEx($model,'supplierid'); ?>
                    <?php echo $form->dropDownList(
                        $model,
                        'supplierid',
                        CHtml::listData(
                            Supplier::model()->findAll(),
                            'supplierid',
                            'name'),
                        array(
                            'data-value' => $model->supplierid,
                        )
                    ); ?>
					<?php echo $form->error($model,'supplierid'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
				<div class='col-xs-6'>		
					<?php echo $form->labelEx($model,'eventid'); ?>
					<?php echo $form->textField($model,'eventid',array('size'=>45,'maxlength'=>45)); ?>
					<?php echo $form->error($model,'eventid'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
			</div>
			<div class='row'>
				<div class='col-xs-6'>
					<?php echo $form->labelEx($model,'stocktype'); ?>
					<?php echo $form->listBox( $model,
						'stocktype',
						array( '' => '', 'SCS' => 'SCS', 'Non-SCS' => 'Non-SCS', 'RS' => 'RS')); ?>
					<?php echo $form->error($model,'stocktype'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
				<div class='col-xs-6'>		
					<?php echo $form->labelEx($model,'warehouse'); ?>
                    <?php echo $form->dropDownList(
                        $model,
                        'warehouse',
                        CHtml::listData(
                            Warehouse::model()->findAllBySql("(Select '' as id, '' as name) UNION (SELECT id,name from warehouse order by name);"),
                            'id',
                            'name'
                        ),
                        array('value' => '0')
                    ); ?>
					<?php echo $form->error($model,'warehouse'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
			</div>
			<div class="row">
				<div class='col-xs-6'>
					<?php echo $form->labelEx($model,'manager'); ?>
					<?php echo $form->dropDownList( $model,
						'manager',
						CHtml::listData( User::model()->findAll( array( 'condition' => 'active=1 and evt_manager=1' ) ),
							'userid',
							'fullname' ) ); ?>
					<?php echo $form->error($model,'manager'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
				<div class='col-xs-6'>
					<?php echo $form->labelEx($model,'status'); ?>
					<?php echo $form->dropDownList($model,'status',CHtml::listData(StatusMaster::model()->findAll(array('order'=>'ordering asc')),'id','status')); ?>
					<?php echo $form->error($model,'status'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
			</div>
			<div class="row">
				<div class='col-xs-6'>
					<?php echo $form->labelEx($model,'revprediction'); ?>
                    <?php echo $form->numberField(
                        $model,
                        'revprediction',
                        array(
                            'class' => 'event__rev-prediction',
                            'min' => '0',
                            'data-revprediction'=>$model->revprediction,
                        )
                    ); ?>
					<?php echo $form->error($model,'revprediction'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
				<div class='col-xs-6'>
					<?php echo $form->labelEx($model,'salesid'); ?>
                    <?php echo $form->dropDownList(
                        $model,
                        'salesid',
                        CHtml::listData(
                            SalesBracket::model()->findAll(),
                            'id',
                            'name'
                        ),
                        array('disabled' => 'disabled')
                    ); ?>
					<?php echo $form->error($model,'salesid'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
			</div>
            <div class="row">
                <div class="col-xs-6">
                    <?php echo $form->labelEx($model,'repeat_styles'); ?>

                    <?php echo $form->checkBox(
                        $model,
                        "repeat_styles",
                        array(
                            'data-on-text' => 'Yes',
                            'data-off-text' => 'No',
                            'class' => 'event__repeat_styles__switch checkboxer',
                        )
                    ); ?>
                    <?php echo $form->error($model,'repeat_styles'); ?>
                </div>
            </div>
		</div>
	</div>
	<div class='panel panel-default'>
		<div class='panel-heading'>
			<h3>Event Information</h3>
		</div>
		<div class='panel-body'>
			<div class='row'>
				<div class='col-xs-6 form-group'>
                    <?php echo CHtml::activeLabel($c,"totunits"); ?>
                    <?php echo CHtml::activeNumberField(
                        $c,
                        "totunits",
                        ($c->winner != 0)
                            ? array('class' => 'form-control', 'readonly' => 'readonly', 'step' => 'any')
                            : array('class' => 'form-control event__tot_units', 'step' => 'any')
                    ); ?>
                    <?php echo CHtml::error($c, 'totunits'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
				<div class='col-xs-6 form-group'>
                    <?php echo CHtml::activeLabel($c,"totalstyles"); ?>
                    <?php echo CHtml::activeNumberField(
                        $c,
                        "totalstyles",
                        ($c->winner != 0)
                            ? array('class' => 'form-control', 'readonly' => 'readonly', 'step' => 'any')
                            : array('class' => 'form-control', 'step' => 'any')
                    ); ?>
                    <?php echo CHtml::error($c, 'totalstyles'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
			</div>
			<div class='row'>
				<div class='col-xs-6 form-group'>
                    <?php echo CHtml::activeLabel($c,"avgdiscount"); ?>
                    <?php echo CHtml::activeNumberField(
                        $c,
                        "avgdiscount",
                        ($c->winner != 0)
                            ? array('class' => 'form-control', 'readonly' => 'readonly', 'step' => 'any')
                            : array('class' => 'form-control', 'step' => 'any')
                    ); ?>
                    <?php echo CHtml::error($c, 'avgdiscount'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
				<div class='col-xs-6 form-group'>
                    <?php echo CHtml::activeLabel($c,"avgpricepoint"); ?>
                    <?php echo CHtml::activeNumberField(
                        $c,
                        "avgpricepoint",
                        ($c->winner != 0)
                            ? array('class' => 'form-control', 'readonly' => 'readonly', 'step' => 'any')
                            : array('class' => 'form-control', 'step' => 'any')
                    ); ?>
                    <?php echo CHtml::error($c, 'avgpricepoint'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
			</div>
			<div class='row'>
				<div class='col-xs-6 form-group'>
                    <?php echo CHtml::activeLabel($c, "link"); ?>
                    <?php echo CHtml::activeTextField(
                        $c,
                        "link",
                        ($c->winner != 0)
                            ? array('class' => 'form-control', 'readonly' => 'readonly', 'step' => 'any')
                            : array('class' => 'form-control', 'step' => 'any')
                    );
                    ?>
                    <?php echo CHtml::error($c, 'link'); ?>
                    <div class="help-block with-errors error"></div>
				</div>
			</div>
		</div>
		<div class='panel panel-default'>
			<div class='panel-heading'>
				<h3>Sales Information</h3>
			</div>
			<div class='panel-body'>
				<div class='row'>
					<div class='col-xs-6'>
						<?php echo $form->labelEx($model,'actualunitsales'); ?>
                        <?php echo $form->numberField(
                            $model,
                            'actualunitsales',
                            array('class' => 'event__actual-unit-sales', 'step' => 'any')
                        ); ?>
						<?php echo $form->error($model,'actualunitsales'); ?>
                        <div class="help-block with-errors error"></div>
					</div>
					
					<div class='col-xs-6'>
					    <?php echo $form->labelEx($model,'costincvat'); ?>
					    <?php echo $form->numberField($model,'costincvat'); ?>
					    <?php echo $form->error($model,'costincvat'); ?>
                        <div class="help-block with-errors error"></div>
					</div>
				</div>
				<div class='row'>
					<div class='col-xs-6'>
                        <?php echo $form->labelEx($model,'actualrevenue'); ?>
                        <?php echo $form->numberField(
                            $model,
                            'actualrevenue',
                            array(
                                'class' => 'event__actual-revenue',
                                'min'   => '0',
                                'step'  => 'any',
                            )
                        ); ?>
                        <?php echo $form->error($model,'actualrevenue'); ?>
                        <div class="help-block with-errors error"></div>
					</div>
					<div class='col-xs-6'>
						<?php echo $form->labelEx($model,'actualsellthrough'); ?>
                        <?php echo $form->numberField(
                            $model,
                            'actualsellthrough',
                            array(
                                'readonly' => 'readonly',
                                'step'     => 'any',
                            )
                        ); ?>
						<?php echo $form->error($model,'actualsellthrough'); ?>
                        <div class="help-block with-errors error"></div>
					</div>
				</div>
				<div class='row'>
					<div class='col-xs-6'>
						<?php echo $form->labelEx($model,'noofcustomers'); ?>
                        <?php echo $form->numberField($model, 'noofcustomers', array('step' => 'any')); ?>
						<?php echo $form->error($model,'noofcustomers'); ?>
                        <div class="help-block with-errors error"></div>
					</div>
					<div class='col-xs-6'>
						<?php echo $form->labelEx($model,'predvsact'); ?>
                        <?php echo $form->numberField(
                            $model,
                            'predvsact',
                            array(
                                'readonly' => 'readonly',
                                'step'     => 'any'
                            )
                        ); ?>
						<?php echo $form->error($model,'predvsact'); ?>
                        <div class="help-block with-errors error"></div>
					</div>
				</div>
			</div>
		</div>		
    </div>

	<div class="row"></div>

    <div class='row'>
        <div class="panel-group" id="mkaccordian" role="tablist">
            <div class="panel panel-default">
                <div class='panel-heading' role='tab'>
                    <a data-toggle='collapse' class='text-center collapsed' data-parent='#mkaccordian' href='#mkpanel'>
                        <button class='pull-right' type="button" id="mk_save"><span class="fa fa-save"></span> Save & Notify</button>
                        <h4><i class='fa fa-plus'></i> MK Details</h4>
                    </a>
                </div>
                <div class='panel-body collapse' id="mkpanel">
                    <?php
                    if($model->isNewRecord){
                        $model2 = new EventMarketingDetails();
                    }
                    else {
                        $model2 = $model->marketingDetails;
                        if($model2 == null)
                            $model2 = new EventMarketingDetails();
                    }
                    ?>
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"event_name"); ?>
                            <?=$form->textField($model2,"event_name"); ?>
                        </div>
                        <div class="col-xs-6">
                            <?=$form->label($model2,"gender_product"); ?>
                            <?=$form->textField($model2,"gender_product"); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"start_datetime"); ?>
                            <?=$form->textField($model2,"start_datetime",array('class'=>'datepicker form-control')); ?>
                        </div>
                        <div class="col-xs-6">
                            <?=$form->label($model2,"end_datetime"); ?>
                            <?=$form->textField($model2,"end_datetime",array('class'=>'datepicker form-control')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"manager"); ?>
                            <?=$form->dropDownList($model2,"manager",CHtml::listData(User::model()->findAll(array('condition' => 'active=1 and evt_manager=1')),"userid","fullname")); ?>
                        </div>
                        <div class="col-xs-6">
                            <?=$form->label($model2,"lifestyle_images"); ?>
                            <?=$form->textField($model2,"lifestyle_images"); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"event_link"); ?>
                            <?=$form->textField($model2,"event_link"); ?>
                        </div>
                        <div class="col-xs-6">
                            <?=$form->label($model2,"product_images"); ?>
                            <?=$form->textField($model2,"product_images"); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"max_discount"); ?>
                            <?=$form->numberField($model2,"max_discount"); ?>
                        </div>
                        <div class="col-xs-6">
                            <?=$form->label($model2,"lowest_price"); ?>
                            <?=$form->numberField($model2,"lowest_price"); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"delivery"); ?>
                            <?=$form->textField($model2,"delivery"); ?>
                        </div>
                        <div class="col-xs-6">
                            <?=$form->label($model2,"promotions"); ?>
                            <?=$form->textField($model2,"promotions"); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?=$form->label($model2,"extra_comments"); ?>
                            <?=$form->textArea($model2,"extra_comments"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-group" id="vkaccordian" role="tablist">
            <div class="panel panel-default">
                <div class='panel-heading' role='tab'>
                    <a data-toggle='collapse' class='text-center' data-parent='#vkaccordian' href='#vkpanel'>
                        <button type="button" class='pull-right' id="vt_save"><span class="fa fa-save"></span> Save & Notify</button>
                        <h4><i class='fa fa-plus'></i> VT Requests</h4>
                    </a>
                </div>
                <div class='panel-body collapse' id="vkpanel">
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"brand"); ?>
                            <?=$form->dropDownList($model2,"brand",CHtml::listData(Manufacturer::model()->findAll(),"id","name")); ?>
                        </div>
                        <div class="col-xs-6">
                            <?=$form->label($model2,"start_date"); ?>
                            <?=$form->textField($model2,"start_date",array('class'=>'form-control datepicker')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"work_required"); ?>
                            <?=$form->dropDownList($model2,"work_required",array(''=>'','Shoot','Edits','Shoots & Edits','Repeats')); ?>
                        </div>
                        <div class="col-xs-6">
                            <?php if($model2->isNewRecord){
                                $model2->samples_arrived_date = date('Y-m-d');
                            }
                            ?>
                            <?=$form->label($model2,"samples_arrived_date"); ?>
                            <?=$form->textField($model2,"samples_arrived_date",array('class'=>'form-control datepicker')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"sample_type"); ?>
                            <?=$form->dropDownList($model2,"sample_type",array(''=>'','Apparel','Accessories','Footwear','Product','Repeats')); ?>
                        </div>
                        <div class="col-xs-6">
                            <?=$form->label($model2,"gender"); ?>
                            <?=$form->dropDownList($model2,"gender",array('','Mens','Ladies','Kids','Mens & Ladies')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"sample_quantity"); ?>
                            <?=$form->numberField($model2,"sample_quantity"); ?>
                        </div>
                        <div class="col-xs-6">
                            <?=$form->label($model2,"repeats_done"); ?>
                            <?=$form->checkBox($model2,"repeats_done",array('class'=>'checkboxer','data-on-text'=>'Yes','data-off-text'=>'No')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <?=$form->label($model2,"final_destination"); ?>
                            <?=$form->dropDownList($model2,"final_destination",array('JHB Warehouse', 'CPT Warehouse', 'RTS')); ?>
                        </div>
                        <div class="col-xs-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?=$form->label($model2,"extra_comments2"); ?>
                            <?=$form->textArea($model2,"extra_comments2"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                <br />
    <div class="row buttons">
        <?php if ($model->isNewRecord) { ?>
            <?php echo CHtml::submitButton('Create', array('class' => 'btn btn-primary')); ?>
        <?php } ?>
    </div>

<?php $this->endWidget(); ?>

<?php if(!$model->isNewRecord) { ?>
<div>
	<div class='row'>
		<div class='panel panel-default'>
			<div class='panel-heading'>
				<div class='panel-title'>Comments</div>
			</div>
			<div class='panel-body event-comment__body' id='commentbody'>
				<?php foreach($comments as $comment) { ?>
						<div class='panel panel-default' id='comment<?php echo $comment->commentid ?>'>
							<div class='panel-body'>
								<div class='col-xs-10'><b>
								<?php echo $comment->user->firstname." ".$comment->user->lastname; ?></b></div>
								<?php if($currentUser->isAdmin()) { ?>
                                    <div class='col-xs-2'>
                                        <button data-comment-id="<?= $comment->commentid ?>"
                                                data-id="<?= $comment->commentid ?>"
                                                class='btn btn-danger event-comment__button-delete'>
                                            <i class='fa fa-times'></i>
                                        </button>
                                    </div>
                                <?php } ?>
								<br />
                                <hr>
								<?php echo $comment->content; ?><br><hr>
								<div class='col-xs-offset-8'>
                                    <small>
                                        <i><?php echo date("d-m-Y H:i:s",strtotime($comment->datetime)); ?></i>
                                    </small>
                                </div>
							</div>
						</div>
                <?php } ?>
			</div>
			<div class='panel-footer'>
				<div class='row'>
					<div class='col-xs-9'>
                        <label for="comment"></label>
                        <textarea id='comment' class="event-comment__message" style='width:100%;' rows='4'></textarea>
                    </div>
					<div class='col-xs-2'>
                        <button class='btn btn-primary event-comment__button-add'
                                data-id="<?= $model->id ?>"
                                type="button"
                                style='height:100%'>
                            Add <br>Comment
                        </button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
</div><!-- form -->
