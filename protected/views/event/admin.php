<?php

/**
 * @var EventController $this
 * @var Event $model
 * @var string $manager
 * @var string $month
 * @var string $year
 */

$this->breadcrumbs = array(
    'Events' => array('index'),
    'Manage',
);

$this->registerJsModule("modules/event/admin");

?>
<h1>Manage Events</h1>
<div class='row text-center'>
    Select Buyer
    <?=
    CHtml::dropDownList(
        '',
        $manager,
        CHtml::listData(
            User::model()->findAll(array('order' => 'firstname asc,lastname asc')),
            'userid',
            'fullname'
        ),
        array(
            'id' => 'manager',
            'empty' => 'All',
        )
    )
    ?>
    <b>Select Month / Year </b>
    <?=
    CHtml::dropDownList(
        '',
        (int)$month,
        array(
            1 => 'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sept',
            'Oct',
            'Nov',
            'Dec'
        ),
        array(
            'id' => 'month',
            'empty' => 'Any',
        )
    )
    ?>
    /
    <?=
    CHtml::dropDownList(
        '',
        (int)$year,
        array_combine(
            range(
                date("Y") + 5,
                date("Y") - 5,
                -1
            ),
            range(
                date("Y") + 5,
                date("Y") - 5,
                -1
            )
        ),
        array(
            'id' => 'year',
            'empty' => 'Any',
        )
    )
    ?>
    <button type='button' class='btn btn-success event__admin__filter-button'>
        <i class='fa fa-filter'></i> Filter
    </button>
</div>
<form method='POST' action='<?php echo Yii::app()->createUrl("event/admin"); ?>'>
    <div class="row">
        <button
                type='button'
                class='btn btn-primary pull-right'
                onclick='document.location.href="<?= Yii::app()->createUrl("Event/create") ?>"'
        >
            <span class='fa fa-plus'>&nbsp;</span>Add New Event
        </button>
        <button type='submit' class='btn btn-primary pull-right'>
            <span class='fa fa-save'>&nbsp;</span>Save Changes
        </button>
    </div>

    <div class='row'>
        <div class='col-xs-12' style='overflow:scroll;'>

            <?php $this->widget(
                'bootstrap.widgets.BsGridView',
                array(
                    'id' => 'event-grid',
                    'dataProvider' => $model->search(),
                    'enableSorting' => true,
                    'ajaxUpdate' => false,
                    'filter' => $model,
                    'columns' => array(
                        array(
                                'name' => 'brand',
                                'value' => '(empty($data->manufacturerlist) ? "" : $data->manufacturerlist->name)'
                        ),
                        'eventid',
                        'startdate',
                        'enddate',
                        'categorylist.name',
                        'managerlist.fullname',
                        'saleslist.name',
                        'statuslist.status',
                        array(
                            'name' => 'competition.totunits',
                            'type' => 'raw',
                            'value' => '"<input size=\'5\' type=\'text\' name=\'totunits_".$data->id."\' value=\'".$data->competition->totunits."\'>"'
                        ),
                        array(
                            'name' => 'competition.totalstyles',
                            'type' => 'raw',
                            'value' => '"<input size=\'5\' type=\'text\' name=\'totalstyles_".$data->id."\' value=\'".$data->competition->totalstyles."\'>"'
                        ),
                        array(
                            'name' => 'competition.avgdiscount',
                            'type' => 'raw',
                            'value' => '"<input size=\'5\' type=\'text\' name=\'avgdiscount_".$data->id."\' value=\'".$data->competition->avgdiscount."\'>"'
                        ),
                        array(
                            'name' => 'competition.avgpricepoint',
                            'type' => 'raw',
                            'value' => '"<input  size=\'5\' type=\'text\' name=\'avgpricepoint_".$data->id."\' value=\'".$data->competition->avgpricepoint."\'>"'
                        ),
                        array(
                            'name' => 'revprediction',
                            'value' => '"<input size=\'5\' type=\'text\' name=\'revprediction_".$data->id."\' value=\'".$data->revprediction."\'>"',
                            'type' => 'raw'
                        ),
                        array(
                            'name' => 'actualrevenue',
                            'value' => '"<input size=\'5\'  type=\'text\' name=\'actualrevenue_".$data->id."\' value=\'".$data->actualrevenue."\'>"',
                            'type' => 'raw'
                        ),
                        array(
                            'name' => 'orderprediction',
                            'value' => '"<input size=\'5\'  type=\'text\' name=\'orderprediction_".$data->id."\' value=\'".$data->orderprediction."\'>"',
                            'type' => 'raw'
                        ),
                        array(
                            'name' => 'actualunitsales',
                            'value' => '"<input size=\'5\'  type=\'text\' name=\'actualunitsales_".$data->id."\' value=\'".$data->actualunitsales."\'>"',
                            'type' => 'raw'
                        ),
                        array(
                            'name' => 'unitprediction',
                            'value' => '"<input  size=\'5\' type=\'text\' name=\'unitprediction_".$data->id."\' value=\'".$data->unitprediction."\'>"',
                            'type' => 'raw'
                        ),
                        array(
                            'name' => 'noofcustomers',
                            'value' => '"<input  size=\'5\' type=\'text\' name=\'noofcustomers_".$data->id."\' value=\'".$data->noofcustomers."\'>"',
                            'type' => 'raw'
                        ),
                        array(
                            'name' => 'costincvat',
                            'value' => '"<input  size=\'5\' type=\'text\' name=\'costincvat_".$data->id."\' value=\'".$data->costincvat."\'>"',
                            'type' => 'raw'
                        ),
                    ),
                )
            ); ?>
        </div>
    </div>
</form>
