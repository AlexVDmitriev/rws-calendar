<?php

/**
 * @var EventController $this
 * @var Event $model
 * @var EventComments[] $comments
 * @var Competition $competition
 * @var Repeating $repeats
 */

$this->breadcrumbs=array(
	'Events'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
?>
<h1>Update Event</h1>

<?php
$this->renderPartial(
    '_form',
    array(
        'model' => $model,
        'comments' => $comments,
        'backToCalendar' => 0,
        'c' => $competition,
        'repeats' => $repeats,
    )
);
?>
