<?php
/* @var $this EventController */
/* @var $data Event */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manufacturer')); ?>:</b>
	<?php echo CHtml::encode($data->manufacturer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eventid')); ?>:</b>
	<?php echo CHtml::encode($data->eventid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('startdate')); ?>:</b>
	<?php echo CHtml::encode($data->startdate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('enddate')); ?>:</b>
	<?php echo CHtml::encode($data->enddate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category')); ?>:</b>
	<?php echo CHtml::encode($data->categorylist->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manager')); ?>:</b>
	<?php echo CHtml::encode($data->manager); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('salesid')); ?>:</b>
	<?php echo CHtml::encode($data->salesid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('revprediction')); ?>:</b>
	<?php echo CHtml::encode($data->revprediction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderprediction')); ?>:</b>
	<?php echo CHtml::encode($data->orderprediction); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unitprediction')); ?>:</b>
	<?php echo CHtml::encode($data->unitprediction); ?>
	<br />

	*/ ?>

</div>