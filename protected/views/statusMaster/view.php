<?php
/* @var $this StatusMasterController */
/* @var $model StatusMaster */

$this->breadcrumbs=array(
	'Status Masters'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List StatusMaster', 'url'=>array('index')),
	array('label'=>'Create StatusMaster', 'url'=>array('create')),
	array('label'=>'Update StatusMaster', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete StatusMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage StatusMaster', 'url'=>array('admin')),
);
?>

<h1>View StatusMaster #<?php echo $model->id; ?></h1>

<?php
$backgroundColor = "#" . $model->color;
$this->widget(
    'bootstrap.widgets.BsDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'id',
            'status',
            array(
                'name' => 'color',
                'value' => '<div style="width:30px; height:30px; background-color:' . $backgroundColor . '"></div>',
                'type' => 'raw'
            ),
        ),
    )
);
?>
