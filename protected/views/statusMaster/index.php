<?php
/* @var $this StatusMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Status Masters',
);

$this->menu=array(
	array('label'=>'Create StatusMaster', 'url'=>array('create')),
	array('label'=>'Manage StatusMaster', 'url'=>array('admin')),
);
?>

<h1>Status Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
