<?php
/* @var $this StatusMasterController */
/* @var $model StatusMaster */

$this->breadcrumbs=array(
	'Status Masters'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List StatusMaster', 'url'=>array('index')),
	array('label'=>'Create StatusMaster', 'url'=>array('create')),
	array('label'=>'View StatusMaster', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage StatusMaster', 'url'=>array('admin')),
);
?>

<h1>Update StatusMaster <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>