<?php
/* @var $this StatusMasterController */
/* @var $model StatusMaster */

$this->breadcrumbs=array(
	'Status Masters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List StatusMaster', 'url'=>array('index')),
	array('label'=>'Manage StatusMaster', 'url'=>array('admin')),
);
?>

<h1>Create StatusMaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>