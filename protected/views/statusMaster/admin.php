<?php

/**
 * @var StatusMasterController $this
 * @var StatusMaster $model
 */

$this->breadcrumbs=array(
	'Status Masters'=>array('index'),
	'Manage',
);

$this->registerJsModule("modules/status-master");

?>

<div class='modal fade' id='changeid'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-body' id='modalbody'>The Status that you are trying to delete is being used in existing Events. 
			You may still delete this Status if you assign another status to these events.<br>
                <div class="text-center">
                    Select a Status : <br>
                    <input type='hidden' id='oristatus' value=''>
                    <label for="changeidlist"></label>
                    <select id='changeidlist'></select>
                </div>
			</div>
			<div class='modal-footer'>
				<button class='btn btn-danger status-master__modal__delete-button'>
                    <span class='fa fa-trash-o'></span> Delete
                </button>
				<button class='btn' data-dismiss="modal"><span class='fa fa-ban'></span> Close</button>
			</div>
		</div>
	</div>
</div>

<h1>Manage Status Masters</h1>
<a class='btn btn-primary pull-right' href="<?= Yii::app()->createUrl("StatusMaster/create"); ?>">
    <span class='fa fa-plus'></span>Add New Status
</a>
<?php $this->widget('bootstrap.widgets.BsGridView', array(
	'id'=>'status-master-grid',
	'dataProvider'=>$model->search(),
	'enableSorting'=>false,
	'columns'=>array(
		'id',
		'status',
		array('name'=>'color','value'=>'\'<div style="width:30px; height:30px; background-color:#\'.$data->color.\'"></div>\'','type'=>'raw'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
