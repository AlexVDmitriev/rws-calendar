<?php
/* @var $this SalesBracketController */
/* @var $model SalesBracket */

$this->breadcrumbs=array(
	'Sales Brackets'=>array('index'),
	'Manage',
);
?>
<h1>Manage Sales Brackets</h1>
<button class='btn btn-primary pull-right' type='button' onclick='document.location.href="<?php echo Yii::app()->createUrl("salesbracket/create"); ?>"'><span class='fa fa-plus'>&nbsp;</span>Add New Sales Bracket</button>
<?php $this->widget('bootstrap.widgets.BsGridView', array(
	'id'=>'sales-bracket-grid',
	'dataProvider'=>$model->search(),
	'enableSorting'=>false,
	'columns'=>array(
		'id',
		'name',
		'from',
		'to',
		array('name'=>'color','value'=>'\'<div style="width:30px; height:30px; background-color:#\'.$data->color.\'"></div>\'','type'=>'raw'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
