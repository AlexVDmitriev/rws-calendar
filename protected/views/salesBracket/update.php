<?php
/* @var $this SalesBracketController */
/* @var $model SalesBracket */

$this->breadcrumbs=array(
	'Sales Brackets'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SalesBracket', 'url'=>array('index')),
	array('label'=>'Create SalesBracket', 'url'=>array('create')),
	array('label'=>'View SalesBracket', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SalesBracket', 'url'=>array('admin')),
);
?>

<h1>Update SalesBracket <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>