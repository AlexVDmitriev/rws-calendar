<?php
/* @var $this SalesBracketController */
/* @var $model SalesBracket */

$this->breadcrumbs=array(
	'Sales Brackets'=>array('index'),
	$model->name,
);

?>

<h1>View SalesBracket #<?php echo $model->id; ?></h1>

<?php
$backgroundColor = "#" . $model->color;
$this->widget(
    'bootstrap.widgets.BsDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'id',
            'name',
            'from',
            'to',
            array(
                'name' => 'color',
                'value' => '<div style="width:30px; height:30px; background-color:' . $backgroundColor . '"></div>',
                'type' => 'raw'
            ),
        ),
    )
);
?>
