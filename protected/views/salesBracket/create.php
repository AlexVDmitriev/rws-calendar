<?php
/* @var $this SalesBracketController */
/* @var $model SalesBracket */

$this->breadcrumbs=array(
	'Sales Brackets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SalesBracket', 'url'=>array('index')),
	array('label'=>'Manage SalesBracket', 'url'=>array('admin')),
);
?>

<h1>Create SalesBracket</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>