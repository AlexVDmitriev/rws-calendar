<?php
/* @var $this SalesBracketController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sales Brackets',
);

$this->menu=array(
	array('label'=>'Create SalesBracket', 'url'=>array('create')),
	array('label'=>'Manage SalesBracket', 'url'=>array('admin')),
);
?>

<h1>Sales Brackets</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
