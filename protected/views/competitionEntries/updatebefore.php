<?php
/**
 * @var CompetitionEntriesController $this
 * @var Event[] $events
 * @var string $manager
 * @var string $month
 * @var string $year
 */

$this->registerJsModule( 'modules/competition-entries/update-before' );

?>

<div class="text-center">
<h1>Update of competition entries</h1>
    <br />
    A single place to update all your entries.
    <hr />
    <div class='row'>
	<div class='col-xs-12'>
		<div class='col-xs-12'>
            <label for="buyer">Filter By Buyer : </label>
            <?=
            CHtml::dropDownList(
                '',
                $manager,
                CHtml::listData(
                    User::model()->findAll(array('condition' => 'active=1')),
                    'userid',
                    'fullname'
                ),
                array(
                    'id' => 'buyer',
                    'empty' => 'All',
                )
            )
            ?>
			<b>Select Month / Year </b>
            <?=
            CHtml::dropDownList(
                '',
                $month,
                array(
                    1 => 'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sept',
                    'Oct',
                    'Nov',
                    'Dec'
                ),
                array(
                    'id' => 'month',
                    'empty' => 'Any',
                )
            )
            ?> / <?=
            CHtml::dropDownList(
                '',
                $year,
                array_combine(
                    range(date('Y') + 5, date('Y') - 5, -1),
                    range(date('Y') + 5, date('Y') - 5, -1)
                ),
                array(
                    'id' => 'year',
                    'empty' => 'Any',
                )
            )
            ?>
            <button type='button' class='btn btn-success update-before__button-change-buyer' >
				<i class='fa fa-filter'></i> Filter
			</button>
		</div>
	</div>
 </div>
 <hr>
 <form method='POST' action='<?php echo Yii::app()->createUrl("competitionEntries/updatebefore"); ?>'>
 <table class='table'>
 <thead>
	<tr>
        <th>Event</th>
        <th>Buyer</th>
        <th>Brand</th>
        <th>Category</th>
        <th>End Date</th>
        <th>Total Units</th>
        <th>No. Of Styles</th>
        <th>Avg Price Point</th>
        <th>Avg Discount</th>
        <th>Link</th>
        <th></th>
    </tr>
 </thead>
 <tbody>
	<?php 
		foreach($events as $event) {
            if ($event->competition->totunits != ""
                && $event->competition->avgpricepoint != ""
                && $event->competition->link != ""
                && $event->competition->avgdiscount != ""
                && $event->competition->totalstyles != ""
            ) {
                continue;
            }
            ?>
            <tr>
                <td><?= $event->eventid ?></td>
                <td><?= $event->managerlist->fullname ?></td>
                <td><?= $event->manufacturerlist->name ?></td>
                <td><?= empty($event->categorylist) ? "" : $event->categorylist->name ?></td>
                <td><?= $event->enddate ?></td>
                <td>
                <?php if($event->competition->totunits == ""){ ?>
                    <input type='text' name='totunits_<?= $event->id ?>'>
                <?php } else { ?>
                    <?= $event->competition->totunits ?>
                <?php } ?>
                </td>
                <td>
                <?php if($event->competition->totalstyles == "") { ?>
                    <input type='text' name='totalstyles_<?= $event->id ?>'>
                <?php } else { ?>
                    <?= $event->competition->totalstyles ?>
                <?php } ?>
                </td>
                <td>
                <?php if($event->competition->avgpricepoint == "") { ?>
                    <input type='text' name='avgpricepoint_<?= $event->id ?>'>
                <?php } else { ?>
                    <?= $event->competition->avgpricepoint ?>
                <?php } ?>
                </td>
                <td>
                <?php if($event->competition->avgdiscount == "") { ?>
                    <input type='text' name='avgdiscount_<?= $event->id ?>'>
                <?php } else { ?>
                    <?= $event->competition->avgdiscount ?>
                <?php } ?>
                </td>
                <td>
                <?php if ($event->competition->link == "") { ?>
                    <input type='text' name='link_<?= $event->id ?>'>
                <?php } else { ?>
                    <?= $event->competition->link ?>
                <?php } ?>
                </td>
            </tr>
    <?php } ?>
 </tbody>
 </table>
 <button class='btn btn-succes'>Save</button>
</div>