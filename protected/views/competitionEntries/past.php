<?php

/**
 * @var CompetitionEntriesController $this
 * @var Competition[] $comps
 * @var CompetitionMonthWinner[] $competitionMonthWinners
 */

/** @var CWebApplication $app */
$app = Yii::app();

?>

<h1>Past Price Prediction for events</h1>
<hr>
<p>
<table class='table'>
	<thead>
		<tr>
			<th>Event</th><th>Brand</th><th>Category</th><th>End Date</th>
			<th>Total Units</th><th>Total Styles</th><th>Average Price Point</th><th>Avg Discount</th>
			<th>More Info</th><th>Actual Unit Sales</th><th>Actual Revenue</th>
		</tr>
	</thead>
	<tbody>
<?php 
foreach($comps as $competition) {
    $ce = $competition->getCompetitionEntryByUser($app->user->id);
?>
	<tr>
	    <td><?= $competition->event->eventid ?></td>
        <td><?= empty($competition->event->manufacturerlist) ? "" : $competition->event->manufacturerlist->name ?></td>
        <td><?= empty($competition->event->categorylist) ? "" : $competition->event->categorylist->name ?></td>
        <td><?= $competition->event->enddate ?></td>
        <td><?= $competition->totunits ?></td>
        <td><?= $competition->totalstyles ?></td>
        <td><?= $competition->avgpricepoint ?></td>
        <td><?= $competition->avgdiscount ?></td>
        <td><a href='<?= $competition->link ?>'>Event Info</a></td>
        <td><b><?= $competition->event->actualunitsales ?></b></td>
        <td><b><?= $competition->event->actualrevenue ?></b></td>
	</tr>
	<tr>
		<td colspan='12' class="text-center">
			<div class='col-xs-10 col-xs-offset-1'>
			    <table class='table table-condensed'>
			    <thead>
				    <tr>
                        <th colspan='4' class="text-center">
                            Other Users Entries
                        </th>
                    </tr>
				    <tr>
                        <th>Name</th>
                        <th>Predicted Units Sale</th>
                        <th>Predicted Amount</th>
                        <th>Date Time Submitted</th>
                    </tr>
			    </thead>
	            <tbody>
                <?php
                /** @var CompetitionEntries[] $ces */
                $ces = $competition->competitionEntries;
                foreach($ces as $competitionEntry) {
                    if ($competition->winner == $competitionEntry->user) {
                ?>
                    <tr class='success'>
                        <td><i class='fa fa-trophy'></i><?= $competitionEntry->ruser->fullname ?></td>
                <?php } else { ?>
                    <tr>
                        <td><?= empty($competitionEntry->ruser) ? "" : $competitionEntry->ruser->fullname ?></td>
                    <?php } ?>
                        <td><?= $competitionEntry->soldamount ?></td>
                        <td><?= $competitionEntry->revenueamount ?></td>
                        <td><?= $competitionEntry->datetime ?></td>
                    </tr>
                <?php } ?>
	            </tbody>
                </table>
            </div>
        </td>
    </tr>
<?php } ?>
</tbody>
</table>

<h1>Past Competition Monthly Winners</h1>
<div class="text-center">
<table class='table'>
    <thead>
        <tr>
            <th>Year - Month</th>
            <th>Winner!</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($competitionMonthWinners as $competitionMonthWinner) { ?>
        <tr>
            <td><?= $competitionMonthWinner->getDateTimeFormat("Y-M") ?></td>
            <td><?= $competitionMonthWinner->winner0->fullname ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
</div>