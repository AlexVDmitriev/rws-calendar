<?php
/**
 * @var CompetitionEntriesController $this
 * @var Event[] $events
 * @var string $manager
 * @var string $month
 * @var string $year
 */

$this->registerJsModule( 'modules/competition-entries/update-after' );

?>

<div class="text-center">
    <h1>Update of competition entries to close them</h1>
    <hr>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='col-xs-12'>
                <label for="buyer">Filter By Buyer : </label>
                <?= CHtml::dropDownList(
                    '',
                    $manager,
                    CHtml::listData(
                        User::model()->findAll(array('condition' => 'active=1')),
                        'userid',
                        'fullname'
                    ),
                    array(
                        'id' => 'buyer',
                        'empty' => 'All',
                    )
                )
                ?>
                <b>Select Month / Year </b>
                <?= CHtml::dropDownList(
                    '',
                    $month,
                    array(
                        1 => 'Jan',
                        'Feb',
                        'Mar',
                        'Apr',
                        'May',
                        'Jun',
                        'Jul',
                        'Aug',
                        'Sept',
                        'Oct',
                        'Nov',
                        'Dec'
                    ),
                    array(
                        'id' => 'month',
                        'empty' => 'Any',
                    )
                )
                ?> / <?=
                CHtml::dropDownList(
                    '',
                    $year,
                    array_combine(
                        range(date("Y") + 5, date("Y") - 5, -1),
                        range(date("Y") + 5, date("Y") - 5, -1)
                    ),
                    array(
                        'id' => 'year',
                        'empty' => 'Any',
                    )
                )
                ?>
                <button type='button' class='btn btn-success update-after__button-change-buyer'>
                    <i class='fa fa-filter'></i> Filter
                </button>
            </div>
        </div>
    </div>
    <hr>
    <form method='POST' action='<?php echo Yii::app()->createUrl("competitionEntries/updateafter"); ?>'>
        <table class='table'>
            <thead>
            <tr>
                <th>Event</th>
                <th>Buyer</th>
                <th>Brand</th>
                <th>Category</th>
                <th>End Date</th>
                <th>Actual Unit Sales</th>
                <th>Actual Revenue</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($events as $event) {
                if (($event->actualunitsales != 0 && $event->actualrevenue != 0)
                    || empty($event->revprediction)
                    || empty($event->competition->totunits)
                ) {
                    continue;
                }
                ?>
                <tr>
                    <td><?= $event->eventid ?></td>
                    <td><?= $event->managerlist->fullname ?></td>
                    <td><?= $event->manufacturerlist->name ?></td>
                    <td><?= $event->categorylist->name ?></td>
                    <td><?= $event->enddate ?></td>
                    <td>
                        <?php if ($event->actualunitsales == 0) { ?>
                            <input type='text' name='actualunitsales_<?= $event->id ?>'>
                        <?php } else { ?>
                            <?= $event->actualunitsales ?>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if ($event->actualrevenue == 0) { ?>
                            <input type='text' name='actualrevenue_<?= $event->id ?>'>
                        <?php } else { ?>
                            <?= $event->actualrevenue ?>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <button class='btn btn-succes'>Save</button>
    </form>
</div>
