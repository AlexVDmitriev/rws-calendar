<?php
/**
 * @var $this CompetitionEntriesController
 * @var string $buyer
 * @var string $fdate
 * @var string $tdate
 * @var Competition[] $competitions
 */

/** @var CWebApplication $app */
$app = Yii::app();

$this->registerJsModule( 'modules/competition-entries/index' );

?>

<h1>Price Prediction for upcoming events</h1>
<div class='row'>
	<div class='col-xs-12'>
		<div class='col-xs-4'>
		    <label for="fromdate">From Date : </label>
            <input type='date' id='fromdate' value='<?= $fdate ?>'>
            <br />
            <label for="todate">To date : </label>
            <input type='date' id='todate' value='<?= $tdate ?>'>
		</div>
        <div class='col-xs-4'>
            <label for="buyer">Buyer : </label>
            <?= CHtml::dropDownList(
                '',
                $buyer,
                CHtml::listData(
                    User::model()->findAll(array('condition' => 'active=1', 'order' => 'firstname asc')),
                    'userid',
                    'fullname'
                ),
                array(
                    'id' => 'buyer',
                    'empty' => 'All',
                )
            )
            ?>
		</div>
        <div class='col-xs-3'>
            <label for="eventid">Event ID :</label> 
            <input type='text' id='eventid'>
		</div>
		<div class='col-xs-1'>
            <button type='button' class='btn btn-success competition-entries__button-apply-filter'>
                <i class='fa fa-filter'></i> Filter
            </button>
		</div>
	</div>
</div>
<hr>
<p>
<table class='table'>
	<thead>
		<tr>
			<th>Event</th>
            <th>Brand</th>
            <th>Category</th>
            <th>End Date</th>
            <th>Days Left</th>
			<th>Total Units</th>
            <th>Total No. Of Styles</th>
            <th>Average Price Point</th>
            <th>Avg Discount</th>
			<th>More Info</th>
            <th>Unit Sales Prediction</th>
            <th>Amount Prediction</th>
		</tr>
	</thead>
	<tbody>

<?php
foreach($competitions as $competition) {
    $ce = $competition->getCompetitionEntryByUser($app->user->id);
?>
	<tr>
        <td><?= $competition->event->eventid ?></td>
        <td><?= $competition->event->manufacturerlist->name ?></td>
        <td><?= $competition->event->categorylist->name ?></td>
        <td><?= $competition->event->enddate ?></td>
        <td><?= $competition->getClosedHtmlCode() ?></td>
        <td><?= $competition->totunits ?></td>
        <td><?= $competition->totalstyles ?></td>
        <td><?= $competition->avgpricepoint ?></td>
        <td><?= $competition->avgdiscount ?></td>
        <td><a href='<?= $competition->link ?>'>Event Info</a></td>
        <td>
            <input type='text'
                   id='soldamount<?= $competition->id ?>'
                   class='input-control'
                   placeholder='None Entered So far'
                   value='<?= $ce->soldamount ?>'
            >
        </td>
        <td>
            <input type='text'
                   id='revenueamount<?= $competition->id ?>'
                   placeholder='None Entered So far'
                   value='<?= $ce->revenueamount ?>'
            >
        </td>
        <td>
            <button type='button'
                    class="competition-entries__button-save-result"
                    data-competition-id="<?= $competition->id ?>"
                    title='Submit your entry'
            >
                <i class='fa fa-save'></i>
            </button>
        </td>
	</tr>
	<tr>
		<td colspan='12' class="text-center">
			<div class='col-xs-10 col-xs-offset-1'>
			<table class='table table-condensed'>
                <thead>
                    <tr>
                        <th colspan='4' class="text-center">
                            Other Users Entries
                        </th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Predicted Units Sale</th>
                        <th>Predicted Amount</th>
                        <th>Date Time Submitted</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $competitionEntries = $competition->getOtherCompetitionEntriesWithoutUser($app->user->id);
                foreach($competitionEntries as $competitionEntry) { ?>
                    <tr>
                        <td><?= $competitionEntry->ruser->fullname ?></td>
                        <td><?= $competitionEntry->soldamount ?></td>
                        <td><?= $competitionEntry->revenueamount ?></td>
                        <td><?= $competitionEntry->datetime ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            </div>
        </td>
    </tr>
<?php } ?>
</tbody>
</table>
