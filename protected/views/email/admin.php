<?php
/* @var $this EmailController */
/* @var $model Email */


$this->breadcrumbs=array(
	'Emails'=>array('index'),
	'Manage',
);

?>

<?php echo BsHtml::pageHeader('Manage','Emails') ?>
<button class='btn btn-primary' style='float:right' onclick='location.href="<?php echo Yii::app()->createUrl("email/create") ?>"'>Add New Entry</button>
<div class="panel panel-default">
    <div class="panel-body">
        <!-- search-form -->

        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'email-grid',
			'dataProvider'=>$model->search(),
			'columns'=>array(
        		'id',
		'name',
		'email',
        'purpose',
				array(
					'class'=>'bootstrap.widgets.BsButtonColumn',
				),
			),
        )); ?>
    </div>
</div>




