<?php
/* @var $this EmailController */
/* @var $model Email */
?>

<?php
$this->breadcrumbs=array(
	'Emails'=>array('index'),
	$model->name,
);
?>
<?php echo BsHtml::pageHeader('View','Email '.$model->id) ?>
<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'email',
		'purpose'
	),
)); ?>