<?php
/* @var $this EmailController */
/* @var $model Email */
?>

<?php
$this->breadcrumbs=array(
	'Emails'=>array('index'),
	'Create',
);
?>

<?php echo BsHtml::pageHeader('Create','Email') ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>