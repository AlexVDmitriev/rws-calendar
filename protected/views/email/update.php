<?php
/* @var $this EmailController */
/* @var $model Email */
?>

<?php
$this->breadcrumbs=array(
	'Emails'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);
?>

<?php echo BsHtml::pageHeader('Update','Email '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>