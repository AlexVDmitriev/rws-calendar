<?php

    /**
     * @var BackendController $this
     *
     * @var string $selected_user
     * @var string $manager
     * @var string $warehouse
     * @var string $date
     */

    $this->registerCssFile('fullcalendar2.css');
    $this->registerCssFile('modules/backend/newindex.css');
    $this->registerCssFile("bootstrap-datepicker.min.css");
    $this->registerCssFile("bootstrap-switch.min.css");
    $this->registerCssFile("bootstrap-multiselect.css");

    $this->registerJsModule('modules/backend/new-index');

    /** @var CWebApplication $app */
    $app = Yii::app();

    /** @var User $currentUser */
    $currentUser = $app->user;
    $isvip = false;
    if($currentUser->has_vip_permission)
        $isvip = true;

    $dateTime = empty($date) ? new DateTime() : new DateTime($date);

?>

<script language="JavaScript">
    var filter_for_user = "<?= $manager ?>";
    var canUpdateEvent = "<?= ($currentUser->has_admin_permission || $currentUser->has_can_add_permission) ? 1 : 0 ?>";
    var datetime_update = "<?= date('Y-m-d H:i:s') ?>";
    var tasklist = <?= json_encode(Tasks::getAllTasksList()) ?>;
    var holidays = <?= json_encode(Holidays::getAllHolidaysList()) ?>;
    var currentDate = "<?= $dateTime->format("Y-m-d"); ?>";
    <?=($isvip && !Yii::app()->params['isVip'])?"var showvip = true;":"var showvip = false;"; ?>
</script>
<div style='position:fixed; top:0;left:0;' id='eq_status'></div>

<div class='modal fade' id='massdelete'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header'>
                <div class='modal-title'>
                    <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3>Mass Delete</h3>
                </div>
            </div>
            <div class='modal-body' id='modalbody'>
                <div class="row text-center">
                    <div class="col-xs-3">
                        <b>Select User</b>
                    </div>
                    <div class="col-xs-3">
                        <?= CHtml::dropDownList(
                            '',
                            '',
                            CHtml::listData(
                                User::model()->findAllByAttributes(array('evt_manager'=>1),array('order'=>'firstname,lastname')),
                                'userid',
                                'fullname'
                            ),
                            array(
                                'id'          => 'md_userlist',
                                'placeholder' => 'Select User for filter',
                                'class'       => 'form-control',
                                'empty'       => 'All',

                            )
                        );
                        ?>
                    </div>
                    <div class="col-xs-3">
                        <b>Select Brand :</b>
                    </div>
                    <div class="col-xs-3">
                       <?= CHtml::dropDownList(
                           '',
                           '',
                           CHtml::listData(
                               Category::model()->findAll(array('order'=>'name')),
                               'id',
                               'name'
                           ),
                           array(
                               'id'          => 'md_brandlist',
                               'placeholder' => 'Select Brand for filter',
                               'class'       => 'form-control',
                               'empty'       => 'All',
                           )
                       );
                       ?>
                    </div>
                </div>
                <h4 class="text-center">Please Select the 2 dates : </h4>
                <div class="row">
                    <div class="col-xs-2">
                        <label for="md_from">
                            <b>From Date:</b>
                        </label>
                    </div>
                    <div class="col-xs-4">
                        <input type="date" id="md_from" value="<?= date('Y-m-d') ?>" class="form-control">
                    </div>
                    <div class="col-xs-2">
                        <label for="md_to">
                            <b>To Date:</b>
                        </label>
                    </div>
                    <div class="col-xs-4">
                        <input type="date" id="md_to" value="<?= date('Y-m-d') ?>" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs 12" id="md_status">

                    </div>
                </div>
                <br />
                <div class="row"
                    <div class="col-xs-6 col-xs-offset-3">
                        <button class='btn btn-block btn-warning' id='md_btn' >Mass Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class='modal fade event-modal' id='eventmodal'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-header'>
				<div class='modal-title'>
					<div class='row'>
						<div class='pull-right'>
                            <?php if ($currentUser->has_can_add_permission || $currentUser->has_admin_permission) { ?>
                                <button class='btn btn-success event-modal__button-save'>
                                    <span class='fa fa-save'></span> Save
                                </button>
                                <button class='btn btn-danger event-modal__button-delete'>
                                    <span class='fa fa-trash-o'></span> Delete
                                </button>
                                <button class='btn' data-dismiss="modal">
                                    <span class='fa fa-ban'></span> Close
                                </button>
                            <?php } else { ?>
                                <button class='btn btn-primary' data-dismiss="modal">
                                    <span class='fa fa-check'></span> Ok
                                </button>
                            <?php } ?>
                        </div>
					</div>
				</div>
			</div>
			<div class='modal-body' id='modalbody'>
			</div>
		</div>
	</div>
</div>

<div class='row'>
	<div class='btn-group' role='group' style='position:absolute; top:30px; right: 30px;'>
        <button class='btn btn-danger' id='massdelete_btn' title='Mass Delete, Delete All Visible Events on the calendar'>
            <i class='fa fa-trash-o'></i> &nbsp;
        </button>
		<button class='btn btn-success' id='downloadascsv' title='Download As CSV'>
            <i class='fa fa-download'></i>
        </button>
		<button class='btn btn-primary button-toggle-full-screen' title='Full Screen'>
            <i class='fa fa-expand'></i>
        </button>

		<div class="btn-group" role="group">
			<button class='btn btn-default dropdown-toggle' data-toggle='dropdown' haspopup="true" aria-expanded="false" type='button'>
                <i id='revprediction' class='pull-right'></i><span class="caret pull-right"></span>
            </button>
			<ul class='dropdown-menu' id='revenue_list'>
            </ul>
		</div>
		<div class="btn-group" role="group">
			<button class='btn btn-default dropdown-toggle' data-toggle='dropdown' haspopup="true" aria-expanded="false" type='button'>
                Daily List <span class="caret pull-right"></span>
            </button>
			<ul class='dropdown-menu pull-right' id='revenue_list2'>
			</ul>
		</div>		
	</div>
</div>
<div class='container'>
	<div class='col-xs-offset-2 col-xs-8'>
		<div class='panel-group' id='accordion'>
		<div class='panel panel-default' style="overflow:inherit !important;">
			<div class='panel-heading'>
				<div class='panel-title'>
                    <a data-toggle='collapse' data-parent='#accordion' href='#collapseone'>
                        <i><b>Filters for</b></i>
                    </a>
                    <?=
                    CHtml::dropDownList(
                        '',
                        $selected_user,
                        CHtml::listData(
                            User::model()->findAll(array('condition' => 'active=1')),
                            'userid',
                            'fullname'
                        ),
                        array(
                            'id' => 'userfilter',
                            'multiple'=>'multiple',
                            'style'=>'display:none'
                        )
                    )
                    ?>
                </div>
			</div>
			<div id='collapseone' class='panel-collapse collapse'>
				<div class='panel-body'>
					<b>Select the filters you want to apply : </b>
					<div class='row'>
					</div>
					<div class='row'>
						<div class='col-xs-6'>
                            <label for="radioCategory">Category :</label>
                        </div>
                        <div class='col-xs-6'>
                            <input id="radioCategory"
                                   class="calendar__radio-filter"
                                   type='radio'
                                   name='as'
                                   value='category'
                                   checked='checked'
                                   data-label='Category'
                            >
                        </div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>
                            <label for="radioManager">Manager :</label>
                        </div>
						<div class='col-xs-6'>
                            <input id="radioManager"
                                   class="calendar__radio-filter"
                                   type='radio'
                                   name='as'
                                   value='manager'
                                   data-label='Manager'
                            >
						</div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>
                            <label for="radioBracket">Sales Bracket :</label>
                        </div>
						<div class='col-xs-6'>
                            <input id="radioBracket"
                                   class="calendar__radio-filter"
                                   type='radio'
                                   name='as'
                                   value='bracket'
                                   data-label='Sales Bracket'
                            >
                        </div>
					</div>
                    <div class='row'>
                        <div class='col-xs-6'>
                            <label for="radioStatus">Repeat Styles :</label>
                        </div>
                        <div class='col-xs-6'>
                            <input id="repeatStyles"
                                   class="calendar__radio-filter"
                                   type='radio'
                                   name='as'
                                   data-label='Repeat Styles'
                                   value='repeat_styles'>
                        </div>
                    </div>
					<div class='row'>
						<div class='col-xs-6'>
                            <label for="radioStatus">Status :</label>
                        </div>
						<div class='col-xs-6'>
                            <input id="radioStatus"
                                   class="calendar__radio-filter"
                                   type='radio'
                                   name='as'
                                   data-label='Status'
                                   value='status'
                            >
                        </div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>
                            <label for="radioWarehouse">Warehouse :</label>
                        </div>
						<div class='col-xs-6'>
						<table>
                            <tr>
                                <td>
                                    <input id="radioWarehouse"
                                           class="calendar__radio-filter"
                                           type='radio'
                                           name='as'
                                           data-show2='1'
                                           data-label='Warehouse'
                                           value='warehouse'
                                    >
                                </td>
                                <td>
						            <div class='col-xs-10'>
                                        <div class='hidden' id='warehouse'>
                                            <?=
                                            CHtml::dropDownList(
                                                "",
                                                $warehouse,
                                                CHtml::listData(
                                                    Warehouse::model()->findAll(),
                                                    'id',
                                                    'name'
                                                ),
                                                array(
                                                    'id' => 'warehouse_sel',
                                                    'class' => 'form-control',
                                                    'empty' => 'All',
                                                )
                                            )
                                            ?>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>
                            <label for="radioTotalUnitsAvailable">Total Units Available :</label>
                        </div>
						<div class='col-xs-6'>
                            <input id="radioTotalUnitsAvailable"
                                   class="calendar__radio-filter"
                                   type='radio'
                                   name='as'
                                   data-label='Total units available'
                                   value='totalunitsavailable'
                            >
                        </div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>
                            <label for="radioTask">Marketing :</label>
                        </div>
						<div class='col-xs-1'>
							<input id="radioTask"
                                   class="calendar__radio-filter"
                                   type='radio'
                                   name='as'
                                   value='task'
                                   data-label='Marketing'
                                   data-show='1'
                            >
						</div>
                    </div>
                    <div class="row hidden taskspart">
                        <div class='col-xs-2'>
                            <label for="tasks">Select Task :</label>
                        </div>
                        <div class='col-xs-4'>
                            <?=
                            CHtml::dropDownList(
                                '',
                                '',
                                CHtml::listData(
                                    Tasks::model()->findAll(),
                                    'shortname',
                                    'taskname'
                                ),
                                array(
                                    'id' => 'tasks',
                                    'class' => 'form-control',
                                    'empty' => 'Show All',
                                )
                            )
                            ?>
                        </div>
                        <div class='col-xs-2'>
                            <label for="states">State :</label>
                        </div>
                        <div class='col-xs-4'>
                            <select id='states' class='form-control'>
                                <option value='0' class="states-not-required">Not Required</option>
                                <option value='1' class="states-pending">Pending</option>
                                <option value='2' class="states-doing">Doing</option>
                                <option value='3' class="states-done">Done</option>
                            </select>
                        </div>
                    </div>
					<div class='row'>
						<div class='col-xs-6'>
                            <label for="radioDayWise">Specific Day :</label>
                        </div>
                        <div class='col-xs-6'>
							<input id="radioDayWise"
                                   class="calendar__radio-filter"
                                   type='radio'
                                   name='as'
                                   value='daywise'
                                   data-label='Specific Date'
                                   data-show3='1'
                            >
							<input type='text' id='selected_from_date' placeholder='From Date' class='datepicker hidden'>
							<input type='text' id='selected_to_date' placeholder='To Date' class='datepicker hidden'>
						</div>
					</div>
                    <div class='row'>
                        <div class='col-xs-6'>
                            <label for="radioBrandFilter">Brand :</label>
                        </div>
                        <div class='col-xs-6'>
                            <input id="radioBrandFilter"
                                   type='radio'
                                   name='as'
                                   value='brandfilter'
                                   data-label='Brand View'
                                   data-show4='1'
                            >
                            <?= CHtml::dropDownList(
                                '',
                                '',
                                CHtml::listData(
                                    Manufacturer::model()->findAll(),
                                    'id',
                                    'name'
                                ),
                                array(
                                    'id'          => 'filter_brand',
                                    'placeholder' => 'Select Brand Filter',
                                    'style'       => 'display:none;',
                                    'empty'       => 'All',
                                )
                            )
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class='col-xs-6'>
                            <label for="radioDayWise">Stock Type :</label>
                        </div>
                        <div class="col-xs-1">
                            <input id="radioStockType"
                                   class="calendar__radio-filter"
                                   type='radio'
                                   name='as'
                                   value='stocktype'
                                   data-label='Stock Type'
                                   data-show5='1'
                            >
                        </div>
                        <div class="col-xs-5">
                            <select id='stock_type' class='form-control' style="display:none;">
                                <option value=''>All</option>
                                <option value='SCS'>SCS</option>
                                <option value='Non-SCS'>Non-SCS</option>
                                <option value='RS'>RS</option>
                                <option value='SCS & RS'>SCS & RS</option>
                            </select>
                        </div>
                    </div>
				</div>
			</div>
		</div>
		</div>
	</div>
	<div class='row'>
		<div class='col-xs-12'>
			<div id='mycalender'></div>
		</div>
	</div>	
</div>

