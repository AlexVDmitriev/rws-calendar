<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * @var $fromdate string
 * @var $todate string
 */
$this->registerJsModule( "modules/backend/mkrequest" );
?>
<center><h2 id="heading">MK Request <?=$fromdate ?> till <?=$todate ?></h2></center><br>
<center>
    <button type="button" id="exportbtn"><span class="fa fa-download"></span> Export To CSV</button>
    <button type="button" id="saveall"><span class="fa fa-clone"></span> Save All</button>
</center>
<div class="container2">
    <div id="floating_head" class="hidden"></div>
<table class="table table-striped" id="data_table">
    <thead class="header">
    <tr>
        <th>Day</th>
        <th>Brand</th>
        <th>Category</th>
        <th>Buyer</th>
        <th>Notes</th>
        <th class="push-budget">Push Budget</th>
        <th>Adwords</th>
        <th>FB Ads</th>
        <th>Email Banner</th>
        <th>Website Banner</th>
        <th>Facebook</th>
        <th>Twitter</th>
        <th>Insta</th>
        <th>MM</th>
        <th>Mobile Notification</th>
        <th>Friday Frenzy</th>

        <th>Midweek Mailer</th>
        <th>Weekend Mailer</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $evts = Event::model()->findAllBySql("SELECT * from event where startdate>=:fromdate and startdate<=:todate and isVip=:isvip order by startdate,manufacturer",array('fromdate'=>$fromdate,'todate'=>$todate,'isvip'=>Yii::app()->params['isVip']));
    /* @var $event Event */
    $olddt = null;
    foreach($evts as $event){
        $data = $event->report_data;
        $report_data = null;
        $islocked = 0;
        if($data != NULL){
            $report_data = json_decode($data->data,true);
            $islocked = $data->islocked;
        }
        if($olddt != $event->startdate || $olddt == null) {
            $dtstr = date('D M dS', strtotime($event->startdate));
            echo "<tr>
                <td colspan='19'><hr></td>
            </tr>";
        }
        else
            $dtstr = "";
        ?>
        <tr data-rowid="<?=$event->id; ?>" <?=$islocked?"class='lockedrow'":""; ?>>
            <td><?=$dtstr ?></td>
            <td><?=$event->manufacturerlist?$event->manufacturerlist->name:"" ?></td>
            <td><?=$event->categorylist?$event->categorylist->name:"" ?></td>
            <td><?=$event->managerlist->getFullName() ?></td>
            <td><?php
                echo "<input type='text' name='notes' value='".(($report_data != null && isset($report_data['notes']))?$report_data['notes']:"")."' ".($islocked?"disabled='disabled'":"").">";
            ?></td>
            <td class='push-budget'><input type="checkbox" name="push_budget" value='1' <?php echo (($report_data != null && isset($report_data['push_budget']))?"checked='checked'":""); ?> <?php echo ($islocked?"disabled='disabled'":""); ?>></td>
            <td><?php
                echo "<input type='checkbox' name='adwords' value='1' ".($islocked?"disabled='disabled'":"")." ".(($report_data != null && isset($report_data['adwords']) && $report_data['adwords'] ==1)?"checked='checked'":"").">";
            ?></td>
            <td><?php
                echo "<input type='checkbox' name='fbads' value='1' ".($islocked?"disabled='disabled'":"")." ".(($report_data != null && isset($report_data['fbads']) && $report_data['fbads'] ==1)?"checked='checked'":"").">";
            ?></td>
            <td><?php
                echo "<input type='checkbox' name='emailbanner' value='1' ".($islocked?"disabled='disabled'":"")." ".(($report_data != null && isset($report_data['emailbanner']) && $report_data['emailbanner'] ==1)?"checked='checked'":"").">";
            ?></td>
            <td><?php
                echo "<input type='checkbox' name='websitebanner' value='1' ".($islocked?"disabled='disabled'":"")." ".(($report_data != null && isset($report_data['websitebanner']) && $report_data['websitebanner'] ==1)?"checked='checked'":"").">";
            ?></td>
            <td><?php
                echo "<input type='checkbox' name='facebook' value='1' ".($islocked?"disabled='disabled'":"")." ".(($report_data != null && isset($report_data['facebook']) && $report_data['facebook'] ==1)?"checked='checked'":"").">";
            ?></td>
            <td><?php
                echo "<input type='checkbox' name='twitter' value='1' ".($islocked?"disabled='disabled'":"")." ".(($report_data != null && isset($report_data['twitter']) && $report_data['twitter'] ==1)?"checked='checked'":"").">";
            ?></td>
            <td><?php
                echo "<input type='checkbox' name='insta' value='1' ".($islocked?"disabled='disabled'":"")." ".(($report_data != null && isset($report_data['insta']) && $report_data['insta'] ==1)?"checked='checked'":"").">";
            ?></td>
            <td><?php
                echo "<input type='checkbox' name='mm' value='1' ".($islocked?"disabled='disabled'":"")." ".(($report_data != null && isset($report_data['mm']) && $report_data['mm'] ==1)?"checked='checked'":"").">";
            ?></td>
            <td><?php
                echo "<input type='checkbox' name='mobilenotification' value='1' ".($islocked?"disabled='disabled'":"")." ".(($report_data != null && isset($report_data['mobilenotification']) && $report_data['mobilenotification'] ==1)?"checked='checked'":"").">";
            ?></td>
            <td><?php
                echo "<input type='checkbox' name='fridayfrenzy' value='1' ".($islocked?"disabled='disabled'":"")." ".(($report_data != null && isset($report_data['fridayfrenzy']) && $report_data['fridayfrenzy'] ==1)?"checked='checked'":"").">";
            ?></td>
            <td><?php
                echo "<input type='text' name='midweekmailer' value='".(($report_data != null && isset($report_data['midweekmailer']))?$report_data['midweekmailer']:"")."' ".($islocked?"disabled='disabled'":"").">";
            ?></td>
            <td><?php
                echo "<input type='text' name='weekendmailer' value='".(($report_data != null && isset($report_data['weekendmailer']))?$report_data['weekendmailer']:"")."' ".($islocked?"disabled='disabled'":"").">";
            ?></td>
            <td>
                <button type="button" class="btn btn-success row-save-btn" data-id="<?=$event->id; ?>" <?=($islocked?"disabled='disabled'":""); ?>><span class="fa fa-save"></span></button>
                <button type="button" class="btn btn-warning row-lock-btn" data-id="<?=$event->id; ?>"><span class="fa <?=($islocked?"fa-lock":"fa-unlock"); ?>"></span></button>
            </td>
        </tr>
        <?php
        $olddt = $event->startdate;
    }
    ?>
    </tbody>
</table>
</div>
<style>
    .containter2 #data_table tbody tr:first-child {
        padding-top:20px;
    }
    .push-budget {
        background-color: #9e9e9e !important;
    }
    .container2 {
        height:500px;
        width:100%;
        overflow-y:auto;
        position:relative;
    }
    .floating {
        position:absolute;
        top:0;
        left:0;
    }
    #floating_head {
        background-color:#ffffff;
        position:absolute;

        left:0;
    }
    #page {
        width:100% !important;
    }
    .lockedrow  > td {
        background-color:#edde34 !important;
    }

</style>