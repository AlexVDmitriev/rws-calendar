<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * Date: 26-Aug-17
 * Time: 12:09
 * @var ChangeLog[] $logs
 */
echo "<button type='button' id='downloadcsv'><div class='fa fa-download'></div> Download CSV</button>\n";

echo "<table class='table'>\n";
echo "<thead>
            <tr>
                <th>Manager</th>
                <th>Event Code</th>
                <th>Brand</th>
                <th>Category</th>
                <th>Supplier</th>
                <th>Month</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Week</th>
                <th>Year</th>
                <th>Sales Prediction</th>
                <th>Field Changed</th>
                <th>Date Time Of Change</th>
                <th>Old Value</th>
            </tr>
            </thead>\n";
echo "<tbody>\n";

foreach($logs as $log){
    $evt = Event::model()->findByPk($log->changeid);
    if($evt != null) {
        echo "<tr>";
        echo "<td>".$log->userlist->fullname."</td>";
        echo "<td>".$evt->eventid."</td>";
        echo "<td>".$evt->manufacturerlist->name."</td>";
        echo "<td>".$evt->categorylist->name."</td>";
        echo "<td>".$evt->supplist->name."</td>";
        $start_color = "";
        if($log->fieldname == "start date") {
            $start_color = "#aaaaaa";
            $evt->startdate = $log->newvalue;
        }
        else
            $start_color = "";

        echo "<td>".date('F',strtotime($evt->startdate))."</td>";
        echo "<td style='background-color:$start_color'>".date('d-m-Y',strtotime($evt->startdate))."</td>";
        $start_color = "";
        if($log->fieldname == "end date") {
            $end_color = "#aaaaaa";
            $evt->enddate = $log->newvalue;
        }
        else
            $end_color = "";

        echo "<td style='background-color:$end_color'>".date('d-m-Y',strtotime($evt->enddate))."</td>";
        echo "<td>".date('W',strtotime($evt->startdate))."</td>";
        echo "<td>".date('Y',strtotime($evt->startdate))."</td>";
        if($log->fieldname == "revenue prediction") {
            $rev_color = "#aaaaaa";
            $evt->revprediction = $log->newvalue;
        }
        else
            $rev_color = "";

        echo "<td style='background-color:$rev_color;'>".number_format($evt->revprediction,2,".",",")."</td>";
        echo "<td>".$log->fieldname."</td>";
        echo "<td>".$log->datetime."</td>";
        echo "<td>".$log->oldvalue."</td>";
        echo "</tr>\n";
    }
}
echo "</tbody>\n";
echo "</table>\n";