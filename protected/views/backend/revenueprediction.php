<?php

/**
 * @var BackendController $this
 * @var string $month
 * @var string $year
 */

$this->registerJsModule('modules/backend/revenue-prediction');

?>

<div class="text-center">
    <h3>Select Date :
        <?=
        CHtml::dropDownList(
            '',
            (int)$month,
            [1 => "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"],
            array(
                'id' => 'month',
            )
        )
        ?>
        /
        <?=
        CHtml::dropDownList(
            '',
            (int)$year,
            array_combine(
                range(2010, 2020, 1),
                range(2010, 2020, 1)
            ),
            array(
                'id' => 'year',
            )
        )
        ?>
        <button class='btn btn-success event__button-refresh-report' >Search</button>
        <button class="btn btn-success event__button-download-report pull-right">
            <i class="fa fa-download"></i> Download As Csv
        </button>
    </h3>
</div>

<table class='table' id='result'>
	<thead>
		<tr>
			<th>Date</th>
			<th>Daily Prediction</th>
			<th>Target</th>
			<th>Difference</th>
            <th>Brand Starting</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>

<div class="text-center">
    <button class='btn btn-success event__button-save-targets' type='button'>
        <i class='fa fa-save'></i> Save Targets
    </button>
</div>
