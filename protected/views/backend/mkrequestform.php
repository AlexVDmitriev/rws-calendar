<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * Date: 05-Jan-18
 * Time: 18:36
 */
$this->registerJsModule( "modules/backend/mkrequestform" );
$this->registerCssFile("bootstrap-datepicker.min.css");
?>
<form method="post" id="mkform" action="<?=Yii::app()->createUrl("Backend/mkrequest"); ?>">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>MK Request Form</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-4">
                            <b>Start Date</b>
                        </div>
                        <div class="col-xs-8">
                            <input type="text" name="fromdate" id='fromdate' class="form-control datepicker">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <b>End Date</b>
                        </div>
                        <div class="col-xs-8">
                            <input type="text" readonly="readonly"  id='todate' name="todate" class="form-control datepicker">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center"><br>
                            <button class="btn btn-success">Generate</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

