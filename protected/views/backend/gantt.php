<?php

/**
 * @var BackendController $this
 */

/** @var CWebApplication $app */
$app = Yii::app();

/** @var User|WebUser $currentUser */
$currentUser = $app->user;

$this->registerCssFile( "bootstrap-switch.min.css" );

$this->registerJsModule('modules/backend/gantt');

?>
<link href="/css/modules/backend/gantt.css" rel="stylesheet">
<h1>Gantt Chart View</h1>
<hr><p>
<script language="JavaScript">
    var revlist = <?= json_encode(SalesBracket::getRevList()) ?>;
</script>
<div class='modal fade event-modal' id='eventmodal'>
	<div class='modal-dialog'>
		<div class='modal-content'>
            <div class='modal-body' id='modalbody'>
                <!-- this contents will be replaced by ajax -->
            </div>
			<div class='modal-footer'>
                <?php if ($currentUser->has_admin_permission || $currentUser->has_can_add_permission) { ?>
                    <button class='btn btn-success event-modal__button-save'>
                        <span class='fa fa-save'></span> Save
                    </button>
                    <button class='btn btn-danger event-modal__button-delete'>
                        <span class='fa fa-trash-o'></span> Delete
                    </button>
                    <button class='btn' data-dismiss="modal"><span class='fa fa-ban'></span> Close</button>
                <?php } else { ?>
                    <button class='btn btn-primary' data-dismiss="modal"><span class='fa fa-check'></span> Ok</button>
                <?php } ?>
			</div>
		</div>
	</div>
</div>
	<div class='panel-group' id='accordion'>
		<div class='panel panel-default'>
			<div class='panel-heading'>	
				<div class='panel-title'><i><b><a data-toggle='collapse' data-parent='#accordion' href='#collapseone'>
				Filters
				</a></b></i></div>
			</div>
			<div id='collapseone' class='panel-collapse collapse'>
				<div class='panel-body'>
					<b>Select the filters you want to apply : </b>
					<div class='row'>
						<div class='col-xs-6'>Category :</div><div class='col-xs-6'>
						    <INPUT type='radio'
                                   name='filter'
                                   value='category'
                                   checked='checked'
                                   class="chart__radio-filter"
                            >
                        </div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>Manager :</div><div class='col-xs-6'>
						    <INPUT type='radio'
                                   name='filter'
                                   value='manager'
                                   class="chart__radio-filter"
                            >
                        </div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>Sales Bracket :</div><div class='col-xs-6'>
						    <INPUT type='radio'
                                   name='filter'
                                   value='sales'
                                   class="chart__radio-filter"
                            >
                        </div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>Status :</div><div class='col-xs-6'>
						    <INPUT type='radio'
                                   name='filter'
                                   value='status'
                                   class="chart__radio-filter"
                            >
                        </div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>Warehouse :</div><div class='col-xs-6'>
						    <INPUT type='radio'
                                   name='filter'
                                   value='warehouse'
                                   class="chart__radio-filter"
                            >
                        </div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>Total Units Available :</div><div class='col-xs-6'>
						    <INPUT type='radio'
                                   name='filter'
                                   value='unitsavailable'
                                   class="chart__radio-filter"
                            >
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class='modal fade' id='eventmodal'>
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class='modal-body'>yoyoyo</div>
			<div class='modal-footer'>
				<button class='btn btn=primary' data-dismiss="modal">
                    <span class='fa fa-check'></span>Ok
                </button>
			</div>
		</div>
	</div>
</div>
<div class='container'>
	<div class='col-xs-2'>
        <button type='button' class='btn btn-primary chart__button-back' >< Prev</button>
    </div>
	<div class='col-xs-8 text-center'>
        <h3 id='datetitle'></h3>
    </div>
	<div class='col-xs-2'>
        <button type='button' class='btn btn-primary chart__button-forward'>Next ></button>
    </div>
</div>
<div id='chart'></div>