<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * Date: 15-Jan-18
 * Time: 18:03
 */
$this->registerJsModule( "modules/backend/dailymailer" );
$this->registerCssFile("bootstrap-datepicker.min.css");
$from = new DateTime($fromdate);
$to = new DateTime($todate);
$cdate = new DateTime($from->format("Y-m-d"));
echo "<center><button type='button' class='btn btn-success' id='download_as_csv'><div class='fa fa-download'></div> Download as CSV</button>";
while($cdate->diff($to)->invert == 0){
$evts = Event::model()->findAllBySql("SELECT * from event where startdate=:date and isVip=:isvip;",array('date'=>$cdate->format("Y-m-d"),'isvip'=>Yii::app()->params['isVip']));
$firsttime = 1;
if(count($evts) > 0){
        /* @var Event $evt */
        $counter = 0;
        foreach ($evts as $evt) {
            if($firsttime) {
                $firsttime = 0;
            ?>
            <div class="date-box" style="text-align: center">
                <h3 class="heading">Events from Date : <?= $cdate->format("D d M Y"); ?></h3>
                <h4>Total Events this day : <?php echo count($evts); ?></h4>
                <hr>
            <?php
            }
            ?>
            <table class="data-table" style="margin-left:auto; margin-right:auto; text-align:left; width:50%">

                <tr>
                    <td><b><?=$counter+1; ?></b></td>
                    <td class="column"><b>Brand:</b></td>
                    <td class="value"><?= $evt->marketingDetails?$evt->marketingDetails->event_name:$evt->manufacturerlist->name; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column"><b>Gender & Product Type :</b></td>
                    <td class="value"><?= $evt->marketingDetails?$evt->marketingDetails->gender_product:""; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column"><b>Buyer :</b></td>
                    <td class="value"><?= $evt->marketingDetails?$evt->marketingDetails->managerlist->fullname:$evt->managerlist->fullname; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column"><b>End Day & Date :</b></td>
                    <td class="value"><?= $evt->marketingDetails?date('l d M Y', strtotime($evt->marketingDetails->end_datetime)):$evt->enddate; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column"><b>Product to Highlight:</b><br>
                        <small>(Link to product or to server)</small>
                    </td>
                    <td class="value"><?= $evt->marketingDetails?$evt->marketingDetails->product_images:""; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column"><b>Event Link :</b></td>
                    <td class="value"><?= $evt->marketingDetails?$evt->marketingDetails->event_link:""; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column"><b>Max Discount :</b></td>
                    <td class="value"><?= $evt->marketingDetails?$evt->marketingDetails->max_discount:""; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column"><b>Lowest Price :</b></td>
                    <td class="value"><?= $evt->marketingDetails?$evt->marketingDetails->lowest_price:""; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column"><b>Delivery :</b><br>
                        <small>(Fast Shipping)</small>
                    </td>
                    <td class="value"><?= $evt->marketingDetails?$evt->marketingDetails->delivery:""; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column"><b>Promotions :</b><br>
                        <small>(Stickers)</small>
                    </td>
                    <td class="value"><?= $evt->marketingDetails?$evt->marketingDetails->promotions:""; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="column"><b>Extra Comments:</b>
                    </td>
                    <td class="value"><?= $evt->marketingDetails?$evt->marketingDetails->extra_comments:""; ?></td>
                </tr>
            </table>
                <br>
            <?php
            $counter++;
        }
        echo "</div>\n";
    }
    $cdate->modify("+1 days");
}
?>

