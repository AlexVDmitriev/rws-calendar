<?php

/**
 * @var BackendController $this
 * @var $date
 * @var $logs
 */

?>
<body style='font-family:arial'>
<table style='width:80%'>
    <tr>
        <th colspan='2'>
            <h2>Changes on <?= $date ?></h2>
        </th>
    </tr>
    <tr>
        <td colspan='2'><hr></td>
    </tr>
    <tr>
        <th>Date Time</th>
        <th>Action</th>
        <th>Reason</th>
    </tr>

    <?php foreach ($logs as $log) { ?>
        <tr>
            <td>
                <b><?= date("d-m-Y H:i:s", strtotime($log->datetime)) ?></b>
            </td>
            <td>
                <?= $this->getLogDescription($log) ?>
            </td>
            <td>
                <?= $log->reason ?>
            </td>
        </tr>
    <?php } ?>
</table>
