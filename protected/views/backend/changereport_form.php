<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * Date: 24-Aug-17
 * Time: 20:53
 */
$this->registerJsModule('modules/backend/changereport');
$this->registerCssFile("bootstrap-datepicker.min.css");
?>
<center>
    <h1>Change Report</h1><br>
    <table style="width:50%;">
        <tbody>
            <tr>
                <td><b>Select Manager</b></td>
                <td>
                    <select id="manager" class="form-control">
                        <option value="">All</option>
                        <?php
                            $users = User::model()->findAllByAttributes(array('active'=>1,'evt_manager'=>1));
                            foreach ($users as $user){
                                echo "<option value='".$user->userid."'>".$user->fullname."</option>\n";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><b>From Date</b></td>
                <td><input type="text" id="fromdate" value="<?php echo date('Y-m-d'); ?>" class="datepicker form-control"></td>
            </tr>
            <tr>
                <td><b>To Date</b></td>
                <td><input type="text" id="todate" value="<?php echo date('Y-m-d'); ?>" class="datepicker form-control"></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><br>
                    <button type="button" id='gen_btn' class="btn btn-success"><div class="fa fa-cog"></div> Generate Report</button>
                </td>
            </tr>
        </tbody>
    </table>
    <br><br>
    <div id="results">

    </div>
</center>

