<?php

/**
 * @var BackendController $this
 * @var CArrayDataProvider $data
 * @var FiltersForm $filtersForm
 */

$this->registerJsModule('modules/backend/brand-report');

?>
<div class='row'>
	<div class='pull-right'>
        <button class='btn btn-success brand-report__button-download-csv' type='button' >
            <i class='fa fa-download'></i> Download as CSV
        </button>
    </div>
</div>
<div class="text-center">
    <h2>Brand Report as on <?= date("d-m-Y") ?></h2>
</div>
<?php
$this->widget(
    "bootstrap.widgets.BsGridView",
    array(
        'dataProvider' => $data,
        'id' => 'table',
        'enableSorting' => true,
        'rowCssClassExpression' => '(!empty($data["terminated"])) ? "terminated" : ""',
        'filter' => $filtersForm,
        'enablePagination' => false,
        'columns' => array(
            array('name' => 'manufacturer', 'header' => 'Brand'),
            array('name' => 'manager', 'header' => 'Manager'),
            array('name' => 'supplier', 'header' => 'Suppliers'),
            array('name' => 'category', 'header' => 'Category'),
            array('name' => 'lastevent', 'header' => 'Days Since Last Event'),
            array('name' => 'nextevent', 'header' => 'Days till Next Event'),
            array('name' => 'totalrun', 'header' => 'Total Campaigns Run')
        )
    )
);
