<?php

/**
 * @var BackendController $this
 * @var Event[] $events
 *
 * @var string $fromdate
 * @var string $todate
 * @var string $manager
 * @var string $supplier
 * @var string $brand
 */

?>
<div class="text-center">
<h1>Sales Report from <?= $fromdate ?> to <?= $todate ?> for <?= $manager ?></h1>
    <a class='btn btn-success'
       type='button'
       href='<?= Yii::app()->createUrl(
           "Backend/SalesReportCSV",
           array(
               'fromdate' => $fromdate,
               'todate'   => $todate,
               'manager'  => $manager,
               'supplier' => $supplier,
               'brand'    => $brand
           )
       ) ?>'
    >
        <i class='fa fa-download'></i>
        Export as CSV
    </a>
<table class='table condensed'>
	<thead>
		<tr>
			<th>Manager</tH>
			<th>Event Code</tH>
			<th>Brand</tH>
			<th>Category</th>
			<th>Supplier</tH>
			<th>Start Date</tH>
			<th>End Date</tH>
			<th>Week</tH>
			<th>Year</tH>
			<th>Warehouse</th>
			<th>Stock Type</th>
			<th>% Discount from RRP</tH>
			<th>Average Price Point</tH>
			<th>Total Styles</tH>
			<th>Total Units Available</tH>
			<th>Units Sold</tH>
			<th>Sell-Through %</tH>
			<th>Number Of Customers</th>
			<th>Total Cost Inc Vat.</th>
			<th>Total Sales</tH>
			<th>Buyers Sale Prediction</th>
			<th>Predicted Vs Actual Sales (%)</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	foreach($events as $event) {
		$startDate = new DateTime($event->startdate);
		$endDate = new DateTime($event->enddate);
		/** @var Supplier $supplier */
		$supplier = Supplier::model()->findByPk($event->supplierid);
		/** @var Competition $competition */
		$competition = Competition::model()->findByAttributes(array('eventid'=>$event->id));
    ?>
		<tr>
			<td><?= $event->managerlist->fullname ?></td>
			<td><?= $event->eventid ?></td>
			<td><?= $event->manufacturerlist->name ?></td>
            <td><?= (empty($event->categorylist)) ? "" : $event->categorylist->name ?></td>
            <td><?= (empty($supplier)) ? "" : $supplier->name ?></td>
			<td><?= $startDate->format("d-m-Y") ?></td>
			<td><?= $endDate->format("d-m-Y") ?></td>
			<td><?= $startDate->format("W") ?></td>
			<td><?= $startDate->format("Y") ?></td>
			<td><?= $event->warehouselist->name ?></td>
			<td><?= $event->stocktype ?></td>
			<td class="text-center"><?= $competition->avgdiscount ?></td>
			<td><?= $competition->avgpricepoint ?></td>
			<td><?= $competition->totalstyles ?></td>
			<td><?= $competition->totunits ?></td>
			<td><?= $event->actualunitsales ?></td>
			<td><?= $event->actualsellthrough ?></td>
			<td><?= $event->noofcustomers ?></td>
			<td><?= $event->costincvat ?></td>
			<td><?= $event->actualrevenue ?></td>
			<td><?= $event->revprediction ?></td>
			<td><?= $event->predvsact ?></td>
		</tr>
    <?php } ?>
	</tbody>
</table>
</div>
