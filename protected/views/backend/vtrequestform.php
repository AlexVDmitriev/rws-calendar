<?php
/**
 * Created by PhpStorm.
 * User: nikhil

 */
$this->registerJsModule( "modules/backend/vtrequestform" );
$this->registerCssFile("bootstrap-datepicker.min.css");
?>
<form method="post" id="vtform" action="<?=Yii::app()->createUrl("Backend/vtrequest"); ?>">
    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3>VT Request Report</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-4">
                            <b>Start Date</b>
                        </div>
                        <div class="col-xs-8">
                            <input type="text" id="fromdate" name="fromdate" class="form-control datepicker">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <b>End Date</b>
                        </div>
                        <div class="col-xs-8">
                            <input type="text" id="todate" name="todate" readonly='readonly' class="form-control datepicker">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-center"><br>
                            <button class="btn btn-success">Generate</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<style>
    .datepicker-days tr:hover {
        background-color: #808080;
    }
</style>
