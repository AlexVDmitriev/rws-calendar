<?php

/**
 * @var BackendController $this
 */

$this->registerCssFile('bootstrap-datepicker.min.css');
$this->registerJsModule( 'modules/backend/sales-report-entry' );

?>
<form method='POST' action='<?= Yii::app()->createUrl("Backend/SalesReportGen") ?>'>
<div class='row text-center'>
		<div class='col-xs-6 col-xs-offset-3'>
			<div class='panel panel-default'>
				<div class='panel-heading text-center'>
					<h3>Sales Report</h3>
				</div>
				<div class='panel-body'>
					<div class='row'>
						<div class='col-xs-6'>
							<b>From Date </b><br>
							<input type='text' name='fromdate' class='datepicker form-control'>
						</div>
						<div class='col-xs-6'>
							<b>To Date </b><br>
							<input type='text' name='todate' class='datepicker form-control'>
						</div>
					</div>
					<div class='row'>
						<div class='col-xs-12'>
							<b>Manager</b><b>
                            <?=
                            CHtml::dropDownList(
                                'manager',
                                '',
                                CHtml::listData(
                                    User::model()->findAll(array('condition' => 'active=1')),
                                    'userid',
                                    'fullName'
                                ),
                                array(
                                    'class' => 'form-control',
                                    'empty' => 'All',
                                )
                            )
                            ?>
						</div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>
							<b>Brand : </b>
							<br>
                            <?=
                            CHtml::dropDownList(
                                'brand',
                                '',
                                CHtml::listData(
                                    Manufacturer::model()->findAll(array('order' => 'name')),
                                    'id',
                                    'name'
                                ),
                                array(
                                    'id' => 'brand',
                                    'class' => 'form-control event__manufacturer',
                                    'empty' => 'Any',
                                )
                            )
                            ?>
						</div>
						<div class='col-xs-6'>
							<b>Suppliers : </b>
							<br>
                            <?=
                            CHtml::dropDownList(
                                'supplier',
                                '',
                                CHtml::listData(
                                    Supplier::model()->findAll(array('order' => 'name')),
                                    'supplierid',
                                    'name'
                                ),
                                array(
                                    'id' => 'supplier',
                                    'class' => 'form-control',
                                    'empty' => 'Any',
                                )
                            )
                            ?>
						</div>
					</div>
					<br>
                    <div class='row text-center'>
                        <button type='submit' class='btn btn-success'>Generate</button>
                    </div>
				</div>
			</div>
		</div>
</div>
</form>
