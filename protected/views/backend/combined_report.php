<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 */
$this->registerJsModule( "modules/backend/combined_report" );
$evts = Event::model()->findAllBySql("SELECT * from event where startdate>=:fromdate and startdate<=:todate and isVip=:isvip order by startdate,manufacturer",array('fromdate'=>$fromdate,'todate'=>$todate,'isvip'=>Yii::app()->params['isVip']));
?>
<center><h2 id="heading">Combined report for <?=$fromdate ?> till <?=$todate ?></h2></center><br>
<center>
    <button type="button" id="exportbtn"><span class="fa fa-download"></span> Export To CSV</button>
</center>
<table class="table table-striped" id="data_table">
    <thead class="header">
    <tr>
        <th>Manager</th>
        <th>Event Code</th>
        <th>Brand</th>
        <th>Category</th>
        <th>Supplier</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Week</th>
        <th>Year</th>
        <th>Warehouse</th>
        <th>Stock Type</th>
        <th>% Discount from RRP</th>
        <th>Average Price Point</th>
        <th>Total Styles</th>
        <th>Total Units Available</th>
        <th>Units Sold</th>
        <th>Sell Through %</th>
        <th>Number Of Customers</th>
        <th>Total Cost Inc. VAT.</th>
        <th>Total Sales</th>
        <th>Buyers Sale Prediction</th>
        <th> Predicted Vs Actual Sales(%)</th>
        <th>Sample Date</th>
        <th>Amount</th>
        <th>Sample Type</th>
        <th>Shoot</th>
        <th>Supplied Images</th>
        <th>Repeats / Done</th>
        <th>Date Change</th>
        <th>Info Date</th>
        <th>Comments</th>
        <th>Life Style Images</th>
        <th>Event Link</th>
        <th>Product Images</th>
        <th>Max Discount</th>
        <th>Lowest Price</th>
        <th>Delivery</th>
        <th>Promotion</th>
        <th>Comments</th>
        <th>Adwords</th>
        <th>FB Ads</th>
        <th>Email Banner</th>
        <th>Website Banner</th>
        <th>Facebook</th>
        <th>Twitter</th>
        <th>Insta</th>
        <th>MM</th>
        <th>Mobile Notification</th>
        <th>Friday Frenzy</th>

    </tr>
    </thead>
    <tbody>
    <?php
    for($i=0; $i < count($evts); $i++){
        /* @var Event $evt */
        $evt = $evts[$i];
        $data=  $evt->report_data;
        $report_data = [];
        if($data != NULL){
            $report_data = json_decode($data->data,true);
            $islocked = $data->islocked;
        }
        echo "<tr>\n";
        echo "<td>".$evt->managerlist->getFullName()."</td>\n";
        echo "<td>".$evt->eventid."</td>\n";
        echo "<td>".$evt->manufacturerlist->name."</td>\n";
        echo "<td>".$evt->categorylist->name."</td>\n";
        echo "<td>".$evt->supplist->name."</td>\n";
        echo "<td>".$evt->startdate."</td>\n";
        echo "<td>".$evt->enddate."</td>\n";
        echo "<td>".date("W",strtotime($evt->startdate))."</td>\n";
        echo "<td>".date("Y",strtotime($evt->startdate))."</td>\n";
        echo "<td>".$evt->warehouselist->name."</td>\n";
        echo "<td>".$evt->stocktype."</td>\n";
        echo "<td>".$evt->competition->avgdiscount."</td>\n";
        echo "<td>".$evt->competition->avgpricepoint."</td>\n";
        echo "<td>".$evt->competition->totalstyles."</td>\n";
        echo "<td>".$evt->competition->totunits."</td>\n";
        echo "<td>".$evt->actualunitsales."</td>\n";
        echo "<td>".$evt->actualsellthrough."</td>\n";
        echo "<td>".$evt->noofcustomers."</td>\n";
        echo "<td>".$evt->costincvat."</td>\n";
        echo "<td>".$evt->enddate."</td>\n";
        echo "<td>".$evt->actualrevenue."</td>\n";
        echo "<td>".$evt->revprediction."</td>\n";
        echo "<td>".$evt->predvsact."%</td>\n";
        echo "<td>".$evt->marketingDetails->samples_arrived_date."</td>\n";
        echo "<td>".$evt->marketingDetails->sample_quantity."</td>\n";
        echo "<td>".$evt->marketingDetails->sample_type."</td>\n";
        echo "<td>".(($evt->marketingDetails->work_required == 0 || $evt->marketingDetails->work_required == 2)?"Yes":"No")."</td>\n";
        echo "<td></td>\n";
        echo "<td>".(($evt->marketingDetails->repeats_done == 1)?"Yes":"No")."</td>\n";
        echo "<td>".$evt->marketingDetails->extra_comments2."</td>\n";
        echo "<td>".$evt->marketingDetails->lifestyle_images."</td>\n";
        echo "<td>".$evt->marketingDetails->event_link."</td>\n";
        echo "<td>".$evt->marketingDetails->product_images."</td>\n";
        echo "<td>".$evt->marketingDetails->max_discount."</td>\n";
        echo "<td>".$evt->marketingDetails->lowest_price."</td>\n";
        echo "<td>".$evt->marketingDetails->delivery."</td>\n";
        echo "<td>".$evt->marketingDetails->promotions."</td>\n";
        echo "<td>".$evt->marketingDetails->extra_comments."</td>\n";
        ?>
    <td><?php
        echo (($report_data != null && isset($report_data['adwords']) && $report_data['adwords'] ==1)?"Yes":"No");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['fbads']) && $report_data['fbads'] ==1)?"Yes":"No");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['emailbanner']) && $report_data['emailbanner'] ==1)?"Yes":"No");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['websitebanner']) && $report_data['websitebanner'] ==1)?"Yes":"No");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['facebook']) && $report_data['facebook'] ==1)?"Yes":"No");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['twitter']) && $report_data['twitter'] ==1)?"Yes":"No");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['insta']) && $report_data['insta'] ==1)?"Yes":"No");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['mm']) && $report_data['mm'] ==1)?"Yes":"No");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['mobilenotification']) && $report_data['mobilenotification'] ==1)?"Yes":"No");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['fridayfrenzy']) && $report_data['fridayfrenzy'] ==1)?"Yes":"No");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['midweekmailer']))?$report_data['midweekmailer']:"");
        ?></td>
    <td><?php
        echo (($report_data != null && isset($report_data['weekendmailer']))?$report_data['weekendmailer']:"");
        ?></td>
    <?php
        echo "</tr>\n";
    }
    ?>
    </tbody>
</table>
<style>
    .containter2 #data_table tbody tr:first-child {
        padding-top:20px;
    }
    .push-budget {
        background-color: #9e9e9e !important;
    }
    .container2 {
        height:500px;
        width:100%;
        overflow-y:auto;
        position:relative;
    }
    .floating {
        position:absolute;
        top:0;
        left:0;
    }
    #floating_head {
        background-color:#ffffff;
        position:absolute;

        left:0;
    }
    #page {
        width:100% !important;
    }
    .lockedrow  > td {
        background-color:#edde34 !important;
    }

</style>