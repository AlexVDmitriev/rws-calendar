<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * Date: 15-Jan-18
 * Time: 16:18
 */
$this->registerJsModule( "modules/backend/dailymailerform" );
$this->registerCssFile("bootstrap-datepicker.min.css");
?>
<form method="post" action="<?=Yii::app()->createUrl("Backend/dailymailer"); ?>">
<center>
    <table>
        <tr><td colspan="2" align="center"><h3>Daily Mailer Form</h3></td></tr>
        <tr>
            <td>Select From Date </td>
            <td><input type="text" name='fromdate' id='fromdate' class="datepicker" style="font-size:18px;"></td>
        </tr>
        <tr>
            <td>Select To Date </td>
            <td><input type="text" name='todate' readonly='readonly' id='todate'  class="datepicker" style="font-size:18px;"></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <br>
                <button type="submit" class="btn btn-success">Generate</button>
            </td>
        </tr>
    </table>
</center>
</form>