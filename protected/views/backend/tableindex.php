<?php

/**
 * @var BackendController $this
 * // TODO add types of variables
 * @var $sales
 * @var $manager
 * @var $category
 * @var $status
 */

$this->registerJsModule('modules/backend/table-index');

?>

<h1>Table List of Events </h1>
<hr>
<p>
<div class='container'>
	<div class='col-xs-offset-2 col-xs-8'>
		<div class='panel panel-default'>
			<div class='panel-heading'>	
				<div class='panel-title'><i><b><a>
				Filters
				</a></b></i></div>
			</div>
				<div class='panel-body'>
					<b>Select the filters you want to apply : </b>
					<div class='row'>
						<div class='col-xs-6'>From Date :</div><div class='col-xs-6'><input type='date' id='fromdate' value='<?php echo isset($date)?$date:""; ?>'class='form-control'></div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>To Date :</div><div class='col-xs-6'><input type='date' id='todate' value='<?php echo isset($date)?$date:""; ?>'class='form-control'></div>
					</div>					
					<div class='row'>
                        <div class='col-xs-6'>Category :</div>
                        <div class='col-xs-6'>
                            <?php
                            echo CHtml::dropDownList(
                                "",
                                $category,
                                CHtml::listData(Category::model()->findAll(), 'id', 'name'),
                                array(
                                    'id' => 'category',
                                    'class' => 'form-control',
                                    'empty' => 'Any',
                                )
                            );
                            ?>
                        </div>
					</div>
					<div class='row'>
                        <div class='col-xs-6'>Manager :</div>
                        <div class='col-xs-6'>
                            <?php
                            echo CHtml::dropDownList(
                                "",
                                $manager,
                                CHtml::listData(
                                    User::model()->findAll(array('condition' => 'active=1')),
                                    'userid',
                                    'fullname'
                                ),
                                array(
                                        'id' => 'manager',
                                    'class' => 'form-control',
                                    'empty' => 'Any',
                                )
                            ); ?>
                        </div>
					</div>
					<div class='row'>
                        <div class='col-xs-6'>Sales Bracket :</div>
                        <div class='col-xs-6'>
                            <?php
                            echo CHtml::dropDownList(
                                "",
                                $sales,
                                CHtml::listData(SalesBracket::model()->findAll(), 'id', 'name'),
                                array(
                                    'id' => 'sales',
                                    'class' => 'form-control',
                                    'empty' => 'Any',
                                )
                            );
                            ?>
                        </div>
					</div>
					<div class='row'>
                        <div class='col-xs-6'>Status :</div>
                        <div class='col-xs-6'>
                            <?php
                            echo CHtml::dropDownList(
                                "",
                                $status,
                                CHtml::listData(StatusMaster::model()->findAll(), 'id', 'status'),
                                array(
                                    'id' => 'status',
                                    'class' => 'form-control',
                                    'empty' => 'Any',
                                )
                            );
                            ?>
                        </div>
					</div>
					<div class='row'>
						<div class='col-xs-6'>Marketting Tasks</div><div class='col-xs-6'>
                            <?php
                            /** @var Tasks[] $tasks */
                            $tasks = Tasks::model()->findAll();
                            foreach ($tasks as $task) { ?>
                                <input type='checkbox' value='<?= $task->shortname ?>'><?= $task->taskname ?>
                                <br/>
                            <?php } ?>
                        </div>
					<div class='row'>
                        <div class='col-xs-offset-6 col-xs-6'>
                            <button class='btn btn-primary event__button-view-data'>
                                <span class='fa fa-eye'></span> View Data
                            </button>
                        </div>
					</div>
				</div>
		</div>
	</div>
</div>
<div id='targetcontent'></div>

<div class="text-center">
    <button id='download' class='btn btn-success hide event__button-download-csv'>
        <i class='fa fa-download'></i> Download As CSV
    </button>
</div>
