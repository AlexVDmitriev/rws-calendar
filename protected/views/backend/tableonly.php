<?php
/**
 * @var CActiveDataProvider $data
 */

$this->widget('bootstrap.widgets.BsGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>$data,
	'enableSorting'=>false,
	'columns'=>array(
		array('name'=>'id','header'=>'Id'),
		array('name'=>'manufacturer','header'=>'Manufacturer'),
		array('name'=>'eventid','header'=>'Event Id'),
		array('name'=>'startdate','header'=>'Start Date','value'=>'date("d-m-Y",strtotime($data->startdate))'),
		array('name'=>'enddate','header'=>'End Date','value'=>'date("d-m-Y",strtotime($data->startdate))'),
		array('name'=>'categorylist.name','header'=>'Category'),
		array('name'=>'managerlist.fullname','header'=>'Manager'),
		array('name'=>'saleslist.name','header'=>'Sales'),
		array('name'=>'statuslist.status','header'=>'Status'),
		array('name'=>'revprediction','header'=>'Rev Prediction'),
		array('name'=>'orderprediction','header'=>'Order Prediction'),
		array('name'=>'unitprediction','header'=>'Unit Prediction'),
		array('header'=>'Tasks','type'=>'raw','value'=>'$data->getlabels(1)')
	),
));
