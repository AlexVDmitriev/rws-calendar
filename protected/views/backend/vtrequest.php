<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * @var $fromdate string
 * @var $todate string
 */
$this->registerJsModule( "modules/backend/vtrequest" );
$ofromdate = $fromdate;
$otodate = $todate;
$startdate =new DateTime($fromdate);
$enddate = new DateTime($todate);
$weeks = [];
while($startdate->diff($enddate)->invert == 0){
    $weeks[] = array($startdate->format("Y-m-d"),$startdate->modify("+1 weeks")->modify("-1 days")->format("Y-m-d"));
    $startdate->modify("+1 days");
}
$fromdate = $weeks[0][0];
$todate = $weeks[0][1];
?>
<center><h2 id="heading">VT Request <?=$ofromdate ?> till <?=$otodate ?></h2></center><br>

<center><button type="button" id="exportbtn"><div class="fa fa-download"></div> Export To CSV</button></center>
<div class="week_block">
<h4>This Week (<?=$fromdate ?> till <?=$todate ?>)</h4><br>

<table class="table data_table">
    <thead>
        <tr>
            <th>Day</th>
            <th>Brand</th>
            <th>Category</th>
            <th>Manager</th>
            <th>Sample Date</th>
            <th>Amount</th>
            <th>Sample Type</th>
            <th>Shoot</th>
            <th>Supplied Images</th>
            <th>Repeats / Done</th>
            <th>Date Change</th>
            <th>Info Date</th>
            <th>Comments</th>
        </tr>
    </thead>
    <tbody>
    <?php

    $evts = Event::model()->findAllBySql("SELECT * from event where startdate>=:fromdate and startdate<=:todate and isVip=:isvip order by startdate,manufacturer",array('fromdate'=>$fromdate,'todate'=>$todate,'isvip'=>Yii::app()->params['isVip']));
    /* @var $event Event */
    $olddt = null;
    $dates = [];
    $sample_type = ['Apparel','Accessories','Footwear','Product'];
    foreach($evts as $event) {
        if($olddt != $event->startdate || $olddt == null) {
            $dtstr = date('D M dS', strtotime($event->startdate));
            //echo "<tr><td colspan='13'><hr></td></tr>";
        }
        else
            $dtstr = "";
        if(array_search(date("D"),$dates)){
            $dates[] = date("D");
        }
        ?>
        <tr>
            <td><?=$dtstr ?></td>
            <td><?=$event->manufacturerlist?$event->manufacturerlist->name:""; ?></td>
            <td><?=$event->categorylist?$event->categorylist->name:""; ?></td>
            <td><?=$event->managerlist?$event->managerlist->fullname:""; ?></td>
            <td><?=$event->marketingDetails?$event->marketingDetails->samples_arrived_date:""; ?></td>
            <td><?=$event->marketingDetails?$event->marketingDetails->sample_quantity:"" ?></td>
            <td><?=$event->marketingDetails?$sample_type[$event->marketingDetails->sample_type]:"" ?></td>
            <td><?php
                if($event->marketingDetails){
                    if($event->marketingDetails->work_required=="0" || $event->marketingDetails->work_required=="2") // shoots
                        echo "Yes";
                    else
                        echo "No";
                }
            ?></td>
	    <td></td>
            <td><?php
                if($event->marketingDetails){
                    if($event->marketingDetails->repeats_done ==1) // repeats
                        echo "Yes";
                    else
                        echo "No";
                }
                ?></td>
		<td></td>
            <td></td>
            <td><?php
                if($event->marketingDetails){
                    echo $event->marketingDetails->extra_comments2;
                }
            ?></td>
        </tr>
    <?php
        $olddt = $event->startdate;
    }
    ?>
    </tbody>
</table>
</div>
<?php for($i=1; $i < count($weeks); $i++){
    $fromdate = $weeks[$i][0];
    $todate = $weeks[$i][1];
?>
<div class="week_block">
    <h4>Next Week Events (<?=$weeks[$i][0]." till ".$weeks[$i][1] ?>)</h4>
    <table class="table data_table">
        <thead>
        <tr>
            <th>Day</th>
            <th>Brand</th>
            <th>Category</th>
            <th>Manager</th>
            <th>Sample Date</th>
            <th>Amount</th>
            <th>Sample Type</th>
            <th>Shoot</th>
            <th>Supplied Images</th>
            <th>Repeats / Done</th>
            <th>Date Change</th>
            <th>Info Date</th>
            <th>Comments</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $fdate = new DateTime($fromdate);
        $tdate = new DateTime($todate);
        //$fdate->modify("+7 days");
        //$tdate->modify("+7 days");
        $evts = Event::model()->findAllBySql("SELECT * from event where startdate>=:fromdate and startdate<=:todate and isVip=:isvip order by startdate,manufacturer",array('fromdate'=>$fdate->format("Y-m-d"),'todate'=>$tdate->format("Y-m-d"),'isvip'=>Yii::app()->params['isVip']));
        /* @var $event Event */
        $olddt = null;
        foreach($evts as $event) {
            if($olddt != $event->startdate || $olddt == null)
                $dtstr = date('D M dS',strtotime($event->startdate));
            else
                $dtstr = "";
            if(array_search(date("D"),$dates)){
                $dates[] = date("D");
            }
            ?>
            <tr>
                <td><?=$dtstr ?></td>
                <td><?=$event->manufacturerlist?$event->manufacturerlist->name:""; ?></td>
                <td><?=$event->categorylist?$event->categorylist->name:""; ?></td>
                <td><?=$event->managerlist?$event->managerlist->fullname:""; ?></td>
                <td><?=$event->marketingDetails?$event->marketingDetails->samples_arrived_date:""; ?></td>
                <td><?=$event->marketingDetails?$event->marketingDetails->sample_quantity:"" ?></td>
                <td><?=$event->marketingDetails?$event->marketingDetails->sample_type:"" ?></td>
                <td><?php
                    if($event->marketingDetails){
                        if($event->marketingDetails->work_required=="0" || $event->marketingDetails->work_required=="2") // shoots
                            echo "Yes";
                        else
                            echo "No";
                    }
                    ?></td>
                <td></td>
                <td><?php
                    if($event->marketingDetails){
                        if($event->marketingDetails->repeats_done=="1") // repeats
                            echo "Yes";
                        else
                            echo "No";
                    }
                    ?></td>
                <td></td>
                <td></td>
                <td><?php
                    if($event->marketingDetails){
                        echo $event->marketingDetails->extra_comments2;
                    }
                    ?></td>
            </tr>
            <?php
            $olddt = $event->startdate;
        }
        ?>
        </tbody>
    </table>
</div>
<?php } ?>
<script>
    var day_dates = <?=json_encode($dates); ?>;
</script>
