<?php

/**
 * @var $this Controller
 * @var string $content
 */

/** @var CWebApplication $app */
$app = Yii::app();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<div class="container" id="page">
    <?php if ($app->user->hasFlash("errormsg")) { ?>
        <div class='container row col-xs-offset-4 col-xs-4 bg-danger'>
            <?= $app->user->getFlash("errormsg") ?>
        </div>
    <?php } ?>

    <?php if ($app->user->hasFlash("successmsg") || $app->user->hasFlash("success")) { ?>
        <div class='container row col-xs-offset-4 col-xs-4 bg-success'>
            <?= $app->user->getFlash("successmsg") . $app->user->hasFlash("success") ?>
        </div>
    <?php } ?>
    <?php echo $content; ?>
    <div class="clear"></div>
    <div id="footer">
        Copyright &copy; <?php echo date('Y'); ?><br/>
        All Rights Reserved.<br/>
    </div><!-- footer -->
</div><!-- page -->
</body>
</html>
