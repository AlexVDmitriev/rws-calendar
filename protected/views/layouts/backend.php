<?php 

/**
 * @var $this Controller
 * @var $content string
 */

/** @var CWebApplication $app */
$app = Yii::app();

/** @var User|WebUser $currentUser */
$currentUser = $app->user;
$isvip = false;
if($currentUser->has_vip_permission)
    $isvip = true;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo $app->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo $app->request->baseUrl; ?>/css/form.css" />
	<link href="<?php echo $app->request->baseUrl; ?>/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300' rel='stylesheet' type='text/css'>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<style type="text/css">
	.fc-widget-content { border: 1px solid #9e9e9e !important;}
	</style>
</head>
<body style='font-family:Open Sans, sans-serif'>
<div class="modal fade" role="dialog" id="issue_modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Report An Issue In Software</h4>
            </div>
            <div class="modal-body" id="issue_modal_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit_report">Submit Report</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="container" id="page">
<div id="header">
		<div id="logo" class='h1' >
            <?php
            echo CHtml::encode(
                $app->name . (Yii::app()->params['isVip'] ? " V.I.P." : "")
            );
            ?>
        </div>
	</div><!-- header -->
	<div id="mainmenu">
        <?php
        $this->widget(
            'bootstrap.widgets.BsNavbar',
            array(
                'collapse' => true,

                'brandLabel' => BsHtml::icon(BsHtml::GLYPHICON_HOME) . " " . $currentUser->fullname,
                'brandUrl' => $app->createUrl("Backend/index"),
                'items' => array(
                    array(
                        'class' => 'bootstrap.widgets.BsNav',
                        'type' => 'navbar',
                        'encodeLabel' => false,
                        'activateParents' => true,
                        'items' => array(
                            array(
                                'label' => 'Masters',
                                'url' => '#',
                                'visible' => $currentUser->has_can_add_permission || $currentUser->has_admin_permission,
                                'items' => array(
                                    array(
                                        'label' => 'User',
                                        'url' => array('User/admin'),
                                        'visible' => $currentUser->has_admin_permission
                                    ),
                                    array(
                                        'label' => 'Daily Emails',
                                        'url' => array('email/index'),
                                        'visible' => $currentUser->has_admin_permission
                                    ),
                                    array(
                                        'label' => 'Brands',
                                        'url' => array('Manufacturer/admin'),
                                        'visible' => $currentUser->has_admin_permission
                                    ),
                                    array(
                                        'label' => 'Suppliers',
                                        'url' => array('Supplier/admin'),
                                        'visible' => $currentUser->has_admin_permission
                                    ),
                                    array(
                                        'label' => 'Warehouse',
                                        'url' => array('Warehouse/admin'),
                                        'visible' => $currentUser->has_admin_permission
                                    ),
                                    array(
                                        'label' => 'Brand -> Supplier Assignment',
                                        'url' => array('ManufacturerCategory/admin'),
                                        'visible' => $currentUser->has_admin_permission
                                    ),
                                    array(
                                        'label' => 'Category',
                                        'url' => array('Category/admin'),
                                    ),
                                    array(
                                        'label' => 'Sales Bracket',
                                        'url' => array('SalesBracket/admin'),
                                    ),
                                    array(
                                        'label' => 'Status List',
                                        'url' => array('StatusMaster/admin'),
                                    )
                                )
                            ),
                            array(
                                'label' => 'Entry Screen',
                                'url' => '#',
                                'visible' => $currentUser->has_can_add_permission || $currentUser->has_admin_permission,
                                'items' => array(
                                    array('label' => 'Events', 'url' => array('event/admin')),
                                    array(
                                        'label' => 'VIP',
                                        'url' => str_replace(
                                            $_SERVER['HTTP_HOST'],
                                            Yii::app()->params['vipDomain'],
                                            $this->createAbsoluteUrl(
                                                'Site/CrossAuth',
                                                array(
                                                    'userId' => $app->user->getId(),
                                                    'authKey' => $app->user->logintoken,
                                                )
                                            )
                                        ),
                                        'visible' => !Yii::app()->params['isVip'],
                                    ),
                                    array(
                                        'label' => 'Common',
                                        'url' => str_replace(
                                            $_SERVER['HTTP_HOST'],
                                            Yii::app()->params['commonDomain'],
                                            $this->createAbsoluteUrl(
                                                'Site/CrossAuth',
                                                array(
                                                    'userId' => $app->user->getId(),
                                                    'authKey' => $app->user->logintoken,
                                                )
                                            )
                                        ),
                                        'visible' => Yii::app()->params['isVip'],
                                    ),
                                ),
                            ),
                            array(
                                'label' => 'Reports',
                                'url' => '#',
                                'items' => array(
                                    array('label' => 'View Calendar', 'url' => array('backend/index')),
                                    array('label' => 'Changes for the Day', 'url' => array('backend/eventlog')),
                                    array('label' => 'Changes Report', 'url' => array('backend/changereport')),
                                    array('label' => 'View Gnatt Chart', 'url' => array('backend/gantt')),
                                    array('label' => 'View Brand Report', 'url' => array('backend/brandreport')),
                                    array('label' => 'View Tabular Report', 'url' => array('backend/gettable')),
                                    array('label' => 'View Sales Report', 'url' => array('backend/salesreport')),
                                    array(
                                        'label' => 'Revenue Prediction Report',
                                        'url' => array('Backend/revenueprediction')
                                    ),
                                    array('label' => 'MK Request', 'url' => array('backend/mkrequestform')),
                                    array('label' => 'VT Request', 'url' => array('backend/vtrequestform')),
                                    array('label' => 'Combined MK-VT Request', 'url' => array('backend/combinedreportform')),
                                    array('label' => 'Daily Mailer Form', 'url' => array('backend/dailymailerform'))
                                )
                            ),
                            array(
                                'label' => 'Price Prediction',
                                'url' => '#',
                                'visible' => $currentUser->has_can_add_permission || $currentUser->has_admin_permission,
                                'items' => array(
                                    array(
                                        'label' => 'Prediction for upcoming Events',
                                        'url' => array('CompetitionEntries/index'),
                                    ),
                                    array(
                                        'label' => 'Prediction for Past Events',
                                        'url' => array('CompetitionEntries/past'),
                                    ),
                                    array(
                                        'label' => 'Update Competition Start details',
                                        'url' => array('CompetitionEntries/updatebefore'),
                                    ),
                                    array(
                                        'label' => 'Update Competition To End',
                                        'url' => array('CompetitionEntries/updateafter'),
                                    )
                                )
                            ),
                            array('label' => 'Change Password', 'url' => array('user/changepassword')),
                            array(
                                'label' => 'Logout (' . $currentUser->username . ')',
                                'url' => array('site/logout')
                            ),
                            array(
                                'label' => '<div class="fa text-danger fa-exclamation-triangle"></div>',
                                'url' => '#',

                                'visible' => $currentUser->has_can_add_permission || $currentUser->has_admin_permission,
                                'items' => array(
                                    array(
                                        'label' => 'Report An Issue',
                                        'url' => '#',
                                        'id'=>'issue_report_icon',
                                        'visible' => $currentUser->has_can_add_permission || $currentUser->has_admin_permission,
                                    ),
                                )
                            )
                        )
                    )
                ),
            )
        ); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)) { ?>
        <?php $this->widget(
            'zii.widgets.CBreadcrumbs',
            array(
                'links' => $this->breadcrumbs,
            )
        ); ?><!-- breadcrumbs -->
	<?php } ?>
    <?php  if($currentUser->hasFlash("errormsg") || $currentUser->hasFlash("error")) { ?>
		<div class='container'>
            <div class='row col-xs-offset-4 col-xs-4'>
                <div class='label label-danger'>
                    <?= $currentUser->getFlash("errormsg").$currentUser->getFlash("error") ?>
                </div>
            </div>
        </div>
	<?php } ?>
	<?php if($currentUser->hasFlash("successmsg") || $currentUser->hasFlash("success")) { ?>
		<div class='container'>
            <div class='row col-xs-offset-4 col-xs-4'>
                <div class='label label-success'>
                    <?= $currentUser->getFlash("successmsg").$currentUser->getFlash("success") ?>
                </div>
            </div>
        </div>
    <?php } ?>
	<?php echo $content; ?>
	<div class="clear"></div>
    <div id="footer" class="text-center">
        <div style="margin-left:auto; margin-right:auto;">
            Copyright &copy; <?php echo date('Y'); ?><br/>
            All Rights Reserved.<br/>
        </div>
    </div><!-- footer -->
</div><!-- page -->
</body>
</html>
<?php
$this->defineRequireJs();
?>