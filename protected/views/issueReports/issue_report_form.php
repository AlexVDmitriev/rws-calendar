<?php
/**
 * Created by PhpStorm.
 * User: nikhil
 * Date: 03-Apr-17
 * Time: 19:04
 */
?>
<style type="text/css">
    .centering {
        margin-left:auto;
        margin-right:auto;
    }
    .center-container {
        text-align:center;
    }
</style>
<div class="row">
    <div class="col-xs-12">
        <p>
            The following form should be used to report issues in the Calendar Software you are currently using.
            When filing a bug report please be as descriptive as possible, this will help the developers reproduce the problem and get it solved quickly.<br>
        </p>
        <br>
        <div class="col-xs-12">
            <p>
                <b>Title</b> <i>(A brief title describing the issue)</i>
                <?php echo BsHtml::textField("issue_title","",array('class'=>'form-control','maxlength'=>'100','id'=>'issue_title')); ?>

            </p>
        </div>
        <div class="col-xs-6">
            <p>
                <b>Steps to Reproduce</b> <i>(What steps should one follow to arrive at the problem)</i>
                <?php echo BsHtml::textArea("issue_steps","",array('class'=>'form-control','rows'=>'5','id'=>'issue_steps')); ?>

            </p>
        </div>
        <div class="col-xs-6">
            <p>
                <b>Expected Result</b> <i>(What should actually be happening if everything was working fine?)</i>
                <?php echo BsHtml::textArea("issue_expected_result","",array('class'=>'form-control','rows'=>'5','id'=>'issue_expected_result')); ?>

            </p>
        </div>
        <div class="col-xs-12">
            <p>
                <b>What is happening now?</b> <i>(What currently hapening that you feel is the wrong result or not expected)</i>
                <?php echo BsHtml::textArea("what_happened","",array('class'=>'form-control','rows'=>'5','id'=>'what_happened')); ?>

            </p>
        </div>
        <div class="col-xs-12">
            <div class="center-container">
                Upload Screenshots if any you can attach multiple : <br>
                <input type="file" id="screenshots" multiple class="centering">
            </div>
        </div>
    </div>
</div>
