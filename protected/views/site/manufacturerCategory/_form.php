<?php
/* @var $this ManufacturerCategoryController */
/* @var $model ManufacturerCategory */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'id'=>'manufacturer-category-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->label($model,'manufacturer'); ?>
	<?php echo $form->listbox($model,"manufacturer",CHtml::listData(Manufacturer::model()->findAll(),"id","name")); ?>
    
    <?php echo $form->label($model,'category'); ?>
	<?php echo $form->listbox($model,"category",CHtml::listData(Category::model()->findAll(),"id","name")); ?>
	<br>
	<?php echo $form->label($model,'supplier'); ?>
	<ul style='list-style-type: none;'>
	<?php 
		$s = Supplier::model()->findAll();
		for($i=0; $i < count($s); $i++) {
            $ms = ManufacturerSupplier::model()->findByAttributes(
                array('supplierid' => $s[$i]->supplierid, 'entryid' => $model->entryid)
            );

			$selected = (empty($ms)) ? "" : "CHECKED";

			echo "<li><input type='checkbox' name='supplier[]' $selected value='".$s[$i]->supplierid."'> <label>".$s[$i]->name."</label></li>";
		}
	?>
	</ul>
    <?php echo BsHtml::submitButton('Save', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>

<?php $this->endWidget(); ?>
