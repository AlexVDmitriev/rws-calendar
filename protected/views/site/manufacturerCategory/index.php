<?php
/* @var $this ManufacturerCategoryController */
/* @var $dataProvider CActiveDataProvider */
?>

<?php
$this->breadcrumbs=array(
	'Brand to Categories',
);
?>

<?php echo BsHtml::pageHeader('Brand to Categories') ?>
<?php $this->widget('bootstrap.widgets.BsListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>