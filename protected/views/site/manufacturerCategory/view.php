<?php
/* @var $this ManufacturerCategoryController */
/* @var $model ManufacturerCategory */
?>

<?php
$this->breadcrumbs=array(
	'Brand to Categories'=>array('index'),
	$model->entryid,
);

?>

<?php echo BsHtml::pageHeader('View','Brand to Category '.$model->entryid) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'manufacturer',
		'category',
		'entryid',
	),
)); ?>