<?php
/* @var $this ManufacturerCategoryController */
/* @var $model ManufacturerCategory */


$this->breadcrumbs=array(
	'Brand to Categories'=>array('index'),
	'Manage',
);

?>

<?php echo BsHtml::pageHeader('Manage','Brand to Categories') ?>
<div class="panel panel-default">
    <div class="panel-heading"  style='text-align:right'>
		<a href='<?php echo Yii::app()->createUrl("ManufacturerCategory/create") ?>' class='btn btn-success'><div class='fa fa-plus'></div> Add New</a>    </div>
    <div class="panel-body">

        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'manufacturer-category-grid',
			'dataProvider'=>$model->search(),
//			'filter'=>$model,
			'columns'=>array(
			'rmanufacturer.name',
			'rcategory.name',
			array('header'=>'Suppliers','value'=>'$data->getsupplierlist()'),
				array(
					'class'=>'bootstrap.widgets.BsButtonColumn',
				),
			),
        )); ?>
    </div>
</div>




