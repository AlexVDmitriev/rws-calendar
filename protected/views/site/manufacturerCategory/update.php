<?php
/* @var $this ManufacturerCategoryController */
/* @var $model ManufacturerCategory */
?>

<?php
$this->breadcrumbs=array(
	'Brand to Categories'=>array('index'),
	$model->entryid=>array('view','id'=>$model->entryid),
	'Update',
);

?>
<?php echo BsHtml::pageHeader('Update','Brand to Category '.$model->entryid) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>