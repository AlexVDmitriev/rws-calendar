<?php
/* @var $this ManufacturerCategoryController */
/* @var $data ManufacturerCategory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('entryid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->entryid),array('view','id'=>$data->entryid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manufacturer')); ?>:</b>
	<?php echo CHtml::encode($data->manufacturer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category')); ?>:</b>
	<?php echo CHtml::encode($data->category); ?>
	<br />


</div>