<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<div class='container'>
<div class='row col-md-offset-3 col-md-6'>
<div class='panel panel-primary'><div class='panel-heading'><h1>Please Login...</h1></div>
<div class='panel-body'>
<p>Please fill out the following form with your login credentials:</p>

<div class="form">
<?php $form=$this->beginWidget('BsActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row form-group">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'username',array('class'=>'alert alert-warning')); ?>
	</div>

	<div class="row form-group">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('class'=>'col-xs-2 form-control')); ?>
		<?php echo $form->error($model,'password',array('class'=>'alert alert-warning')); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe',array('class'=>'alert alert-warning')); ?>
	</div>

	<div class="row buttons text-center">
		<?php echo CHtml::submitButton('Login',array("class"=>'btn btn-primary')); ?>
	</div>
	<div class='row'>
	
	</div>
<?php $this->endWidget(); ?>
</div><!-- form -->
</div>
</div>
</div>
</div>
