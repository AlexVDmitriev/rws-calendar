<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */
?>

<?php
$this->breadcrumbs=array(
	'Brands'=>array('index'),
	$model->name,
);

?>

<?php echo BsHtml::pageHeader('View','Brand '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'address',
		'contactperson',
		'contactnumber',
	),
)); ?>