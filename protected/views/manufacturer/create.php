<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */
?>

<?php
$this->breadcrumbs=array(
	'Brands'=>array('index'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader('Create','Brand') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>