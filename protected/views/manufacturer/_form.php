<?php

/**
 * @var ManufacturerController $this
 * @var Manufacturer $model
 * @var BSActiveForm $form
 */

$this->registerJsModule('modules/manufacturer');

?>

<script language="JavaScript">
    var currentDate = "<?= date('d-m-Y') ?>";
</script>

<?php
$form = $this->beginWidget(
    'bootstrap.widgets.BsActiveForm',
    array(
        'id' => 'manufacturer-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )
);
?>

    <p class="help-block">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>
	<div class='col-xs-6'>
        <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>145)); ?>
        <?php echo $form->textFieldControlGroup($model,'address',array('maxlength'=>145)); ?>
        <?php echo $form->textFieldControlGroup($model,'contactperson',array('maxlength'=>45)); ?>
        <?php echo $form->textFieldControlGroup($model,'contactnumber',array('maxlength'=>45)); ?>
        <div class='form-group'>
            <b class='control-label'>Doing Business / Business Termination Date?</b>
            <div class='col-xs-1'>
                <label for="doingbusiness"></label>
                <input
                    type='checkbox'
                    id='doingbusiness'
                    <?= ($model->isNewRecord
                        || $model->terminationdate == "0000-00-00"
                        || $model->terminationdate == "")
                        ? "checked=checked"
                        : ""
                    ?>
                >
            </div>
            <div class='col-xs-11'>
                <?= $form->dateField(
                    $model,
                    "terminationdate",
                    ($model->isNewRecord
                        || $model->terminationdate == "0000-00-00"
                        || $model->terminationdate == "")
                        ? array('style' => 'display: none')
                        : array('style' => 'display: inline')
                ) ?>
            </div>
        </div>
        <?php echo $form->label($model,'handlerid'); ?>
        <?php echo $form->dropDownList(
            $model,
            'handlerid',
            CHtml::listData(User::model()->findAll(), "userid", "fullname"),
            array('input-control')
        ); ?>
        <br />
        <?php echo BsHtml::submitButton('Submit', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
	</div>
<?php $this->endWidget(); ?>
