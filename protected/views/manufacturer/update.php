<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */
?>

<?php
$this->breadcrumbs=array(
	'Brands'=>array('index'),
	$model->name=>array('view','id'=>$model->name),
	'Update',
);


?>

<?php echo BsHtml::pageHeader('Update','Brand '.$model->name) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>