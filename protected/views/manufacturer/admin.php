<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */


$this->breadcrumbs=array(
	'Brands'=>array('index'),
	'Manage',
);

?>

<?php echo BsHtml::pageHeader('Manage','Brands') ?>
<div class="panel panel-default">
    <div class="panel-heading" style='text-align:right'>
			<a href='<?php echo Yii::app()->createUrl("Manufacturer/Create"); ?>' class='btn btn-success right'><div class='fa fa-plus'></div> Add New</a>
    </div>
    <div class="panel-body">

        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'manufacturer-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			//'sortingAllowed'=>false,
			'columns'=>array(
		'name',
		'address',
		'contactperson',
		'contactnumber',
		'handler.fullname',
		'terminationdate',
				array(
					'class'=>'bootstrap.widgets.BsButtonColumn',
				),
			),
        )); ?>
    </div>
</div>




