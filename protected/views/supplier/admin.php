<?php
/* @var $this SupplierController */
/* @var $model Supplier */


$this->breadcrumbs=array(
	'Suppliers'=>array('index'),
	'Manage',
);


?>

<?php echo BsHtml::pageHeader('Manage','Suppliers') ?>
<div class="panel panel-default">
    <div class="panel-heading" style='text-align:right;'>
        <a href='<?php echo Yii::app()->createUrl("supplier/create") ?>' class='btn btn-success'><div class='fa fa-plus'></div> Add New</a>
    </div>
    <div class="panel-body">
        <!-- search-form -->

        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'supplier-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
		'name',
		'address',
		'contactno',
				array(
					'class'=>'bootstrap.widgets.BsButtonColumn',
				),
			),
        )); ?>
    </div>
</div>




