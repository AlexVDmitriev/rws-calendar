<?php
/* @var $this SupplierController */
/* @var $model Supplier */
?>

<?php
$this->breadcrumbs=array(
	'Suppliers'=>array('index'),
	'Create',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Supplier', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Supplier', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Create','Supplier') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>