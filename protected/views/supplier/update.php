<?php
/* @var $this SupplierController */
/* @var $model Supplier */
?>

<?php
$this->breadcrumbs=array(
	'Suppliers'=>array('index'),
	$model->name=>array('view','id'=>$model->supplierid),
	'Update',
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Supplier', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Supplier', 'url'=>array('create')),
    array('icon' => 'glyphicon glyphicon-list-alt','label'=>'View Supplier', 'url'=>array('view', 'id'=>$model->supplierid)),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Supplier', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('Update','Supplier '.$model->supplierid) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>