<?php
/* @var $this SupplierController */
/* @var $model Supplier */
?>

<?php
$this->breadcrumbs=array(
	'Suppliers'=>array('index'),
	$model->name,
);

$this->menu=array(
    array('icon' => 'glyphicon glyphicon-list','label'=>'List Supplier', 'url'=>array('index')),
	array('icon' => 'glyphicon glyphicon-plus-sign','label'=>'Create Supplier', 'url'=>array('create')),
	array('icon' => 'glyphicon glyphicon-edit','label'=>'Update Supplier', 'url'=>array('update', 'id'=>$model->supplierid)),
	array('icon' => 'glyphicon glyphicon-minus-sign','label'=>'Delete Supplier', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->supplierid),'confirm'=>'Are you sure you want to delete this item?')),
    array('icon' => 'glyphicon glyphicon-tasks','label'=>'Manage Supplier', 'url'=>array('admin')),
);
?>

<?php echo BsHtml::pageHeader('View','Supplier '.$model->supplierid) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'supplierid',
		'name',
		'address',
		'contactno',
	),
)); ?>