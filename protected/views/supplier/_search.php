<?php
/* @var $this SupplierController */
/* @var $model Supplier */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'supplierid'); ?>
    <?php echo $form->textFieldControlGroup($model,'name',array('maxlength'=>145)); ?>
    <?php echo $form->textFieldControlGroup($model,'address',array('maxlength'=>45)); ?>
    <?php echo $form->textFieldControlGroup($model,'contactno',array('maxlength'=>45)); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
