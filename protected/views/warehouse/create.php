<?php
/* @var $this WarehouseController */
/* @var $model Warehouse */
?>

<?php
$this->breadcrumbs=array(
	'Warehouses'=>array('index'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader('Create','Warehouse') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>