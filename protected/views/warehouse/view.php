<?php
/* @var $this WarehouseController */
/* @var $model Warehouse */
?>

<?php
$this->breadcrumbs=array(
	'Warehouses'=>array('index'),
	$model->name,
);

?>

<?php echo BsHtml::pageHeader('View','Warehouse '.$model->id) ?>

<?php
	$backgroundColor = "#" . $model->color;
	$this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'address',
		array(
			'name'=>'color',
			'value'=>'<div style="width:30px; height:30px; background-color: ' . $backgroundColor . '"></div>',
			'type'=>'raw'
		),
	),
)); ?>