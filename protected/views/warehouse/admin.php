<?php
/* @var $this WarehouseController */
/* @var $model Warehouse */


$this->breadcrumbs=array(
	'Warehouses'=>array('index'),
	'Manage',
);

?>

<?php echo BsHtml::pageHeader('Manage','Warehouses') ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <a href='<?php echo Yii::app()->createUrl("warehouse/create") ?>' class='btn btn-success pull-right'><div class='fa fa-plus'></div> Create New </a><br><br>
    </div>
    <div class="panel-body">
    
        <?php $this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'warehouse-grid',
			'dataProvider'=>$model->search(),
			'filter'=>$model,
			'columns'=>array(
        		'id',
		'name',
		'address',
		array('name'=>'color','value'=>'\'<div style="width:30px; height:30px; background-color:#\'.$data->color.\'"></div>\'','type'=>'raw'),
				array(
					'class'=>'bootstrap.widgets.BsButtonColumn',
				),
			),
        )); ?>
    </div>
</div>




