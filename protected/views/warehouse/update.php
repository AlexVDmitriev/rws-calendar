<?php
/* @var $this WarehouseController */
/* @var $model Warehouse */
?>

<?php
$this->breadcrumbs=array(
	'Warehouses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

?>

<?php echo BsHtml::pageHeader('Update','Warehouse '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>