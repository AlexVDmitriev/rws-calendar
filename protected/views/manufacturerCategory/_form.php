<?php

/**
 * @var ManufacturerCategoryController $this
 * @var Manufacturer $model
 * @var BSActiveForm $form
 */
$this->registerJsModule('modules/manufacturer-category');
$this->registerCssFile('bootstrap-multiselect.css');
?>

<?php
$form = $this->beginWidget(
    'bootstrap.widgets.BsActiveForm',
    array(
        'id' => 'manufacturer-category-form',
        'enableAjaxValidation' => false,
    )
);
?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>
<i>
    <span class="text-danger">WARNING : Changing the manager here for this brand will cause all future events to be updated to this manager.</span>
</i>

<?php echo $form->errorSummary($model); ?>
<div class="row">
    <div class="col-xs-4">
        <?php echo $form->label($model, 'id'); ?>
        <?php echo $form->listbox(
            $model,
            "id",
            CHtml::listData(Manufacturer::model()->findAll(array('order' => 'name asc')), "id", "name"),
            array(
                'class' => 'manufacturer__list',
            )
        ); ?>
    </div>
    <div class="col-xs-4">
        <?= $form->label($model, 'category', array('for' => 'categorylist')); ?>
        <br/>
        <?=
        CHtml::dropDownList(
            'category[]',
            ManufacturerCategory::getCategoryIdsByManufacturer($model->id),
            CHtml::listData(
                Category::model()->findAll(),
                'id',
                'name'
            ),
            array(
                'id' => 'categorylist',
                'multiple' => 'multiple',
            )
        )
        ?>
    </div>
    <div class="col-xs-4">
        <?= $form->label($model, 'supplier', array('for' => 'supplierlist')) ?>
        <br/>
        <?=
        CHtml::dropDownList(
            'supplier[]',
            ManufacturerSupplier::getSupplierIdsByManufacturer($model->id),
            CHtml::listData(
                Supplier::model()->findAll(array('order' => 'name asc')),
                'supplierid',
                'name'
            ),
            array(
                'id' => 'supplierlist',
                'multiple' => 'multiple',
            )
        )
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-4">
        <b>Manager</b><br>
        <?=
        BsHtml::dropDownList(
            'manager',
            $model->marketingDetails?$model->marketingDetails->managerid:0,
            CHtml::listData(
                User::model()->findAll( array( 'condition' => 'active=1 and evt_manager=1' )),
                'userid',
                'fullname'
            ),
            array(
                'id' => 'managerlist',
            )
        )
        ?>
    </div>
    <div class="col-xs-4">
        <b>Stock Type</b><br>
        <?=
        BsHtml::dropDownList(
            'stocktype',
            $model->marketingDetails?$model->marketingDetails->stocktype:0,
            array('SCS'=>'SCS','Non-SCS'=>'Non-SCS','RS'=>'RS'),
            array(
                'id' => 'stock_type',
                'required'=>'required'
            )
        )
        ?>
    </div>
    <div class="col-xs-4">
        <b>Work Required</b><br>
        <?=
        BsHtml::dropDownList(
            'work_required',
            $model->marketingDetails?$model->marketingDetails->work_required:0,
            array('0'=>'Shoots','1'=>'Edits','2'=>'Shoots & Edits'),
            array(
                'id' => 'work_required',
                'required'=>'required'
            )
        )
        ?>
    </div>
</div>

<style>
    .hide-native-select select{
        display:none;
    }
</style>
<br>
<?php echo BsHtml::submitButton('Save', array('color' => BsHtml::BUTTON_COLOR_PRIMARY)); ?>
<?php $this->endWidget(); ?>
