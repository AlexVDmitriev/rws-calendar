<?php
/**
 * @var ManufacturerCategoryController $this
 * @var ManufacturerCategory $model
 */

$this->breadcrumbs=array(
	'Brand to Categories'=>array('index'),
	'Manage',
);

?>

<?php echo BsHtml::pageHeader('Manage','Brand to Categories') ?>
<div class="panel panel-default">
    <div class="panel-heading"  style='text-align:right'>
		<a href='<?php echo Yii::app()->createUrl("ManufacturerCategory/create") ?>' class='btn btn-success'><div class='fa fa-plus'></div> Add New</a>    </div>
    <div class="panel-body">

        <?php 
		$this->widget('bootstrap.widgets.BsGridView',array(
			'id'=>'manufacturer-category-grid',
			'dataProvider'=>$model->search(),
			'enablePagination'=> true,
			'filter'=>$model,
			'columns'=>array(
			array('name'=>'name','header'=>'Brand'),
			array('header'=>'Category','type'=>'raw','value'=>'$data->getCategoryList()'),
			array('header'=>'Suppliers','type'=>'raw','value'=>'$data->getsupplierlist();'),
			array( //we have to change the default url of the button(s)(Yii by default use $data->id.. but $data in our case is an array...)
				'class' => 'CButtonColumn',
				'template' => '{view}{update}{delete}',
				'buttons' => array(
					'delete' => array('url' => '$this->grid->controller->createUrl("delete",array("id"=>$data["id"]))'),
					'update' => array('url' => '$this->grid->controller->createUrl("update",array("id"=>$data["id"]))'),
					'view' => array('url' => '$this->grid->controller->createUrl("view",array("id"=>$data["id"]))'),
				),
			),			
		),
        )); ?>
    </div>
</div>




