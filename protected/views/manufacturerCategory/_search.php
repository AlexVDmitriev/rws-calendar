<?php
/* @var $this ManufacturerCategoryController */
/* @var $model ManufacturerCategory */
/* @var $form BSActiveForm */
?>

<?php $form=$this->beginWidget('bootstrap.widgets.BsActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

    <?php echo $form->textFieldControlGroup($model,'manufacturer'); ?>
    <?php echo $form->textFieldControlGroup($model,'category'); ?>
    <?php echo $form->textFieldControlGroup($model,'entryid'); ?>

    <div class="form-actions">
        <?php echo BsHtml::submitButton('Search',  array('color' => BsHtml::BUTTON_COLOR_PRIMARY,));?>
    </div>

<?php $this->endWidget(); ?>
