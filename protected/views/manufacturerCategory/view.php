<?php
/* @var $this ManufacturerCategoryController */
/* @var $model Manufacturer */
?>

<?php
$this->breadcrumbs=array(
	'Brand to Categories'=>array('index'),
	$model->id,
);

?>

<?php echo BsHtml::pageHeader('View','Brand to Category '.$model->id) ?>

<?php $this->widget('zii.widgets.CDetailView',array(
	'htmlOptions' => array(
		'class' => 'table table-striped table-condensed table-hover',
	),
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		array('label'=>'Category list','type'=>'raw','value'=>$model->getcategorylist()),
		array('label'=>'Supplier list','type'=>'raw','value'=>$model->getsupplierlist()),
	),
)); ?>