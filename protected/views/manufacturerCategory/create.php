<?php
/* @var $this ManufacturerCategoryController */
/* @var $model Manufacturer */
?>

<?php
$this->breadcrumbs=array(
	'Brand to Categories'=>array('index'),
	'Create',
);

?>

<?php echo BsHtml::pageHeader('Create','Brand Category') ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>