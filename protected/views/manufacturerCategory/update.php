<?php
/* @var $this ManufacturerCategoryController */
/* @var $model Manufacturer */
?>

<?php
$this->breadcrumbs=array(
	'Brand to Categories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

?>
<?php echo BsHtml::pageHeader('Update','Brand to Category '.$model->id) ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>